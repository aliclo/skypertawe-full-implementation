package filemanager;

import java.io.File;
import java.io.IOException;

/**
 * @File	-FileManager.java
 * @author	-Alistair Cloney
 * @date	-9/3/2017
 * @see		-https://docs.oracle.com/javase/7/docs/api/java/io/File.html
 * @brief	Provides methods to help when managing files, e.g. creating files
 * 			when necessary, deleting entire directories.
 */
public class FileManager {
	
	/**
	 * Check if the given filename/path exists.
	 * 
	 * @param fileName The filename to test.
	 * @return If it exists or not.
	 */
	public static boolean checkIfExist(String fileName){
		 File file = new File(fileName);
		 
		 return file.exists();
	}
	
	/**
	 * Creates a directory from the given file if it doesn't exist.
	 * 
	 * @param file The file to create if non existent.
	 */
	public static void createDirectoryWhenNonExistent(File file) {
		if(!file.exists()) {
			file.mkdirs();
		}
	}
	
	/**
	 * Creates a file (and parent directories) if it doesn't exist.
	 * 
	 * @param file The file to create if non existent.
	 */
	public static void createFileWhenNonExistent(File file) {
		if(!file.exists()) {
			try {
				file.getParentFile().mkdirs();
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Deletes a given directory, recursively deleting any files/directories
	 * within.
	 * 
	 * @param file The directory to delete.
	 */
	public static void deleteDirectory(File file) {
		if(file.isDirectory()) {
			for(File childFile : file.listFiles()) {
				deleteDirectory(childFile);
			}
		}
		
		file.delete();
	}
	
	/**
	 * Tests the methods of the class FileManager.
	 * 
	 * @param args Not used.
	 */
	public static void main(String[] args) {
		if(checkIfExist(TEST_PATH)) {
			File file = new File(TEST_PATH);
			
			deleteDirectory(file);
		}
		
		File testFile = new File(TEST_FILE_PATH);
		File testFolder = new File(TEST_FOLDER_PATH);
		File testParentFolder = new File(TEST_PATH);
		
		System.out.println("Test file exists: " + testFile.exists());
		System.out.println("Test folder exists: " + testFolder.exists());
		
		createFileWhenNonExistent(testFile);
		
		System.out.println("Test file exists: " + testFile.exists());
		System.out.println("Test folder exists: " + testFolder.exists());
		
		deleteDirectory(testParentFolder);
		
		System.out.println("Test file exists: " + testFile.exists());
		System.out.println("Test folder exists: " + testFolder.exists());
		
		createDirectoryWhenNonExistent(testFolder);
		
		System.out.println("Test file exists: " + testFile.exists());
		System.out.println("Test folder exists: " + testFolder.exists());
	}
	
	private static final String TEST_PATH = "Tests";
	private static final String TEST_FOLDER_PATH = TEST_PATH + "/Testing";
	private static final String TEST_FILE_PATH = TEST_FOLDER_PATH
			+ "/test.txt";
	
}
