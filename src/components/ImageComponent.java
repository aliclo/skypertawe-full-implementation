package components;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * @File	-ImageComponent.java
 * @author	-Alistair Cloney
 * @date	-9/3/2017
 * @see		-Java For Everyone Second Edition, by Cay Horstmann, page 487
 * @brief	A class used to display a given image.
 * 
 * Uses a JPanel to draw the given image and display it,
 * maintaining its aspect ratio.
 */
public class ImageComponent extends JPanel {
	
	/**
	 * Create component with no image.
	 */
	public ImageComponent() {
		super();
	}
	
	/**
	 * Create component with an initial image.
	 * @param image The image for the component to display.
	 */
	public ImageComponent(BufferedImage image) {
		super();
		
		m_image = image;
	}
	
	/**
	 * @return The image used by the component.
	 */
	public BufferedImage getImage() {
		return m_image;
	}
	
	/**
	 * @param image The new image to be used for displaying.
	 */
	public void setImage(BufferedImage image) {
		m_image = image;
		
		repaint();
	}
	
	/**
	 * Testing methods of ImageComponent.
	 * 
	 * @param args Not used.
	 */
	public static void main(String[] args) {
		JFrame testFrame = new JFrame();
		testFrame.setSize(TEST_WINDOW_SIZE);
		JPanel testPanel = (JPanel) testFrame.getContentPane();
		testPanel.setLayout(new BoxLayout(testPanel, BoxLayout.PAGE_AXIS));
		
		try {
			File backgroundFile = new File("background.png");
			BufferedImage backgroundImage = ImageIO.read(backgroundFile);
			
			ImageComponent imageComp = new ImageComponent(backgroundImage);
			
			testPanel.add(imageComp);
			
			testFrame.setVisible(true);
			
			System.out.println("Current image is the same as given image: "
				+ imageComp.getImage().equals(backgroundImage));
			
			try {
				Thread.sleep(TEST_DELAY);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			//Switch image
			File logoFile = new File("Logo.png");
			BufferedImage logoImage = ImageIO.read(logoFile);
			
			imageComp.setImage(logoImage);
			
			System.out.println("Current image is the same as given image: "
					+ imageComp.getImage().equals(backgroundImage));
			
			//Indirect call to paintComponent
			imageComp.repaint();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Draw the image.
	 * 
	 * @param g The graphics object passed to allow for displaying
	 * of graphics.
	 */
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		//Need an image to display
		if(m_image != null) {
			double aspectRatio =
					(double) m_image.getHeight()/m_image.getWidth();
			
			int imageWidth;
			int imageHeight;
			
			//Calculate the width and height of the image maintaining its ratio
			if(getWidth() <= getHeight()) {
				imageWidth = getWidth();
				imageHeight = (int) Math.round(getWidth()*aspectRatio);
			} else {
				imageHeight = getHeight();
				imageWidth = (int) Math.round(getHeight()/aspectRatio);
			}
			
			int imageX = (int) Math.round((getWidth()-imageWidth)/2.0);
			int imageY = (int) Math.round((getHeight()-imageHeight)/2.0);
			
			g.drawImage(m_image, imageX, imageY, imageWidth, imageHeight,
					null);
		}
	}
	
	private static final Dimension TEST_WINDOW_SIZE = new Dimension(200,250);
	private static final int TEST_DELAY = 1000;
	
	private BufferedImage m_image;
	
}
