package gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import dataStructure.BST;
import user.User;

/**
 * @File	-GUIAddFriend.java
 * @author	-Alistair Cloney
 * @date	-9/3/2017
 * @see		-User.java, Contact.java
 * @brief	GUI that allows the user to send contact requests.
 * 
 * Provides a text field used for entering the name of the user
 * that the loggedUser wants be contacts with.
 */
public class GUIAddFriend extends JFrame {
	
	/**
	 * Constructor which will display the GUI for the current logged user.
	 * 
	 * @param currentUser The current logged in user.
	 * @param userDB The existing users of skypertawe.
	 */
	public GUIAddFriend(User currentUser, BST userDB) {
		setSize(ADD_FRIEND_FRAME_SIZE);
		JPanel panel = (JPanel) getContentPane();
		
		panel.setLayout(new GridBagLayout());
		GridBagConstraints panelConstraints = new GridBagConstraints();
		
		//Set padding
		panelConstraints.insets = new Insets(
				SPACING_BOARDER_AMOUNT,
				SPACING_BOARDER_AMOUNT,
				SPACING_BOARDER_AMOUNT,
				SPACING_BOARDER_AMOUNT);
		
		JLabel addFriendMessage = new JLabel("Enter the username of your "
				+ "friend and press OK");
		
		JLabel friendUsername = new JLabel("Username:");
		JTextField friendUsernameInput = new JTextField();
		JButton addContact = new JButton("Add Contact");
		
		addContact.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				addContact(currentUser, friendUsernameInput.getText(), userDB);
			}
		});
		
		panelConstraints.gridwidth = ADD_FRIEND_MESSAGE_GRID_WIDTH;
		panelConstraints.gridheight = ADD_FRIEND_MESSAGE_GRID_HEIGHT;
		panelConstraints.gridx = ADD_FRIEND_MESSAGE_GRID_X;
		panelConstraints.gridy = ADD_FRIEND_MESSAGE_GRID_Y;
		panelConstraints.weightx = ADD_FRIEND_MESSAGE_WEIGHT_X;
		panelConstraints.weighty = ADD_FRIEND_MESSAGE_WEIGHT_Y;
		panelConstraints.fill = ADD_FRIEND_GRID_FILL;
		
		panel.add(addFriendMessage, panelConstraints);
		
		panelConstraints.gridwidth = FRIEND_USERNAME_GRID_WIDTH;
		panelConstraints.gridheight = FRIEND_USERNAME_GRID_HEIGHT;
		panelConstraints.gridx = FRIEND_USERNAME_GRID_X;
		panelConstraints.gridy = FRIEND_USERNAME_GRID_Y;
		panelConstraints.weightx = FRIEND_USERNAME_WEIGHT_X;
		panelConstraints.weighty = FRIEND_USERNAME_WEIGHT_Y;
		panelConstraints.fill = FRIEND_USERNAME_FILL;
		
		panel.add(friendUsername, panelConstraints);
		
		panelConstraints.gridwidth = FRIEND_USERNAME_INPUT_GRID_WIDTH;
		panelConstraints.gridheight = FRIEND_USERNAME_INPUT_GRID_HEIGHT;
		panelConstraints.gridx = FRIEND_USERNAME_INPUT_GRID_X;
		panelConstraints.gridy = FRIEND_USERNAME_INPUT_GRID_Y;
		panelConstraints.weightx = FRIEND_USERNAME_INPUT_WEIGHT_X;
		panelConstraints.weighty = FRIEND_USERNAME_INPUT_WEIGHT_Y;
		panelConstraints.fill = FRIEND_USERNAME_INPUT_FILL;
		
		panel.add(friendUsernameInput, panelConstraints);
		
		panelConstraints.gridwidth = ADD_CONTACT_BUTTON_GRID_WIDTH;
		panelConstraints.gridheight = ADD_CONTACT_BUTTON_GRID_HEIGHT;
		panelConstraints.gridx = ADD_CONTACT_BUTTON_GRID_X;
		panelConstraints.gridy = ADD_CONTACT_BUTTON_GRID_Y;
		panelConstraints.weightx = ADD_CONTACT_BUTTON_GRID_WEIGHT_X;
		panelConstraints.weighty = ADD_CONTACT_BUTTON_GRID_WEIGHT_Y;
		panelConstraints.fill = ADD_CONTACT_BUTTON_FILL;
		
		panel.add(addContact, panelConstraints);
		
		setVisible(true);
	}
	
	/**
	 * Tests the methods of the class GUIAddFriend.
	 * 
	 * Integration test with classes: User, BST
	 * 
	 * @param args Not used.
	 */
	public static void main(String[] args) {
		User userTest = new User().withUsername("Test")
				.withBirthDate("10/05/1994");
		
		User userHello = new User().withUsername("Hello")
				.withBirthDate("05/07/2000");
		
		User userWorld = new User().withUsername("World")
				.withBirthDate("09/09/1999");
		
		User currentUser = userTest;
		
		BST users = new BST();
		
		users.insertUser(userTest);
		users.insertUser(userHello);
		users.insertUser(userWorld);
		
		userHello.removeContactRequest(currentUser);
		
		addContact(currentUser, userHello.getUsername(), users);
		
		addContact(currentUser, userHello.getUsername(), users);
		
		new GUIAddFriend(currentUser, users);
	}
	
	/**
	 * Add the user with the given username to the contact list of
	 * the currently logged in user.
	 * 
	 * @param currentUser The user logged in currently.
	 * @param friendUsername The username of the user to be added to contacts.
	 * @param userDB The existing users of Skypertawe.
	 */
	private static void addContact(User currentUser, String friendUsername,
			BST userDB) {
		
		User user = userDB.getProfileRoot(friendUsername);
		
		//Check that the user actually exists
		if(user != null) {
			currentUser.sendContactRequest(user, userDB);
		} else {
			JOptionPane.showMessageDialog(null,
					"Not an existing account");
		}
	}
	
	private static final int SPACING_BOARDER_AMOUNT = 2;

	private static final int ADD_FRIEND_MESSAGE_GRID_WIDTH = 2;
	private static final int ADD_FRIEND_MESSAGE_GRID_HEIGHT = 1;
	private static final int ADD_FRIEND_MESSAGE_GRID_X = 0;
	private static final int ADD_FRIEND_MESSAGE_GRID_Y = 0;
	private static final int ADD_FRIEND_MESSAGE_WEIGHT_X = 0;
	private static final int ADD_FRIEND_MESSAGE_WEIGHT_Y = 0;
	private static final int ADD_FRIEND_GRID_FILL =
			GridBagConstraints.HORIZONTAL;

	private static final int FRIEND_USERNAME_GRID_WIDTH = 1;
	private static final int FRIEND_USERNAME_GRID_HEIGHT = 1;
	private static final int FRIEND_USERNAME_GRID_X = 0;
	private static final int FRIEND_USERNAME_GRID_Y = 1;
	private static final int FRIEND_USERNAME_WEIGHT_X = 0;
	private static final int FRIEND_USERNAME_WEIGHT_Y = 0;
	private static final int FRIEND_USERNAME_FILL = GridBagConstraints.NONE;

	private static final int FRIEND_USERNAME_INPUT_GRID_WIDTH = 1;
	private static final int FRIEND_USERNAME_INPUT_GRID_HEIGHT = 1;
	private static final int FRIEND_USERNAME_INPUT_GRID_X = 1;
	private static final int FRIEND_USERNAME_INPUT_GRID_Y = 1;
	private static final int FRIEND_USERNAME_INPUT_WEIGHT_X = 0;
	private static final int FRIEND_USERNAME_INPUT_WEIGHT_Y = 0;
	private static final int FRIEND_USERNAME_INPUT_FILL =
			GridBagConstraints.HORIZONTAL;
	
	private static final int ADD_CONTACT_BUTTON_GRID_WIDTH = 1;
	private static final int ADD_CONTACT_BUTTON_GRID_HEIGHT = 1;
	private static final int ADD_CONTACT_BUTTON_GRID_X = 1;
	private static final int ADD_CONTACT_BUTTON_GRID_Y = 2;
	private static final int ADD_CONTACT_BUTTON_GRID_WEIGHT_X = 0;
	private static final int ADD_CONTACT_BUTTON_GRID_WEIGHT_Y = 0;
	private static final int ADD_CONTACT_BUTTON_FILL =
			GridBagConstraints.HORIZONTAL;
	
	private static final Dimension ADD_FRIEND_FRAME_SIZE =
			new Dimension(300,200);
	
}
