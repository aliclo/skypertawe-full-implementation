package gui;

import user.User;

/**
 * @File	-UserManagerListener.java
 * @author	-Alistair Cloney
 * @date	-21/3/2017
 * @see		-UserDetailsGUI.java
 * @brief	Listens to when a user is created or modified using the
 * 			UserDetailsGUI.
 */
public interface UserManagerListener {
	
	/**
	 * Called when user details are changed.
	 * 
	 * @param user The user whose details were changed.
	 * @return True to stop listening (removes listener).
	 */
	public boolean onUserChanged(User user);
	
	/**
	 * Called when a new user is created.
	 * 
	 * @param user The user thats created.
	 * @return True to stop listening (removes listener).
	 */
	public boolean onUserCreated(User user);
	
}
