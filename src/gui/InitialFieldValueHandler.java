package gui;

import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * @File	-InitialFieldValueHandler
 * @author	-Alistair Cloney
 * @date	-22/3/2017
 * @see		-https://docs.oracle.com/javase/7/docs/api/java/awt/event/
 * 			FocusListener.html
 * @brief	Allows fields to display initial text (e.g. enter value here)
 * 			until the user enters something.
 */
public class InitialFieldValueHandler implements FocusListener  {
	
	/**
	 * Create the hander that will initialise the text to be of the colour
	 * that indicates that the field is unfilled if the field is unfilled.
	 * 
	 * @param field The field to handle.
	 * @param initialValue The initial value thats displayed
	 * when nothing is entered.
	 * 
	 * @param filledColour The colour of the text when something has
	 * been entered.
	 * 
	 * @param unfilledColour The colour of the text when nothing has
	 * been entered yet.
	 */
	public InitialFieldValueHandler(JTextField field, String initialValue,
			Color filledColour, Color unfilledColour) {
		
		m_field = field;
		m_initialValue = initialValue;
		m_filledColour = filledColour;
		m_unfilledColour = unfilledColour;
		
		if(field.getText().equals("")) {
			field.setText(initialValue);
			field.setForeground(m_unfilledColour);
		} else {
			field.setForeground(m_filledColour);
		}
	}
	
	/**
	 * @param e The event object, not used.
	 * 
	 * Clears the field for user's input if not filled.
	 */
	@Override
	public void focusGained(FocusEvent e) {
		if(m_field.getText().equals(m_initialValue)) {
			m_field.setText("");
			m_field.setForeground(m_filledColour);
		}
	}
	
	/**
	 * @param e The event, not used.
	 * 
	 * Gives the field the initial value if nothing is entered.
	 */
	@Override
	public void focusLost(FocusEvent e) {
		if(m_field.getText().equals("")) {
			m_field.setText(m_initialValue);
			m_field.setForeground(m_unfilledColour);
		}
	}
	
	/**
	 * Tests methods of the class InitialFieldValueHandler.
	 * 
	 * @param args Not used.
	 */
	public static void main(String[] args) {
		JFrame testFrame = new JFrame("Test Frame");
		
		JPanel contentsPanel = (JPanel) testFrame.getContentPane();
		contentsPanel.setLayout(new BoxLayout(contentsPanel,
				BoxLayout.PAGE_AXIS));
		
		JTextField testing = new JTextField();
		InitialFieldValueHandler handler = new InitialFieldValueHandler(
				testing, "Enter value here", Color.BLACK, Color.GRAY);
		
		testing.addFocusListener(handler);
		
		contentsPanel.add(testing);
		
		testing = new JTextField();
		handler = new InitialFieldValueHandler(testing, "Enter value here",
				Color.BLACK, Color.GRAY);
		
		testing.addFocusListener(handler);
		
		contentsPanel.add(testing);
		testFrame.pack();
		testFrame.setVisible(true);
	}
	
	private JTextField m_field;
	private String m_initialValue;
	private Color m_filledColour;
	private Color m_unfilledColour;
	
}
