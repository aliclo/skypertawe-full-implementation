package gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

import conversation.Chat;
import message.FileTransferMessage;
import message.ImageMessage;
import message.Message;
import message.MessageBuilder;
import message.TextMessage;
import message.UrlMessage;
import message.VideoMessage;
import user.User;

/**
 * @File	-GUIMessageChoice.java
 * @author	-Alistair Cloney
 * @date	-10/3/2017
 * @see		-TextMessage, UrlMessage, FileTransferMessage
 * @brief	The GUI that gives the user the choice of which message to send.
 * 
 * Currently a set of buttons that the user can click to indicate which
 * type of message is desired to be sent.
 */
public class GUIMessageChoice extends JFrame {
	
	/**
	 * Constructor that displays the choice window.
	 * 
	 * @param currentUser The currently logged in user.
	 * @param author The user that will be the author of the message.
	 * @param chat The chat the message will be sent to.
	 */
	public GUIMessageChoice(User currentUser, User author, Chat chat) {
		setSize(MESSAGE_CHOICE_SIZE);
		JPanel panel = (JPanel) getContentPane();
		
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
		
		//Space away from the top
		panel.add(Box.createRigidArea(SPACING_BOARDER_DIMENSION));
		
		String choiceMessage = "What kind of message?";
		JLabel choiceLabel = new JLabel(choiceMessage);
		
		choiceLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		panel.add(choiceLabel);
		
		//Make sure there is even spacing between the label and the buttons
		panel.add(Box.createGlue());
		
		//Add buttons
		JPanel buttonChoice = createButtonChoice(currentUser, author, chat);
		
		panel.add(buttonChoice);
		
		//Space away from the bottom
		panel.add(Box.createRigidArea(SPACING_BOARDER_DIMENSION));
		
		setVisible(true);
	}
	
	/**
	 * Displays the GUI that allows the user to send a file message.
	 * 
	 * @param currentUser The user currently logged in.
	 * @param author The user that will be the author of the new message.
	 * @param chat The chat the message will be sent to.
	 */
	public void displaySendFileMessage(User currentUser, User author,
			Chat chat) {
		
		JPanel sendFileDisplay = new JPanel();
		sendFileDisplay.setLayout(new BoxLayout(sendFileDisplay,
				BoxLayout.PAGE_AXIS));
		
		JButton selectFile = new JButton("Select a file");
		
		JTextField textInput = new JTextField();
		
		JFileChooser fc = new JFileChooser();
		fc.setDialogTitle("Select a file to send.");
		
		JLabel descMessage =
				new JLabel("Write a short description for your file: ");
		
		JLabel fileMessage = new JLabel("Choose a file you want to upload: ");
		
		sendFileDisplay.add(descMessage);
		sendFileDisplay.add(textInput);
		sendFileDisplay.add(fileMessage);
		sendFileDisplay.add(selectFile);
		
		selectFile.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				/**
				 * Display the file chooser GUI, and if the approve option
				 * button was selected, set the file path.
				 */
				if (fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
					File pathfile = fc.getSelectedFile();
					m_pathFile = pathfile.getAbsolutePath();
				}
			}
		});
		
		//Display message creation GUI
		int dialogResult = JOptionPane.showConfirmDialog(null, sendFileDisplay,
				"Create file message", JOptionPane.OK_CANCEL_OPTION,
				JOptionPane.PLAIN_MESSAGE);
		
		if (dialogResult == JOptionPane.OK_OPTION) {
			MessageBuilder newMessage = new MessageBuilder(author)
					.withText(textInput.getText());
			
			FileTransferMessage newFileMessage =
					new FileTransferMessage(newMessage)
					.withFilePath(m_pathFile);
			
			onMessageSend(newFileMessage, chat, currentUser);
		}
		
		dispose();
	}
	
	/**
	 * Displays the GUI that allows the user to send an Image message.
	 * 
	 * @param currentUser The user currently logged in.
	 * @param author The user that will be the author of the new message.
	 * @param chat The chat the message will be sent to.
	 */
	public void displaySendImageMessage(User currentUser, User author,
			Chat chat) {
		
		JPanel sendImageDisplay = new JPanel();
		sendImageDisplay.setLayout(new BoxLayout(sendImageDisplay,
				BoxLayout.PAGE_AXIS));
		
		JButton selectImage = new JButton("Select an image");
		
		JTextField textInput = new JTextField();
		
		JFileChooser fc = new JFileChooser();
		fc.setDialogTitle("Select an image to send.");
		
		FileNameExtensionFilter filter = new FileNameExtensionFilter(
				"Image Files", "jpg", "png", "gif", "jpeg");
		
		fc.setFileFilter(filter);
		
		JLabel descMessage =
				new JLabel("Write a short description for your image: ");
		
		JLabel imageMessageLabel = new JLabel("Choose an image you want to "
				+ "use: ");
		
		sendImageDisplay.add(descMessage);
		sendImageDisplay.add(textInput);
		sendImageDisplay.add(imageMessageLabel);
		sendImageDisplay.add(selectImage);
		
		selectImage.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				/**
				 * Display the file chooser GUI, and if the approve option
				 * button was selected, set the file path.
				 */
				if (fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
					File pathfile = fc.getSelectedFile();
					m_pathFile = pathfile.getAbsolutePath();
				}
			}
		});
		
		//Display message creation GUI
		int dialogResult = JOptionPane.showConfirmDialog(null,
				sendImageDisplay, "Create image message",
				JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
		
		if (dialogResult == JOptionPane.OK_OPTION) {
			MessageBuilder newMessage = new MessageBuilder(author)
					.withText(textInput.getText());
			
			ImageMessage imageMessage = new ImageMessage(newMessage)
					.withFilePath(m_pathFile);
			
			onMessageSend(imageMessage, chat, currentUser);
		}
		
		dispose();
	}
	
	/**
	 * Displays the GUI that allows the user to send a text message.
	 * 
	 * @param currentUser The user currently logged in.
	 * @param author The user that will be the author of the new message.
	 * @param chat The chat the message will be sent to.
	 */
	public void displaySendTextMessage(User currentUser, User author,
			Chat chat) {
		
		JPanel sendTextDisplay = new JPanel();
		
		sendTextDisplay.setLayout(new BoxLayout(sendTextDisplay,
				BoxLayout.PAGE_AXIS));
		
		JLabel inputMessage = new JLabel("Input your message:");
		JTextField textInput = new JTextField();
		
		sendTextDisplay.add(inputMessage);
		sendTextDisplay.add(textInput);
		
		//Display message creation GUI
		int dialogResult = JOptionPane.showConfirmDialog(null, sendTextDisplay,
				"Create text message", JOptionPane.OK_CANCEL_OPTION,
				JOptionPane.PLAIN_MESSAGE);
		
		if (dialogResult == JOptionPane.OK_OPTION){
			MessageBuilder message = new MessageBuilder(author)
					.withText(textInput.getText());
			
			TextMessage newMessage = new TextMessage(message);
			
			onMessageSend(newMessage, chat, currentUser);
		}
		
		dispose();
	}

	/**
	 * Displays the GUI that allows the user to send a URL message.
	 * 
	 * @param currentUser The user currently logged in.
	 * @param author The user that will be the author of the new message.
	 * @param chat The chat the message will be sent to.
	 */
	public void displaySendUrlMessage(User currentUser, User author,
			Chat chat) {
		
		JPanel sendUrlMessageDisplay = new JPanel();
		sendUrlMessageDisplay.setLayout(new BoxLayout(sendUrlMessageDisplay,
				BoxLayout.PAGE_AXIS));
		
		JTextField linkInput = new JTextField();
		JTextField descInput = new JTextField();
		
		String descriptionMessage = "Write a short description for your link:";
		JLabel descMessageLabel = new JLabel(descriptionMessage);
		
		String linkMessage = "Write or Copy/Paste your link in this format: "
				+ "https://www.website.com/:";
		
		JLabel linkMessageLabel = new JLabel(linkMessage);
		
		sendUrlMessageDisplay.add(descMessageLabel);
		sendUrlMessageDisplay.add(descInput);
		sendUrlMessageDisplay.add(linkMessageLabel);
		sendUrlMessageDisplay.add(linkInput);
		
		//Display message creation GUI
		int dialogResult = JOptionPane.showConfirmDialog(null,
				sendUrlMessageDisplay, "Create a URL message",
				JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
		
		if (dialogResult == JOptionPane.OK_OPTION) {
			MessageBuilder newMessage = new MessageBuilder(author)
					.withText(descInput.getText());
			
			UrlMessage newUrlMessage = new UrlMessage(newMessage)
					.withLink(linkInput.getText());
			
			onMessageSend(newUrlMessage, chat, currentUser);
		}
		
		dispose();
	}
	
	/**
	 * Displays the GUI that allows the user to send a video message.
	 * 
	 * @param currentUser The user currently logged in.
	 * @param author The user that will be the author of the new message.
	 * @param chat The chat the message will be sent to.
	 */
		public void displaySendVideoMessage(User currentUser, User author,
			Chat chat) {
		
		JPanel sendVideoDisplay = new JPanel();
		sendVideoDisplay.setLayout(new BoxLayout(sendVideoDisplay,
				BoxLayout.PAGE_AXIS));
		
		JButton selectVideo = new JButton("Select a video");
		
		JTextField textInput = new JTextField();
		
		JFileChooser fc = new JFileChooser();
		fc.setDialogTitle("Select a video to send.");
		
		FileNameExtensionFilter filter = new FileNameExtensionFilter(
				"Video Files", "mp4", "hls", "fxm", "flv");
		
		fc.setFileFilter(filter);
		
		JLabel descMessage =
				new JLabel("Write a short description for your video: ");
		
		JLabel videoMessage = new JLabel("Choose a video you want to use: ");
		
		sendVideoDisplay.add(descMessage);
		sendVideoDisplay.add(textInput);
		sendVideoDisplay.add(videoMessage);
		sendVideoDisplay.add(selectVideo); 
		selectVideo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				/**
				 * Display the file chooser GUI, and if the approve option
				 * button was selected, set the file path.
				 */
				if (fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
					File pathfile = fc.getSelectedFile();
					m_pathFile = pathfile.getAbsolutePath();
                                         
				}
			}
		});
		
		//Display message creation GUI
		int dialogResult = JOptionPane.showConfirmDialog(null,
				sendVideoDisplay, "Create video message",
				JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
		
		if (dialogResult == JOptionPane.OK_OPTION) {
			MessageBuilder newMessage = new MessageBuilder(author)
					.withText(textInput.getText());
			

			VideoMessage  vidMsg= new VideoMessage(newMessage)
					.withFilePath(m_pathFile); 
			
            onMessageSend(vidMsg, chat, currentUser);
			
			//onMessageChosen(newVideoMessage, chat, currentUser);
		}
		
		dispose();
	}
	
	/**
	 * Tests the methods of the class GUIMessageChoice.
	 * 
	 * Integration testing of classes: User, Chat, MessageBuilder,
	 * TextMessage and the other type of messages sent during the
	 * GUI testing.
	 * 
	 * @param args Not used.
	 */
	public static void main(String[] args) {
		System.out.println("Adding users");
		User userTest = new User().withUsername("Test")
				.withBirthDate("10/05/1994");
		
		User userHello = new User().withUsername("Hello")
				.withBirthDate("05/07/2000");
		
		User userWorld = new User().withUsername("World")
				.withBirthDate("09/09/1999");
		
		User currentUser = userTest;
		
		ArrayList<User> members = new ArrayList<User>();
		members.add(userTest);
		members.add(userHello);
		members.add(userWorld);
		
		Chat chat = new Chat(members);
		
		//Display choice gui
		GUIMessageChoice choiceGUI = new GUIMessageChoice(currentUser,
				currentUser, chat);
		
		//Test chosen message listener
		MessageBuilder newMessage = new MessageBuilder(currentUser)
				.withText("Hello");
		
		TextMessage textMessage = new TextMessage(newMessage);
		
		System.out.println("Sent new message");
		choiceGUI.onMessageSend(textMessage, chat, currentUser);
		
		//Look for the sent message
		System.out.println(chat.getMessages().get(0).getText());
		
		//Test the individual message creation GUIs
		choiceGUI.displaySendTextMessage(currentUser, currentUser, chat);
		choiceGUI.displaySendUrlMessage(currentUser, currentUser, chat);
		choiceGUI.displaySendFileMessage(currentUser, currentUser, chat);
	}
	
	/**
	 * Called when a message has been chosen and the send button
	 * has been pressed.
	 * 
	 * @param message The message to send.
	 * @param chat The chat to send the message to.
	 * @param chooser The user who chose the message.
	 */
	public void onMessageSend(Message message, Chat chat, User chooser) {
		chat.sendMessage(message);
	}
	
	/**
	 * Creates the panel containing the buttons that allow the user to choose
	 * what message to send.
	 * 
	 * @param currentUser User currently logged in.
	 * @param author The user that will be the author of the message.
	 * @param chat The chat the message would be sent to.
	 * @return The panel that contains the choice buttons.
	 */
	private JPanel createButtonChoice(User currentUser, User author,
			Chat chat) {
		
		JPanel buttonChoice = new JPanel();
		buttonChoice.setLayout(new BoxLayout(buttonChoice,
				BoxLayout.LINE_AXIS));
		
		JButton textMessageButton = new JButton("Text message");
		
		textMessageButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				displaySendTextMessage(currentUser, author, chat);
			}
		});
		
		buttonChoice.add(textMessageButton);
		
		JButton fileMessageButton = new JButton("File message");
		
		fileMessageButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				displaySendFileMessage(currentUser, author, chat);
			}
		});
		
		buttonChoice.add(fileMessageButton);
		
		JButton urlMessageButton = new JButton("URL message");
		
		urlMessageButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				displaySendUrlMessage(currentUser, author, chat);
			}
		});
		
		buttonChoice.add(urlMessageButton);
		
		JButton imageMessageButton = new JButton("Image message");
		
		imageMessageButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				displaySendImageMessage(currentUser, author, chat);
			}
		});
		
		buttonChoice.add(imageMessageButton);
		
		JButton videoMessageButton = new JButton("Video message");
		
		videoMessageButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				displaySendVideoMessage(currentUser, author, chat);
			}
		});
		
		buttonChoice.add(videoMessageButton);
		
		return buttonChoice;
	}
	
	private static final Dimension MESSAGE_CHOICE_SIZE =
			new Dimension(620,120);
	
	//Spacing away from the edge of the frame
	private static final int SPACING_BOARDER_AMOUNT = 2;
	private static final Dimension SPACING_BOARDER_DIMENSION =
			new Dimension(SPACING_BOARDER_AMOUNT, SPACING_BOARDER_AMOUNT);
	
	//Chosen file path for the file message
	private String m_pathFile = "";
	
}
