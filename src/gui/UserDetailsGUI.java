package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import components.ImageComponent;
import dataStructure.BST;
import user.User;

/**
 * @File	-UserDetailsGUI.java
 * @author	-Alistair Cloney
 * @date	-10/3/2017
 * @see		-User.java
 * @brief	Provides GUIs that allows the manipulation and creation
 * 			of user details.
 * 
 * Provides a GUI that allows the user to create an account with text field
 * components that allow entry of username, name, password, telephone number,
 * city, and combo boxes that allow the choice of date of birth and a button
 * that allows the user to browse for the image to use for their profile
 * picture.
 * 
 * The manipulation of a user account is similar but without the username
 * text field.
 */
public class UserDetailsGUI {
	
	/**
	 * Add a listener that can listen to when an account is created or changed.
	 * 
	 * @param listener The listener to add.
	 */
	public void addListener(UserManagerListener listener) {
		m_listeners.add(listener);
	}
	
	/**
	 * Displays a GUI that allows the user to change details.
	 * 
	 * @param user The user whose details will be changed.
	 */
	public void changeUserGUI(User user) {
		JFrame updateAccountFrame = new JFrame("Change Details");
		updateAccountFrame.setSize(WINDOW_SIZE);
		
		JPanel contentsPanel = (JPanel) updateAccountFrame.getContentPane();
		contentsPanel.setBorder(PADDING_BORDER);
		contentsPanel.setLayout(new BoxLayout(contentsPanel,
				BoxLayout.PAGE_AXIS));
		
		addPasswordInput(contentsPanel, INITIAL_PASSWORD);
		
		contentsPanel.add(Box.createVerticalGlue());
		
		addNameInput(contentsPanel, user.getName());
		
		contentsPanel.add(Box.createVerticalGlue());
		
		addTelephoneInput(contentsPanel, user.getTNum());
		
		contentsPanel.add(Box.createVerticalGlue());
		
		addCityInput(contentsPanel, user.getCity());
		
		contentsPanel.add(Box.createVerticalGlue());
		
		addDayBirthInput(contentsPanel);
		
		contentsPanel.add(Box.createVerticalGlue());
		
		addMonthBirthInput(contentsPanel);
		
		contentsPanel.add(Box.createVerticalGlue());
		
		addYearBirthInput(contentsPanel);
		
		contentsPanel.add(Box.createVerticalGlue());
		
		addPictureInput(contentsPanel, user.getProfilePicturePath());
		
		JButton updateAccountButton = new JButton("Change Details");
		applyStripFormat(updateAccountButton);
		
		updateAccountButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				changeDetails(user, updateAccountFrame);
			}
		});
		
		contentsPanel.add(Box.createVerticalGlue());
		contentsPanel.add(new JSeparator());
		
		contentsPanel.add(updateAccountButton);
		
		updateAccountFrame.setVisible(true);
	}
	
	/**
	 * Displays a GUI that allows a user account to be created.
	 * 
	 * @param users All existing users of Skypertawe, used to
	 * check if a user already exists with the same username as inputted.
	 */
	public void createUserGUI(BST users) {
		JFrame createAccountFrame = new JFrame("Create Account");
		createAccountFrame.setSize(WINDOW_SIZE);
		
		JPanel contentsPanel = (JPanel) createAccountFrame.getContentPane();
		contentsPanel.setBorder(PADDING_BORDER);
		contentsPanel.setLayout(new BoxLayout(contentsPanel,
				BoxLayout.PAGE_AXIS));
		
		addUsernameInput(contentsPanel, "");
		
		contentsPanel.add(Box.createVerticalGlue());
		
		addPasswordInput(contentsPanel, "");
		
		contentsPanel.add(Box.createVerticalGlue());
		
		addNameInput(contentsPanel, "");
		
		contentsPanel.add(Box.createVerticalGlue());
		
		addTelephoneInput(contentsPanel, "");
		
		contentsPanel.add(Box.createVerticalGlue());
		
		addCityInput(contentsPanel, "");
		
		contentsPanel.add(Box.createVerticalGlue());
		
		addDayBirthInput(contentsPanel);
		
		contentsPanel.add(Box.createVerticalGlue());
		
		addMonthBirthInput(contentsPanel);
		
		contentsPanel.add(Box.createVerticalGlue());
		
		addYearBirthInput(contentsPanel);
		
		contentsPanel.add(Box.createVerticalGlue());
		
		addPictureInput(contentsPanel, "");
		
		JButton createAccountButton = new JButton("Create Account");
		applyStripFormat(createAccountButton);
		
		createAccountButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				createNewAccount(users, createAccountFrame);
			}
		});
		
		contentsPanel.add(Box.createVerticalGlue());
		contentsPanel.add(new JSeparator());
		
		contentsPanel.add(createAccountButton);
		
		createAccountFrame.setVisible(true);
	}
	
	/**
	 * Tests the methods of UserDetailsGUI.
	 * 
	 * Integration testing with classes: User, BST
	 * 
	 * @param args Not used.
	 */
	public static void main(String[] args) {
		System.out.println("Adding users");
		User userTest = new User().withUsername("Test")
				.withBirthDate("10/05/1994");
		
		User userHello = new User().withUsername("Hello")
				.withBirthDate("05/07/2000");
		
		User userWorld = new User().withUsername("World")
				.withBirthDate("09/09/1999");
		
		BST users = new BST();
		users.insertUser(userTest);
		users.insertUser(userHello);
		users.insertUser(userWorld);
		
		validationTest(users);
		
		UserDetailsGUI userDetailsGUI = new UserDetailsGUI();
		
		userCreationTest(userDetailsGUI, users);
		
		guiTests(userDetailsGUI);
		
		userDetailsGUIListenersTest(userDetailsGUI, users);
	}
	
	/**
	 * Removes all listeners that are currently listening for
	 * user creation/changes.
	 */
	public void removeAllListeners() {
		m_listeners.clear();
	}
	
	/**
	 * Removes the given listener thats listening for user creation/changes.
	 * 
	 * @param listener The listener to remove.
	 */
	public void removeListener(UserManagerListener listener) {
		m_listeners.remove(listener);
	}
	
	/**
	 * Validates the user account to check if its acceptable for creation,
	 * otherwise an error message is outputted to the user as to why the
	 * account creation failed.
	 * 
	 * @param userDB All existing users of Skypertawe.
	 * @param user The account the user wants to created.
	 * @param password The password entered for the account.
	 * @return Whether the user account is valid or not.
	 */
	public static boolean validateNewUser(BST userDB, User user,
			String password) {
		
		String username = user.getUsername();
		String profilePicturePath = user.getProfilePicturePath();
		
		boolean selectedPicturePath = !(profilePicturePath == ""
				|| profilePicturePath == null);
		
		boolean correctFields = validateCorrectFields(user, password);
	    
		if(correctFields) {
			if(selectedPicturePath) {
				//If User is not in the database create profile
				if (userDB.getProfileRoot(username) == null) {
					return true;
		    	} else {
		    		String message = "The username is already taken";
		    		
		    		JOptionPane.showMessageDialog(null, message);
		    	}
			} else {
				String message = "Please select a picture";
				
		    	JOptionPane.showMessageDialog(null, message);
			}
		}
		
		return false;
	}
	
	/**
	 * Add the components needed for the city to be inputted.
	 * 
	 * @param contentsPanel The panel to add the components to.
	 * @param initValue The initial value of the city text field.
	 */
	private void addCityInput(JPanel contentsPanel, String initValue) {
		JLabel cityLabel = new JLabel("Current city:");
		cityLabel.setAlignmentX(JComponent.LEFT_ALIGNMENT);
		m_cityInput = new JTextField(initValue);
		
		/**
		 * This will display some text in the text field indicating
		 * that nothing has currently been entered yet.
		 */
		InitialFieldValueHandler handler = new InitialFieldValueHandler(
				m_cityInput, INITIAL_CITY, FILLED_COLOUR,
				UNFILLED_COLOUR);
		
		m_cityInput.addFocusListener(handler);
		
		applyStripFormat(m_cityInput);
		
		contentsPanel.add(cityLabel);
		contentsPanel.add(m_cityInput);
	}
	
	/**
	 * Add the components needed for the user to input their day of birth.
	 * 
	 * @param contentsPanel The panel to add the components to.
	 */
	private void addDayBirthInput(JPanel contentsPanel) {
		JLabel dayOfBirthLabel = new JLabel("Day of birth:");
		dayOfBirthLabel.setAlignmentX(JComponent.LEFT_ALIGNMENT);
		m_dayList = new JComboBox<String>();
		
		//Make sure the correct amount of days are allowed as choices
		refreshDayListItems(START_YEAR, START_MONTH);
		
		applyStripFormat(m_dayList);
		
		contentsPanel.add(dayOfBirthLabel);
		contentsPanel.add(m_dayList);
	}
	
	/**
	 * Add the components needed for the user to input their month of birth.
	 * 
	 * @param contentsPanel The panel to add the components to.
	 */
	private void addMonthBirthInput(JPanel contentsPanel) {
		//Array of strings that are used in the drop down list.
		String[] months = {
				"01","02","03","04","05","06","07","08","09","10","11","12"
		};
		
		JLabel monthOfBirthLabel = new JLabel("Month of birth:");
		monthOfBirthLabel.setAlignmentX(JComponent.LEFT_ALIGNMENT);
		m_monthList = new JComboBox<String>(months);
		m_monthList.setSelectedIndex(MONTH_START_CHOICE_INDEX);
		
		applyStripFormat(m_monthList);
		
		m_monthList.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				/**
				 * When a month is chosen, need to change the amount
				 * of days available.
				 */
				refreshDayListItems();
			}
			
		});
		
		contentsPanel.add(monthOfBirthLabel);
		contentsPanel.add(m_monthList);
	}
	
	/**
	 * Add the components needed for the user to input their name.
	 * 
	 * @param contentsPanel The panel to add the components to.
	 * @param initValue The initial value of the name text field.
	 */
	private void addNameInput(JPanel contentsPanel, String initValue) {
		JLabel nameLabel = new JLabel("First and last name:");
		nameLabel.setAlignmentX(JComponent.LEFT_ALIGNMENT);
		m_nameInput = new JTextField(initValue);
		
		/**
		 * This will display some text in the text field indicating
		 * that nothing has currently been entered yet.
		 */
		InitialFieldValueHandler handler = new InitialFieldValueHandler(
				m_nameInput, INITIAL_NAME, FILLED_COLOUR,
				UNFILLED_COLOUR);
		
		m_nameInput.addFocusListener(handler);
		
		applyStripFormat(m_nameInput);
		
		contentsPanel.add(nameLabel);
		contentsPanel.add(m_nameInput);
	}
	
	/**
	 * Add the components needed for the user to enter a password.
	 * 
	 * @param contentsPanel The panel to add the components to.
	 * @param initValue The initial value of the password field.
	 */
	private void addPasswordInput(JPanel contentsPanel, String initValue) {
		JLabel passwordLabel = new JLabel("Password:");
		passwordLabel.setAlignmentX(JComponent.LEFT_ALIGNMENT);
		m_passwordInput = new JPasswordField(initValue);
		
		applyStripFormat(m_passwordInput);
		
		contentsPanel.add(passwordLabel);
		contentsPanel.add(m_passwordInput);
	}
	
	/**
	 * Add the components needed for the user to choose a profile picture.
	 * 
	 * @param contentsPanel The panel to add the components to.
	 * @param initImagePath The initial value of the path to the image.
	 */
	private void addPictureInput(JPanel contentsPanel, String initImagePath) {
		JLabel imageLabel = new JLabel("Image:");
		imageLabel.setAlignmentX(JComponent.LEFT_ALIGNMENT);
		
		JButton selectPicture = new JButton("Select a profile picture");
		applyStripFormat(selectPicture);
		
		ImageComponent imageViewer = new ImageComponent();
		Dimension imageViewerSize = new Dimension(Integer.MAX_VALUE,
				IMAGE_VIEWER_HEIGHT);
		
		//Restrict size of image viewer
		imageViewer.setMaximumSize(imageViewerSize);
		imageViewer.setMinimumSize(imageViewerSize);
		imageViewer.setPreferredSize(imageViewerSize);
		imageViewer.setSize(imageViewerSize);
		imageViewer.setAlignmentX(JComponent.LEFT_ALIGNMENT);
		
		//Create a line border around the image viewer
		imageViewer.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		
		//Load and display the image if possible
		if(initImagePath != null && !initImagePath.equals("")) {
			File imageFile = new File(initImagePath);
			
			try {
				BufferedImage currentSelectedImage = ImageIO.read(imageFile);
				imageViewer.setImage(currentSelectedImage);
			} catch (IOException e) {
				System.out.println("Couldn't load image at: " + initImagePath);
			}
		}
		
		selectPicture.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				choosePicture(imageViewer);
			}
		});
		
		contentsPanel.add(imageLabel);
		contentsPanel.add(selectPicture);
		contentsPanel.add(Box.createVerticalGlue());
		contentsPanel.add(imageViewer);
	}
	
	/**
	 * Add components needed for the user to input their telephone number.
	 * 
	 * @param contentsPanel The panel to add the components to.
	 * @param initValue The initial value of the telephone number text field.
	 */
	private void addTelephoneInput(JPanel contentsPanel, String initValue) {
		JLabel telephoneLabel = new JLabel("Telephone number:");
		telephoneLabel.setAlignmentX(JComponent.LEFT_ALIGNMENT);
		m_telInput = new JTextField(initValue);
		
		/**
		 * This will display some text in the text field indicating
		 * that nothing has currently been entered yet.
		 */
		InitialFieldValueHandler handler = new InitialFieldValueHandler(
				m_telInput, INITIAL_TELE_NUM, FILLED_COLOUR,
				UNFILLED_COLOUR);
		
		m_telInput.addFocusListener(handler);
		
		applyStripFormat(m_telInput);
		
		contentsPanel.add(telephoneLabel);
		contentsPanel.add(m_telInput);
	}
	
	/**
	 * Add the components needed to input the username.
	 * 
	 * @param contentsPanel The panel to add the components to.
	 * @param initValue The initial value of the username text field.
	 */
	private void addUsernameInput(JPanel contentsPanel, String initValue) {
		JLabel usernameLabel = new JLabel("Username:");
		usernameLabel.setAlignmentX(JComponent.LEFT_ALIGNMENT);
		m_usernameInput = new JTextField(initValue);
		
		/**
		 * This will display some text in the text field indicating
		 * that nothing has currently been entered yet.
		 */
		InitialFieldValueHandler handler = new InitialFieldValueHandler(
				m_usernameInput, INITIAL_USERNAME, FILLED_COLOUR,
				UNFILLED_COLOUR);
		
		m_usernameInput.addFocusListener(handler);
		
		applyStripFormat(m_usernameInput);
		
		contentsPanel.add(usernameLabel);
		contentsPanel.add(m_usernameInput);
	}
	
	/**
	 * Add the components needed for the user to input their birth year.
	 * 
	 * @param contentsPanel The panel to add the components to.
	 */
	private void addYearBirthInput(JPanel contentsPanel) {
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		
		/**
		 * Amount of years between now and the minimum year
		 * (+1 to include the year).
		 */
		int amountOfYears = currentYear-MIN_YEAR+1;
		
		//Array of years that are used in the drop down list.
		String[] years = new String[amountOfYears];
		
		//Populates years with actual year dates
		for (int i = 0; i < amountOfYears;i ++){
			years[i] = Integer.toString(i+MIN_YEAR);
		}
		
		JLabel yearOfBirthLabel = new JLabel("Year of birth:");
		yearOfBirthLabel.setAlignmentX(JComponent.LEFT_ALIGNMENT);
		m_yearList = new JComboBox<String>(years);
		m_yearList.setSelectedIndex(YEAR_START_CHOICE_INDEX);
		
		applyStripFormat(m_yearList);
		
		m_yearList.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				/**
				 * When a year is chosen, need to change the amount
				 * of days available.
				 */
				refreshDayListItems();
			}
			
		});
		
		contentsPanel.add(yearOfBirthLabel);
		contentsPanel.add(m_yearList);
	}
	
	/**
	 * Applies the format to given components so that they are stretched
	 * from left to right.
	 * 
	 * @param component The component to apply the format to.
	 */
	private void applyStripFormat(JComponent component) {
		component.setAlignmentX(JComponent.LEFT_ALIGNMENT);
		
		int fieldHeight = (int) component.getPreferredSize().getHeight();
		Dimension maxSize = new Dimension(Integer.MAX_VALUE, fieldHeight);
		component.setMaximumSize(maxSize);
	}
	
	/**
	 * Changes the details of the user after a successful validation
	 * of its inputs.
	 * 
	 * @param user The user account that is changed after a
	 * successful validation.
	 */
	private void changeDetails(User user, JFrame frame) {
		String password = new String(m_passwordInput.getPassword());
	    String name = m_nameInput.getText();
	    String telephone = m_telInput.getText();
	    String birthDay = (String)m_dayList.getSelectedItem();
	    String birthMonth = (String)m_monthList.getSelectedItem();
	    String birthYear = (String)m_yearList.getSelectedItem();
	    String birthDate = (birthDay + "/" + birthMonth + "/" + birthYear);
	    String currCity = m_cityInput.getText();
	    String imageLocation;
	    
	    if(m_pictureFile == null) {
	    	imageLocation = user.getProfilePicturePath();
	    } else {
	    	imageLocation = m_pictureFile.getAbsolutePath();
	    }
	    
	    boolean valid = validateCorrectFields(user, password);
	    
	    if(valid) {
	    	user.setName(name);
		    user.setTNum(telephone);
		    user.setBirthDate(birthDate);
		    user.setCity(currCity);
		    user.setProfilePicturePath(imageLocation);
		    User.changePassword(user.getUsername(), password);
		    
		    /**
		     * Notify listeners that an account has been changed and remove
		     * the listeners that request to be removed (needs to be done
		     * using a list to resolve a concurrency issue).
		     */
		    ArrayList<UserManagerListener> removeList =
		    		new ArrayList<UserManagerListener>();
		    
	    	for(UserManagerListener listener : m_listeners) {
				boolean remove = listener.onUserChanged(user);
				
				if(remove) {
					removeList.add(listener);
				}
			}
	    	
	    	m_listeners.removeAll(removeList);
		    
			frame.dispose();
	    	
			JOptionPane.showMessageDialog(null, "User details changed");
	    }
	}
	
	/**
	 * Allows the user to browse for an image to use.
	 * 
	 * @param imageViewer The component that will display the chosen image.
	 */
	private void choosePicture(ImageComponent imageViewer) {
		/**
		 * Display the file chooser allowing the user to browse for an image
		 * file (extension restrictions are used to help ensure that only an
		 * image is chosen)
		 */
		JFileChooser imageChooser = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter(
				"Image Files", "jpg", "png", "gif", "jpeg");
		
		imageChooser.setFileFilter(filter);
		imageChooser.setDialogTitle("Select a profile picture.");
		
		int result = imageChooser.showOpenDialog(null);
		
		if (result == JFileChooser.APPROVE_OPTION) {
			File imageFile = imageChooser.getSelectedFile();
			
			/**
			 * If any part of the try catch fails, the new picture
			 * won't be used.
			 */
			try {
				BufferedImage currentSelectedImage = ImageIO.read(imageFile);
				imageViewer.setImage(currentSelectedImage);
				
				m_pictureFile = imageFile;
			} catch (IOException exception) {
				String message = "Couldnt load image at: "
					+ imageFile.getPath();
				
				JOptionPane.showMessageDialog(null, message);
			}
		}
	}
	
	/**
	 * Create a new account after a successful validation of its inputs.
	 * 
	 * @param users All existing users of Skypertawe.
	 */
	private void createNewAccount(BST users, JFrame frame) {
		String username = m_usernameInput.getText().toLowerCase();
		String password = new String(m_passwordInput.getPassword());
	    String name = m_nameInput.getText();
	    String telephone = m_telInput.getText();
	    String birthDay = (String)m_dayList.getSelectedItem();
	    String birthMonth = (String)m_monthList.getSelectedItem();
	    String birthYear = (String)m_yearList.getSelectedItem();
	    String birthDate = (birthDay + "/" + birthMonth + "/" + birthYear);
	    String currCity = m_cityInput.getText();
	    String imageLocation;
	    
	    if(m_pictureFile == null) {
	    	imageLocation = null;
	    } else {
	    	imageLocation = m_pictureFile.getAbsolutePath();
	    }
	    
	    User user = new User()
	    		.withUsername(username)
	    		.withName(name)
	    		.withTelNumber(telephone)
	    		.withBirthDate(birthDate)
	    		.withCurrentCity(currCity)
	    		.withProfilePicturePath(imageLocation);
	    
	    boolean valid = validateNewUser(users, user, password);
	    
	    if(valid) {
	    	createUser(user, password, users);
	    	
			frame.dispose();
	    	
			JOptionPane.showMessageDialog(null, "Thank you for "
					+ "registering! You can now login using your "
					+ "username and password.");
	    }
	}
	
	/**
	 * Creates, stores and saves the user.
	 * 
	 * @param user The user to create.
	 * @param password The password the user will have.
	 * @param users All existing users of Skypertawe, the user
	 * would be added to this.
	 */
	private void createUser(User user, String password, BST users) {
		User.writeUser(user, password);
		users.insertUser(user);
		
		/**
	     * Notify listeners that an account has been created and remove
	     * the listeners that request to be removed (needs to be done
	     * using a list to resolve a concurrency issue).
	     */
		ArrayList<UserManagerListener> removeList =
				new ArrayList<UserManagerListener>();
		
		for(UserManagerListener listener : m_listeners) {
			boolean remove = listener.onUserCreated(user);
			
			if(remove) {
				removeList.add(listener);
			}
		}
		
		m_listeners.removeAll(removeList);
	}
	
	/**
	 * Tests the date choices to check if they change the day choices properly.
	 * 
	 * @param userDetailsGUI The GUI to test.
	 */
	private static void dateChoicesTest(UserDetailsGUI userDetailsGUI) {
		int yearIndex = TEST_YEAR-MIN_YEAR;
		userDetailsGUI.m_yearList.setSelectedIndex(yearIndex);
		userDetailsGUI.m_yearList.repaint();
		
		int monthIndex = TEST_MONTH-1;
		userDetailsGUI.m_monthList.setSelectedIndex(monthIndex);
		userDetailsGUI.m_monthList.repaint();
		
		System.out.println("Set year/month to: " + TEST_YEAR
			+ "/" + TEST_MONTH);
		
		userDetailsGUI.refreshDayListItems();
		userDetailsGUI.m_dayList.repaint();
		
		waitTest();
		
		yearIndex = SECOND_TEST_YEAR-MIN_YEAR;
		monthIndex = SECOND_TEST_MONTH-1;
		userDetailsGUI.refreshDayListItems(yearIndex, monthIndex);
		userDetailsGUI.m_dayList.repaint();
		
		System.out.println("Set choices (without list changes) to: "
			+ SECOND_TEST_YEAR + "/" + SECOND_TEST_MONTH);
	}
	
	/**
	 * Test the GUI form creation methods.
	 * 
	 * @param userDetailsGUI The GUI to test on.
	 */
	private static void guiTests(UserDetailsGUI userDetailsGUI) {
		JFrame testFrame = new JFrame("Test");
		testFrame.setSize(WINDOW_SIZE);
		JPanel testPanel = (JPanel) testFrame.getContentPane();
		testPanel.setLayout(new BoxLayout(testPanel, BoxLayout.PAGE_AXIS));
		
		userDetailsGUI.addUsernameInput(testPanel, "");
		
		userDetailsGUI.addPasswordInput(testPanel, "");
		
		userDetailsGUI.addNameInput(testPanel, "");
		
		userDetailsGUI.addTelephoneInput(testPanel, "");
		
		userDetailsGUI.addDayBirthInput(testPanel);
		
		userDetailsGUI.addMonthBirthInput(testPanel);
		
		userDetailsGUI.addYearBirthInput(testPanel);
		
		userDetailsGUI.addCityInput(testPanel, "");
		
		userDetailsGUI.addPictureInput(testPanel, "");
		
		testFrame.setVisible(true);
		
		waitTest();
		
		dateChoicesTest(userDetailsGUI);
	}
	
	/**
	 * Refreshes the list of days available to choose from based on the
	 * chosen month and year.
	 */
	private void refreshDayListItems() {
		String yearString = (String) m_yearList.getSelectedItem();
		String monthString = (String) m_monthList.getSelectedItem();
		
		int year = Integer.parseInt(yearString);
		int month = Integer.parseInt(monthString);
		
		refreshDayListItems(year, month);
	}
	
	/**
	 * Refreshes the list of days available to choose from base on the
	 * given month and year inputs.
	 * 
	 * @param year The given year thats chosen.
	 * @param month The given month thats chosen.
	 */
	private void refreshDayListItems(int year, int month) {
		/**
		 * Use the calendar class to calculate how many days there should
		 * be given the month and year.
		 */
		Calendar c = Calendar.getInstance();
		c.set(Calendar.YEAR, year);
		
		c.set(Calendar.MONTH, month+CALENDAR_MONTH_OFFSET);
		
		int amountOfDays = c.getActualMaximum(Calendar.DAY_OF_MONTH);
		
		int daySelectedIndex;
		
		//If there are currently no days to choose from
		if(m_dayList.getItemCount() == 0) {
			daySelectedIndex = DAY_START_CHOICE_INDEX;
		} else {
			daySelectedIndex = m_dayList.getSelectedIndex();
		}
		
		//Reset the day choices
		m_dayList.removeAllItems();
		
		for(int i = 1; i <= amountOfDays; i++) {
			m_dayList.addItem(Integer.toString(i));
		}
		
		if(daySelectedIndex >= amountOfDays) {
			m_dayList.setSelectedIndex(amountOfDays-1);
		} else {
			m_dayList.setSelectedIndex(daySelectedIndex);
		}
	}
	
	/**
	 * Test the creation of a user.
	 * 
	 * Integration testing with class: User
	 * 
	 * @param userDetailsGUI The GUI to test on.
	 * @param users All the existing users of Skypertawe.
	 */
	private static void userCreationTest(UserDetailsGUI userDetailsGUI,
			BST users) {
		
		User user = new User()
				.withUsername("different username")
				.withName("test")
				.withBirthDate("05/05/2005")
				.withCurrentCity("")
				.withTelNumber("07123456789")
				.withProfilePicturePath("test");
		
		String password = "test";
		
		userDetailsGUI.createUser(user, password, users);
	}
	
	/**
	 * Test the listeners while testing the user form GUIs.
	 * 
	 * @param userDetailsGUI The GUI to test on.
	 * @param users All existing users of Skypertawe.
	 */
	private static void userDetailsGUIListenersTest(
			UserDetailsGUI userDetailsGUI, BST users) {
		
		userDetailsGUI.createUserGUI(users);
		
		userDetailsGUI.addListener(new UserManagerListener() {

			@Override
			public boolean onUserChanged(User user) {
				System.out.println("This should not be printed");
					
				return true;
			}

			@Override
			public boolean onUserCreated(User user) {
				System.out.println("Removable listener: Created user: "
					+ user.getUsername());
				
				/**
				 * Should not listen anymore afterwards and so can't
				 * listen to the user details change.
				 */
				return true;
			}
			
		});
		
		userDetailsGUI.addListener(new UserManagerListener() {

			@Override
			public boolean onUserChanged(User user) {
				System.out.println("Listener: Changed user: "
					+ user.getUsername());
				
				System.out.println("Details:");
				System.out.println(user.getDescription());
				
				return false;
			}

			@Override
			public boolean onUserCreated(User user) {
				System.out.println("Listener: Created user: "
					+ user.getUsername());
				
				System.out.println("Details:");
				System.out.println(user.getDescription());
				
				userDetailsGUI.changeUserGUI(user);
				
				return false;
			}
			
		});
	}
	
	/**
	 * Validate the data of the user that was inputted from text fields.
	 * 
	 * @param user The user to validate.
	 * @param password The password the user will have.
	 * @return Whether the validation was successful or not.
	 */
	private static boolean validateCorrectFields(User user, String password) {
		String username = user.getUsername();
		String name = user.getName();
		String telephone = user.getTNum();
		
		/**
		 * Check that a real telephone number was provided
		 * and if not declare this to the User.
		 */
		boolean correctTelLength = (telephone.length() ==
				TELEPHONE_NUM_LENGTH);
		
		boolean correctDigits = telephone.startsWith(TELEPHONE_START_DIGITS);
	    
		boolean correctTel = correctTelLength && correctDigits;
		
		if(!correctTel) {
			String message = "Please enter a correct number with "
	    			+ TELEPHONE_NUM_LENGTH + " digits starting with "
	    			+ TELEPHONE_START_DIGITS;
			
			JOptionPane.showMessageDialog(null, message);
		}
		
		//check that the required inputs were entered
		boolean notFilledUsername = (username.equals("") || username == null
				|| username.equals(INITIAL_USERNAME));
		
		boolean notFilledPassword = (password.equals("") || password == null
				|| password.equals(INITIAL_PASSWORD));
		
		boolean notFilledName = (name.equals("") || name == null
				|| name.equals(INITIAL_NAME));
		
		boolean notFilledTeleNum = (telephone.equals("") || telephone == null
				|| telephone.equals(INITIAL_TELE_NUM));
		
	    boolean emptyFields = (notFilledUsername || notFilledPassword
	    		|| notFilledName || notFilledTeleNum);
	    
	    if(emptyFields) {
	    	String message = "Please fill out all the spaces";
	    	
	    	JOptionPane.showMessageDialog(null, message);
	    }
	    
	    return (!emptyFields && correctTel);
	}
	
	/**
	 * Tests the validation of whether the required data was filled in.
	 * 
	 * Integration testing with class: User
	 * 
	 * @param user The user to test on.
	 * @param password The password the user inputted.
	 * @param users All the existing users of Skypertawe.
	 */
	private static void validateFieldsFilledTest(User user, String password,
			BST users) {
		
		String errorNotAllEntered = "Should have error: not all data entered";
		String errorNoPictureSelected = "Should have error: picture not"
				+ " selected";
		
		String errorBadTeleNumNotAllEntered = "Should have errors: not correct"
				+ " telephone number, and then not all data entered";
		
		String correctField = "Fields correct: ";
		String allDataEntered = "All data should be entered";
		
		user.withUsername("");
		
		System.out.println(errorNotAllEntered);
		System.out.println(correctField
				+ validateCorrectFields(user, password));
		
		System.out.println(errorNotAllEntered);
		validateNewUser(users, user, password);
		
		user.withUsername("different username");
		user.withName("");
		
		System.out.println(errorNotAllEntered);
		System.out.println(correctField
				+ validateCorrectFields(user, password));
		
		System.out.println(errorNotAllEntered);
		validateNewUser(users, user, password);
		
		user.withName("test");
		user.withTelNumber("");
		
		System.out.println(errorBadTeleNumNotAllEntered);
		System.out.println(correctField
				+ validateCorrectFields(user, password));
		
		System.out.println(errorBadTeleNumNotAllEntered);
		validateNewUser(users, user, password);
		
		user.withTelNumber("07123456789");
		user.withProfilePicturePath("");
		
		System.out.println(correctField
				+ validateCorrectFields(user, password));
		
		System.out.println(errorNoPictureSelected);
		validateNewUser(users, user, password);
		
		user.withProfilePicturePath("Test");
		
		System.out.println(allDataEntered);
		System.out.println(correctField
				+ validateCorrectFields(user, password));
		
		System.out.println(allDataEntered);
		validateNewUser(users, user, password);
	}
	
	/**
	 * Tests the validation of the telephone number input.
	 * 
	 * Integration testing with class: User
	 * 
	 * @param user The user to test on.
	 * @param password The password the user inputted.
	 * @param users All the existing users of Skypertawe.
	 */
	private static void validateTelNumTest(User user, String password,
			BST users) {
		
		String errorBadTeleNum = "Should have error: incorrect telephone"
				+ " number";
		
		String correctField = "Fields correct: ";
		String correctData = "Should be correct";
		
		user.setTNum("5");
		System.out.println(errorBadTeleNum);
		System.out.println(correctField
				+ validateCorrectFields(user, password));
		
		System.out.println(errorBadTeleNum);
		validateNewUser(users, user, password);
		
		user.setTNum("222222222222");
		System.out.println(errorBadTeleNum);
		System.out.println(correctField
				+ validateCorrectFields(user, password));
		
		System.out.println(errorBadTeleNum);
		validateNewUser(users, user, password);
		
		user.setTNum("12345678910");
		System.out.println(errorBadTeleNum);
		System.out.println(correctField
				+ validateCorrectFields(user, password));
		
		System.out.println(errorBadTeleNum);
		validateNewUser(users, user, password);
		
		user.setTNum("07123456789");
		System.out.println(correctData);
		System.out.println(correctField
				+ validateCorrectFields(user, password));
		
		System.out.println(correctData);
		validateNewUser(users, user, password);
	}
	
	/**
	 * Tests the validation of the user's inputs.
	 * 
	 * Integration testing with class: User
	 * 
	 * @param users All the existing users of Skypertawe.
	 */
	private static void validationTest(BST users) {
		User user = new User()
				.withUsername("different username")
				.withName("test")
				.withBirthDate("05/05/2005")
				.withCurrentCity("")
				.withTelNumber("07123456789")
				.withProfilePicturePath("test");
		
		String password = "test";
		
		User.writeUser(user, password);
		
		System.out.println("Should be correct");
		System.out.println("Fields correct: "
				+ validateCorrectFields(user, password));
		
		validateNewUser(users, user, password);
		
		user.withUsername("test");
		
		System.out.println("Should have error: already existing username");
		System.out.println("Fields correct: "
				+ validateCorrectFields(user, password));
		
		validateNewUser(users, user, password);
		
		validateFieldsFilledTest(user, password, users);
		
		validateTelNumTest(user, password, users);
	}
	
	/**
	 * Used for putting delays in the testing (especially for GUI testing) to
	 * help analyse the tests.
	 */
	private static void waitTest() {
		try {
			Thread.sleep(TEST_DELAY);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private static final String INITIAL_USERNAME = "Enter username here";
	private static final String INITIAL_NAME = "Enter name here";
	private static final String INITIAL_PASSWORD = "";
	private static final String INITIAL_TELE_NUM = "Enter telephone number"
			+ " here";
	private static final String INITIAL_CITY = "Enter city here";
	
	private static final Dimension WINDOW_SIZE = new Dimension(300,650);
	private static final int IMAGE_VIEWER_HEIGHT = 150;
	
	//Padding around the edges
	private static final int WINDOW_PADDING = 10;
	private static final EmptyBorder PADDING_BORDER = new EmptyBorder(
			WINDOW_PADDING, WINDOW_PADDING, WINDOW_PADDING, WINDOW_PADDING);
	
	private static final int MIN_YEAR = 1900;
	private static final int YEAR_START_CHOICE_INDEX = 0;
	private static final int MONTH_START_CHOICE_INDEX = 0;
	private static final int DAY_START_CHOICE_INDEX = 0;
	private static final int START_YEAR = MIN_YEAR+YEAR_START_CHOICE_INDEX;
	private static final int START_MONTH = 1+MONTH_START_CHOICE_INDEX;
	private static final int CALENDAR_MONTH_OFFSET = -1;
	private static final int TELEPHONE_NUM_LENGTH = 11;
	private static final String TELEPHONE_START_DIGITS = "07";
	
	private static final Color FILLED_COLOUR = Color.BLACK;
	private static final Color UNFILLED_COLOUR = Color.GRAY;
	
	private static final int TEST_DELAY = 4000;
	private static final int TEST_YEAR = 2000;
	private static final int TEST_MONTH = 2;
	private static final int SECOND_TEST_YEAR = 2010;
	private static final int SECOND_TEST_MONTH = 5;
	
	private JTextField m_usernameInput;
	private JTextField m_nameInput;
	private JPasswordField m_passwordInput;
	private JTextField m_telInput;
	private JComboBox<String> m_dayList;
	private JComboBox<String> m_monthList;
	private JComboBox<String> m_yearList;
	private JTextField m_cityInput;
	private File m_pictureFile;
	
	private ArrayList<UserManagerListener> m_listeners =
			new ArrayList<UserManagerListener>();
}
