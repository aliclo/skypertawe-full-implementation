package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import components.ImageComponent;
import contactList.Contact;
import conversation.Chat;
import conversation.ChatListener;
import dataStructure.BST;
import drawingEnvironment.DrawingEnvironment;
import message.Message;
import message.MessageBuilder;
import message.TextMessage;
import resources.Resources;
import user.User;

/** @File	-GUIHub.java
 *  @author	-Ianis Tutunaru 874225, edited by Alistair Cloney
			and Michael Andress
 *  @date	-11 Dec 16, edited on 11/3/2017
 *  @see	-GUI.java, GUIAddFriend.java, GUICreateGroup.java,
 *  		UserDetailsGUI.java, DrawingEnvironment.java, GUIMessageChoice.java
 *  
 *  @brief	The home GUI for the logged in user, displays the user's details,
 *  		contacts, received contact requests, group chats and provides
 *  		buttons to interact with these, also provides a menu that allows
 *  		the user to do several other actions.
 *  
 *  Home GUI of the user allowing the user to:
 *  
 *  -	Send messages to either Contacts or Group Chats.
 *  
 *  -	View the message history of either the contact chat
 *  	or group chat.
 *  
 *  -	Remove contacts.
 *  
 *  -	Delete group chats.
 *  
 *  -	Accept or decline contact requests.
 *  
 *  -	Send contact requests to users.
 *  
 *  -	View own account details including the user's details, amount of
 *  	new messages the user hasn't seen and also the last time the
 *  	user logged in last.
 *  
 *  -	View contact's account details apart from the number of new messages.
 *  
 *  -	Update account's current details.
 *  
 *  -	Open collaborative drawing environment to draw images that are shared
 *  	to other users.
 */
public class GUIHub extends GUI implements ChatListener, CreateGroupListener,
UserManagerListener {
	
	/**
	 * Constructor that displays the hub GUI for the given user.
	 * 
	 * @param loggedUser the user currently logged in.
	 * @param userDB The existing users of Skypertawe.
	 */
	public GUIHub(User loggedUser, BST userDB) {
		m_userDB = userDB;
		m_currentUser = loggedUser;
		updateChatListeners();
		createHub();
	}
	
	/**
	 * Creates the components needed to display the date of birth.
	 * 
	 * @param panel The panel to create the components on.
	 */
	public void displayBirthDate(JPanel panel) {
		JLabel birthDateLabel = new JLabel("Birth Date:");
		birthDateLabel.setHorizontalAlignment(JLabel.CENTER);
		birthDateLabel.setFont(TITLE_FONT);
		
		m_constraints.gridwidth = BIRTH_DATE_LABEL_GRID_WIDTH;
		m_constraints.gridheight = BIRTH_DATE_LABEL_GRID_HEIGHT;
		m_constraints.gridx = BIRTH_DATE_LABEL_GRID_X;
		m_constraints.gridy = BIRTH_DATE_LABEL_GRID_Y;
		m_constraints.weightx = BIRTH_DATE_LABEL_WEIGHT_X;
		m_constraints.weighty = BIRTH_DATE_LABEL_WEIGHT_Y;
		m_constraints.fill = BIRTH_DATE_LABEL_FILL;
		
		panel.add(birthDateLabel, m_constraints);
		
		m_birthDateDataLabel = new JLabel();
		m_birthDateDataLabel.setHorizontalAlignment(JLabel.CENTER);
		m_birthDateDataLabel.setFont(TEXT_FONT);
		
		m_constraints.gridwidth = BIRTH_DATE_DATA_LABEL_GRID_WIDTH;
		m_constraints.gridheight = BIRTH_DATE_DATA_LABEL_GRID_HEIGHT;
		m_constraints.gridx = BIRTH_DATE_DATA_LABEL_GRID_X;
		m_constraints.gridy = BIRTH_DATE_DATA_LABEL_GRID_Y;
		m_constraints.weightx = BIRTH_DATE_DATA_LABEL_WEIGHT_X;
		m_constraints.weighty = BIRTH_DATE_DATA_LABEL_WEIGHT_Y;
		m_constraints.fill = BIRTH_DATE_DATA_LABEL_FILL;
		
		panel.add(m_birthDateDataLabel, m_constraints);
	}
	
	/**
	 * Creates the components needed to display the city of residence.
	 * 
	 * @param panel The panel to create the components on.
	 */
	public void displayCity(JPanel panel) {
		JLabel cityLabel = new JLabel("Current city of residence:");
		cityLabel.setHorizontalAlignment(JLabel.CENTER);
		cityLabel.setFont(TITLE_FONT);
		
		m_constraints.gridwidth = CITY_LABEL_GRID_WIDTH;
		m_constraints.gridheight = CITY_LABEL_GRID_HEIGHT;
		m_constraints.gridx = CITY_LABEL_GRID_X;
		m_constraints.gridy = CITY_LABEL_GRID_Y;
		m_constraints.weightx = CITY_LABEL_WEIGHT_X;
		m_constraints.weighty = CITY_LABEL_WEIGHT_Y;
		m_constraints.fill = CITY_LABEL_FILL;
		
		panel.add(cityLabel, m_constraints);
		
		m_cityDataLabel = new JLabel();
		m_cityDataLabel.setHorizontalAlignment(JLabel.CENTER);
		m_cityDataLabel.setFont(TEXT_FONT);
		
		m_constraints.gridwidth = CITY_DATA_LABEL_GRID_WIDTH;
		m_constraints.gridheight = CITY_DATA_LABEL_GRID_HEIGHT;
		m_constraints.gridx = CITY_DATA_LABEL_GRID_X;
		m_constraints.gridy = CITY_DATA_LABEL_GRID_Y;
		m_constraints.weightx = CITY_DATA_LABEL_WEIGHT_X;
		m_constraints.weighty = CITY_DATA_LABEL_WEIGHT_Y;
		m_constraints.fill = CITY_DATA_LABEL_FILL;
		
		panel.add(m_cityDataLabel, m_constraints);
	}
	
	/**
	 * Creates the components needed to display the contact list.
	 */
	public void displayContactList() {
		JLabel contactListLabel = new JLabel("Contact List:");
		contactListLabel.setHorizontalAlignment(JLabel.CENTER);
		contactListLabel.setFont(TITLE_FONT);
		
		m_constraints.gridwidth = CONTACT_LIST_LABEL_GRID_WIDTH;
		m_constraints.gridheight = CONTACT_LIST_LABEL_GRID_HEIGHT;
		m_constraints.gridx = CONTACT_LIST_LABEL_GRID_X;
		m_constraints.gridy = CONTACT_LIST_LABEL_GRID_Y;
		m_constraints.weightx = CONTACT_LIST_LABEL_WEIGHT_X;
		m_constraints.weighty = CONTACT_LIST_LABEL_WEIGHT_Y;
		m_constraints.fill = CONTACT_LIST_LABEL_FILL;
		
		add(contactListLabel, m_constraints);
		
		m_contactListModel = new DefaultListModel<String>();
		
		updateContactList();

		m_contactList = new JList<String>(m_contactListModel);
		m_contactList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		m_contactList.setLayoutOrientation(JList.VERTICAL);
		JScrollPane listScroller = new JScrollPane(m_contactList);
		m_contactList.addListSelectionListener(new ContactListListener());
		
		m_constraints.gridwidth = CONTACT_LIST_SCROLLER_GRID_WIDTH;
		m_constraints.gridheight = CONTACT_LIST_SCROLLER_GRID_HEIGHT;
		m_constraints.gridx = CONTACT_LIST_SCROLLER_GRID_X;
		m_constraints.gridy = CONTACT_LIST_SCROLLER_GRID_Y;
		m_constraints.weightx = CONTACT_LIST_SCROLLER_WEIGHT_X;
		m_constraints.weighty = CONTACT_LIST_SCROLLER_WEIGHT_Y;
		m_constraints.fill = CONTACT_LIST_SCROLLER_FILL;
		
		add(listScroller, m_constraints);
	}
	
	/**
	 * Creates the components needed to display the list of group chats.
	 */
	public void displayGroupChatList() {
		JLabel groupChatListLabel = new JLabel("Group Chats:");
		groupChatListLabel.setHorizontalAlignment(JLabel.CENTER);
		groupChatListLabel.setFont(TITLE_FONT);
		
		m_constraints.gridwidth = GROUP_CHAT_LABEL_GRID_WIDTH;
		m_constraints.gridheight = GROUP_CHAT_LABEL_GRID_HEIGHT;
		m_constraints.gridx = GROUP_CHAT_LABEL_GRID_X;
		m_constraints.gridy = GROUP_CHAT_LABEL_GRID_Y;
		m_constraints.weightx = GROUP_CHAT_LABEL_WEIGHT_X;
		m_constraints.weighty = GROUP_CHAT_LABEL_WEIGHT_Y;
		m_constraints.fill = GROUP_CHAT_LABEL_FILL;
		
		add(groupChatListLabel, m_constraints);
		
		m_groupChatListModel = new DefaultListModel<String>();
		
		updateGroupChatList();
		
		m_groupChatList = new JList<String>(m_groupChatListModel);
		m_groupChatList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		m_groupChatList.setLayoutOrientation(JList.VERTICAL);
		JScrollPane listScroller = new JScrollPane(m_groupChatList);
		m_groupChatList.addListSelectionListener(new GroupChatListListener());
		
		m_constraints.gridwidth = GROUP_CHAT_SCROLLER_GRID_WIDTH;
		m_constraints.gridheight = GROUP_CHAT_SCROLLER_GRID_HEIGHT;
		m_constraints.gridx = GROUP_CHAT_SCROLLER_GRID_X;
		m_constraints.gridy = GROUP_CHAT_SCROLLER_GRID_Y;
		m_constraints.weightx = GROUP_CHAT_SCROLLER_WEIGHT_X;
		m_constraints.weighty = GROUP_CHAT_SCROLLER_WEIGHT_Y;
		m_constraints.fill = GROUP_CHAT_SCROLLER_FILL;
		
		add(listScroller, m_constraints);
	}
	
	/**
	 * Creates the components needed to display the last time the
	 * user logged in.
	 * 
	 * @param panel The panel to create the components on.
	 */
	public void displayLastLogin(JPanel panel) {
		JLabel lastLoginLabel = new JLabel("Last login:");
		lastLoginLabel.setHorizontalAlignment(JLabel.CENTER);
		lastLoginLabel.setFont(TITLE_FONT);
		
		m_constraints.gridwidth = LAST_LOGIN_LABEL_GRID_WIDTH;
		m_constraints.gridheight = LAST_LOGIN_LABEL_GRID_HEIGHT;
		m_constraints.gridx = LAST_LOGIN_LABEL_GRID_X;
		m_constraints.gridy = LAST_LOGIN_LABEL_GRID_Y;
		m_constraints.weightx = LAST_LOGIN_LABEL_WEIGHT_X;
		m_constraints.weighty = LAST_LOGIN_LABEL_WEIGHT_Y;
		m_constraints.fill = LAST_LOGIN_LABEL_FILL;
		
		panel.add(lastLoginLabel, m_constraints);
		
		m_lastLoginDataLabel = new JLabel();
		m_lastLoginDataLabel.setHorizontalAlignment(JLabel.CENTER);
		m_lastLoginDataLabel.setFont(TEXT_FONT);
		
		m_constraints.gridwidth = LAST_LOGIN_DATA_LABEL_GRID_WIDTH;
		m_constraints.gridheight = LAST_LOGIN_DATA_LABEL_GRID_HEIGHT;
		m_constraints.gridx = LAST_LOGIN_DATA_LABEL_GRID_X;
		m_constraints.gridy = LAST_LOGIN_DATA_LABEL_GRID_Y;
		m_constraints.weightx = LAST_LOGIN_DATA_LABEL_WEIGHT_X;
		m_constraints.weighty = LAST_LOGIN_DATA_LABEL_WEIGHT_Y;
		m_constraints.fill = LAST_LOGIN_DATA_LABEL_FILL;
		
		panel.add(m_lastLoginDataLabel, m_constraints);
	}
	
	/**
	 * Creates the components needed to display the message history
	 * of the given chat.
	 * 
	 * @param chat The chat to get the message history from.
	 */
	public void displayMessageHistory(Chat chat) {
		m_messagesDisplay.removeAll();
		
		chat.displayMessages(m_messagesDisplay, m_currentUser);
		m_messagesScroll.revalidate();
		m_messagesScroll.repaint();
		
		m_messagesWindow.setVisible(true);
	}
	//TODO
	public void test()
	{
		repaint();
	}
	
	/**
	 * Creates the components needed to display the name.
	 * 
	 * @param panel The panel to create the components on.
	 */
	public void displayName(JPanel panel) {
		JLabel nameLabel = new JLabel("Name:");
		nameLabel.setHorizontalAlignment(JLabel.CENTER);
		nameLabel.setFont(TITLE_FONT);
		
		m_constraints.gridwidth = NAME_LABEL_GRID_WIDTH;
		m_constraints.gridheight = NAME_LABEL_GRID_HEIGHT;
		m_constraints.gridx = NAME_LABEL_GRID_X;
		m_constraints.gridy = NAME_LABEL_GRID_Y;
		m_constraints.weightx = NAME_LABEL_WEIGHT_X;
		m_constraints.weighty = NAME_LABEL_WEIGHT_Y;
		m_constraints.fill = NAME_LABEL_FILL;
		
		panel.add(nameLabel, m_constraints);
		
		m_nameDataLabel = new JLabel();
		m_nameDataLabel.setHorizontalAlignment(JLabel.CENTER);
		m_nameDataLabel.setFont(TEXT_FONT);
		
		m_constraints.gridwidth = NAME_DATA_LABEL_GRID_WIDTH;
		m_constraints.gridheight = NAME_DATA_LABEL_GRID_HEIGHT;
		m_constraints.gridx = NAME_DATA_LABEL_GRID_X;
		m_constraints.gridy = NAME_DATA_LABEL_GRID_Y;
		m_constraints.weightx = NAME_DATA_LABEL_WEIGHT_X;
		m_constraints.weighty = NAME_DATA_LABEL_WEIGHT_Y;
		m_constraints.fill = NAME_DATA_LABEL_FILL;
		
		panel.add(m_nameDataLabel, m_constraints);
	}
	
	/**
	 * Display the notification which will be a list of users who request
	 * to be contacts with the logged user.
	 */
	public void displayNotification() {
		m_textRequest = new JLabel("Friend requests:");
		m_textRequest.setFont(TITLE_FONT);
		
		m_constraints.gridwidth = TEXT_REQUEST_GRID_WIDTH;
		m_constraints.gridheight = TEXT_REQUEST_GRID_HEIGHT;
		m_constraints.gridx = TEXT_REQUEST_GRID_X;
		m_constraints.gridy = TEXT_REQUEST_GRID_Y;
		m_constraints.weightx = TEXT_REQUEST_WEIGHT_X;
		m_constraints.weighty = TEXT_REQUEST_WEIGHT_Y;
		m_constraints.fill = TEXT_REQUEST_FILL;
		
		add(m_textRequest, m_constraints);
		
		m_friendReqListModel = new DefaultListModel<String>();
		
		m_friendReqList = new JList<String>(m_friendReqListModel);
		m_friendReqList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		m_friendReqList.setLayoutOrientation(JList.VERTICAL);
		
		m_friendReqList.addListSelectionListener(new RequestListListener());
		
		m_friendReqScroller = new JScrollPane(m_friendReqList);
		
		m_constraints.gridwidth = FRIEND_REQ_SCROLLER_GRID_WIDTH;
		m_constraints.gridheight = FRIEND_REQ_SCROLLER_GRID_HEIGHT;
		m_constraints.gridx = FRIEND_REQ_SCROLLER_GRID_X;
		m_constraints.gridy = FRIEND_REQ_SCROLLER_GRID_Y;
		m_constraints.weightx = FRIEND_REQ_SCROLLER_WEIGHT_X;
		m_constraints.weighty = FRIEND_REQ_SCROLLER_WEIGHT_Y;
		m_constraints.fill = FRIEND_REQ_SCROLLER_FILL;
		
		add(m_friendReqScroller, m_constraints);
		
		addRequestButtons();
		
		updateNotifications();
	}
	
	/**
	 * Creates the components needed to display the number of new messages.
	 * 
	 * @param panel The panel to create the components on.
	 */
	public void displayNumNewMessages(JPanel panel) {
		JLabel totalMessagesLabel = new JLabel("Number of new messages:");
		totalMessagesLabel.setHorizontalAlignment(JLabel.CENTER);
		totalMessagesLabel.setFont(TITLE_FONT);
		
		m_constraints.gridwidth = TOTAL_MESSAGES_LABEL_GRID_WIDTH;
		m_constraints.gridheight = TOTAL_MESSAGES_LABEL_GRID_HEIGHT;
		m_constraints.gridx = TOTAL_MESSAGES_LABEL_GRID_X;
		m_constraints.gridy = TOTAL_MESSAGES_LABEL_GRID_Y;
		m_constraints.weightx = TOTAL_MESSAGES_LABEL_WEIGHT_X;
		m_constraints.weighty = TOTAL_MESSAGES_LABEL_WEIGHT_Y;
		m_constraints.fill = TOTAL_MESSAGES_LABEL_FILL;
		
		panel.add(totalMessagesLabel, m_constraints);
		
		m_totalMessagesDataLabel = new JLabel();
		m_totalMessagesDataLabel.setHorizontalAlignment(JLabel.CENTER);
		m_totalMessagesDataLabel.setFont(TEXT_FONT);
		
		m_constraints.gridwidth = TOTAL_MESSAGES_DATA_LABEL_GRID_WIDTH;
		m_constraints.gridheight = TOTAL_MESSAGES_DATA_LABEL_GRID_HEIGHT;
		m_constraints.gridx = TOTAL_MESSAGES_DATA_LABEL_GRID_X;
		m_constraints.gridy = TOTAL_MESSAGES_DATA_LABEL_GRID_Y;
		m_constraints.weightx = TOTAL_MESSAGES_DATA_LABEL_WEIGHT_X;
		m_constraints.weighty = TOTAL_MESSAGES_DATA_LABEL_WEIGHT_Y;
		m_constraints.fill = TOTAL_MESSAGES_DATA_LABEL_FILL;
		
		panel.add(m_totalMessagesDataLabel, m_constraints);
	}
	
	/**
	 * Creates the components needed to display the profile picture.
	 * 
	 * @param panel The panel to create the components on.
	 */
	public void displayProfilePicture(JPanel panel) {
		m_profilePicture = new ImageComponent();
		m_profilePicture.setBackground(USER_INFO_BACKGROUND_COLOUR);
		
		m_constraints.gridwidth = PROFILE_PICTURE_GRID_WIDTH;
		m_constraints.gridheight = PROFILE_PICTURE_GRID_HEIGHT;
		m_constraints.gridx = PROFILE_PICTURE_GRID_X;
		m_constraints.gridy = PROFILE_PICTURE_GRID_Y;
		m_constraints.weightx = PROFILE_PICTURE_WEIGHT_X;
		m_constraints.weighty = PROFILE_PICTURE_WEIGHT_Y;
		m_constraints.fill = PROFILE_PICTURE_FILL;
		
		panel.add(m_profilePicture, m_constraints);
	}
	
	/**
	 * Creates the components needed to display the telephone number.
	 * 
	 * @param panel The panel to create the components on.
	 */
	public void displayTelephoneNumber(JPanel panel) {
		JLabel telephoneLabel = new JLabel("Telephone number:");
		telephoneLabel.setHorizontalAlignment(JLabel.CENTER);
		telephoneLabel.setFont(TITLE_FONT);
		
		m_constraints.gridwidth = TELEPHONE_LABEL_GRID_WIDTH;
		m_constraints.gridheight = TELEPHONE_LABEL_GRID_HEIGHT;
		m_constraints.gridx = TELEPHONE_LABEL_GRID_X;
		m_constraints.gridy = TELEPHONE_LABEL_GRID_Y;
		m_constraints.weightx = TELEPHONE_LABEL_WEIGHT_X;
		m_constraints.weighty = TELEPHONE_LABEL_WEIGHT_Y;
		m_constraints.fill = TELEPHONE_LABEL_FILL;
		
		panel.add(telephoneLabel, m_constraints);
		
		m_telDataLabel = new JLabel();
		m_telDataLabel.setHorizontalAlignment(JLabel.CENTER);
		m_telDataLabel.setFont(TEXT_FONT);
		
		m_constraints.gridwidth = TELEPHONE_DATA_LABEL_GRID_WIDTH;
		m_constraints.gridheight = TELEPHONE_DATA_LABEL_GRID_HEIGHT;
		m_constraints.gridx = TELEPHONE_DATA_LABEL_GRID_X;
		m_constraints.gridy = TELEPHONE_DATA_LABEL_GRID_Y;
		m_constraints.weightx = TELEPHONE_DATA_LABEL_WEIGHT_X;
		m_constraints.weighty = TELEPHONE_DATA_LABEL_WEIGHT_Y;
		m_constraints.fill = TELEPHONE_DATA_LABEL_FILL;
		
		panel.add(m_telDataLabel, m_constraints);
	}
	
	/**
	 * Display the given user's account information.
	 * 
	 * @param user The user whose account information will be displayed.
	 */
	public void displayUserInfo(User user) {
		m_usernameDataLabel.setText(user.getUsername());
		m_nameDataLabel.setText(user.getName());
		m_telDataLabel.setText(user.getTNum());
		m_cityDataLabel.setText(user.getCity());
		m_birthDateDataLabel.setText(user.getBirthDate());
		
		m_lastLoginDataLabel.setText(user.getLastLogTime());
		
		m_profilePicture.setImage(user.getProfilePicture());
	}
	
	/**
	 * Creates the components needed to display the username.
	 * 
	 * @param panel The panel to create the components on.
	 */
	public void displayUsername(JPanel panel) {
		JLabel usernameLabel = new JLabel("Username:");
		usernameLabel.setHorizontalAlignment(JLabel.CENTER);
		usernameLabel.setFont(TITLE_FONT);
		
		m_constraints.gridwidth = USERNAME_LABEL_GRID_WIDTH;
		m_constraints.gridheight = USERNAME_LABEL_GRID_HEIGHT;
		m_constraints.gridx = USERNAME_LABEL_GRID_X;
		m_constraints.gridy = USERNAME_LABEL_GRID_Y;
		m_constraints.weightx = USERNAME_LABEL_WEIGHT_X;
		m_constraints.weighty = USERNAME_LABEL_WEIGHT_Y;
		m_constraints.fill = USERNAME_LABEL_FILL;
		
		panel.add(usernameLabel, m_constraints);
		
		m_usernameDataLabel = new JLabel();
		m_usernameDataLabel.setHorizontalAlignment(JLabel.CENTER);
		m_usernameDataLabel.setFont(TEXT_FONT);
		
		m_constraints.gridwidth = USERNAME_DATA_LABEL_GRID_WIDTH;
		m_constraints.gridheight = USERNAME_DATA_LABEL_GRID_HEIGHT;
		m_constraints.gridx = USERNAME_DATA_LABEL_GRID_X;
		m_constraints.gridy = USERNAME_DATA_LABEL_GRID_Y;
		m_constraints.weightx = USERNAME_DATA_LABEL_WEIGHT_X;
		m_constraints.weighty = USERNAME_DATA_LABEL_WEIGHT_Y;
		m_constraints.fill = USERNAME_DATA_LABEL_FILL;
		
		panel.add(m_usernameDataLabel, m_constraints);
	}
	
	/**
	 * Tests the methods of the class GUIHub.
	 * 
	 * Integration testing with classes: User, BST, Chat
	 * 
	 * @param args Not used.
	 */
	public static void main(String[] args) {
		System.out.println("Adding users");
		User userTest = new User().withUsername("Test")
				.withBirthDate("10/05/1994");
		User userHello = new User().withUsername("Hello")
				.withBirthDate("05/07/2000");
		User userWorld = new User().withUsername("World")
				.withBirthDate("09/09/1999");
		User userAnn = new User().withUsername("Ann")
				.withBirthDate("26/03/1996");
		User userJames = new User().withUsername("James")
				.withBirthDate("30/06/1994");
		User userAlice = new User().withUsername("Alice")
				.withBirthDate("22/01/2004");
		User userBob = new User().withUsername("Bob")
				.withBirthDate("11/03/2001");
		
		BST users = new BST();
		users.insertUser(userTest);
		users.insertUser(userHello);
		users.insertUser(userWorld);
		users.insertUser(userAnn);
		users.insertUser(userJames);
		users.insertUser(userAlice);
		users.insertUser(userBob);
		
		User loggedUser = userTest;
		
		ArrayList<User> members = new ArrayList<User>();
		members.add(loggedUser);
		members.add(userAlice);
		members.add(userHello);
		members.add(userWorld);
		Chat groupChatA = new Chat("Group Chat A", members);
		
		members = new ArrayList<User>();
		members.add(loggedUser);
		members.add(userAlice);
		members.add(userBob);
		members.add(userJames);
		Chat groupChatB = new Chat("Group Chat B", members);
		
		loggedUser.addGroupChat(groupChatA);
		loggedUser.addGroupChat(groupChatB);
		loggedUser.acceptContactRequest(userAnn);
		loggedUser.acceptContactRequest(userBob);
		
		Contact annContact = loggedUser.getContacts().get(0);
		
		groupDetailsTest(loggedUser, userBob, groupChatA);
		contactDetailsTest(loggedUser, annContact);
		
		boolean result = displayRemoveConfirm();
		
		System.out.println("Remove result: " + result);
		
		GUIHub hub = guiTest(loggedUser, userAlice, users, groupChatA);
		
		waitTest();
		
		testRemoval(loggedUser, annContact, groupChatB, hub);
		
		waitTest();
		
		hub.displayUserInfo(userAlice);
		hub.updateChatListeners();
		
		waitTest();
		
		hub.onCreateGroup(groupChatB);
		
		waitTest();
		
		listenersTest(loggedUser, groupChatA, hub);
	}
	
	/**
	 * Listens to when a new group chat has been created.
	 * 
	 * Adds the new group chat to the JList.
	 * 
	 * @param groupChat The group chat that was created.
	 */
	@Override
	public void onCreateGroup(Chat groupChat) {
		String chatDetails = getGroupChatDetails(groupChat, m_currentUser);
		m_groupChatListModel.addElement(chatDetails);
		
		updateChatListeners();
	}
	
	/**
	 * Listens to when a message was deleted from a chat.
	 * 
	 * Updates the message history display of the chat that was modified.
	 * 
	 * @param chat The chat the message was deleted from.
	 * @param message The deleted message.
	 */
	@Override
	public void onMessageDeleted(Chat chat, Message message) {
		displayMessageHistory(chat);
	}
	
	/**
	 * Listens to when a message has been edited from a chat.
	 * 
	 * Updates the message history display of the chat that was modified.
	 * 
	 * @param chat The chat the message was edited from.
	 * @param message The message that was edited.
	 */
	@Override
	public void onMessageEdited(Chat chat, Message message) {
		displayMessageHistory(chat);
	}
	
	/**
	 * Listens to when a message has been sent to a chat.
	 * 
	 * Updates the message history display of the chat that was modified.
	 * 
	 * @param chat The chat the message was sent to.
	 * @param message The message that was sent.
	 */
	@Override
	public void onMessageSent(Chat chat, Message message) {
		displayMessageHistory(chat);
	}
	
	/**
	 * Listens to when the user has changed their details.
	 * 
	 * Updates the display of this user's details.
	 * 
	 * @param user The user whose details are changed.
	 * @return True to stop listening (removes listener).
	 */
	@Override
	public boolean onUserChanged(User user) {
		displayUserInfo(user);
		repaint();
		
		return true;
	}
	
	/**
	 * Listens to when a user has been created.
	 * 
	 * @param user The user that was created.
	 * @return False to continue listening.
	 */
	@Override
	public boolean onUserCreated(User user) {
		return false;
	}
	
	/**
	 * Creates the components needed to display the user information.
	 */
	public void setupUserInfo() {
		m_userInfo = new JPanel();
		m_userInfo.setLayout(new GridBagLayout());
		m_userInfo.setBackground(USER_INFO_BACKGROUND_COLOUR);
		
		JLabel userInfoLabel = new JLabel("User Information:");
		userInfoLabel.setHorizontalAlignment(JLabel.CENTER);
		userInfoLabel.setFont(TITLE_FONT);
		
		m_constraints.gridwidth = USER_INFO_LABEL_GRID_WIDTH;
		m_constraints.gridheight = USER_INFO_LABEL_GRID_HEIGHT;
		m_constraints.gridx = USER_INFO_LABEL_GRID_X;
		m_constraints.gridy = USER_INFO_LABEL_GRID_Y;
		m_constraints.weightx = USER_INFO_LABEL_WEIGHT_X;
		m_constraints.weighty = USER_INFO_LABEL_WEIGHT_Y;
		m_constraints.fill = USER_INFO_LABEL_FILL;
		
		m_userInfo.add(userInfoLabel, m_constraints);
		
		displayUsername(m_userInfo);
		
		m_constraints.gridwidth = USER_DETAILS_SEPARATOR_GRID_WIDTH;
		m_constraints.gridheight = USER_DETAILS_SEPARATOR_GRID_HEIGHT;
		m_constraints.gridx = USER_DETAILS_SEPARATOR_GRID_X;
		m_constraints.gridy = USER_DETAILS_SEPARATOR_GRID_Y;
		m_constraints.weightx = USER_DETAILS_SEPARATOR_WEIGHT_X;
		m_constraints.weighty = USER_DETAILS_SEPARATOR_WEIGHT_Y;
		m_constraints.fill = USER_DETAILS_SEPARATOR_FILL;
		
		m_userInfo.add(new JSeparator(), m_constraints);
		
		displayName(m_userInfo);
		
		displayBirthDate(m_userInfo);
		
		displayTelephoneNumber(m_userInfo);
		
		displayCity(m_userInfo);
		
		m_constraints.gridwidth = LAST_LOG_SEPARATOR_GRID_WIDTH;
		m_constraints.gridheight = LAST_LOG_SEPARATOR_GRID_HEIGHT;
		m_constraints.gridx = LAST_LOG_SEPARATOR_GRID_X;
		m_constraints.gridy = LAST_LOG_SEPARATOR_GRID_Y;
		m_constraints.weightx = LAST_LOG_SEPARATOR_WEIGHT_X;
		m_constraints.weighty = LAST_LOG_SEPARATOR_WEIGHT_Y;
		m_constraints.fill = LAST_LOG_SEPARATOR_FILL;
		
		m_userInfo.add(new JSeparator(), m_constraints);
		
		displayProfilePicture(m_userInfo);
		
		displayLastLogin(m_userInfo);
		
		m_constraints.gridwidth = NEW_MESSAGES_SEPARATOR_GRID_WIDTH;
		m_constraints.gridheight = NEW_MESSAGES_SEPARATOR_GRID_HEIGHT;
		m_constraints.gridx = NEW_MESSAGES_SEPARATOR_GRID_X;
		m_constraints.gridy = NEW_MESSAGES_SEPARATOR_GRID_Y;
		m_constraints.weightx = NEW_MESSAGES_SEPARATOR_WEIGHT_X;
		m_constraints.weighty = NEW_MESSAGES_SEPARATOR_WEIGHT_Y;
		m_constraints.fill = NEW_MESSAGES_SEPARATOR_FILL;
		
		m_userInfo.add(new JSeparator(), m_constraints);
		
		displayNumNewMessages(m_userInfo);
		
		m_constraints.gridwidth = USER_INFO_GRID_WIDTH;
		m_constraints.gridheight = USER_INFO_GRID_HEIGHT;
		m_constraints.gridx = USER_INFO_GRID_X;
		m_constraints.gridy = USER_INFO_GRID_Y;
		m_constraints.weightx = USER_INFO_WEIGHT_X;
		m_constraints.weighty = USER_INFO_WEIGHT_Y;
		m_constraints.fill = USER_INFO_FILL;
		
		add(m_userInfo, m_constraints);
	}
	
	/**
	 * Ensures that all of the chats the user is a member of
	 * are being listened to.
	 */
	public void updateChatListeners() {
		for(Contact contact : m_currentUser.getContacts()) {
			contact.getChat().removeAllListeners();
			contact.getChat().addListener(this);
		}
		
		for(Chat groupChat : m_currentUser.getGroupChats()) {
			groupChat.removeAllListeners();
			groupChat.addListener(this);
		}
	}
	
	/**
	 * Update the contact JList to display the current contacts
	 * of the logged user.
	 */
	public void updateContactList() {
		m_contactListModel.clear();
		
		for(Contact contact : m_currentUser.getContacts()) {
			String details = getContactChatDetails(contact,m_currentUser);
			
			m_contactListModel.addElement(details);
		}
	}
	
	/**
	 * Update the group chat JList to display the current group chats
	 * the logged user is a member of.
	 */
	public void updateGroupChatList() {
		m_groupChatListModel.clear();
		
		for(Chat groupChat : m_currentUser.getGroupChats()) {
			String details = getGroupChatDetails(groupChat,m_currentUser);
			
			m_groupChatListModel.addElement(details);
		}
	}
	
	/**
	 * Update the message counter to display the current amount of
	 * new messages the logged user hasn't seen yet.
	 */
	public void updateMessageCounter() {
		int totalMessages = m_currentUser.getTotalNumNewMessages();
		String totalMessagesString = Integer.toString(totalMessages);
		m_totalMessagesDataLabel.setText(totalMessagesString);
	}
	
	/**
	 * Updates the notification display if there are any notifications.
	 * Otherwise nothing is displayed.
	 */
	public void updateNotifications() {
		m_friendRequest.setVisible(false);
		m_declineRequest.setVisible(false);
		
		// if User has notifications
		if (m_currentUser.hasNotification()){
			m_friendReqListModel.clear();
			
			ArrayList<User> contactRequests = m_currentUser
					.getContactRequests();
			
			for(User contactRequest : contactRequests) {
				m_friendReqListModel.addElement(contactRequest.getUsername());
			}
			
			m_textRequest.setVisible(true);
			m_friendReqScroller.setVisible(true);
		} else {
			m_textRequest.setVisible(false);
			m_friendReqScroller.setVisible(false);
		}
		
		revalidate();
	}
	
	/**
	 * Adds the logout button component.
	 */
	private void addLogoutButton() {
		JButton logoutButton = new JButton("Logout");
		
		m_constraints.gridwidth = LOGOUT_BUTTON_GRID_WIDTH;
		m_constraints.gridheight = LOGOUT_BUTTON_GRID_HEIGHT;
		m_constraints.gridx = LOGOUT_BUTTON_GRID_X;
		m_constraints.gridy = LOGOUT_BUTTON_GRID_Y;
		m_constraints.weightx = LOGOUT_BUTTON_WEIGHT_X;
		m_constraints.weighty = LOGOUT_BUTTON_WEIGHT_Y;
		m_constraints.fill = LOGOUT_BUTTON_FILL;
		
		logoutButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				m_currentUser.updateLastLogTime();
				Resources.loadAllResources();
				GUILogin loginScreen = new GUILogin();
				loginScreen.setVisible(true);
				dispose();
				//System.exit(0);
			}
		});	
		
		m_userInfo.add(logoutButton, m_constraints);
	}
	
	/**
	 * Adds the accept and decline request buttons.
	 */
	private void addRequestButtons() {
		m_friendRequest = new JButton("Accept");
		
		m_friendRequest.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int selectedRequestIndex = m_friendReqList.getSelectedIndex();
				ArrayList<User> requests = m_currentUser.getContactRequests();
				User selectedRequest = requests.get(selectedRequestIndex);
				
				m_currentUser.acceptContactRequest(selectedRequest);
				
				updateNotifications();
				updateContactList();
				
				updateChatListeners();
			}
		});
		
		m_friendRequest.setVisible(false);
		
		m_constraints.gridwidth = FRIEND_REQUEST_GRID_WIDTH;
		m_constraints.gridheight = FRIEND_REQUEST_GRID_HEIGHT;
		m_constraints.gridx = FRIEND_REQUEST_GRID_X;
		m_constraints.gridy = FRIEND_REQUEST_GRID_Y;
		m_constraints.weightx = FRIEND_REQUEST_WEIGHT_X;
		m_constraints.weighty = FRIEND_REQUEST_WEIGHT_Y;
		m_constraints.fill = FRIEND_REQUEST_FILL;
		
		add(m_friendRequest, m_constraints);
		
		m_declineRequest = new JButton("Decline");
		
		m_declineRequest.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int selectedRequestIndex = m_friendReqList.getSelectedIndex();
				ArrayList<User> requests = m_currentUser.getContactRequests();
				User selectedRequest = requests.get(selectedRequestIndex);
				
				m_currentUser.removeContactRequest(selectedRequest);
				
				updateNotifications();
			}
		});
		
		m_declineRequest.setVisible(false);
		
		m_constraints.gridwidth = DECLINE_REQUEST_GRID_WIDTH;
		m_constraints.gridheight = DECLINE_REQUEST_GRID_HEIGHT;
		m_constraints.gridx = DECLINE_REQUEST_GRID_X;
		m_constraints.gridy = DECLINE_REQUEST_GRID_Y;
		m_constraints.weightx = DECLINE_REQUEST_WEIGHT_X;
		m_constraints.weighty = DECLINE_REQUEST_WEIGHT_Y;
		m_constraints.fill = DECLINE_REQUEST_FILL;
		
		add(m_declineRequest, m_constraints);
	}
	
	/**
	 * Tests the display of the details that are used for the contact JList
	 * display.
	 * 
	 * Integration testing with classes: MessageBuilder, TextMessage, Chat
	 * 
	 * @param loggedUser The user to test on.
	 * @param contact The contact the details are based on.
	 */
	private static void contactDetailsTest(User loggedUser, Contact contact) {
		MessageBuilder message = new MessageBuilder(contact.getUser())
				.withText("Test");
		
		TextMessage textMessage = new TextMessage(message);
		
		contact.getChat().sendMessage(textMessage);
		
		String details = getContactChatDetails(contact, loggedUser);
		System.out.println("Contact description " + details);
	}
	
	/**
	 * Creates and displays the GUI.
	 */
	private void createHub(){
		//When the window closes, the last login time is updated
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				m_currentUser.updateLastLogTime();
			}
		});
		
		setupMessagesDisplay();
		
		setLayout(new GridBagLayout());
		
		m_constraints = new GridBagConstraints();
		m_constraints.insets = HUB_INSETS;
		
		displayContactList();
		
		displayGroupChatList();
		
		setupUserInfo();
		
		addLogoutButton();
		
		displayUserInfo(m_currentUser);
		
		updateMessageCounter();
		
		JPanel messagingButtonsPanel = createSelectionButtons();
		
		m_constraints.gridwidth = MESSAGE_BUTTONS_PANEL_GRID_WIDTH;
		m_constraints.gridheight = MESSAGE_BUTTONS_PANEL_GRID_HEIGHT;
		m_constraints.gridx = MESSAGE_BUTTONS_PANEL_GRID_X;
		m_constraints.gridy = MESSAGE_BUTTONS_PANEL_GRID_Y;
		m_constraints.weightx = MESSAGE_BUTTONS_PANEL_WEIGHT_X;
		m_constraints.weighty = MESSAGE_BUTTONS_PANEL_WEIGHT_Y;
		m_constraints.fill = MESSAGE_BUTTONS_PANEL_FILL;
		
		add(messagingButtonsPanel, m_constraints);
		
		JPanel menuButtonsPanel = displayMenuButtons();
		
		m_constraints.gridwidth = MENU_BUTTONS_PANEL_GRID_WIDTH;
		m_constraints.gridheight = MENU_BUTTONS_PANEL_GRID_HEIGHT;
		m_constraints.gridx = MENU_BUTTONS_PANEL_GRID_X;
		m_constraints.gridy = MENU_BUTTONS_PANEL_GRID_Y;
		m_constraints.weightx = MENU_BUTTONS_PANEL_WEIGHT_X;
		m_constraints.weighty = MENU_BUTTONS_PANEL_WEIGHT_Y;
		m_constraints.fill = MENU_BUTTONS_PANEL_FILL;
		
		add(menuButtonsPanel, m_constraints);
		
		displayNotification();
		
		setVisible(true);
	}
	
	/**
	 * Create the buttons that will be displayed when a contact or chat is
	 * selected.
	 * 
	 * @return The JPanel with the added buttons.
	 */
	private JPanel createSelectionButtons() {
		JPanel selectionButtonsPanel = new JPanel();
		selectionButtonsPanel.setLayout(new BoxLayout(selectionButtonsPanel,
				BoxLayout.LINE_AXIS));
		
		selectionButtonsPanel.add(Box.createHorizontalGlue());
		
		m_sendMessage = new JButton("Send message");
		m_sendMessage.setVisible(false);
		m_sendMessage.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				sendSelectedMessage();
			}
		});
		
		selectionButtonsPanel.add(m_sendMessage);
		
		selectionButtonsPanel.add(Box.createRigidArea(SPACING_DIMENSION));
		
		m_messageHistory = new JButton("View message history");
		m_messageHistory.setVisible(false);
		m_messageHistory.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showSelectedMessageHistory();
			}
		});
		
		selectionButtonsPanel.add(m_messageHistory);
		
		selectionButtonsPanel.add(Box.createRigidArea(SPACING_DIMENSION));
		
		m_removeButton = new JButton("Remove");
		m_removeButton.setVisible(false);
		
		m_removeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(displayRemoveConfirm()) {
					removeSelected();
					
					updateChatListeners();
				}
			}
		});
		
		selectionButtonsPanel.add(m_removeButton);
		
		selectionButtonsPanel.add(Box.createHorizontalGlue());
		
		return selectionButtonsPanel;
	}
	
	/**
	 * Create the menu buttons that when clicked will direct the user to
	 * other GUIs that carry out other actions.
	 * 
	 * @return The panel with the added buttons.
	 */
	private JPanel displayMenuButtons() {
		JPanel menuButtonsPanel = new JPanel();
		menuButtonsPanel.setLayout(new BoxLayout(menuButtonsPanel,
				BoxLayout.LINE_AXIS));
		
		//Add padding around the edges
		menuButtonsPanel.setBorder(new EmptyBorder(HUB_INSETS));
		menuButtonsPanel.setBackground(MENU_BUTTONS_PANEL_COLOUR);
		
		//Allows the buttons to be centred out in the middle
		menuButtonsPanel.add(Box.createHorizontalGlue());
		
		JButton addFriendButton = new JButton("Add Friend");
		addFriendButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new GUIAddFriend(m_currentUser, m_userDB);
			}
		});
		
		menuButtonsPanel.add(addFriendButton);
		
		//Adds fixed spacing between the buttons
		menuButtonsPanel.add(Box.createRigidArea(SPACING_DIMENSION));
		
		//Allows the listeners to access this object
		GUIHub guiHub = this;
		
		JButton createGroupButton = new JButton("Create Group");
		createGroupButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GUICreateGroup createGroup = new GUICreateGroup(m_currentUser);
				createGroup.addCreateGroupListener(guiHub);
			}
		});
		menuButtonsPanel.add(createGroupButton, m_constraints);
		
		//Adds fixed spacing between the buttons
		menuButtonsPanel.add(Box.createRigidArea(SPACING_DIMENSION));
		
		JButton updateProfileButton = new JButton("Update Profile");
		
		updateProfileButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				m_detailsManager.addListener(guiHub);
				m_detailsManager.changeUserGUI(m_currentUser);
			}
		});
		
		menuButtonsPanel.add(updateProfileButton);
		
		JButton drawButton = new JButton("Draw");
		
		//Adds fixed spacing between the buttons
		menuButtonsPanel.add(Box.createRigidArea(SPACING_DIMENSION));
		
		drawButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ArrayList<String> contacts = new ArrayList<String>();
				
				for(Contact contact : m_currentUser.getContacts()) {
					contacts.add(contact.getUser().getUsername());
				}
				
				DrawingEnvironment de = new DrawingEnvironment(m_currentUser.getUsername(), contacts);
				de.setLocationRelativeTo(null);
			}
		});
		
		menuButtonsPanel.add(drawButton);
		
		//Allows the buttons to be centred out in the middle
		menuButtonsPanel.add(Box.createHorizontalGlue());
		
		return menuButtonsPanel;
	}
	
	/**
	 * Displays the confirm box that allows the user to confirm the removal.
	 * 
	 * @return Whether the user wants to remove or not.
	 */
	private static boolean displayRemoveConfirm() {
		int result = JOptionPane.showConfirmDialog(null, "Are you sure?",
				"Remove", JOptionPane.YES_NO_OPTION,
				JOptionPane.PLAIN_MESSAGE);
		
		return (result == JOptionPane.YES_OPTION);
	}
	
	/**
	 * Get the details of the conversation of the contact, this includes
	 * the username of the contact, the amount of new messages that haven't
	 * been seen, and the time and date of when the last message was sent
	 * (if there exists a message).
	 * 
	 * @param contact The contact to describe.
	 * @param currentUser The logged user where the displayed details are
	 * for the perspective of this user.
	 * 
	 * @return The details of the contact.
	 */
	private static String getContactChatDetails(Contact contact,
			User currentUser) {
		
		Message lastMessage = contact.getChat().getLastMessageSent();
		
		String dateAndTime;
		
		if(lastMessage == null) {
			dateAndTime = "No Messages";
		} else {
			dateAndTime = "last sent " + lastMessage.getDate() + " "
					+ lastMessage.getTime();
		}
		
		//Uses html to allow for more control of how the details are displayed
		return "<html><p align='left'>" + contact.getUser().getUsername()
				+ ", " + contact.getChat().getNumNewMessages(currentUser)
				+ " new messages<br>" + dateAndTime + "</p></html>";
	}
	
	/**
	 * Get the details of the group conversation, this includes the name of
	 * the chat, the amount of new messages that haven't been seen, and the
	 * time and date of when the last message was sent (if there exists a
	 * message).
	 * 
	 * @param chat The group chat to describe
	 * @param currentUser The logged user where the displayed details are
	 * for the perspective of this user.
	 * 
	 * @return The details of the group chat.
	 */
	private static String getGroupChatDetails(Chat chat, User currentUser) {
		Message lastMessage = chat.getLastMessageSent();
		
		String dateAndTime;
		
		if(lastMessage == null) {
			dateAndTime = "No Messages";
		} else {
			dateAndTime = "last sent " + lastMessage.getDate() + " "
					+ lastMessage.getTime();
		}
		
		//Uses html to allow for more control of how the details are displayed
		return "<html>" + chat.getName() + ", "
			+ chat.getNumNewMessages(currentUser) + " new messages<br>"
			+ dateAndTime + "</html>";
	}
	
	/**
	 * Tests the display of the details that are used for the group chat JList
	 * display.
	 * 
	 * Integration testing with classes: MessageBuilder, TextMessage, Chat
	 * 
	 * @param loggedUser The user to test on.
	 * @param sender User that will send a message to the given chat.
	 * @param groupChat The group chat to test on.
	 */
	private static void groupDetailsTest(User loggedUser, User sender,
			Chat groupChat) {
		
		MessageBuilder message = new MessageBuilder(loggedUser)
				.withText("Test");
		
		TextMessage textMessage = new TextMessage(message);
		
		groupChat.sendMessage(textMessage);
		
		message = new MessageBuilder(sender).withText("Hello?");
		
		textMessage = new TextMessage(message);
		
		groupChat.sendMessage(textMessage);
		
		String details = getGroupChatDetails(groupChat, loggedUser);
		System.out.println("Group chat A description " + details);
	}
	
	/**
	 * Tests the GUI methods.
	 * 
	 * @param loggedUser The user to test on.
	 * @param testUser User for testing.
	 * @param users Users for testing.
	 * @param testChat Chat for testing.
	 * @return The GUIHub generated from these tests.
	 */
	private static GUIHub guiTest(User loggedUser, User testUser, BST users,
			Chat testChat) {
		
		GUIHub hub = new GUIHub(loggedUser, users);
		
		waitTest();
		
		notificationTest(loggedUser, testUser, hub);
		
		waitTest();
		
		messageHistoryTest(hub, testChat);
		
		return hub;
	}
	
	/**
	 * Tests the listeners.
	 * 
	 * Integration testing with classes: MessageBuilder, TextMessage, Chat
	 * 
	 * @param loggedUser The user to test on.
	 * @param chat The chat used for the test.
	 * @param hub The hub GUI to test on.
	 */
	private static void listenersTest(User loggedUser, Chat chat, GUIHub hub) {
		MessageBuilder message = new MessageBuilder(loggedUser)
				.withText("A new message");
		
		TextMessage textMessage = new TextMessage(message);
		
		chat.getMessages().add(textMessage);
		
		hub.onMessageSent(chat, textMessage);
		
		waitTest();
		
		chat.getMessages().remove(textMessage);
		
		message = new MessageBuilder(loggedUser).withText("Edited message");
		
		textMessage = new TextMessage(message);
		
		chat.getMessages().add(textMessage);
		
		hub.onMessageEdited(chat, textMessage);
		
		waitTest();
		
		chat.getMessages().remove(textMessage);
		
		hub.onMessageDeleted(chat, textMessage);
	}
	
	/**
	 * Tests the message history display.
	 * 
	 * @param hub The GUI hub to test on.
	 * @param chat The chat used for the test.
	 */
	private static void messageHistoryTest(GUIHub hub, Chat chat) {
		hub.sendSelectedMessage();
		hub.showSelectedMessageHistory();
		
		hub.displayMessageHistory(chat);
		
		hub.removeSelected();
	}
	
	/**
	 * Tests the notification display.
	 * 
	 * Integration testing with class: User
	 * 
	 * @param loggedUser The user to test on.
	 * @param request The user who would send a contact request
	 * to the logged user, which should in turn allow the notification
	 * to be displayed.
	 * 
	 * @param hub The GUI hub to test on.
	 */
	private static void notificationTest(User loggedUser, User request,
			GUIHub hub) {
		
		loggedUser.addContactRequest(request);
		
		hub.displayNotification();
		hub.updateNotifications();
	}
	
	/**
	 * Remove the contact or group chat selected from either the contact JList
	 * or the group chat JList respectively.
	 */
	private void removeSelected() {
		int selectedContactIndex = m_contactList.getSelectedIndex();
		int selectedChatIndex = m_groupChatList.getSelectedIndex();
		
		//If a contact is selected
		if(selectedContactIndex != -1) {
			ArrayList<Contact> contacts = m_currentUser.getContacts();
			Contact selectedContact = contacts.get(selectedContactIndex);
			
			m_currentUser.removeContact(selectedContact);
			
			m_contactListModel.remove(selectedContactIndex);
			
			updateMessageCounter();
			
		//If a group chat is selected
		} else if (selectedChatIndex != -1) {
			ArrayList<Chat> groupChats = m_currentUser.getGroupChats();
			Chat selectedChat = groupChats.get(selectedChatIndex);
			
			selectedChat.deleteChat();
			
			m_groupChatListModel.remove(selectedChatIndex);
			
			updateMessageCounter();
		}
	}
	
	/**
	 * Allows the user to send a message to the contact or group chat selected
	 * from the contact JList or group chat JList respectively.
	 * 
	 * Will open the GUI to choose the message to send to the selected contact
	 * or group chat.
	 */
	private void sendSelectedMessage() {
		int contactIndex = m_contactList.getSelectedIndex();
		int chatIndex = m_groupChatList.getSelectedIndex();
		
		//If a contact is selected
		if(contactIndex != -1) {
			Contact contact = m_currentUser.getContacts().get(contactIndex);
			
			Chat chat = contact.getChat();
			
			new GUIHubMessageChoice(m_currentUser, m_currentUser, chat);
			
		//If a group chat is selected
		} else if (chatIndex != -1) {
			Chat chat = m_currentUser.getGroupChats().get(chatIndex);
			
			new GUIHubMessageChoice(m_currentUser, m_currentUser, chat);
		}
	}
	
	/**
	 * Setup the window that will display the message history.
	 */
	private void setupMessagesDisplay() {
		m_messagesWindow = new JFrame("Messages");
		m_messagesWindow.setSize(MESSAGE_FRAME_SIZE);
		
		m_messagesDisplay = new JPanel();
		m_messagesDisplay.setLayout(new BoxLayout(m_messagesDisplay,
				BoxLayout.PAGE_AXIS));
		
		m_messagesScroll = new JScrollPane(m_messagesDisplay);
		
		m_messagesWindow.add(m_messagesScroll);
	}
	
	/**
	 * Display the message history of the contact chat or group chat selected
	 * from the contact JList or group chat JList respectively.
	 */
	private void showSelectedMessageHistory() {
		int selectedContactIndex = m_contactList.getSelectedIndex();
		int selectedChatIndex = m_groupChatList.getSelectedIndex();
		
		//If a contact is selected
		if(selectedContactIndex != -1) {
			ArrayList<Contact> contacts = m_currentUser.getContacts();
			Contact selectedContact = contacts.get(selectedContactIndex);
			
			Chat chat = selectedContact.getChat();
			
			displayMessageHistory(chat);
			
			/**
			 * The message counters will change in response to seeing
			 * the new messages from the message history.
			 */
			updateMessageCounter();
			updateContactList();
			
		//If a group chat is selected
		} else if(selectedChatIndex != -1) {
			ArrayList<Chat> groupChats = m_currentUser.getGroupChats();
			Chat selectedGroupChat = groupChats.get(selectedChatIndex);
			
			displayMessageHistory(selectedGroupChat);
			
			/**
			 * The message counters will change in response to seeing
			 * the new messages from the message history.
			 */
			updateMessageCounter();
			updateGroupChatList();
		}
	}
	
	/**
	 * Test the removal of contacts and group chats.
	 * 
	 * Integration testing with class: User
	 * 
	 * @param loggedUser The user to test on.
	 * @param testContact The test contact.
	 * @param testChat The test chat.
	 * @param hub The hub GUI to test on.
	 */
	private static void testRemoval(User loggedUser, Contact testContact,
			Chat testChat, GUIHub hub) {
		
		loggedUser.removeContact(testContact);
		hub.updateContactList();
		
		waitTest();
		
		loggedUser.removeGroupChat(testChat);
		hub.updateGroupChatList();
	}
	
	/**
	 * Used for putting delays in the testing (especially for GUI testing) to
	 * help analyse the tests.
	 */
	private static void waitTest() {
		try {
			Thread.sleep(TEST_DELAY);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @file	-GUIHub.java
	 * @author	-Alistair Cloney
	 * @date	-11/3/2017
	 * @see		-GUIMessageChoice.java
	 * @brief	Used to handle when a message is sent, it should update the
	 * 			contact and group chat JLists.
	 */
	private class GUIHubMessageChoice extends GUIMessageChoice {
		
		/**
		 * Constructor that displays the choice window.
		 * 
		 * @param currentUser The currently logged in user.
		 * @param author The user that will be the author of the message.
		 * @param chat The chat the message will be sent to.
		 */
		public GUIHubMessageChoice(User currentUser, User author, Chat chat) {
			super(currentUser, author, chat);
		}
		
		/**
		 * Called when a message has been chosen and the send button
		 * has been pressed.
		 * 
		 * This will also update the contact and group chat JLists.
		 * 
		 * @param message The message to send.
		 * @param chat The chat to send the message to.
		 * @param chooser The user who chose the message.
		 */
		@Override
		public void onMessageSend(Message message, Chat chat, User chooser) {
			super.onMessageSend(message, chat, chooser);
			updateContactList();
			updateGroupChatList();
		}
		
	}
	
	/**
	 * @file	-GUIHub.java
	 * @author	-Alistair Cloney
	 * @date	-11/3/2017
	 * @see		-https://docs.oracle.com/javase/7/docs/api/javax/swing/event/
	 * 			ListSelectionListener.html
	 * 
	 * @brief	Listens to and handles when there is a selection made in the
	 * 			contact request JList.
	 */
	private class RequestListListener implements ListSelectionListener {
		
		/**
		 * Handles when the request is selected, the request buttons should
		 * be available to click on when a request is selected.
		 * 
		 * @param e Event object.
		 */
		@Override
		public void valueChanged(ListSelectionEvent e) {
			if (!e.getValueIsAdjusting()
					&& !m_friendReqList.isSelectionEmpty()) {
				
				m_friendRequest.setVisible(true);
				m_declineRequest.setVisible(true);
				
				revalidate();
			}
		}
	}
	
	/**
	 * @file	-GUIHub.java
	 * @author	-Alistair Cloney
	 * @date	-11/3/2017
	 * @see		-https://docs.oracle.com/javase/7/docs/api/javax/swing/event/
	 * 			ListSelectionListener.html
	 * 
	 * @brief	Listens to and handles when there is a selection made in the
	 * 			contact JList.
	 */
	private class ContactListListener implements ListSelectionListener {
		
		/**
		 * Handles when the contact is selected, displays the selected
		 * contact's details and makes the messaging buttons available
		 * for the user to click on.
		 * 
		 * @param e Event object.
		 */
		@Override
		public void valueChanged(ListSelectionEvent e){
			//If finished selecting
			if (!e.getValueIsAdjusting()
					&& !m_contactList.isSelectionEmpty()) {
				
				int selectedIndex = m_contactList.getSelectedIndex();
				ArrayList<Contact> userContacts = m_currentUser.getContacts();
				Contact selectedContact = userContacts.get(selectedIndex);
				
				displayUserInfo(selectedContact.getUser());
				
				/**
				 * Make sure that only one item between the contact list
				 * and group chat list can be chosen.
				 */
				m_groupChatList.clearSelection();
				
				m_sendMessage.setVisible(true);
				m_messageHistory.setVisible(true);
				m_removeButton.setVisible(true);
			}
		}
	}
	
	/**
	 * @file	-GUIHub.java
	 * @author	-Alistair Cloney
	 * @date	-11/3/2017
	 * @see		-https://docs.oracle.com/javase/7/docs/api/javax/swing/event/
	 * 			ListSelectionListener.html
	 * 
	 * @brief	Listens to and handles when there is a selection made in the
	 * 			group chat JList.
	 */
	private class GroupChatListListener implements ListSelectionListener {
		
		/**
		 * Handles when the group chat is selected, makes the messaging
		 * buttons available for the user to click on.
		 * 
		 * @param e Event object.
		 */
		@Override
		public void valueChanged(ListSelectionEvent e) {
			//If finished selecting
			if (!e.getValueIsAdjusting() &&
					!m_groupChatList.isSelectionEmpty()) {
				
				/**
				 * Make sure that only one item between the contact list
				 * and group chat list can be chosen.
				 */
				m_contactList.clearSelection();
				
				m_sendMessage.setVisible(true);
				m_messageHistory.setVisible(true);
				m_removeButton.setVisible(true);
			}
		}
	}
	
	private static final int SPACING_BOARDER_AMOUNT = 6;
	private static final Dimension SPACING_DIMENSION = new Dimension(
			SPACING_BOARDER_AMOUNT, SPACING_BOARDER_AMOUNT);
	
	private static final Insets HUB_INSETS = new Insets(
			SPACING_BOARDER_AMOUNT,
			SPACING_BOARDER_AMOUNT,
			SPACING_BOARDER_AMOUNT,
			SPACING_BOARDER_AMOUNT);

	private static final int CONTACT_LIST_LABEL_GRID_WIDTH = 3;
	private static final int CONTACT_LIST_LABEL_GRID_HEIGHT = 1;
	private static final int CONTACT_LIST_LABEL_GRID_X = 0;
	private static final int CONTACT_LIST_LABEL_GRID_Y = 0;
	private static final int CONTACT_LIST_LABEL_WEIGHT_X = 0;
	private static final int CONTACT_LIST_LABEL_WEIGHT_Y = 0;
	private static final int CONTACT_LIST_LABEL_FILL = GridBagConstraints.BOTH;

	private static final int CONTACT_LIST_SCROLLER_GRID_WIDTH = 3;
	private static final int CONTACT_LIST_SCROLLER_GRID_HEIGHT = 5;
	private static final int CONTACT_LIST_SCROLLER_GRID_X =
			CONTACT_LIST_LABEL_GRID_X;
	private static final int CONTACT_LIST_SCROLLER_GRID_Y =
			CONTACT_LIST_LABEL_GRID_Y + CONTACT_LIST_LABEL_GRID_HEIGHT;
	private static final int CONTACT_LIST_SCROLLER_WEIGHT_X = 1;
	private static final int CONTACT_LIST_SCROLLER_WEIGHT_Y = 1;
	private static final int CONTACT_LIST_SCROLLER_FILL =
			GridBagConstraints.BOTH;
	
	private static final int MESSAGE_BUTTONS_PANEL_GRID_WIDTH = 6;
	private static final int MESSAGE_BUTTONS_PANEL_GRID_HEIGHT = 1;
	private static final int MESSAGE_BUTTONS_PANEL_GRID_X = 0;
	private static final int MESSAGE_BUTTONS_PANEL_GRID_Y =
			CONTACT_LIST_SCROLLER_GRID_Y + CONTACT_LIST_SCROLLER_GRID_HEIGHT;
	private static final int MESSAGE_BUTTONS_PANEL_WEIGHT_X = 0;
	private static final int MESSAGE_BUTTONS_PANEL_WEIGHT_Y = 0;
	private static final int MESSAGE_BUTTONS_PANEL_FILL =
			GridBagConstraints.HORIZONTAL;
	
	private static final Color MENU_BUTTONS_PANEL_COLOUR = new Color(153, 204, 255);
	private static final int MENU_BUTTONS_PANEL_GRID_WIDTH = 8;
	private static final int MENU_BUTTONS_PANEL_GRID_HEIGHT = 1;
	private static final int MENU_BUTTONS_PANEL_GRID_X = 0;
	private static final int MENU_BUTTONS_PANEL_GRID_Y =
			MESSAGE_BUTTONS_PANEL_GRID_Y + MESSAGE_BUTTONS_PANEL_GRID_HEIGHT;
	private static final int MENU_BUTTONS_PANEL_WEIGHT_X = 0;
	private static final int MENU_BUTTONS_PANEL_WEIGHT_Y = 0;
	private static final int MENU_BUTTONS_PANEL_FILL =
			GridBagConstraints.HORIZONTAL;
	
	private static final int GROUP_CHAT_LABEL_GRID_WIDTH = 3;
	private static final int GROUP_CHAT_LABEL_GRID_HEIGHT = 1;
	private static final int GROUP_CHAT_LABEL_GRID_X =
			CONTACT_LIST_LABEL_GRID_X + CONTACT_LIST_LABEL_GRID_WIDTH;
	private static final int GROUP_CHAT_LABEL_GRID_Y =
			CONTACT_LIST_LABEL_GRID_Y;
	private static final int GROUP_CHAT_LABEL_WEIGHT_X = 0;
	private static final int GROUP_CHAT_LABEL_WEIGHT_Y = 0;
	private static final int GROUP_CHAT_LABEL_FILL =
			GridBagConstraints.BOTH;

	private static final int GROUP_CHAT_SCROLLER_GRID_WIDTH = 3;
	private static final int GROUP_CHAT_SCROLLER_GRID_HEIGHT = 5;
	private static final int GROUP_CHAT_SCROLLER_GRID_X =
			GROUP_CHAT_LABEL_GRID_X;
	private static final int GROUP_CHAT_SCROLLER_GRID_Y =
			GROUP_CHAT_LABEL_GRID_Y + GROUP_CHAT_LABEL_GRID_HEIGHT;
	private static final int GROUP_CHAT_SCROLLER_WEIGHT_X = 1;
	private static final int GROUP_CHAT_SCROLLER_WEIGHT_Y = 1;
	private static final int GROUP_CHAT_SCROLLER_FILL =
			GridBagConstraints.BOTH;

	private static final int TEXT_REQUEST_GRID_WIDTH = 2;
	private static final int TEXT_REQUEST_GRID_HEIGHT = 1;
	private static final int TEXT_REQUEST_GRID_X = GROUP_CHAT_SCROLLER_GRID_X
			+ GROUP_CHAT_SCROLLER_GRID_WIDTH;
	private static final int TEXT_REQUEST_GRID_Y = 0;
	private static final int TEXT_REQUEST_WEIGHT_X = 0;
	private static final int TEXT_REQUEST_WEIGHT_Y = 0;
	private static final int TEXT_REQUEST_FILL = GridBagConstraints.NONE;

	private static final int FRIEND_REQ_SCROLLER_GRID_WIDTH = 2;
	private static final int FRIEND_REQ_SCROLLER_GRID_HEIGHT =
			CONTACT_LIST_SCROLLER_GRID_HEIGHT;
	private static final int FRIEND_REQ_SCROLLER_GRID_X = TEXT_REQUEST_GRID_X;
	private static final int FRIEND_REQ_SCROLLER_GRID_Y = TEXT_REQUEST_GRID_Y
			+ TEXT_REQUEST_GRID_HEIGHT;
	private static final int FRIEND_REQ_SCROLLER_WEIGHT_X = 0;
	private static final int FRIEND_REQ_SCROLLER_WEIGHT_Y = 0;
	private static final int FRIEND_REQ_SCROLLER_FILL =
			GridBagConstraints.BOTH;

	private static final int FRIEND_REQUEST_GRID_WIDTH = 1;
	private static final int FRIEND_REQUEST_GRID_HEIGHT = 1;
	private static final int FRIEND_REQUEST_GRID_X = TEXT_REQUEST_GRID_X;
	private static final int FRIEND_REQUEST_GRID_Y = FRIEND_REQ_SCROLLER_GRID_Y
			+ FRIEND_REQ_SCROLLER_GRID_HEIGHT;
	private static final int FRIEND_REQUEST_WEIGHT_X = 0;
	private static final int FRIEND_REQUEST_WEIGHT_Y = 0;
	private static final int FRIEND_REQUEST_FILL =
			GridBagConstraints.HORIZONTAL;
	
	private static final int DECLINE_REQUEST_GRID_WIDTH = 1;
	private static final int DECLINE_REQUEST_GRID_HEIGHT = 1;
	private static final int DECLINE_REQUEST_GRID_X = FRIEND_REQUEST_GRID_X
			+ FRIEND_REQUEST_GRID_WIDTH;
	private static final int DECLINE_REQUEST_GRID_Y = FRIEND_REQUEST_GRID_Y;
	private static final int DECLINE_REQUEST_WEIGHT_X = 0;
	private static final int DECLINE_REQUEST_WEIGHT_Y = 0;
	private static final int DECLINE_REQUEST_FILL =
			GridBagConstraints.HORIZONTAL;
	
	private static final Color USER_INFO_BACKGROUND_COLOUR = new Color(153, 204, 255);
	private static final int USER_INFO_GRID_WIDTH = 2;
	private static final int USER_INFO_GRID_HEIGHT = 8;
	private static final int USER_INFO_GRID_X = FRIEND_REQ_SCROLLER_GRID_X
			+ FRIEND_REQ_SCROLLER_GRID_WIDTH;
	private static final int USER_INFO_GRID_Y = 0;
	private static final int USER_INFO_WEIGHT_X = 0;
	private static final int USER_INFO_WEIGHT_Y = 0;
	private static final int USER_INFO_FILL = GridBagConstraints.BOTH;

	private static final int USER_INFO_LABEL_GRID_WIDTH = 2;
	private static final int USER_INFO_LABEL_GRID_HEIGHT = 1;
	private static final int USER_INFO_LABEL_GRID_X = 0;
	private static final int USER_INFO_LABEL_GRID_Y = 0;
	private static final int USER_INFO_LABEL_WEIGHT_X = 0;
	private static final int USER_INFO_LABEL_WEIGHT_Y = 0;
	private static final int USER_INFO_LABEL_FILL =
			GridBagConstraints.HORIZONTAL;

	private static final int USERNAME_LABEL_GRID_WIDTH = 2;
	private static final int USERNAME_LABEL_GRID_HEIGHT = 1;
	private static final int USERNAME_LABEL_GRID_X = 0;
	private static final int USERNAME_LABEL_GRID_Y = USER_INFO_LABEL_GRID_Y
			+ USER_INFO_LABEL_GRID_HEIGHT;
	private static final int USERNAME_LABEL_WEIGHT_X = 0;
	private static final int USERNAME_LABEL_WEIGHT_Y = 0;
	private static final int USERNAME_LABEL_FILL = GridBagConstraints.NONE;

	private static final int USERNAME_DATA_LABEL_GRID_WIDTH = 2;
	private static final int USERNAME_DATA_LABEL_GRID_HEIGHT = 1;
	private static final int USERNAME_DATA_LABEL_GRID_X = 0;
	private static final int USERNAME_DATA_LABEL_GRID_Y = USERNAME_LABEL_GRID_Y
			+ USERNAME_LABEL_GRID_HEIGHT;
	private static final int USERNAME_DATA_LABEL_WEIGHT_X = 0;
	private static final int USERNAME_DATA_LABEL_WEIGHT_Y = 0;
	private static final int USERNAME_DATA_LABEL_FILL =
			GridBagConstraints.NONE;
	
	private static final int USER_DETAILS_SEPARATOR_GRID_WIDTH = 2;
	private static final int USER_DETAILS_SEPARATOR_GRID_HEIGHT = 0;
	private static final int USER_DETAILS_SEPARATOR_GRID_X = 0;
	private static final int USER_DETAILS_SEPARATOR_GRID_Y =
			USERNAME_DATA_LABEL_GRID_Y + USERNAME_DATA_LABEL_GRID_HEIGHT;
	private static final int USER_DETAILS_SEPARATOR_WEIGHT_X = 0;
	private static final int USER_DETAILS_SEPARATOR_WEIGHT_Y = 0;
	private static final int USER_DETAILS_SEPARATOR_FILL =
			GridBagConstraints.BOTH;
	
	private static final int NAME_LABEL_GRID_WIDTH = 1;
	private static final int NAME_LABEL_GRID_HEIGHT = 1;
	private static final int NAME_LABEL_GRID_X = 0;
	private static final int NAME_LABEL_GRID_Y = USER_DETAILS_SEPARATOR_GRID_Y
			+ USER_DETAILS_SEPARATOR_GRID_HEIGHT;
	private static final int NAME_LABEL_WEIGHT_X = 0;
	private static final int NAME_LABEL_WEIGHT_Y = 0;
	private static final int NAME_LABEL_FILL = GridBagConstraints.NONE;

	private static final int NAME_DATA_LABEL_GRID_WIDTH = 1;
	private static final int NAME_DATA_LABEL_GRID_HEIGHT = 1;
	private static final int NAME_DATA_LABEL_GRID_X = 0;
	private static final int NAME_DATA_LABEL_GRID_Y = NAME_LABEL_GRID_Y
			+ NAME_LABEL_GRID_HEIGHT;
	private static final int NAME_DATA_LABEL_WEIGHT_X = 0;
	private static final int NAME_DATA_LABEL_WEIGHT_Y = 0;
	private static final int NAME_DATA_LABEL_FILL = GridBagConstraints.NONE;

	private static final int BIRTH_DATE_LABEL_GRID_WIDTH = 1;
	private static final int BIRTH_DATE_LABEL_GRID_HEIGHT = 1;
	private static final int BIRTH_DATE_LABEL_GRID_X = 0;
	private static final int BIRTH_DATE_LABEL_GRID_Y = NAME_DATA_LABEL_GRID_Y
			+ NAME_DATA_LABEL_GRID_HEIGHT;
	private static final int BIRTH_DATE_LABEL_WEIGHT_X = 0;
	private static final int BIRTH_DATE_LABEL_WEIGHT_Y = 0;
	private static final int BIRTH_DATE_LABEL_FILL = GridBagConstraints.NONE;

	private static final int BIRTH_DATE_DATA_LABEL_GRID_WIDTH = 1;
	private static final int BIRTH_DATE_DATA_LABEL_GRID_HEIGHT = 1;
	private static final int BIRTH_DATE_DATA_LABEL_GRID_X = 0;
	private static final int BIRTH_DATE_DATA_LABEL_GRID_Y =
			BIRTH_DATE_LABEL_GRID_Y + BIRTH_DATE_LABEL_GRID_HEIGHT;
	private static final int BIRTH_DATE_DATA_LABEL_WEIGHT_X = 0;
	private static final int BIRTH_DATE_DATA_LABEL_WEIGHT_Y = 0;
	private static final int BIRTH_DATE_DATA_LABEL_FILL =
			GridBagConstraints.NONE;

	private static final int TELEPHONE_LABEL_GRID_WIDTH = 1;
	private static final int TELEPHONE_LABEL_GRID_HEIGHT = 1;
	private static final int TELEPHONE_LABEL_GRID_X = 1;
	private static final int TELEPHONE_LABEL_GRID_Y =
			USER_DETAILS_SEPARATOR_GRID_Y + USER_DETAILS_SEPARATOR_GRID_HEIGHT;
	private static final int TELEPHONE_LABEL_WEIGHT_X = 0;
	private static final int TELEPHONE_LABEL_WEIGHT_Y = 0;
	private static final int TELEPHONE_LABEL_FILL = GridBagConstraints.NONE;

	private static final int TELEPHONE_DATA_LABEL_GRID_WIDTH = 1;
	private static final int TELEPHONE_DATA_LABEL_GRID_HEIGHT = 1;
	private static final int TELEPHONE_DATA_LABEL_GRID_X = 1;
	private static final int TELEPHONE_DATA_LABEL_GRID_Y =
			TELEPHONE_LABEL_GRID_Y + TELEPHONE_LABEL_GRID_HEIGHT;
	private static final int TELEPHONE_DATA_LABEL_WEIGHT_X = 0;
	private static final int TELEPHONE_DATA_LABEL_WEIGHT_Y = 0;
	private static final int TELEPHONE_DATA_LABEL_FILL =
			GridBagConstraints.NONE;

	private static final int CITY_LABEL_GRID_WIDTH = 1;
	private static final int CITY_LABEL_GRID_HEIGHT = 1;
	private static final int CITY_LABEL_GRID_X = 1;
	private static final int CITY_LABEL_GRID_Y = TELEPHONE_DATA_LABEL_GRID_Y
			+ TELEPHONE_DATA_LABEL_GRID_HEIGHT;
	private static final int CITY_LABEL_WEIGHT_X = 0;
	private static final int CITY_LABEL_WEIGHT_Y = 0;
	private static final int CITY_LABEL_FILL = GridBagConstraints.NONE;

	private static final int CITY_DATA_LABEL_GRID_WIDTH = 1;
	private static final int CITY_DATA_LABEL_GRID_HEIGHT = 1;
	private static final int CITY_DATA_LABEL_GRID_X = 1;
	private static final int CITY_DATA_LABEL_GRID_Y = CITY_LABEL_GRID_Y
			+ CITY_LABEL_GRID_HEIGHT;
	private static final int CITY_DATA_LABEL_WEIGHT_X = 0;
	private static final int CITY_DATA_LABEL_WEIGHT_Y = 0;
	private static final int CITY_DATA_LABEL_FILL = GridBagConstraints.NONE;
	
	private static final int LAST_LOG_SEPARATOR_GRID_WIDTH = 2;
	private static final int LAST_LOG_SEPARATOR_GRID_HEIGHT = 0;
	private static final int LAST_LOG_SEPARATOR_GRID_X = 0;
	private static final int LAST_LOG_SEPARATOR_GRID_Y = CITY_DATA_LABEL_GRID_Y
			+ CITY_DATA_LABEL_GRID_HEIGHT;
	private static final int LAST_LOG_SEPARATOR_WEIGHT_X = 0;
	private static final int LAST_LOG_SEPARATOR_WEIGHT_Y = 0;
	private static final int LAST_LOG_SEPARATOR_FILL = GridBagConstraints.BOTH;
	
	private static final int LAST_LOGIN_LABEL_GRID_WIDTH = 2;
	private static final int LAST_LOGIN_LABEL_GRID_HEIGHT = 1;
	private static final int LAST_LOGIN_LABEL_GRID_X = 0;
	private static final int LAST_LOGIN_LABEL_GRID_Y =
			LAST_LOG_SEPARATOR_GRID_Y + LAST_LOG_SEPARATOR_GRID_HEIGHT;
	private static final int LAST_LOGIN_LABEL_WEIGHT_X = 0;
	private static final int LAST_LOGIN_LABEL_WEIGHT_Y = 0;
	private static final int LAST_LOGIN_LABEL_FILL = GridBagConstraints.NONE;
	
	private static final int LAST_LOGIN_DATA_LABEL_GRID_WIDTH = 2;
	private static final int LAST_LOGIN_DATA_LABEL_GRID_HEIGHT = 1;
	private static final int LAST_LOGIN_DATA_LABEL_GRID_X = 0;
	private static final int LAST_LOGIN_DATA_LABEL_GRID_Y =
			LAST_LOGIN_LABEL_GRID_Y + LAST_LOGIN_LABEL_GRID_HEIGHT;
	private static final int LAST_LOGIN_DATA_LABEL_WEIGHT_X = 0;
	private static final int LAST_LOGIN_DATA_LABEL_WEIGHT_Y = 0;
	private static final int LAST_LOGIN_DATA_LABEL_FILL =
			GridBagConstraints.NONE;
	
	private static final int PROFILE_PICTURE_GRID_WIDTH = 2;
	private static final int PROFILE_PICTURE_GRID_HEIGHT = 1;
	private static final int PROFILE_PICTURE_GRID_X = 0;
	private static final int PROFILE_PICTURE_GRID_Y =
			LAST_LOGIN_DATA_LABEL_GRID_Y + LAST_LOGIN_DATA_LABEL_GRID_HEIGHT;
	private static final int PROFILE_PICTURE_WEIGHT_X = 1;
	private static final int PROFILE_PICTURE_WEIGHT_Y = 1;
	private static final int PROFILE_PICTURE_FILL = GridBagConstraints.BOTH;
	
	private static final int LOGOUT_BUTTON_GRID_WIDTH = 2;
	private static final int LOGOUT_BUTTON_GRID_HEIGHT = 1;
	private static final int LOGOUT_BUTTON_GRID_X = 0;
	private static final int LOGOUT_BUTTON_GRID_Y = PROFILE_PICTURE_GRID_Y
			+ PROFILE_PICTURE_GRID_HEIGHT;
	private static final int LOGOUT_BUTTON_WEIGHT_X = 0;
	private static final int LOGOUT_BUTTON_WEIGHT_Y = 0;
	private static final int LOGOUT_BUTTON_FILL = GridBagConstraints.NONE;
	
	private static final int NEW_MESSAGES_SEPARATOR_GRID_WIDTH = 2;
	private static final int NEW_MESSAGES_SEPARATOR_GRID_HEIGHT = 0;
	private static final int NEW_MESSAGES_SEPARATOR_GRID_X = 0;
	private static final int NEW_MESSAGES_SEPARATOR_GRID_Y =
			LOGOUT_BUTTON_GRID_Y + LOGOUT_BUTTON_GRID_HEIGHT;
	private static final int NEW_MESSAGES_SEPARATOR_WEIGHT_X = 0;
	private static final int NEW_MESSAGES_SEPARATOR_WEIGHT_Y = 0;
	private static final int NEW_MESSAGES_SEPARATOR_FILL =
			GridBagConstraints.BOTH;
	
	private static final int TOTAL_MESSAGES_LABEL_GRID_WIDTH = 2;
	private static final int TOTAL_MESSAGES_LABEL_GRID_HEIGHT = 1;
	private static final int TOTAL_MESSAGES_LABEL_GRID_X = 0;
	private static final int TOTAL_MESSAGES_LABEL_GRID_Y =
			NEW_MESSAGES_SEPARATOR_GRID_Y + NEW_MESSAGES_SEPARATOR_GRID_HEIGHT;
	private static final int TOTAL_MESSAGES_LABEL_WEIGHT_X = 0;
	private static final int TOTAL_MESSAGES_LABEL_WEIGHT_Y = 0;
	private static final int TOTAL_MESSAGES_LABEL_FILL =
			GridBagConstraints.BOTH;
	
	private static final int TOTAL_MESSAGES_DATA_LABEL_GRID_WIDTH = 2;
	private static final int TOTAL_MESSAGES_DATA_LABEL_GRID_HEIGHT = 1;
	private static final int TOTAL_MESSAGES_DATA_LABEL_GRID_X = 0;
	private static final int TOTAL_MESSAGES_DATA_LABEL_GRID_Y =
			TOTAL_MESSAGES_LABEL_GRID_Y + TOTAL_MESSAGES_LABEL_GRID_HEIGHT;
	private static final int TOTAL_MESSAGES_DATA_LABEL_WEIGHT_X = 0;
	private static final int TOTAL_MESSAGES_DATA_LABEL_WEIGHT_Y = 0;
	private static final int TOTAL_MESSAGES_DATA_LABEL_FILL =
			GridBagConstraints.BOTH;
	
	private static final Dimension MESSAGE_FRAME_SIZE = new Dimension(300,450);
	
	//Font used by titles.
	private static final Font TITLE_FONT =
			new Font("Helvetica", Font.PLAIN, 18);
	
	//Normal text font.
	private static final Font TEXT_FONT =
			new Font("Helvetica", Font.PLAIN, 15);
	
	private static final int TEST_DELAY = 1000;
	
	private JFrame m_messagesWindow;
	
	//Scroll pane of the message history
	private JScrollPane m_messagesScroll;
	
	private JPanel m_messagesDisplay;
	
	//Button to call message history.
	private JButton m_messageHistory;
	
	//Button to call send message.
	private JButton m_sendMessage;
	
	//Button to remove contact or chat depending on which is selected
	private JButton m_removeButton;
	
	//Button to send friendRequest.
	private JButton m_friendRequest;
	
	//Button to decline friendRequest.
	private JButton m_declineRequest;
	
	private JLabel m_textRequest;
	
	//Scroll pane of the friend requests
	private JScrollPane m_friendReqScroller;
	
	//User info components
	private JLabel m_usernameDataLabel;
	private JLabel m_nameDataLabel;
	private JLabel m_telDataLabel;
	private JLabel m_cityDataLabel;
	private JLabel m_totalMessagesDataLabel;
	private JLabel m_lastLoginDataLabel;
	private JLabel m_birthDateDataLabel;
	private ImageComponent m_profilePicture;
	
	//Panel that contains all user info.
	private JPanel m_userInfo;
	
	private GridBagConstraints m_constraints;
	
	private DefaultListModel<String> m_contactListModel;
	
	//JList containing the logged user's contacts.
	private JList<String> m_contactList;
	
	private DefaultListModel<String> m_groupChatListModel;
	
	//JList containing the logged user's group chats.
	private JList<String> m_groupChatList;
	
	private DefaultListModel<String> m_friendReqListModel;
	
	//JList containing the logged user's received friend requests.
	private JList<String> m_friendReqList;
	
	//User whose currently logged in
	private User m_currentUser;
	
	//Binary search tree containing all Users on the system.
	private BST m_userDB;
	
	//Provides GUIs for user creation and details modification
	private UserDetailsGUI m_detailsManager = new UserDetailsGUI();
	
}
