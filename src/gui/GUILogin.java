package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import components.ImageComponent;
import contactList.Contact;
import conversation.ChatIO;
import dataStructure.BST;
import resources.Resources;
import user.User;
/** @file	-GUILogin.java
 *  @author	-Ianis Tutunaru 874225, edited by Michael Andress
 *  @date	-11 Dec 16, edited on 9/3/2017
 *  @see	-GUI.java, UserDetailsGUI, GUIHub, User.java, Contact.java,
 *  		ContactIO.java
 *  
 *  @brief 	Allows the user to login to Skypertawe, also allows users to reset
 *  		their password if forgotten and links to an account creation GUI.
 *  
 *  Validates the user's input when logging in to check if the correct details
 *  are entered, once entered correctly, the user is directed to the GUIHub.
 *  
 *  The user can also reset their password if forgotten, this is currently
 *  handled by entering the username of the user thats forgotten and the
 *  password is changed and the user is notified of the new password.
 */
public class GUILogin extends GUI {
	
	/**
	 * Default constructor that initiates the creation of JFrame.
	 */
	public GUILogin() {
		createLogin();
	}
	
	/**
	 * Tests methods of class GUILogin.
	 * 
	 * Integration testing with classes: Resources, User
	 * 
	 * @param args Not used.
	 */
	public static void main(String[] args){
		Resources.loadAllResources();
		GUILogin loginScreen = new GUILogin();
		
		User test = new User().withUsername("Test User");
		
		User.writeUser(test, "A Password");
		
		forgotPasswordTest(loginScreen, test);
		
		guiTests(loginScreen);
		
		loginScreen.setVisible(true);
	}
	
	/**
	 * Attempt to change the password to the given password, notify the user
	 * if this succeeds or fails.
	 * 
	 * @param username The username of the user whose password needs changing.
	 */
	private static void attemptChangePassword(String username) {
		boolean succ = User.changePassword(username, RESET_PASSWORD);
		
		if(succ) {
			String message = "Password successfully changed to "
				+ RESET_PASSWORD;
			
			JOptionPane.showMessageDialog(null, message);
		} else {
			String message = "Unable to change password";
				
			JOptionPane.showMessageDialog(null, message);
		}
	}
	
	/**
	 * Create the button that will direct the user to the GUI to create
	 * an account.
	 * 
	 * @param panel The panel that the button will be created on.
	 */
	private void createAccountCreationButton(JPanel panel) {
		JButton createAccountButton = new JButton("Create new account");
		
		m_constraints.gridx = CREATE_ACCOUNT_BUTTON_GRID_X;
		m_constraints.gridy = CREATE_ACCOUNT_BUTTON_GRID_Y;
		m_constraints.weightx = CREATE_ACCOUNT_BUTTON_WEIGHT_X;
		m_constraints.weighty = CREATE_ACCOUNT_BUTTON_WEIGHT_Y;
		m_constraints.gridwidth = CREATE_ACCOUNT_BUTTON_GRID_WIDTH;
		m_constraints.gridheight = CREATE_ACCOUNT_BUTTON_GRID_HEIGHT;
		m_constraints.fill = CREATE_ACCOUNT_BUTTON_FILL;
		
		createAccountButton.setForeground(MANAGE_USER_BUTTON_TEXT_COLOUR);
		createAccountButton.setBackground(MANAGE_USER_BUTTON_COLOUR);
		createAccountButton.setFocusPainted(false);
		createAccountButton.setBorder(null);
		createAccountButton.setFont(BUTTON_FONT);
		
		createAccountButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				UserDetailsGUI createUser = new UserDetailsGUI();
				createUser.createUserGUI(m_userDB);
			}
		});
		
		panel.add(createAccountButton, m_constraints);
	}
	
	/**
	 * Method responsible for creation of content area.
	 */
	private void createContent(){
		JPanel contentsPane = (JPanel) getContentPane();
		createLogo(contentsPane);
		
		JPanel container = new JPanel();
		container.setLayout(new GridBagLayout());
		
		//Padding in the x axis between components
		m_constraints.insets = new Insets(0, COMPONENTS_X_PADDING,
				0, COMPONENTS_X_PADDING);
		
		createUsernameInput(container);
		
		createPasswordInput(container);
		
		createForgotPasswordButton(container);
		
		createLoginButton(container);
		
		createAccountCreationButton(container);
		
		m_constraints.gridx = CONTAINER_GRID_X;
		m_constraints.gridy = CONTAINER_GRID_Y;
		m_constraints.weightx = CONTAINER_WEIGHT_X;
		m_constraints.weighty = CONTAINER_WEIGHT_Y;
		m_constraints.gridwidth = CONTAINER_GRID_WIDTH;
		m_constraints.gridheight = CONTAINER_GRID_HEIGHT;
		m_constraints.fill = CONTAINER_FILL;
		
		add(container, m_constraints);
	}
	
	/**
	 * Create the button that will direct the user to the GUI that
	 * handles forgotten passwords.
	 * 
	 * @param panel The panel to create the button on.
	 */
	private void createForgotPasswordButton(JPanel panel) {
		JButton forgotPasswordButton = new JButton("Forgot password?");
		m_constraints.gridx = FORGOT_PASSWORD_BUTTON_GRID_X;
		m_constraints.gridy = FORGOT_PASSWORD_BUTTON_GRID_Y;
		m_constraints.weightx = FORGOT_PASSWORD_BUTTON_WEIGHT_X;
		m_constraints.weighty = FORGOT_PASSWORD_BUTTON_WEIGHT_Y;
		m_constraints.gridwidth = FORGOT_PASSWORD_BUTTON_GRID_WIDTH;
		m_constraints.gridheight = FORGOT_PASSWORD_BUTTON_GRID_HEIGHT;
		m_constraints.fill = FORGOT_PASSWORD_BUTTON_FILL;
		forgotPasswordButton.setForeground(MANAGE_USER_BUTTON_TEXT_COLOUR);
		forgotPasswordButton.setBackground(MANAGE_USER_BUTTON_COLOUR);
		forgotPasswordButton.setFocusPainted(false);
		forgotPasswordButton.setBorder(null);
		forgotPasswordButton.setFont(BUTTON_FONT);
		
		forgotPasswordButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				forgotPassword();
			}
		});
		
		panel.add(forgotPasswordButton, m_constraints);
	}
	
	/**
	 * Main method responsible for populating JFrame
	 */
	private void createLogin(){
		setLayoutAttributes();
		createContent();
		initialiseFrame();
	}
	
	/**
	 * Create the button that allows the user to log in to Skypertawe and
	 * directs the user to the GUIHub.
	 * 
	 * @param panel The panel to create the button on.
	 */
	private void createLoginButton(JPanel panel) {
		JButton logInButton = new JButton("Log in");
		logInButton.setHorizontalAlignment(JButton.CENTER);
		
		m_constraints.gridx = LOGIN_BUTTON_GRID_X;
		m_constraints.gridy = LOGIN_BUTTON_GRID_Y;
		m_constraints.weightx = LOGIN_BUTTON_WEIGHT_X;
		m_constraints.weighty = LOGIN_BUTTON_WEIGHT_Y;
		m_constraints.gridwidth = LOGIN_BUTTON_GRID_WIDTH;
		m_constraints.gridheight = LOGIN_BUTTON_GRID_HEIGHT;
		m_constraints.fill = LOGIN_BUTTON_FILL;
		
		logInButton.setForeground(LOGIN_BUTTON_TEXT_COLOUR);
		logInButton.setBackground(LOGIN_BUTTON_COLOUR);
		logInButton.setFocusPainted(false);
		logInButton.setBorder(null);
		logInButton.setFont(BUTTON_FONT);
		
		logInButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				logIn();
			}
		});
		
		panel.add(logInButton, m_constraints);
	}
	
	/**
	 * Method that is responsible for logo creation.
	 */
	private void createLogo(JPanel panel) {
		BufferedImage logoImage = Resources.getLogoImage();
		
		if(logoImage != null) {
			ImageComponent imagePanel = new ImageComponent(logoImage);
			
			m_constraints.gridx = LOGO_IMAGE_GRID_X;
			m_constraints.gridy = LOGO_IMAGE_GRID_Y;
			m_constraints.weightx = LOGO_IMAGE_WEIGHT_X;
			m_constraints.weighty = LOGO_IMAGE_WEIGHT_Y;
			m_constraints.gridwidth = LOGO_IMAGE_GRID_WIDTH;
			m_constraints.gridheight = LOGO_IMAGE_GRID_HEIGHT;
			m_constraints.fill = LOGO_IMAGE_FILL;
			
			//Restrict the image component size to the size of the image
			imagePanel.setPreferredSize(new Dimension(logoImage.getWidth(),
					logoImage.getHeight()));
			
			imagePanel.setMinimumSize(new Dimension(logoImage.getWidth(),
					logoImage.getHeight()));
			
			panel.add(imagePanel, m_constraints);
		}
	}
	
	/**
	 * Create the components needed to input the password.
	 * 
	 * @param panel The panel to create the components on.
	 */
	private void createPasswordInput(JPanel panel) {
		JLabel passwordText = new JLabel("Password:");
		passwordText.setFont(BUTTON_FONT);
		m_constraints.gridx = PASSWORD_TEXT_GRID_X;
		m_constraints.gridy = PASSWORD_TEXT_GRID_Y;
		m_constraints.weightx = PASSWORD_TEXT_WEIGHT_X;
		m_constraints.weighty = PASSWORD_TEXT_WEIGHT_Y;
		m_constraints.gridwidth = PASSWORD_TEXT_GRID_WIDTH;
		m_constraints.gridheight = PASSWORD_TEXT_GRID_HEIGHT;
		m_constraints.fill = PASSWORD_TEXT_FILL;
		panel.add(passwordText, m_constraints);
		
		m_passwordInput = new JPasswordField("");
		m_passwordInput.setEchoChar('*');
		m_constraints.gridx = PASSWORD_INPUT_GRID_X;
		m_constraints.gridy = PASSWORD_INPUT_GRID_Y;
		m_constraints.weightx = PASSWORD_INPUT_WEIGHT_X;
		m_constraints.weighty = PASSWORD_INPUT_WEIGHT_Y;
		m_constraints.gridwidth = PASSWORD_INPUT_GRID_WIDTH;
		m_constraints.gridheight = PASSWORD_INPUT_GRID_HEIGHT;
		m_constraints.fill = PASSWORD_INPUT_FILL;
		panel.add(m_passwordInput, m_constraints);
	}
	
	/**
	 * Create the components needed to input the username.
	 * 
	 * @param panel The panel to create the components on.
	 */
	private void createUsernameInput(JPanel panel) {
		JLabel usernameText = new JLabel("Username:");
		usernameText.setHorizontalAlignment(JLabel.CENTER);
		usernameText.setFont(BUTTON_FONT);
		m_constraints.gridx = USERNAME_TEXT_GRID_X;
		m_constraints.gridy = USERNAME_TEXT_GRID_Y;
		m_constraints.weightx = USERNAME_TEXT_WEIGHT_X;
		m_constraints.weighty = USERNAME_TEXT_WEIGHT_Y;
		m_constraints.gridwidth = USERNAME_TEXT_GRID_WIDTH;
		m_constraints.gridheight = USERNAME_TEXT_GRID_HEIGHT;
		m_constraints.fill = USERNAME_TEXT_FILL;
		panel.add(usernameText, m_constraints);
		
		m_usernameInput = new JTextField("");
		m_constraints.gridx = USERNAME_INPUT_GRID_X;
		m_constraints.gridy = USERNAME_INPUT_GRID_Y;
		m_constraints.weightx = USERNAME_INPUT_WEIGHT_X;
		m_constraints.weighty = USERNAME_INPUT_WEIGHT_Y;
		m_constraints.gridwidth = USERNAME_INPUT_GRID_WIDTH;
		m_constraints.gridheight = USERNAME_INPUT_GRID_HEIGHT;
		m_constraints.fill = USERNAME_INPUT_FILL;
		panel.add(m_usernameInput, m_constraints);
	}
	
	/**
	 * Method that adds functionality to the forgot password button.
	 */
	private void forgotPassword(){
		//Input box that allows the user to input their username
		String forgotUsername = JOptionPane.showInputDialog(null,
				"Please enter your username and press OK.",
				"Forgot Password", JOptionPane.PLAIN_MESSAGE);
		
		if(forgotUsername != null) {
			//Find the user from the entered username
			User user = m_userDB.getProfileRoot(forgotUsername);
			
			if(user == null) {
				JOptionPane.showMessageDialog(null,
					"Please enter a correct username.");
			} else {
				attemptChangePassword(forgotUsername);
			}
		}
	}
	
	/**
	 * Tests the forgot password methods.
	 * 
	 * Integration testing with class: User
	 * 
	 * @param login The login GUI to test on.
	 * @param user The user to test on.
	 */
	private static void forgotPasswordTest(GUILogin login, User user) {
		boolean result = User.validateUserDetails(user.getUsername(),
				RESET_PASSWORD);
		
		System.out.println("Password is " + RESET_PASSWORD + ": " + result);
		
		GUILogin.attemptChangePassword(user.getUsername());
		
		result = User.validateUserDetails(user.getUsername(),
				RESET_PASSWORD);
		
		System.out.println("Password is " + RESET_PASSWORD + ": " + result);
		
		User.changePassword(user.getUsername(), "TESTS");
		
		result = User.validateUserDetails(user.getUsername(),
				RESET_PASSWORD);
		
		System.out.println("Password is " + RESET_PASSWORD + ": " + result);
		
		login.forgotPassword();
		
		result = User.validateUserDetails(user.getUsername(),
				RESET_PASSWORD);
		
		System.out.println("Password is " + RESET_PASSWORD + ": " + result);
	}
	
	/**
	 * Tests the GUI components' display.
	 * 
	 * @param login The login GUI to test on.
	 */
	private static void guiTests(GUILogin login) {
		JFrame frame = new JFrame("Test");
		JPanel contentsPane = (JPanel) frame.getContentPane();
		contentsPane.setLayout(new GridBagLayout());
		
		login.createUsernameInput(contentsPane);
		login.createPasswordInput(contentsPane);
		login.createForgotPasswordButton(contentsPane);
		login.createLoginButton(contentsPane);
		login.createAccountCreationButton(contentsPane);
		
		frame.pack();
		
		frame.setVisible(true);
	}
	
	/**
	 * Method that initialises all the main parameters of the JFrame.
	 */
	private void initialiseFrame(){
		m_userDB = User.readAllUsers();
	}
	
	/**
	 * Method that adds functionality to the login button.
	 */
	private void logIn(){
		String username = m_usernameInput.getText();
		String password = new String(m_passwordInput.getPassword());
		
		if (User.validateUserDetails(username, password)) {
			m_loggedInUser = m_userDB.getProfileRoot(username);
			Contact.loadContacts(m_loggedInUser, m_userDB);
			User.loadContactRequests(m_loggedInUser, m_userDB);
			ChatIO.loadGroupChat(m_loggedInUser, m_userDB);
			
			dispose();
			new GUIHub(m_loggedInUser, m_userDB);
		}
		else
		{
			JOptionPane.showMessageDialog(this, "Username or password incorrect");
		}
	}
	
	/**
	 * Method that sets main parameters of the JFrame.
	 */
	private void setLayoutAttributes(){
		setLayout(new GridBagLayout());
		getContentPane().setBackground(PANE_COLOUR);
	}
	
	private static final String RESET_PASSWORD = "swansea";
	private static final Color LOGIN_BUTTON_TEXT_COLOUR =
			new Color(255, 255, 255);
	
	private static final Color LOGIN_BUTTON_COLOUR = new Color(153, 204, 255);
	private static final Color MANAGE_USER_BUTTON_COLOUR = Color.WHITE;
	private static final Color MANAGE_USER_BUTTON_TEXT_COLOUR =
			new Color(0, 0, 0);
	
	private static final int FONT_SIZE = 25;
	private static final Font BUTTON_FONT = new Font("Helvetica",
			Font.PLAIN, FONT_SIZE);
	
	private static final Color PANE_COLOUR = Color.WHITE;
	
	private static final int LOGO_IMAGE_GRID_X = 0;
	private static final int LOGO_IMAGE_GRID_Y = 0;
	private static final int LOGO_IMAGE_WEIGHT_X = 0;
	private static final int LOGO_IMAGE_WEIGHT_Y = 0;
	private static final int LOGO_IMAGE_GRID_WIDTH = 2;
	private static final int LOGO_IMAGE_GRID_HEIGHT = 1;
	private static final int LOGO_IMAGE_FILL = GridBagConstraints.NONE;

	private static final int CONTAINER_GRID_X = 1;
	private static final int CONTAINER_GRID_Y = 1;
	private static final int CONTAINER_WEIGHT_X = 0;
	private static final int CONTAINER_WEIGHT_Y = 0;
	private static final int CONTAINER_GRID_WIDTH = 1;
	private static final int CONTAINER_GRID_HEIGHT = 1;
	private static final int CONTAINER_FILL = GridBagConstraints.NONE;
	
	private static final int USERNAME_TEXT_GRID_X = 0;
	private static final int USERNAME_TEXT_GRID_Y = 1;
	private static final int USERNAME_TEXT_WEIGHT_X = 0;
	private static final int USERNAME_TEXT_WEIGHT_Y = 0;
	private static final int USERNAME_TEXT_GRID_WIDTH = 3;
	private static final int USERNAME_TEXT_GRID_HEIGHT = 1;
	private static final int USERNAME_TEXT_FILL = GridBagConstraints.NONE;

	private static final int USERNAME_INPUT_GRID_X = 0;
	private static final int USERNAME_INPUT_GRID_Y = 2;
	private static final int USERNAME_INPUT_WEIGHT_X = 0;
	private static final int USERNAME_INPUT_WEIGHT_Y = 0;
	private static final int USERNAME_INPUT_GRID_WIDTH = 3;
	private static final int USERNAME_INPUT_GRID_HEIGHT = 1;
	private static final int USERNAME_INPUT_FILL = GridBagConstraints.BOTH;

	private static final int PASSWORD_TEXT_GRID_X = 0;
	private static final int PASSWORD_TEXT_GRID_Y = 3;
	private static final int PASSWORD_TEXT_WEIGHT_X = 0;
	private static final int PASSWORD_TEXT_WEIGHT_Y = 0;
	private static final int PASSWORD_TEXT_GRID_WIDTH = 3;
	private static final int PASSWORD_TEXT_GRID_HEIGHT = 1;
	private static final int PASSWORD_TEXT_FILL = GridBagConstraints.NONE;

	private static final int PASSWORD_INPUT_GRID_X = 0;
	private static final int PASSWORD_INPUT_GRID_Y = 4;
	private static final int PASSWORD_INPUT_WEIGHT_X = 0;
	private static final int PASSWORD_INPUT_WEIGHT_Y = 0;
	private static final int PASSWORD_INPUT_GRID_WIDTH = 3;
	private static final int PASSWORD_INPUT_GRID_HEIGHT = 1;
	private static final int PASSWORD_INPUT_FILL = GridBagConstraints.BOTH;

	private static final int FORGOT_PASSWORD_BUTTON_GRID_X = 0;
	private static final int FORGOT_PASSWORD_BUTTON_GRID_Y = 6;
	private static final int FORGOT_PASSWORD_BUTTON_WEIGHT_X = 1;
	private static final int FORGOT_PASSWORD_BUTTON_WEIGHT_Y = 0;
	private static final int FORGOT_PASSWORD_BUTTON_GRID_WIDTH = 1;
	private static final int FORGOT_PASSWORD_BUTTON_GRID_HEIGHT = 1;
	private static final int FORGOT_PASSWORD_BUTTON_FILL =
			GridBagConstraints.BOTH;

	private static final int LOGIN_BUTTON_GRID_X = 0;
	private static final int LOGIN_BUTTON_GRID_Y = 5;
	private static final int LOGIN_BUTTON_WEIGHT_X = 0;
	private static final int LOGIN_BUTTON_WEIGHT_Y = 0;
	private static final int LOGIN_BUTTON_GRID_WIDTH = 3;
	private static final int LOGIN_BUTTON_GRID_HEIGHT = 1;
	private static final int LOGIN_BUTTON_FILL = GridBagConstraints.BOTH;

	private static final int CREATE_ACCOUNT_BUTTON_GRID_X = 2;
	private static final int CREATE_ACCOUNT_BUTTON_GRID_Y = 6;
	private static final int CREATE_ACCOUNT_BUTTON_WEIGHT_X = 1;
	private static final int CREATE_ACCOUNT_BUTTON_WEIGHT_Y = 0;
	private static final int CREATE_ACCOUNT_BUTTON_GRID_WIDTH = 1;
	private static final int CREATE_ACCOUNT_BUTTON_GRID_HEIGHT = 1;
	private static final int CREATE_ACCOUNT_BUTTON_FILL =
			GridBagConstraints.BOTH;
	
	private static final int COMPONENTS_X_PADDING = 20;
	
	//Text Field that is used to capture username input.
	private JTextField m_usernameInput;
	
	//Password field that is used to capture password.
	private JPasswordField m_passwordInput;
	
	//Constraints of the placement of the GUI components
	private GridBagConstraints m_constraints = new GridBagConstraints();
	
	//Existing users of Skypertawe are held in this binary search tree
	private BST m_userDB;
	
	//currently logged in user.
	private User m_loggedInUser;
}
