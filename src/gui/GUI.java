package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.UIManager;

/** @file	-GUI.java
 *  @author	-Ianis Tutunaru 874225, Edited by Alistair Cloney and
			Michael Andress
 *  @date	-11 Dec 16, edited on 9/3/2017
 *  @see	-GUIHub.java, GUILogin.java
 *  @brief	Class that defines important parameters such as default
 *  		dimensions of the screen and its position.
 *  
 *  All GUIs that extend this class will have the same title, frame size
 *  and background colour.
 */
public class GUI extends JFrame {
	
	/**Constructor of JFrame that creates a new instance of GUI.
	 * 
	 */
	public GUI() {
		setScreen();
	}
	
	/**
	 * Creates a button that displays the given icon.
	 * 
	 * @param icon The icon to display.
	 * @return The button that displays the icon.
	 */
	public static JButton createIconButton(ImageIcon icon) {
		JButton button = new JButton(icon);
		
		//Set button to be transparent
		button.setOpaque(false);
		button.setContentAreaFilled(false);
		button.setFocusPainted(false);
		
		/**
		 * Restrict the size of the button to be an offset larger than
		 * the size of the logo.
		 */
		int buttonSizeX = icon.getIconWidth()+BUTTON_ICON_SIZE_OFFSET;
		int buttonSizeY = icon.getIconHeight()+BUTTON_ICON_SIZE_OFFSET;
		Dimension buttonSize = new Dimension(buttonSizeX, buttonSizeY);
		
		button.setPreferredSize(buttonSize);
		button.setMaximumSize(buttonSize);
		button.setMinimumSize(buttonSize);
		
		return button;
	}
	
	/**
	 * Tests the methods of the class GUI.
	 * 
	 * @param args Not used.
	 */
	public static void main(String[] args) {
		try {
			GUI gui = new GUI();
			
			gui.setScreen();
			
			BufferedImage image = ImageIO.read(new File("edit.png"));
			
			ImageIcon icon = new ImageIcon(image);
			
			JButton button = createIconButton(icon);
			
			gui.add(button);
			
			gui.setVisible(true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/** Method that sets all of the main parameters for all instances,
	 *  such as the frame colour and size.
	 * 
	 */
	private void setScreen(){
		m_tk = Toolkit.getDefaultToolkit();
		m_screenSize = m_tk.getScreenSize();
		m_screenWidth = (int) m_screenSize.getWidth();
		m_screenHeight = (int) m_screenSize.getHeight();
		
		UIManager.put("OptionPane.background",Color.WHITE);
		UIManager.put("Panel.background", Color.white);
		setTitle("Skypertawe");
		
		int frameWidth = (int) (m_screenWidth*SIZE_RATIO);
		int frameHeight = (int) (m_screenHeight*SIZE_RATIO);
		
		setSize(frameWidth, frameHeight);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	private static final float SIZE_RATIO = 0.7f;
	private static final int BUTTON_ICON_SIZE_OFFSET = 10;
	
	//Default toolkit used by the application.
	private Toolkit m_tk;
	
	//Dimensions which store screen size.
	private Dimension m_screenSize;
	
	//Integer representation of screen width.
	private int m_screenWidth;
	
	//Integer representation of screen height.
	private int m_screenHeight;
}
