package gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import contactList.Contact;
import conversation.Chat;
import user.User;

/**
 * @File	-GUICreateGroup.java
 * @author	-Alistair Cloney
 * @date	-9/3/2017
 * @see		-Chat.java, User.java, Contact.java
 * @brief	Allows the user to create a group consisting of a chat and
 * 			the chosen contacts to be members.
 * 
 * Provides the user with two lists, the left list being the contacts that
 * are not going to be members of the group and the right list being the
 * contacts that are going to be members.
 * 
 * Buttons are provided to move contacts from one list to the other.
 * 
 * The text field at the top of the GUI is the group name text field.
 */
public class GUICreateGroup extends JFrame {
	
	/**
	 * Constructor that displays the GUI to create the group.
	 * 
	 * @param currentUser The user currently logged in.
	 */
	public GUICreateGroup(User currentUser) {
		setSize(CREATE_GROUP_FRAME_SIZE);
		
		setLayout(new GridBagLayout());
		
		GridBagConstraints constraints = new GridBagConstraints();
		
		//Set padding
		constraints.insets = new Insets(PADDING, PADDING, PADDING, PADDING);
		
		createGroupNameComponents();
		
		createContactListComponent(currentUser);
		
		createGroupMembersListComponent();
		
		createMoveMemberButtons();
		
		createGroupCreationButton(currentUser);
		
		setVisible(true);
	}
	
	/**
	 * @param listener The listener to add.
	 */
	public void addCreateGroupListener(CreateGroupListener listener) {
		m_createGroupListeners.add(listener);
	}
	
	/**
	 * Tests the methods of the class GUICreateGroup.
	 * 
	 * Integration test with classes: User, CreateGroupListener, Chat.
	 * 
	 * @param args Not used
	 */
	public static void main(String[] args) {
		User userTest = new User().withUsername("Test")
				.withBirthDate("10/05/1994");
		
		User userHello = new User().withUsername("Hello")
				.withBirthDate("05/07/2000");
		
		User userWorld = new User().withUsername("World")
				.withBirthDate("09/09/1999");
		
		userTest.acceptContactRequest(userHello);
		userTest.acceptContactRequest(userWorld);
		
		User currentUser = userTest;
		
		GUICreateGroup groupCreateGUI = new GUICreateGroup(currentUser);
		
		//Listeners used to test add and removal of listeners work properly
		CreateGroupListener secondListener = new CreateGroupListener() {
			@Override
			public void onCreateGroup(Chat groupChat) {
				System.out.println("Second listener: " + groupChat.getName()
					+ " group chat created");
			}
		};
		
		CreateGroupListener thirdListener = new CreateGroupListener() {
			@Override
			public void onCreateGroup(Chat groupChat) {
				System.out.println("Third listener: " + groupChat.getName()
					+ " group chat created");
				
				System.out.println("Creating another group form");
				GUICreateGroup groupCreateGUI =
					new GUICreateGroup(currentUser);
				
				System.out.println("Adding second and thrid listeners");
				groupCreateGUI.addCreateGroupListener(secondListener);
				groupCreateGUI.addCreateGroupListener(this);
				
				System.out.println("Removing all listeners");
				groupCreateGUI.removeAllCreateGroupListeners();
			}
		};
		
		CreateGroupListener firstListener = new CreateGroupListener() {
			@Override
			public void onCreateGroup(Chat groupChat) {
				System.out.println("First listener: " + groupChat.getName()
					+ " group chat created");
				
				System.out.println("Creating another group form");
				GUICreateGroup groupCreateGUI =
					new GUICreateGroup(currentUser);
				
				System.out.println("Adding second and thrid listeners");
				groupCreateGUI.addCreateGroupListener(secondListener);
				groupCreateGUI.addCreateGroupListener(thirdListener);
				
				System.out.println("Removing second listener");
				groupCreateGUI.removeCreateGroupListener(secondListener);
			}
		};
		
		groupCreateGUI.addCreateGroupListener(firstListener);
	}
	
	/**
	 * Remove all the listeners that listen to when the group is created.
	 */
	public void removeAllCreateGroupListeners() {
		m_createGroupListeners.clear();
	}
	
	/**
	 * Remove the given listener that listens to when the group is created.
	 * 
	 * @param listener The listener to remove.
	 */
	public void removeCreateGroupListener(CreateGroupListener listener) {
		m_createGroupListeners.remove(listener);
	}
	
	/**
	 * Creates the contact list of non members of the chat.
	 * 
	 * @param currentUser The user currently logged in.
	 */
	private void createContactListComponent(User currentUser) {
		GridBagConstraints constraints = new GridBagConstraints();
		
		JLabel contactsText = new JLabel("Contacts:");
		
		constraints.gridwidth = CONTACTS_TEXT_GRID_WIDTH;
		constraints.gridheight = CONTACTS_TEXT_GRID_HEIGHT;
		constraints.gridx = CONTACTS_TEXT_GRID_X;
		constraints.gridy = CONTACTS_TEXT_GRID_Y;
		constraints.weightx = CONTACTS_TEXT_WEIGHT_X;
		constraints.weighty = CONTACTS_TEXT_WEIGHT_Y;
		constraints.fill = CONTACTS_TEXT_FILL;
		
		add(contactsText, constraints);
		
		m_contactsListModel = new DefaultListModel<Contact>();
		
		//Add all the logged user's contacts to the list.
		for(Contact contact : currentUser.getContacts()) {
			m_contactsListModel.addElement(contact);
		}
		
		m_contactsList = new JList<Contact>(m_contactsListModel);
		
		JScrollPane contactListScroller = new JScrollPane(m_contactsList);
		
		constraints.gridwidth = CONTACTS_LIST_GRID_WIDTH;
		constraints.gridheight = CONTACTS_LIST_GRID_HEIGHT;
		constraints.gridx = CONTACTS_LIST_GRID_X;
		constraints.gridy = CONTACTS_LIST_GRID_Y;
		constraints.weightx = CONTACTS_LIST_WEIGHT_X;
		constraints.weighty = CONTACTS_LIST_WEIGHT_Y;
		constraints.fill = CONTACTS_LIST_FILL;
		
		add(contactListScroller, constraints);
	}
	
	/**
	 * Create the button that will create the group.
	 * 
	 * @param currentUser The user currently logged in.
	 */
	private void createGroupCreationButton(User currentUser) {
		GridBagConstraints constraints = new GridBagConstraints();
		
		JButton createGroup = new JButton("Create Group");
		
		createGroup.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ArrayList<Contact> selectedContacts = new ArrayList<Contact>();
				
				for(int i = 0; i < m_groupMembersListModel.size(); i++) {
					selectedContacts.add(m_groupMembersListModel.get(i));
				}
				
				Chat newChat = Chat.addChat(m_groupName.getText(),
					selectedContacts, currentUser);
				
				//Call all listeners' onCreateGroup method
				for(CreateGroupListener listener : m_createGroupListeners) {
					listener.onCreateGroup(newChat);
				}
				
				//Remove all listeners as the frame is disposed of
				removeAllCreateGroupListeners();
				
				dispose();
			}
			
		});
		
		constraints.gridwidth = CREATE_GROUP_BUTTON_GRID_WIDTH;
		constraints.gridheight = CREATE_GROUP_BUTTON_GRID_HEIGHT;
		constraints.gridx = CREATE_GROUP_BUTTON_GRID_X;
		constraints.gridy = CREATE_GROUP_BUTTON_GRID_Y;
		constraints.weightx = CREATE_GROUP_BUTTON_WEIGHT_X;
		constraints.weighty = CREATE_GROUP_BUTTON_WEIGHT_Y;
		constraints.fill = CREATE_GROUP_BUTTON_FILL;
		
		add(createGroup, constraints);
	}
	
	/**
	 * Create the list that displays the current members
	 * of the group chat.
	 */
	private void createGroupMembersListComponent() {
		GridBagConstraints constraints = new GridBagConstraints();
		
		JLabel groupMembersText = new JLabel("Group Members:");
		
		constraints.gridwidth = GROUP_MEMBERS_TEXT_GRID_WIDTH;
		constraints.gridheight = GROUP_MEMBERS_TEXT_GRID_HEIGHT;
		constraints.gridx = GROUP_MEMBERS_TEXT_GRID_X;
		constraints.gridy = GROUP_MEMBERS_TEXT_GRID_Y;
		constraints.weightx = GROUP_MEMBERS_TEXT_WEIGHT_X;
		constraints.weighty = GROUP_MEMBERS_TEXT_WEIGHT_Y;
		constraints.fill = GROUP_MEMBERS_TEXT_FILL;
		
		add(groupMembersText, constraints);
		
		m_groupMembersListModel = new DefaultListModel<Contact>();
		m_groupMembersList = new JList<Contact>(m_groupMembersListModel);
		
		JScrollPane listScroller = new JScrollPane(m_groupMembersList);
		
		constraints.gridwidth = GROUP_MEMBERS_LIST_GRID_WIDTH;
		constraints.gridheight = GROUP_MEMBERS_LIST_GRID_HEIGHT;
		constraints.gridx = GROUP_MEMBERS_LIST_GRID_X;
		constraints.gridy = GROUP_MEMBERS_LIST_GRID_Y;
		constraints.weightx = GROUP_MEMBERS_LIST_WEIGHT_X;
		constraints.weighty = GROUP_MEMBERS_LIST_WEIGHT_Y;
		constraints.fill = GROUP_MEMBERS_LIST_FILL;
		
		add(listScroller, constraints);
	}
	
	/**
	 * Create the components that are needed for input of the group name.
	 */
	private void createGroupNameComponents() {
		GridBagConstraints constraints = new GridBagConstraints();
		
		JLabel groupNameText = new JLabel("Group Name:");
		
		constraints.gridwidth = GROUP_NAME_TEXT_GRID_WIDTH;
		constraints.gridheight = GROUP_NAME_TEXT_GRID_HEIGHT;
		constraints.gridx = GROUP_NAME_TEXT_GRID_X;
		constraints.gridy = GROUP_NAME_TEXT_GRID_Y;
		constraints.weightx = GROUP_NAME_TEXT_WEIGHT_X;
		constraints.weighty = GROUP_NAME_TEXT_WEIGHT_Y;
		constraints.fill = GROUP_NAME_TEXT_FILL;
		
		add(groupNameText, constraints);
		
		m_groupName = new JTextField();
		
		constraints.gridwidth = GROUP_NAME_GRID_WIDTH;
		constraints.gridheight = GROUP_NAME_GRID_HEIGHT;
		constraints.gridx = GROUP_NAME_GRID_X;
		constraints.gridy = GROUP_NAME_GRID_Y;
		constraints.weightx = GROUP_NAME_WEIGHT_X;
		constraints.weighty = GROUP_NAME_WEIGHT_Y;
		constraints.fill = GROUP_NAME_FILL;
		
		add(m_groupName, constraints);
	}
	
	/**
	 * Create the buttons that are used for moving the contacts from the
	 * non members list to the members list and vice versa.
	 */
	private void createMoveMemberButtons() {
		GridBagConstraints constraints = new GridBagConstraints();
		
		JButton moveToMember = new JButton("=>");
		
		moveToMember.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//Move from non members list to members list
				Contact selectedContact = m_contactsList.getSelectedValue();
				
				m_contactsListModel.removeElement(selectedContact);
				m_groupMembersListModel.addElement(selectedContact);
			}
		});
		
		constraints.gridwidth = MOVE_TO_MEMBER_BUTTON_GRID_WIDTH;
		constraints.gridheight = MOVE_TO_MEMBER_BUTTON_GRID_HEIGHT;
		constraints.gridx = MOVE_TO_MEMBER_BUTTON_GRID_X;
		constraints.gridy = MOVE_TO_MEMBER_BUTTON_GRID_Y;
		constraints.weightx = MOVE_TO_MEMBER_BUTTON_WEIGHT_X;
		constraints.weighty = MOVE_TO_MEMBER_BUTTON_WEIGHT_Y;
		constraints.fill = MOVE_TO_MEMBER_BUTTON_FILL;
		
		add(moveToMember, constraints);
		
		JButton moveToContact = new JButton("<=");
		
		moveToContact.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//Move from members list to non members list
				Contact selectedContact = m_groupMembersList
					.getSelectedValue();
				
				m_groupMembersListModel.removeElement(selectedContact);
				m_contactsListModel.addElement(selectedContact);
			}
		});
		
		constraints.gridwidth = MOVE_TO_CONTACT_BUTTON_GRID_WIDTH;
		constraints.gridheight = MOVE_TO_CONTACT_BUTTON_GRID_HEIGHT;
		constraints.gridx = MOVE_TO_CONTACT_BUTTON_GRID_X;
		constraints.gridy = MOVE_TO_CONTACT_BUTTON_GRID_Y;
		constraints.weightx = MOVE_TO_CONTACT_BUTTON_WEIGHT_X;
		constraints.weighty = MOVE_TO_CONTACT_BUTTON_WEIGHT_Y;
		constraints.fill = MOVE_TO_CONTACT_BUTTON_FILL;
		
		add(moveToContact, constraints);
	}
	
	private static final Dimension CREATE_GROUP_FRAME_SIZE =
			new Dimension(800,500);
	
	private static final int GROUP_NAME_TEXT_GRID_WIDTH = 3;
	private static final int GROUP_NAME_TEXT_GRID_HEIGHT = 1;
	private static final int GROUP_NAME_TEXT_GRID_X = 0;
	private static final int GROUP_NAME_TEXT_GRID_Y = 0;
	private static final int GROUP_NAME_TEXT_WEIGHT_X = 0;
	private static final int GROUP_NAME_TEXT_WEIGHT_Y = 0;
	private static final int GROUP_NAME_TEXT_FILL = GridBagConstraints.NONE;
	
	private static final int GROUP_NAME_GRID_WIDTH = 3;
	private static final int GROUP_NAME_GRID_HEIGHT = 1;
	private static final int GROUP_NAME_GRID_X = 0;
	private static final int GROUP_NAME_GRID_Y = 1;
	private static final int GROUP_NAME_WEIGHT_X = 0;
	private static final int GROUP_NAME_WEIGHT_Y = 0;
	private static final int GROUP_NAME_FILL = GridBagConstraints.HORIZONTAL;
	
	private static final int CONTACTS_TEXT_GRID_WIDTH = 1;
	private static final int CONTACTS_TEXT_GRID_HEIGHT = 1;
	private static final int CONTACTS_TEXT_GRID_X = 0;
	private static final int CONTACTS_TEXT_GRID_Y = 2;
	private static final int CONTACTS_TEXT_WEIGHT_X = 0;
	private static final int CONTACTS_TEXT_WEIGHT_Y = 0;
	private static final int CONTACTS_TEXT_FILL = GridBagConstraints.NONE;
	
	private static final int CONTACTS_LIST_GRID_WIDTH = 1;
	private static final int CONTACTS_LIST_GRID_HEIGHT = 12;
	private static final int CONTACTS_LIST_GRID_X = CONTACTS_TEXT_GRID_X;
	private static final int CONTACTS_LIST_GRID_Y = CONTACTS_TEXT_GRID_Y
			+ CONTACTS_TEXT_GRID_HEIGHT;
	private static final int CONTACTS_LIST_WEIGHT_X = 1;
	private static final int CONTACTS_LIST_WEIGHT_Y = 1;
	private static final int CONTACTS_LIST_FILL = GridBagConstraints.BOTH;
	
	private static final int GROUP_MEMBERS_TEXT_GRID_WIDTH = 1;
	private static final int GROUP_MEMBERS_TEXT_GRID_HEIGHT = 1;
	private static final int GROUP_MEMBERS_TEXT_GRID_X = 2;
	private static final int GROUP_MEMBERS_TEXT_GRID_Y = 2;
	private static final int GROUP_MEMBERS_TEXT_WEIGHT_X = 0;
	private static final int GROUP_MEMBERS_TEXT_WEIGHT_Y = 0;
	private static final int GROUP_MEMBERS_TEXT_FILL = GridBagConstraints.NONE;
	
	private static final int GROUP_MEMBERS_LIST_GRID_WIDTH = 1;
	private static final int GROUP_MEMBERS_LIST_GRID_HEIGHT = 12;
	private static final int GROUP_MEMBERS_LIST_GRID_X =
			GROUP_MEMBERS_TEXT_GRID_X;
	
	private static final int GROUP_MEMBERS_LIST_GRID_Y =
			GROUP_MEMBERS_TEXT_GRID_Y + GROUP_MEMBERS_TEXT_GRID_HEIGHT;
	
	private static final int GROUP_MEMBERS_LIST_WEIGHT_X = 1;
	private static final int GROUP_MEMBERS_LIST_WEIGHT_Y = 1;
	private static final int GROUP_MEMBERS_LIST_FILL = GridBagConstraints.BOTH;
	
	private static final int MOVE_TO_MEMBER_BUTTON_GRID_WIDTH = 1;
	private static final int MOVE_TO_MEMBER_BUTTON_GRID_HEIGHT = 1;
	private static final int MOVE_TO_MEMBER_BUTTON_GRID_X = 1;
	private static final int MOVE_TO_MEMBER_BUTTON_GRID_Y = 5;
	private static final int MOVE_TO_MEMBER_BUTTON_WEIGHT_X = 1;
	private static final int MOVE_TO_MEMBER_BUTTON_WEIGHT_Y = 1;
	private static final int MOVE_TO_MEMBER_BUTTON_FILL =
			GridBagConstraints.NONE;
	
	private static final int MOVE_TO_CONTACT_BUTTON_GRID_WIDTH = 1;
	private static final int MOVE_TO_CONTACT_BUTTON_GRID_HEIGHT = 1;
	private static final int MOVE_TO_CONTACT_BUTTON_GRID_X = 1;
	private static final int MOVE_TO_CONTACT_BUTTON_GRID_Y = 6;
	private static final int MOVE_TO_CONTACT_BUTTON_WEIGHT_X = 1;
	private static final int MOVE_TO_CONTACT_BUTTON_WEIGHT_Y = 1;
	private static final int MOVE_TO_CONTACT_BUTTON_FILL =
			GridBagConstraints.NONE;
	
	private static final int CREATE_GROUP_BUTTON_GRID_WIDTH = 3;
	private static final int CREATE_GROUP_BUTTON_GRID_HEIGHT = 1;
	private static final int CREATE_GROUP_BUTTON_GRID_X = 0;
	private static final int CREATE_GROUP_BUTTON_GRID_Y = 15;
	private static final int CREATE_GROUP_BUTTON_WEIGHT_X = 1;
	private static final int CREATE_GROUP_BUTTON_WEIGHT_Y = 1;
	private static final int CREATE_GROUP_BUTTON_FILL =
			GridBagConstraints.NONE;
	
	private static final int PADDING = 10;
	
	private JTextField m_groupName;
	
	//List used for displaying the non-members list
	private DefaultListModel<Contact> m_contactsListModel;
	private JList<Contact> m_contactsList;
	
	//List used for displaying the members list
	private DefaultListModel<Contact> m_groupMembersListModel;
	private JList<Contact> m_groupMembersList;
	
	private ArrayList<CreateGroupListener> m_createGroupListeners =
			new ArrayList<CreateGroupListener>();
	
}
