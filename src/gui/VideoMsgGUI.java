package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.embed.swing.JFXPanel;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.util.Duration;

/**
 * @File	-VideoMsgGUI.java
 * @author	-Alikhan Tagybergen
 * @date	-26/03/2017
 * @see		-VideoMessage.java
 * @brief	Displays embedded video from the file
 *
 * This class uses JavaFX and Swing to implement and display a simple video
 * player from the file. It only accepts formats .mp4 and .flv (see JavaFX
 * documentation)
 *
 */
public class VideoMsgGUI {

	/**
     * This method is used to import a Sting with the location of the video
     * file, it changes it to URL format which the JavaFX Media takes.
     *
     * @Param file The location of the file
     */
    public void importFile(String file) {
        m_filename = ("file:///" + file)
        		.replace("\\", "/")
        		.replace(" ", "%20");
        
        System.out.println(m_filename);
    }
    
    /**
     * Tests the VideoMsgGui class
     *
     * Unit test of the VideoMsgGui class using the scanner to input a video
     * file.
     *
     * @param args Not used.
     */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Please enter an absolute path to the "
        		+ "video file (.mp4 or .flv format)");
        
        String testFile = in.nextLine();
        VideoMsgGUI vmsg = new VideoMsgGUI();
        vmsg.importFile(testFile);
        vmsg.runVideoGui();
    }
    
    /**
     * This method runs the whole VideoMsgGUI in a separate thread in order to
     * have concurrency with the JavaFx thread.
     */
    public void runVideoGui() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                videoGUI();
            }
        });
    }

    /**
     * Updates the playtime slider as the video plays.
     */
    public void updateTimeSlider() {
        if (m_runtimeSlider != null) {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    updateValue();
                }
            });
        }
    }
    
    /**
     * This method sets up action handlers for the controls.
     */
    private void addPlaybackActionHandler() {

        m_pauseButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                if ("||".equals(m_pauseButton.getText())) {
                    m_viewer.getMediaPlayer().pause();
                    m_pauseButton.setText(">");
                } else {
                    m_viewer.getMediaPlayer().play();
                    m_pauseButton.setText("||");
                }
            }
        });
		
		m_exitButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                m_jFrame.dispose();
                m_player.pause();
                m_player.dispose();
            }
        });
		
        m_runtimeSlider.valueProperty().addListener(
        		new InvalidationListener() {
        			
            @Override
            public void invalidated(Observable o) {

                updateRuntime();
                
                updateTimeSlider();

            }

        });

        m_volSlider.valueProperty().addListener(new InvalidationListener() {
            @Override
            public void invalidated(Observable o) {
                updateVolume();
            }

        });

    }
    
    /**
     * This method sets up the GUI controls for the video and adds them to the
     * panel.
     */
    private void controls() {
        m_controlsHolder = new HBox();
        m_pauseButton = new Button("||");
        m_controlsHolder.getChildren().add(m_pauseButton);
        BorderPane.setAlignment(m_controlsHolder, Pos.CENTER);

        m_runtimeSlider = new Slider();
        HBox.setHgrow(m_runtimeSlider, Priority.ALWAYS);
        m_runtimeSlider.setMinWidth(RUN_SLIDER_MIN_WIDTH);
        m_runtimeSlider.setMaxWidth(Double.MAX_VALUE);
        m_runtimeSlider.setMax(RUN_SLIDER_MAX_VALUE);
        m_runtimeSlider.setValue(RUN_SLIDER_INITIAL_VALUE);
        m_controlsHolder.getChildren().add(m_runtimeSlider);

        m_volLabel = new Label("Volume: ");
        m_controlsHolder.getChildren().add(m_volLabel);

        m_volSlider = new Slider();
        m_volSlider.setPrefWidth(VOL_SLIDER_PREFERRED_WIDTH);
        m_volSlider.setMax(VOL_SLIDER_MAX_VALUE);
        m_volSlider.setMaxWidth(Region.USE_PREF_SIZE);
        m_volSlider.setMinWidth(VOL_SLIDER_MIN_WIDTH);
        m_volSlider.setValue(VOL_SLIDER_INITIAL_VALUE);
        m_controlsHolder.getChildren().add(m_volSlider);
		m_exitButton = new Button("exit");
		m_controlsHolder.getChildren().add(m_exitButton);
        m_videoBP.setBottom(m_controlsHolder);
        
        addPlaybackActionHandler();
    }
    
    /**
     * This method initialises all the the JavaFX panels so the video can be
     * played and controlled.
     *
     * @param videoPanel the panel that will display the video.
     */
    private void initJFX(JFXPanel videoPanel) {

        m_videoBP = new BorderPane();
        controls();
        Scene scene = new Scene(m_videoBP, m_width, m_height);
        Media video = new Media(m_filename);
        m_player = new MediaPlayer(video);
        m_player.setAutoPlay(true);
        m_viewer = new MediaView(m_player);
        /**
         * Run the MediaPlayer to get the values for duration of video and set
         * video viewer panel resolution to a specified width and height
         * relative to resolution of the screen.
         */
        m_player.setOnReady(new Runnable() {

            public void run() {
                m_videoDuration = m_player.getMedia().getDuration();
                m_viewer.setFitWidth(m_width * VIEWER_VIDEO_SCALE);
                m_viewer.setFitHeight(m_height * VIEWER_VIDEO_SCALE);
                updateTimeSlider();
            }
        });

        m_videoBP.getChildren().add(m_viewer);
        videoPanel.setScene(scene);
        m_jFrame.pack();
    }
    
    /**
     * Update the time the video is running from.
     */
    private void updateRuntime() {
    	if (m_runtimeSlider.isValueChanging()) {
        	double durationFrac = m_runtimeSlider.getValue()
        			/ RUN_SLIDER_MAX_VALUE;
        	
            m_player.seek(m_videoDuration.multiply(durationFrac));
        }
    }
    
    /**
     * Update the slider position.
     */
    private void updateValue() {
    	Duration time = m_player.getCurrentTime();
        m_runtimeSlider.setDisable(false);
        
    	
    	if (!m_runtimeSlider.isDisabled() &&
    			!m_runtimeSlider.isValueChanging()) {
    		double mil = time.divide(m_videoDuration.toMillis()).toMillis();
    		int newDuration = (int) (mil * RUN_SLIDER_MAX_VALUE);
            m_runtimeSlider.setValue(newDuration);
            updateTimeSlider();
        }
    }
    
    /**
     * Update the volume of the video.
     */
    private void updateVolume() {
    	if (m_volSlider.isValueChanging()) {
        	double volumeFrac = m_volSlider.getValue()
        			/ VOL_SLIDER_MAX_VALUE;
        	
            m_player.setVolume(volumeFrac);
        }
    }
    
    /**
     * This method sets up the jFrame and runs the JavaFx in a thread.
     */
    private void videoGUI() {
        Dimension userScreen = Toolkit.getDefaultToolkit().getScreenSize();
        m_width = userScreen.getWidth() * VIDEO_SCALE;
        m_height = userScreen.getHeight() * VIDEO_SCALE;
        m_jFrame = new JFrame("Video Message");
        m_jFrame.setSize((int) m_width, (int) m_height);
        m_jFrame.setVisible(true);
        m_jFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        m_jFrame.getContentPane().setBackground(Color.black);
        m_videoPanel = new JFXPanel();
        m_jFrame.add(m_videoPanel);
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                initJFX(m_videoPanel);
            }
        });

    }
    
	private static final double VIDEO_SCALE = 0.90;
	private static final double VIEWER_VIDEO_SCALE = 0.95;
	private static final int RUN_SLIDER_MIN_WIDTH = 50;
	private static final int RUN_SLIDER_MAX_VALUE = 100;
	private static final int RUN_SLIDER_INITIAL_VALUE = 1;
	
	private static final int VOL_SLIDER_PREFERRED_WIDTH = 100;
	private static final int VOL_SLIDER_MAX_VALUE = 100;
	private static final int VOL_SLIDER_MIN_WIDTH = 30;
	private static final int VOL_SLIDER_INITIAL_VALUE = 100;
	
    private String m_filename;
    private Slider m_runtimeSlider;
    private HBox m_controlsHolder;
    private JFXPanel m_videoPanel;
    private BorderPane m_videoBP;
    private Slider m_volSlider;
    private Label m_volLabel;
    private Button m_pauseButton, m_exitButton;
    private MediaView m_viewer;
    private MediaPlayer m_player;
    private Duration m_videoDuration = new Duration(0);
    private JFrame m_jFrame;
    private double m_width, m_height;
}
