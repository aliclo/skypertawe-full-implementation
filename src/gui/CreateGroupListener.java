package gui;

import conversation.Chat;

/**
 * @File CreateGroupListener.java
 * @author Alistair Cloney
 * @date 12/3/2017
 * @see GUICreateGroup
 * @brief Listener that allows handling of the creation of group chats.
 */
public interface CreateGroupListener {
	
	/**
	 * Called when a new group is created.
	 * 
	 * @param groupChat The new chat created.
	 */
	public void onCreateGroup(Chat groupChat);
	
}
