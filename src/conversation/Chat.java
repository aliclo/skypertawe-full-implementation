package conversation;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import contactList.Contact;
import dataStructure.BST;
import filemanager.FileManager;
import gui.GUI;
import gui.GUIMessageChoice;
import message.Message;
import message.MessageBuilder;
import message.TextMessage;
import resources.Resources;
import user.User;

/**
 * @File	-Chat.java
 * @author	-Alistair Cloney
 * @date	-9/3/2017
 * @see		-Message.java, User.java
 * @brief	Stores all members and messages (in memory and in storage) of the
 * 			chat and handles the sending of messages.
 * 
 * Stores the chat and saves it when changes are made including the members of
 * the chat and the messages.
 * 
 * The chat is saved in a file named after its id and contains the chat's
 * name, and a new line that lists the members of the chat and then the
 * remaining lines are the messages sent to the chat, see the message class
 * for a description of how a message is formatted.
 * 
 * Also handles the displaying of the chat and manipulation of the messages in
 * the chat (e.g. adding, removing and editing).
 */
public class Chat {
	
	/**
	 * Creates the chat with the given members.
	 * 
	 * @param members The initial members of the chat.
	 */
	public Chat(ArrayList<User> members) {
		m_messages = new ArrayList<Message>();
		
		m_members = members;
		
		m_id = getNextAvailableID();
		
		createFile(m_id);
		
		saveData();
	}
	
	/**
	 * Creates the chat with the given name (used for group chats with names)
	 * and members.
	 * 
	 * @param name The initial name of the chat.
	 * @param members The initial members of the chat.
	 */
	public Chat(String name, ArrayList<User> members) {
		this.m_name = name;
		this.m_messages = new ArrayList<Message>();
		
		m_members = members;
		
		m_id = getNextAvailableID();
		
		createFile(m_id);
		
		saveData();
	}
	
	/**
	 * Creates the chat with the given name, messages, members and id
	 * (usually used for loading already existing chats).
	 * 
	 * @param name The initial name of the chat.
	 * @param messages The initial messages of the chat.
	 * @param members The initial members of the chat.
	 * @param id The initial id of the chat.
	 */
	public Chat(String name, ArrayList<Message> messages,
			ArrayList<User> members, int id) {
		
		this.m_name = name;
		this.m_messages = messages;
		
		m_members = members;
		
		m_id = id;
	}
	
	/**
	 * @return The ID of the chat.
	 */
	public int getID() {
		return m_id;
	}
	
	/**
	 * @return The message last sent.
	 */
	public Message getLastMessageSent() {
		if(m_messages.isEmpty()) {
			return null;
		}
		
		int lastMessageIndex = m_messages.size()-1;
		Message lastMessage = m_messages.get(lastMessageIndex);
		
		return lastMessage;
	}
	
	/**
	 * @param user The member of the chat.
	 * @return The last message sent by the given user.
	 */
	public Message getLastMessageSent(User user) {
		int lastMessageIndex = m_messages.size()-1;
		
		for(int i = lastMessageIndex; i >= 0; i--) {
			Message message = m_messages.get(i);
			
			if(message.getAuthor().equals(user)) {
				return message;
			}
		}
		
		return null;
	}
	
	/**
	 * @return The members that are part of the chat.
	 */
	public ArrayList<User> getMembers() {
		return m_members;
	}
	
	/**
	 * @return The message history of the chat.
	 */
	public ArrayList<Message> getMessages() {
		return m_messages;
	}
	
	/**
	 * @return The name of the chat.
	 */
	public String getName() {
		return m_name;
	}
	
	/**
	 * @param name The new name of the chat.
	 */
	public void setName(String name) {
		this.m_name = name;
	}
	
	/**
	 * @param user The user that will be used for counting the amount
	 * of messages they haven't seen.
	 * 
	 * @return The amount of new messages the user hasn't seen.
	 */
	public int getNumNewMessages(User user) {
		int num = 0;
		
		for(Message message : m_messages) {
			if(!message.hasSeenMessage(user)) {
				num++;
			}
		}
		
		return num;
	}
	
	/**
	 * Creates a new chat (usually group chats) and stores it.
	 * 
	 * @param name The initial name of the chat.
	 * @param contacts The initial members of the chat (not including the
	 * user that created the chat).
	 * 
	 * @param currentUser The user that created the chat.
	 * @return The new chat created.
	 */
	public static Chat addChat(String name, ArrayList<Contact> contacts,
			User currentUser) {
		
		boolean test = true;
		
		ArrayList<User> members = new ArrayList<User>();
		
		for(Contact contact : contacts) {
			members.add(contact.getUser());
		}
		
		members.add(currentUser);
		
		Chat newChat = new Chat(name, members);
		
		for(User member : members) {
			if(test) {
				System.out.println("Chat::addChat() " + member.getUsername()
					+ " added reference to chat");
			}
			
			member.addGroupChat(newChat);
		}
		
		if(test) {
			System.out.println("Chat::addChat() saving data");
		}
		newChat.saveData();
		
		return newChat;
	}
	
	/**
	 * @param listener The new listener that listens to this chat.
	 */
	public void addListener(ChatListener listener) {
		m_chatListeners.add(listener);
	}
	
	/**
	 * Adds a new member to the chat.
	 * 
	 * @param user The new member to add to the chat.
	 */
	public void addMember(User user) {
		m_members.add(user);
		
		for(Message message : m_messages) {
			message.addRecipient(user);
		}
	}
	
	/**
	 * Deletes this chat and the references to this chat.
	 */
	public void deleteChat() {
		boolean test = true;
		
		File file = new File(PATH + m_id + FILE_EXTENSION);
		
		if(test) {
			System.out.println("Chat::deleteChat() deleting chat at file "
				+ file.getAbsolutePath());
		}
		
		for(User member : getMembers()) {
			member.removeGroupChat(this);
			
			if(test) {
				System.out.println("Chat::deleteChat() removed chat reference"
					+ " from " + member.getUsername());
			}
		}
		
		file.delete();
	}
	
	/**
	 * Deletes the message at the given index.
	 * 
	 * @param index The index of the message to delete.
	 */
	public void deleteMessage(int index) {
		boolean test = true;
		
		Message removeMessage = m_messages.get(index);
		
		m_messages.remove(index);
		
		for(ChatListener listener : m_chatListeners) {
			listener.onMessageDeleted(this, removeMessage);
		}
		
		if(test) {
			System.out.println("Chat::deleteMessage() save data");
		}
		
		saveData();
	}
	
	/**
	 * Adds the message history (as a displayable form) into the given JPanel.
	 * 
	 * @param messageDisplay The JPanel that will contain the contents of the
	 * message history (as a displayable form).
	 * 
	 * @param currentUser The user who is reading the display.
	 */
	public void displayMessages(JPanel messageDisplay, User currentUser) {
		boolean test = true;
		
		ArrayList<Message> messages = getMessages();
		Message lastMessage = getLastMessageSent(currentUser);
		
        for(int i = 0; i < messages.size();  i++) {
        	Message message = messages.get(i);
        	
        	JPanel messageComponents = new JPanel();
        	messageComponents.setLayout(new BoxLayout(messageComponents,
        			BoxLayout.PAGE_AXIS));
        	
        	messageComponents.setBackground(MESSAGE_COLOUR);
        	
        	//Make sure the message box stretches out to the far right
        	messageComponents.add(Box.createHorizontalGlue());
        	
        	JPanel manageButtons = createMessageManageButtons(i);
        	
        	//Allow only the last message to be edited
        	if(message.equals(lastMessage)) {
        		addEditButton(manageButtons, currentUser, i);
        	}
        	
        	messageComponents.add(manageButtons);
        	
        	/**
        	 * Add fixed spacing away from the buttons at the top of the
        	 * message and the message contents itself.
        	 */
        	messageComponents.add(Box.createRigidArea(
        			CONTENTS_TO_BUTTONS_SPACING_AREA));
        	
        	JPanel messageContents = message.getContents();
        	messageContents.setBackground(MESSAGE_COLOUR);
        	messageComponents.add(messageContents);
        	
        	messageDisplay.add(messageComponents);
        	
        	//Add spacing between messages
        	messageDisplay.add(Box.createRigidArea(MESSAGE_SPACING_AREA));
        	
        	message.seenMessage(currentUser);
        }
        
        if(test) {
        	System.out.println("Chat::displayMessages() save data");
        }
        saveData();
	}
	
	/**
	 * Edit a message at the given index.
	 * 
	 * @param index The index of the message to edit.
	 * @param currentUser The user who edits the message.
	 */
	public void editMessage(int index, User currentUser) {
		boolean test = true;
		
		User author = m_messages.get(index).getAuthor();
		
		new GUIMessageChoice(currentUser, author, this) {
			@Override
			public void onMessageSend(Message message, Chat chat,
					User chooser) {
				
				for(User user : m_members) {
					message.addRecipient(user);
				}
				
				//Set the editor of the message to be the one modifying it
				message.setEdit(chooser);
				
				m_messages.set(index, message);
				
				/**
				 * Notify all listeners to handle the event of the message
				 * being modified.
				 */
				for(ChatListener listener : m_chatListeners) {
					listener.onMessageEdited(chat, message);
				}
				
				if(test) {
					System.out.println("Chat::editMessage() save data");
				}
				saveData();
			}
		};
	}
	
	/**
	 * @param object The object to find if there is a match.
	 * @return If this chat object and the given object are equal.
	 */
	@Override
	public boolean equals(Object object) {
		if(object == null) {
			return false;
		}
		
		if(object.getClass() != this.getClass()) {
			return false;
		}
		
		//Both chats are equal if both have the same IDs
		Chat otherChat = (Chat) object;
		if(otherChat.getID() == this.getID()) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Loads the chat given the id of it along with the existing
	 * users of Skypertawe.
	 * 
	 * @param id The id of the chat to load.
	 * @param users The existing users of Skypertawe.
	 * @return The loaded chat.
	 */
	public static Chat loadChat(int id, BST users) {
		String name = null;
		ArrayList<Message> messages = new ArrayList<Message>();
		ArrayList<User> members = new ArrayList<User>();
		
		//Find the chat file from the given id
		File file = new File(PATH + id + FILE_EXTENSION);
		
    	try {
			Scanner in = new Scanner(file);
			
			if(in.hasNextLine()) {
				//Load chat name
				name = in.nextLine();
				
				//Load members of the chat
				members = loadMembers(in.nextLine(), users);
				
				//Load all the messages
				while(in.hasNextLine()) {
					messages.add(Message.loadMessage(in.nextLine(), users));
				}
			}
			
			in.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
    	
    	return new Chat(name, messages, members, id);
	}
	
	/**
	 * Test unit for testing all methods belonging to the Chat class.
	 * 
	 * Integration testing with classes: BST, User, Chat
	 * 
	 * @param args Not used.
	 */
	public static void main(String[] args) {
		System.out.println("Chat::main() BEGIN");
		
		idTest();
		
		//Create test users
		BST users = new BST();
		
		User userA = new User().withUsername("User A")
				.withBirthDate("10/11/2009");
		
		User userB = new User().withUsername("User B")
				.withBirthDate("05/04/2000");
		
		User userC = new User().withUsername("User C")
				.withBirthDate("25/07/2002");
		
		User userD = new User().withUsername("User D")
				.withBirthDate("22/02/1998");
		
		users.insertUser(userA);
		users.insertUser(userB);
		users.insertUser(userC);
		users.insertUser(userD);
		
		User currentUser = userA;
		
		addChatTest(currentUser, userB, userC);
		
		int addedChatID = getNextAvailableID()-1;
		System.out.println("Getting chat with ID " + addedChatID);
		Chat addedChat = loadChat(addedChatID, users);
		
		membersTest(addedChat, userD);
		
		sendMessagesTest(addedChat, currentUser, userA, userB);
		
		System.out.println("Removed first message");
		addedChat.deleteMessage(0);
		
		System.out.println("Num new messages user A "
				+ addedChat.getNumNewMessages(userA));
		
		System.out.println("Num new messages user B "
				+ addedChat.getNumNewMessages(userB));
		
		System.out.println("Num new messages user C "
				+ addedChat.getNumNewMessages(userC));
		
		listenersTest(addedChat, userD);
		
		addedChat.saveData();
		
		equalityTest(addedChat);
		
		membersReaderTest(users);
		
		deleteChatTest(userA);
		
		guiTest(addedChat, currentUser);
		
		System.out.println("Chat::main() END");
	}
	
	/**
	 * Remove all listeners that are listening to the chat.
	 */
	public void removeAllListeners() {
		m_chatListeners.clear();
	}
	
	/**
	 * Remove a particular listener that's currently listening to the chat.
	 * 
	 * @param listener The listener to remove.
	 */
	public void removeListener(ChatListener listener) {
		m_chatListeners.remove(listener);
	}
	
	/**
	 * Save the data of the chat as a text file.
	 */
	public void saveData() {
		boolean test = true;
		
		//Find the file of the chat using its id
		File file = new File(PATH + m_id + FILE_EXTENSION);
        
		if(test) {
			System.out.println("Chat:saveData() saving data to file "
				+ file.getAbsolutePath());
		}
		
        try {
            FileWriter fileWriter = new FileWriter(file);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            PrintWriter out = new PrintWriter(bufferedWriter);
            
            //Store the name of the chat
            out.println(m_name);
            
            //Print the members and their amount of new messages
            for(User user : getMembers()) {
            	out.print(user.getUsername());
            	
            	out.print(MEMBERS_SEPARATOR);
            }
            
            out.println();
            
            //Store the messages
            for(Message message : m_messages) {
                out.println(message.getSaveData());
            }
            
            out.close();
            bufferedWriter.close();
            fileWriter.close();
        }
        catch (IOException e) {
            System.out.println("ERROR");
        }
	}
	
	/**
	 * Send the given message to the chat so that all members of the chat can
	 * receive it.
	 * 
	 * @param message The message to send.
	 */
	public void sendMessage(Message message) {
		boolean test = true;
		
		//All members apart from the author are going to receive the message.
		for(User member : getMembers()) {
        	if(member != message.getAuthor()) {
        		message.addRecipient(member);
        	}
        }
		
		m_messages.add(message);
		
		//Notify all listeners about the message being sent
		for(ChatListener listener : m_chatListeners) {
			listener.onMessageSent(this, message);
		}
		
		if(test) {
			System.out.println("Chat::sendMessage() save data");
		}
		
		saveData();
	}
	
	/**
	 * Test if the chat is added properly.
	 * 
	 * Integration testing with class: Contact
	 * 
	 * @param currentUser The user that will create the chat.
	 * @param userA Going to be a member of the chat.
	 * @param userB Going to be a member of the chat.
	 */
	private static void addChatTest(User currentUser, User userA, User userB) {
		ArrayList<Contact> addedUsers = new ArrayList<Contact>();
		
		Contact contactA = new Contact(userA);
		Contact contactB = new Contact(userB);
		
		addedUsers.add(contactA);
		addedUsers.add(contactB);
		
		addChat("Test Chat", addedUsers, currentUser);
	}
	
	/**
	 * Add the edit message button to the panel (which would contain
	 * the message contents).
	 * 
	 * @param panel The panel to add the button to.
	 * @param currentUser The user that could potentially edit the message.
	 * @param messageIndex The index of the message that could potentially
	 * be modified.
	 */
	private void addEditButton(JPanel panel, User currentUser,
			int messageIndex) {
		
		JButton editButton;
    	
    	ImageIcon editImage = Resources.getEditIcon();
    	
    	//Handle the problem of if the resource doesn't exist
    	if(editImage == null) {
    		editButton = new JButton("Edit");
    	} else {
    		editButton = GUI.createIconButton(editImage);
    	}
    	
    	editButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(messageIndex == m_messages.size()-1) {
					editMessage(messageIndex, currentUser);
				} else {
					confirmEditMessage(messageIndex, currentUser);
				}
			}
    	});
    	
    	panel.add(editButton);
	}
	
	/**
	 * Test to check if the getters and setters work properly.
	 * 
	 * @param chat The chat to test on.
	 */
	private static void chatDetailsTest(Chat chat) {
		System.out.println("Chat ID: " + chat.getID());
		System.out.println("Chat name: " + chat.getName());
		
		System.out.println("Messages:");
		for(Message message : chat.getMessages()) {
			System.out.println(message.getDescription());
		}
		
		chat.setName("A New Chat Name");
		System.out.println("New chat name: " + chat.getName());
	}
	
	/**
	 * Notifies the user that the message has been replied and allows the
	 * option of whether they want to still continue with editing the message.
	 * 
	 * @param messageIndex The index of the message to edit.
	 * @param currentUser The user whose editing it.
	 */
	private void confirmEditMessage(int messageIndex, User currentUser) {
		String message = "Someone seems to have replied to this "
				+ "message.\nAre you sure you want to modify it?";
		
		int result = JOptionPane.showConfirmDialog(null, message,
				"Already been replied", JOptionPane.YES_NO_OPTION);
		
		if(result == JOptionPane.YES_OPTION) {
			editMessage(messageIndex, currentUser);
		}
	}
	
	/**
	 * Create a chat file from the given chat ID.
	 * 
	 * @param id The id of the chat.
	 */
	private static void createFile(int id) {
		FileManager.createDirectoryWhenNonExistent(new File(PATH));
		
		File newFile = new File(PATH + id + FILE_EXTENSION);
		
		try {
			newFile.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Create the buttons that allow deletion of a
	 * message located with the given message index (edit button is not added
	 * because only the last message should be modified).
	 * 
	 * @param messageIndex The index of the message that could be affected.
	 * @return The JPanel containing the buttons.
	 */
	private JPanel createMessageManageButtons(int messageIndex) {
    	JPanel messageManageButtonsPanel = new JPanel();
    	messageManageButtonsPanel.setLayout(new BoxLayout(
    			messageManageButtonsPanel, BoxLayout.LINE_AXIS));
    	
    	//Make sure all buttons start from the left side of the message
    	messageManageButtonsPanel.setAlignmentX(JComponent.LEFT_ALIGNMENT);
    	
    	messageManageButtonsPanel.setBackground(MESSAGE_COLOUR);
		
		JButton deleteButton;
    	
    	ImageIcon deleteImage = Resources.getBinIcon();
    	
    	//Handle the problem of if the resource doesn't exist
    	if(deleteImage == null) {
    		deleteButton = new JButton("Delete");
    	} else {
    		deleteButton = GUI.createIconButton(deleteImage);
    	}
    	
    	deleteButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				deleteMessage(messageIndex);
			}
    	});
    	
    	messageManageButtonsPanel.add(deleteButton);
    	
    	return messageManageButtonsPanel;
	}
	
	/**
	 * Tests the method that deletes the chat and references to the chat.
	 * 
	 * @param user The user that will be used for this test.
	 */
	private static void deleteChatTest(User user) {
		ArrayList<User> members = new ArrayList<User>();
		members.add(user);
		
		Chat chat = new Chat("Removable Chat", members);
		user.addGroupChat(chat);
		
		System.out.println("User group chats");
		for(Chat groupChat : user.getGroupChats()) {
			System.out.println(groupChat.getName());
		}
		
		File chatFile = new File(PATH + chat.getID() + FILE_EXTENSION);
		
		System.out.println("Chat file exists: " + chatFile.exists());
		
		System.out.println("Delete chat");
		chat.deleteChat();
		
		System.out.println("User group chats");
		for(Chat groupChat : user.getGroupChats()) {
			System.out.println(groupChat.getName());
		}
		
		System.out.println("Chat file exists: " + chatFile.exists());
	}
	
	/**
	 * Test the equals method.
	 * 
	 * @param chat The chat to test on.
	 */
	private static void equalityTest(Chat chat) {
		ArrayList<User> members = new ArrayList<User>();
		
		Chat differentChat = new Chat(members);
		
		System.out.println("Different chats equal: "
			+ chat.equals(differentChat));
		
		System.out.println("Same chats equal: " + chat.equals(chat));
		
		System.out.println("Same chats equal: "
			+ differentChat.equals(differentChat));
	}
	
	/**
	 * @return The next new ID that can be used for a new chat.
	 */
	private static int getNextAvailableID() {
		boolean test = true;
		File chatFolder = new File(PATH);
		
		/**
		 * Calculate the largest ID value so that the increment of that
		 * value would be unique.
		 */
		int highestValue = -1;
		
		if(chatFolder.exists()) {
			for(File file : chatFolder.listFiles()) {
				int nameLength = file.getName().length()
					-FILE_EXTENSION.length();
				
				String nameWithoutExtension = file.getName().substring(0,
					nameLength);
				
				/**
				 * The files are named by their numeric IDs, so if the
				 * extension is removed, the ID can be converted to an Integer.
				 */
				int fileID = Integer.parseInt(nameWithoutExtension);
				
				if(fileID > highestValue) {
					highestValue = fileID;
				}
			}
		}
		
		int nextID = highestValue+1;
		
		if(test) {
			System.out.println("Chat::getNextAvailableID() Next ID is: "
				+ nextID);
		}
		
		return nextID;
	}
	
	/**
	 * Test the display of the messages.
	 * 
	 * Integration testing with class: Chat
	 * 
	 * @param chat The chat to test on.
	 * @param currentUser The user that would read the messages.
	 */
	private static void guiTest(Chat chat, User currentUser) {
		JFrame testFrame = new JFrame("Chat Test");
		testFrame.setSize(TEST_WINDOW_SIZE);
		
		JPanel contentsPanel = (JPanel) testFrame.getContentPane();
		contentsPanel.setLayout(new BoxLayout(contentsPanel,
				BoxLayout.PAGE_AXIS));
		
		JPanel messageDisplay = new JPanel();
		chat.displayMessages(messageDisplay, currentUser);
		contentsPanel.add(messageDisplay);
		
		//Create the message manage buttons for the first message
		JPanel messageManagement = chat.createMessageManageButtons(0);
		
		//Add edit button for the first message
		chat.addEditButton(messageManagement, currentUser, 0);
		
		contentsPanel.add(messageManagement);
		
		testFrame.setVisible(true);
	}
	
	/**
	 * Test if the ID functions work properly.
	 */
	private static void idTest() {
		int id = getNextAvailableID();
		
		System.out.println("Next available ID: " + id);
		
		System.out.println("Create file with new id");
		
		createFile(id);
		
		id = getNextAvailableID();
		
		System.out.println("Next available ID: " + id);
		
		createFile(TEST_INCORRECT_ID_INPUT_ATTEMPT);
		
		id = getNextAvailableID();
		
		System.out.println("Next available ID: " + id);
	}
	
	/**
	 * Test if the method that gets the last message of the chat
	 * returns the correct message.
	 * 
	 * Integration testing with classes: Chat, Message
	 * 
	 * @param chat The chat to test on.
	 * @param actualLastMessage The expected result.
	 */
	private static void lastMessageSentTest(Chat chat,
			Message actualLastMessage) {
		
		Message lastMessageSent = chat.getLastMessageSent();
		
		System.out.println("Text of last message sent: "
				+ lastMessageSent.getText());
		
		System.out.println("Was last message: "
				+ (actualLastMessage == lastMessageSent));
	}
	
	/**
	 * Test if the method that gets the last message sent by the given
	 * user returns the correct message.
	 * 
	 * Integration testing with classes: Chat, Message
	 * 
	 * @param user The user that is used to find the last message
	 * sent by the user.
	 * 
	 * @param chat The chat to test on.
	 * @param actualLastMessage The expected result.
	 */
	private static void lastMessageSentTest(User user, Chat chat,
			Message actualLastMessage) {
		
		Message lastMessageSent = chat.getLastMessageSent(user);
		
		System.out.println("Text of last message sent: "
				+ lastMessageSent.getText());
		
		System.out.println("Was last message: "
				+ (actualLastMessage == lastMessageSent));
	}
	
	/**
	 * Tests the listeners to check that they are correctly notified to
	 * messages being deleted, modified and sent.
	 * 
	 * Integration testing with classes: ChatListener, MessageBuilder,
	 * TextMessage
	 * 
	 * @param chat The chat to test on.
	 * @param sender The user who would send the messages.
	 */
	private static void listenersTest(Chat chat, User sender) {
		chat.addListener(new ListenerTestDescribeMessage());
		
		chat.addListener(new TestListener("Test Listener 2"));
		
		ChatListener chatListener3 = new TestListener("Test Listener 3");
		
		chat.addListener(chatListener3);
		
		MessageBuilder messageBuild = new MessageBuilder(sender)
				.withDate("17/03/2017")
				.withTime("14:25")
				.withText("Test listeners");
		
		TextMessage textMessage = new TextMessage(messageBuild);
		
		chat.sendMessage(textMessage);
		chat.removeListener(chatListener3);
		
		//Delete first message
		chat.deleteMessage(0);
		chat.removeAllListeners();
		
		//Edit first message
		chat.editMessage(0, sender);
		
		chat.confirmEditMessage(0, sender);
	}
	
	/**
	 * From the given data and the existing users from Skypertawe this method
	 * loads all the members of the chat into an array list.
	 * 
	 * @param data The data to read from.
	 * @param users The existing users of Skypertawe.
	 * @return The list of members of the chat.
	 */
	private static ArrayList<User> loadMembers(String data, BST users) {
		boolean test = true;
		
		ArrayList<User> members = new ArrayList<User>();
		
		Scanner in = new Scanner(data);
		in.useDelimiter(MEMBERS_SEPARATOR);
		
		while(in.hasNext()) {
			String memberUsername = in.next();
			
			User user = users.getProfileRoot(memberUsername);
			
			if(test) {
				System.out.println("Chat::loadMembers() Found username: "
					+ memberUsername + ", exists: " + (user != null));
			}
			
			members.add(user);
		}
		
		in.close();
		
		return members;
	}
	
	/**
	 * Test if the members data is being read properly.
	 * 
	 * Integration testing with class: User
	 * 
	 * @param users The test users, where there exists users with
	 * the usernames "User A" and "User C".
	 */
	private static void membersReaderTest(BST users) {
		String data = "User A" + MEMBERS_SEPARATOR + "User C";
		ArrayList<User> members = loadMembers(data, users);
		
		System.out.println("Member details: ");
		for(User member : members) {
			System.out.println("Member username: " + member.getUsername());
			System.out.println("Member birth date: " + member.getBirthDate());
		}
	}
	
	/**
	 * Test the list of members.
	 * 
	 * Integration testing with class: User
	 * 
	 * @param chat The chat to test on.
	 * @param newMember The new member to add.
	 */
	private static void membersTest(Chat chat, User newMember) {
		System.out.println("Members:");
		
		for(User user : chat.getMembers()) {
			System.out.println(user.getUsername());
		}
		
		System.out.println("Added " + newMember.getUsername() + " as member");
		chat.addMember(newMember);
		
		System.out.println("Members:");
		
		for(User user : chat.getMembers()) {
			System.out.println(user.getUsername());
		}
	}
	
	/**
	 * Test to check if messages are being sent correctly.
	 * 
	 * Integration testing with classes: MessageBuilder, TextMessage
	 * 
	 * @param chat The chat to test on.
	 * @param currentUser The user that will be logged on.
	 * @param userA A user that is a member of the given chat.
	 * @param userB A user that is a member of the given chat.
	 */
	private static void sendMessagesTest(Chat chat, User currentUser,
			User userA, User userB) {
		
		MessageBuilder messageBuild = new MessageBuilder(userA)
				.withDate("15/03/2017")
				.withTime("18:36")
				.withText("Hello World");
		
		TextMessage textMessageUserA = new TextMessage(messageBuild);
		
		chat.sendMessage(textMessageUserA);
		
		messageBuild = new MessageBuilder(userB)
				.withDate("15/03/2017")
				.withTime("18:42")
				.withText("Goodbye");
		
		TextMessage textMessageUserB = new TextMessage(messageBuild);
		
		chat.sendMessage(textMessageUserB);
		
		chatDetailsTest(chat);
		
		lastMessageSentTest(chat, textMessageUserB);
		lastMessageSentTest(currentUser, chat, textMessageUserA);
	}
	
	/**
	 * @File	-Chat.java
	 * @author	-Alistair Cloney
	 * @date	-17\3\2017
	 * @see		-ChatListener.java
	 * @brief	Used to test the chat listener.
	 */
	private static class ListenerTestDescribeMessage implements ChatListener {
		
		/**
		 * @param chat The chat the message was deleted from.
		 * @param deletedMessage The message that was deleted.
		 */
		@Override
		public void onMessageDeleted(Chat chat, Message deletedMessage) {
			System.out.println("Message from "
				+ deletedMessage.getAuthor().getUsername() + " deleted");
			
			for(Message message : chat.getMessages()) {
				System.out.println(message.getText());
			}
		}
		
		/**
		 * @param chat The chat the message was edited from.
		 * @param editedMessage The message that was edited.
		 */
		@Override
		public void onMessageEdited(Chat chat, Message editedMessage) {
			System.out.println("Message from "
				+ editedMessage.getAuthor().getUsername() + " edited");
			
			for(Message message : chat.getMessages()) {
				System.out.println(message.getText());
			}
		}
		
		/**
		 * @param chat The chat the message was sent to.
		 * @param sentMessage The message that was sent.
		 */
		@Override
		public void onMessageSent(Chat chat, Message sentMessage) {
			System.out.println("Message from "
				+ sentMessage.getAuthor().getUsername() + " sent");
			
			for(Message message : chat.getMessages()) {
				System.out.println(message.getText());
			}
		}
	}
	
	/**
	 * @File	-Chat.java
	 * @author	-Alistair Cloney
	 * @date	-17\3\2017
	 * @see		-ChatListener.java
	 * @brief	Used to test the chat listener.
	 */
	private static class TestListener implements ChatListener {
		
		/**
		 * @param name The name of the listener that will be printed within
		 * its handling methods.
		 */
		public TestListener(String name) {
			m_name = name;
		}
		
		/**
		 * @param chat The chat the message was deleted from.
		 * @param message The message that was deleted.
		 */
		@Override
		public void onMessageDeleted(Chat chat, Message message) {
			System.out.println(m_name + ": message deleted");
		}
		
		/**
		 * @param chat The chat the message was edited from.
		 * @param message The message that was edited.
		 */
		@Override
		public void onMessageEdited(Chat chat, Message message) {
			System.out.println(m_name + ": message edited");
		}
		
		/**
		 * @param chat The chat the message was sent to.
		 * @param message The message that was sent.
		 */
		@Override
		public void onMessageSent(Chat chat, Message message) {
			System.out.println(m_name + ": message sent");
		}
		
		private String m_name;
	}
	
	//Path to chat files location
	private static final String PATH = "chat/";
	
	private static final int UNIT_SEPARATOR_ASCII_NUM = 31;
	private static final String MEMBERS_SEPARATOR = Character.toString(
			((char) UNIT_SEPARATOR_ASCII_NUM));
	
	//File extension for each chat file
	private static final String FILE_EXTENSION = ".txt";
	private static final Color MESSAGE_COLOUR = new Color(153, 204, 255);
	
	//Spacing of each message
	private static final int MESSAGE_SPACING = 10;
	private static final Dimension MESSAGE_SPACING_AREA = new Dimension(0,
			MESSAGE_SPACING);
	
	//Spacing between the message buttons and the contents of the message
	private static final int CONTENTS_TO_BUTTONS_SPACING = 5;
	private static final Dimension CONTENTS_TO_BUTTONS_SPACING_AREA =
			new Dimension(0, CONTENTS_TO_BUTTONS_SPACING);
	
	private static final Dimension TEST_WINDOW_SIZE = new Dimension(200,300);
	private static final int TEST_INCORRECT_ID_INPUT_ATTEMPT = -200;
	
	private int m_id;
	private String m_name;
	private ArrayList<Message> m_messages;
	private ArrayList<User> m_members;
	
	private ArrayList<ChatListener> m_chatListeners =
			new ArrayList<ChatListener>();
	
}
