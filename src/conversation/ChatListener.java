package conversation;

import message.Message;

/**
 * @File	-ChatListener.java
 * @author	-Alistair Cloney
 * @date	-13/3/2017
 * @see		-ChatListener.java
 * @brief	Allows other classes to listen to when a message
 * 			is either being deleted, sent or edited.
 */
public interface ChatListener {
	
	/**
	 * Called when a message has been deleted.
	 * 
	 * @param chat The chat the message was deleted from.
	 * @param message The message that was deleted.
	 */
	public void onMessageDeleted(Chat chat, Message message);
	
	/**
	 * Called when a message has been edited.
	 * 
	 * @param chat The chat the message was edited from.
	 * @param message The message that was edited.
	 */
	public void onMessageEdited(Chat chat, Message message);
	
	/**
	 * Called when a message has been sent.
	 * 
	 * @param chat The chat the message was sent to.
	 * @param message The message that was sent.
	 */
	public void onMessageSent(Chat chat, Message message);
	
}
