package conversation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

import dataStructure.BST;
import filemanager.FileManager;
import user.User;

/**
 * @File	-ChatIO.java
 * @author	-Alistair Cloney
 * @date	-16/3/2017
 * @see		-Chat.java, Message.java
 * @brief	Handles the saving and loading of chats.
 * 
 * The chat is saved in a file named after its id and contains the chat's
 * name, and a new line that lists the members of the chat and then the
 * remaining lines are the messages sent to the chat, see the message class
 * for a description of how a message is formatted.
 * 
 * The list of group chats of a user is stored in a text file named after the
 * user and contains lines of the chat id's that reference the chat the user
 * is a member of.
 */
public class ChatIO {
	
	/**
	 * Load the group chats that the given user is a member of.
	 * 
	 * @param user The user whose group chats are loaded.
	 * @param users The existing users of Skypertawe.
	 */
	public static void loadGroupChat(User user, BST users) {
		ArrayList<Chat> groupChats = new ArrayList<Chat>();
		
		/**
		 * Find the location of the file of group chat references
		 * for the given user.
		 */
		File file = new File(GROUP_CHATS_PATH + user.getUsername() + ".txt");
		
		FileManager.createFileWhenNonExistent(file);
		
		try {
			Scanner in = new Scanner(file);
			
			while(in.hasNextLine()) {
				groupChats.add(loadGroupChatData(users, in.nextLine()));
			}
			
			in.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		user.setGroupChats(groupChats);
	}
	
	/**
	 * Creates a group chat from the given data.
	 * 
	 * @param users The existing users of Skypertawe.
	 * @param data The data to read.
	 * @return The loaded group chat.
	 */
	public static Chat loadGroupChatData(BST users, String data) {
		int chatID = Integer.parseInt(data);
		
		return Chat.loadChat(chatID, users);
	}
	
	/**
	 * Tests the methods of the class ChatIO.
	 * 
	 * Integration testing with classes: User, BST, Chat
	 * 
	 * @param args Not used.
	 */
	public static void main(String[] args) {
		//Create test users
		User userTest = new User().withUsername("Test");
		User userAlice = new User().withUsername("Alice");
		User userBob = new User().withUsername("Bob");
		User userJake = new User().withUsername("Jake");
		User userJess = new User().withUsername("Jess");
		
		BST users = new BST();
		users.insertUser(userTest);
		users.insertUser(userAlice);
		users.insertUser(userBob);
		users.insertUser(userJake);
		users.insertUser(userJess);
		
		ArrayList<User> members = new ArrayList<User>();
		members.add(userTest);
		members.add(userAlice);
		members.add(userJake);
		
		Chat groupChatA = new Chat("Group Chat A", members);
		
		//Tests on saving of group chats
		
		System.out.println("Saving " + userBob.getUsername()
			+ "'s reference to chat " + groupChatA.getName());
		
		saveGroupChat(userBob, groupChatA);
		
		members = new ArrayList<User>();
		members.add(userTest);
		members.add(userJake);
		members.add(userJess);
		
		Chat groupChatB = new Chat("Group Chat B", members);
		
		System.out.println("Assigning chats " + groupChatA.getName() + " and "
			+ groupChatB.getName() + " to user " + userTest.getUsername());
		userTest.addGroupChat(groupChatA);
		userTest.addGroupChat(groupChatB);
		
		System.out.println("Saving " + userTest.getUsername()
			+ "'s references to assigned chats");
		saveGroupChats(userTest);
		
		System.out.println("Loading " + groupChatA.getName() + " chat");
		String data = Integer.toString(groupChatA.getID());
		
		//Tests of loading group chats
		
		Chat loadedChatA = loadGroupChatData(users, data);
		
		System.out.println("Load successful: "
			+ (loadedChatA.getName().equals(groupChatA.getName())));
		
		System.out.println("Current " + userTest.getUsername()
		+ " user's group chats");

		User loadChatUser = new User().withUsername(userTest.getUsername());
		
		for(Chat groupChat : loadChatUser.getGroupChats()) {
			System.out.println(groupChat.getName());
		}
		
		System.out.println("Loading chats of " + loadChatUser.getUsername());
		
		loadGroupChat(loadChatUser, users);
		
		System.out.println("Current " + userTest.getUsername()
			+ " user's group chats");
		
		for(Chat groupChat : loadChatUser.getGroupChats()) {
			System.out.println(groupChat.getName());
		}
	}
	
	/**
	 * Appends to the save file a reference to the given group chat that the
	 * given user is a member of.
	 * 
	 * @param user The user which will have an added reference
	 * to the given group chat.
	 * 
	 * @param groupChat The group chat that is being referenced.
	 */
	public static void saveGroupChat(User user, Chat groupChat) {
		/**
		 * Find the location of the file of group chat references
		 * for the given user.
		 */
		File file = new File(GROUP_CHATS_PATH + user.getUsername() +
				".txt");
		
		FileManager.createFileWhenNonExistent(file);
		
		try {
			//FileWriter will append to the data already in the text file
			FileWriter fileWriter = new FileWriter(file, true);
			PrintWriter out = new PrintWriter(fileWriter);
			
			//The reference is the id of the chat
			out.println(groupChat.getID());
			
			out.close();
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Save references of all group chats the user is currently a member of.
	 * 
	 * @param user The user whose group chat references will be saved.
	 */
	public static void saveGroupChats(User user) {
		/**
		 * Find the location of the file of group chat references
		 * for the given user.
		 */
		File file = new File(GROUP_CHATS_PATH + user.getUsername() +
				".txt");
		
		FileManager.createFileWhenNonExistent(file);
		
		try {
			PrintWriter out = new PrintWriter(file);
			
			for(Chat groupChat : user.getGroupChats()) {
				out.println(groupChat.getID());
			}
			
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//Path to group chat references
	private static final String GROUP_CHATS_PATH = "Group Chats/";
	
}
