package drawingEnvironment;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * @File DrawingToolBar.java
 * @author Jonathan Hepplewhite
 * @date 9 Dec 2016
 * @brief Panel that holds the tools and colours available to the user
 * \n \n
 * DrawingToolBar is a panel that holds all the tools and colours that the user
 * can use when drawing on the DrawingArea
 */
public class DrawingToolBar extends JPanel implements ActionListener
{
	/**
	 * @return The current element
	 */
	public static int getCurrentElement()
	{
		return m_CurrentElement;
	}
	
	/**
	 * Set the currently selected element
	 * @param element The element selected
	 */
	public static void setCurrentElement(int element)
	{
		m_CurrentElement = element;
	}
	
	/**
	 * Method that handles the mouse clicks
	 * @param The action that was triggered
	 */
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		switch(e.getActionCommand())
		{
		case TRACE_NAME: 	//Set the current element to 0 (ParticleTrace)
							setCurrentElement(PARTICLE_TRACE_VALUE);
							m_ToolTip.setText(TRACE_SELECTED);
							break;
		case LINE_NAME:		//Set the current element to 1 (Line)
							setCurrentElement(LINE_VALUE);
							m_ToolTip.setText(LINE_SELECTED);
							break;
		default:			break;
		}
	}
	
	/**
	 * Constructor that creates a DrawingToolBar
	 */
	public DrawingToolBar()
	{
		//Setup the buttons
		initialiseButtons();
		
		//Create a new colour panel and set its size
		ColourPanel colourPanel = new ColourPanel();
		colourPanel.setPreferredSize(new Dimension(COLOUR_PANEL_WIDTH, 
												COLOUR_PANEL_HEIGHT));
		//Add this colour panel to this toolbar
		this.add(colourPanel);
		//Add each of the initialized buttons to this toolbar
		this.add(m_TraceSelect);
		this.add(m_LineSelect);
		this.add(m_ToolTip);
		
		//Set the size and colour of this toolbar and make it visible
		this.setPreferredSize(new Dimension(TOOLBAR_WIDTH, TOOLBAR_HEIGHT));
		this.setBackground(new Color(BAR_RED_RGB,BAR_GREEN_RGB,BAR_BLUE_RGB));
		this.setVisible(true);
	}
	
	/**
	 * Sets up the button
	 */
	private void initialiseButtons()
	{
		//Setup the trace button
		m_TraceSelect = new JButton(TRACE_NAME);
		m_TraceSelect.setActionCommand(TRACE_NAME);
		m_TraceSelect.addActionListener(this);		
		
		//Setup the line button
		m_LineSelect = new JButton(LINE_NAME);
		m_LineSelect.setActionCommand(LINE_NAME);
		m_LineSelect.addActionListener(this);
	}
	
	/**
	 * Method to test DrawingToolBar class
	 * @param args Command line arguments
	 */
	public static void main(String[] args)
	{
		System.out.println("DrawingToolBar::main() BEGIN");
		System.out.println("Set the drawing tool to line (1)");
		DrawingToolBar.setCurrentElement(1);
		System.out.println("The current drawing tool is: " + DrawingToolBar.getCurrentElement());
		JFrame frame = new JFrame();
		DrawingToolBar testToolBar = new DrawingToolBar();
		frame.add(testToolBar);
		frame.setSize(500, 75);
		frame.setVisible(true);
		
		System.out.println("DrawingToolBar::main() END");
		frame.dispose();
	}
	
	/** Integer that represents which tool is currently being used */
	private static int m_CurrentElement;
	/** Value corresponding to particle trace */
	private final int PARTICLE_TRACE_VALUE = 0;
	/** Value corresponding to line */
	private final int LINE_VALUE = 1;
	
	/** The width of the colour panel */
	private final int COLOUR_PANEL_WIDTH = 200;
	/** The height of the colour panel */
	private final int COLOUR_PANEL_HEIGHT = 40;
	/** The total width of the toolbar */
	private final int TOOLBAR_WIDTH = 500;
	/** The total height of the toolbar */
	private final int TOOLBAR_HEIGHT = 50;
	
	/** Blue channel for tool bar */
	private final int BAR_BLUE_RGB = 255;
	/** Green channel for tool bar */
	private final int BAR_GREEN_RGB = 200;
	/** Red channel for tool bar */
	private final int BAR_RED_RGB = 150;
	/** Create a reference to the trace select button */
	private JButton m_TraceSelect;
	/** Create a reference to the line select button */
	private JButton m_LineSelect;

	/** Set the name and action command of the trace button */
	private final String TRACE_NAME = "Trace";
	/** Set the name and action command of the line button */
	private final String LINE_NAME = "Line";

	/** Create a tooltip string for when trace is selected */
	private final String TRACE_SELECTED = "Trace Selected";
	/** Create a tooltip string for when line is selected */
	private final String LINE_SELECTED = "Line Selected";
	/** Create a reference to the tooltip label */
	private JLabel m_ToolTip = new JLabel(TRACE_SELECTED);

	
	/**
	 * @File ColourPanel
	 * @author Jonathan Hepplewhite
	 * @date 22 March 2017
	 * @brief Creates a panel that holds the colours the user can use
	 *
	 * ColourPanel class creates a panel with each of the colours available
	 * to the user. It also handles the input when the user clicks on a 
	 * button to change the colour.
	 */
	private class ColourPanel extends JPanel implements ActionListener
	{
		/**
		 * Method than handles the button presses for colours
		 * @param e The event that was triggered
		 */
		@Override
		public void actionPerformed(ActionEvent e)
		{
			switch (e.getActionCommand())
			{
				case(RED):		DrawingElement.setColour(Color.RED);
								break;
				case(BLACK):	DrawingElement.setColour(Color.BLACK);	
								break;
				case(WHITE):	DrawingElement.setColour(Color.WHITE);
								break;
				case(GREEN):	DrawingElement.setColour(Color.GREEN);
								break;
				case(YELLOW):	DrawingElement.setColour(Color.YELLOW);
								break;
				case(PINK):		DrawingElement.setColour(Color.PINK);
								break;
				case(BLUE):		DrawingElement.setColour(Color.BLUE);
								break;
				case(CYAN):		DrawingElement.setColour(Color.CYAN);
								break;
				case(GREY):		DrawingElement.setColour(Color.GRAY);
								break;
				case(ORANGE):	DrawingElement.setColour(Color.ORANGE);
								break;
				default:		DrawingElement.setColour(Color.BLACK);
								break;
			}
			
		}
		
		/**
		 * Constructor that sets up the ColourPanel
		 */
		public ColourPanel()
		{
			//Set the panel to have a grid layout
			this.setLayout(new GridLayout(COLOUR_ROWS,COLOUR_COLUMNS));
			//Setup each colour button
			JButton blackButton = buttonSetup(Color.BLACK, BLACK);
			JButton redButton = buttonSetup(Color.RED, RED);
			JButton whiteButton = buttonSetup(Color.WHITE, WHITE);
			JButton greenButton = buttonSetup(Color.GREEN, GREEN);
			JButton yellowButton = buttonSetup(Color.YELLOW, YELLOW);
			JButton pinkButton = buttonSetup(Color.PINK, PINK);
			JButton blueButton = buttonSetup(Color.BLUE, BLUE);
			JButton cyanButton = buttonSetup(Color.CYAN, CYAN);
			JButton greyButton = buttonSetup(Color.GRAY, GREY);
			JButton orangeButton = buttonSetup(Color.ORANGE, ORANGE);
			
			//Add each of the created buttons to the panel
			this.add(blackButton);
			this.add(whiteButton);
			this.add(redButton);
			this.add(greenButton);
			this.add(yellowButton);
			this.add(pinkButton);
			this.add(blueButton);
			this.add(cyanButton);
			this.add(greyButton);
			this.add(orangeButton);
			repaint();
		}
		
		/**
		 * Method that paints the ColourPanel
		 * @param Reference to this panel's graphics
		 */
		@Override
		public void paintComponent(Graphics g)
		{
			super.paintComponent(g);
		}
		
		/**
		 * Universal method that sets up each button
		 * @param colour The colour of the button
		 * @param actionCommand The action command for the button
		 * @return Returns the JButton created
		 */
		private JButton buttonSetup(Color colour, String actionCommand)
		{
			JButton b = new JButton();
			b.setBackground(colour);
			b.setOpaque(true);
			b.setBorderPainted(false);
			b.setVisible(true);
			b.addActionListener(this);
			b.setActionCommand(actionCommand);
			return b;
		}

		
		/** String for the name and action command for the red button */
		private final String RED = "red";
		/** String for the name and action command for the black button */
		private final String BLACK = "black";
		/** String for the name and action command for the white button */
		private final String WHITE = "white";
		/** String for the name and action command for the green button */
		private final String GREEN = "green";
		/** String for the name and action command for the yellow button */
		private final String YELLOW = "yellow";
		/** String for the name and action command for the pink button */
		private final String PINK = "pink";
		/** String for the name and action command for the blue button */
		private final String BLUE = "blue";
		/** String for the name and action command for the cyan button */
		private final String CYAN = "cyan";
		/** String for the name and action command for the grey button */
		private final String GREY = "grey";
		/** String for the name and action command for the orange button */
		private final String ORANGE = "orange";
		/** Number of rows the panel has */
		private final int COLOUR_ROWS = 2;
		/** Number of columns the panel has */
		private final int COLOUR_COLUMNS = 5;
	}

}