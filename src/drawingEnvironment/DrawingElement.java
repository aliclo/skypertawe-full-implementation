package drawingEnvironment;

import java.awt.Color;
import java.awt.Graphics;

/**
 * @File DrawingElement.java
 * @author Jonathan Hepplewhite
 * @date 5 Dec 2016
 * @brief Abstract class that specifies objects that can be drawn to screen
 * \n \n
 * Abstract class that specifies information about a DrawingElement
 * that can drawn to screen.
 */
public abstract class DrawingElement 
{
	/**
	 * @return The current colour
	 */
	public static Color getColour()
	{
		return m_Colour;
	}
	
	/**
	 * @return The current drawing size
	 */
	public static int getDrawSize()
	{
		return m_DrawSize;
	}
	
	/**
	 * @return The start x position
	 */
	public int getStartX()
	{
		return m_X1;
	}
	
	/**
	 * @return The start y position
	 */
	public int getStartY()
	{
		return m_Y1;
	}
	
	/**
	 * Set the current colour of the DrawingElement
	 * @param c The colour to set to
	 */
	public static void setColour(Color c)
	{
		m_Colour = c;
	}
	
	/**
	 * Set the drawing size
	 * @param size
	 */
	public static void setDrawSize(int size)
	{
		m_DrawSize = size;
	}
	
	/**
	 * @param The start x position
	 */
	public void setStartX(int x1)
	{
		this.m_X1 = x1;
	}
	
	/**
	 * @param The start y position
	 */
	public void setStartY(int y1)
	{
		this.m_Y1 = y1;
	}
	
	/**
	 * Constructor for a DrawingElement
	 * @param x1 The start x position of the element
	 * @param y1 The start y position of the element
	 */
	public DrawingElement(int x1, int y1)
	{
		setStartX(x1);
		setStartY(y1);
		setDrawSize(INITIAL_DRAWSIZE);
	}
	
	/**
	 * Abstract method that the child classes implement to 
	 * draw themselves to screen
	 * @param g The graphics reference for the DrawingElement
	 */
	public abstract void draw(Graphics g);
	
	
	/** The initial draw size of the element */
	private final int INITIAL_DRAWSIZE = 5;
	/** The colour of the element */
	private static Color m_Colour;
	/** The current draw size of the element */
	private static int m_DrawSize;
	/** The start x position */
	private int m_X1;
	/** The start y position */
	private int m_Y1;
}
