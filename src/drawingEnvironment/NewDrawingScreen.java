package drawingEnvironment;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.JPanel;

/**
 * @File NewDrawingScreen.java
 * @author Jonathan Hepplewhite
 * @date 17 March 2017
 * @brief Panel class that holds the GUI for creating a new drawing
 * \n\n
 * NewDrawingScreen is a panel class that holds the GUI that allows 
 * the user to create a new image file.
 */
public class NewDrawingScreen extends JPanel implements ActionListener
{
	/**
	 * @return The ArrayList file names
	 */
	public static ArrayList<String> getFileNames()
	{
		return m_FileNames;
	}
	
	/**
	 * Set the list of current file names
	 */
	public static void setFileNames()
	{
		m_FileNames = ReferenceFileReader.getFileNames();
	}
	
	/**
	 * Method that handles the mouse clicks on buttons
	 * @param The event that was triggered
	 */
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		switch(e.getActionCommand())
		{
		case ADD_BUTTON_NAME:		//For each of the selected items in the first list
									for(int i = 0; i < 
											m_ListOfContacts.getSelectedValuesList().size(); i++)
									{
										//If they're not in the second list
										if(!m_Model2.contains(
												m_ListOfContacts.getSelectedValuesList().get(i)))
										{
											//Add them to the second list
											m_Model2.addElement(
												m_ListOfContacts.getSelectedValuesList().get(i));
										}
									}
									break;
		case REMOVE_BUTTON_NAME:	//For each selected object in the second list
									for(Object o : m_ListOfSelectedContacts.getSelectedValuesList())
									{
										//Remove the object
										m_Model2.removeElement(o);
									}
									break;
		case CREATE_BUTTON_NAME:	boolean valid = true;
									//For each file name that currently exists
									for(String s : getFileNames())
									{
										//If the input matches an existing name
										if(m_FileNameInput.getText().equals(s))
										{
											if(valid)
											{
												//Show popup with issue
												JOptionPane.showMessageDialog(this, 
															FILE_NAME_INVALID, 
															FILE_ERROR_POPUP_TITLE, 
															JOptionPane.ERROR_MESSAGE);
											}
											//Make the click invalid
											valid = false;
										}
									}
									//If the input is empty or has not been changed
									if(m_FileNameInput.getText().equals(DEFAULT_FILENAME) || 
													m_FileNameInput.getText().equals(""))
									{
										//Show error
										if(valid)
										{
											JOptionPane.showMessageDialog(this, 
															FILE_NAME_INVALID, 
															FILE_ERROR_POPUP_TITLE, 
															JOptionPane.ERROR_MESSAGE);
										}
										valid = false;
									}
									//If input is valid
									else if (valid == true)
									{
										/* Create a new String array and add the selected 
										  users to it*/
										String[] selectedContacts = new String[m_Model2.size()];
										for(int i = 0; i < selectedContacts.length; i++)
										{
											selectedContacts[i] = (String)m_Model2.getElementAt(i);
										}
										//Pass the input file name to the reference file reader
										ReferenceFileReader.setFileName(
														m_FileNameInput.getText().toString());
										/*Pass the users who can access the file to
										 the reference file reader */
										ReferenceFileReader.setUsers(selectedContacts);
										//Pass the user to the reference file reader
										ReferenceFileReader.setUserName(m_User);
										//Initiate the creation of a new drawing in the drawing environment
										m_DrawingEnvironment.setupDrawingArea(m_FileNameInput.getText(), true);
										
										DrawingEnvironment.setMessage(true);
									}
									break;
		default:					break;
		}	
	}
	
	/**
	 * Constructor for the NewDrawingPane
	 * @param user The current user
	 * @param contacts The current user's contacts
	 * @param de Reference to the DrawingEnvironment
	 */
	public NewDrawingScreen(String user, ArrayList<String> contacts, DrawingEnvironment de) 
	{		
		this.m_User = user;
		m_ContactList = contacts;
		this.m_DrawingEnvironment = de;
		this.setLayout(null);
		setFileNames();
		
		//Initialize the GUI
		initialiseLists();
		initialiseButtons();
		initialiseTextField();

		//Add each of the components to the panel
		this.add(m_ScrollPane1);
		this.add(m_ScrollPane2);
		this.add(m_AddContact);
		this.add(m_RemoveContact);
		this.add(m_FileNameInput);
		this.add(m_NewDrawing);
	}
	
	/**
	 * Setup each of the buttons on the GUI
	 */
	private void initialiseButtons()
	{
		/*Setup the add contact button, its position, size,
		   action command and listener and make it visible */
		m_AddContact = new JButton(ADD_BUTTON_NAME);
		m_AddContact.setBounds(ADD_BOUNDS_X, ADD_REMOVE_BOUNDS_Y, 
							ADD_REMOVE_BUTTON_WIDTH, BUTTON_HEIGHT);
		m_AddContact.addActionListener(this);
		m_AddContact.setActionCommand(ADD_BUTTON_NAME);
		m_AddContact.setVisible(true);
		
		/*Setup the remove contact button, its position, size,
		   action command and listener and make it visible */
		m_RemoveContact = new JButton(REMOVE_BUTTON_NAME);
		m_RemoveContact.setBounds(REMOVE_BOUNDS_X, ADD_REMOVE_BOUNDS_Y, 
							ADD_REMOVE_BUTTON_WIDTH, BUTTON_HEIGHT);
		m_RemoveContact.addActionListener(this);
		m_RemoveContact.setActionCommand(REMOVE_BUTTON_NAME);
		m_RemoveContact.setVisible(true);
		
		/*Setup the create button, its position, size,
		   action command and listener and make it visible */
		m_NewDrawing = new JButton(CREATE_BUTTON_NAME);
		m_NewDrawing.setBounds(CREATE_BOUNDS_X, CREATE_BOUNDS_Y, 
							CREATE_BUTTON_WIDTH, BUTTON_HEIGHT);
		m_NewDrawing.setActionCommand(CREATE_BUTTON_NAME);
		m_NewDrawing.addActionListener(this);

	}
	
	/**
	 * Initialize the lists
	 */
	private void initialiseLists()
	{
		DefaultListModel model = new DefaultListModel();
		//For each of the user's contacts, add them to the list
		for(int i = 0; i < m_ContactList.size(); i++)
		{
			model.addElement(m_ContactList.get(i));
		}
		
		m_ListOfContacts  = new JList(model);
		m_ListOfContacts.setName("List1");
		//Create a scrollpane using the list and set its size and position
		m_ScrollPane1 = new JScrollPane(m_ListOfContacts);
		m_ScrollPane1.setBounds(SCROLLPANE_BOUNDS_X, 0, 
								SCROLLPANE_WIDTH, SCROLLPANE_HEIGHT);
		m_ScrollPane1.setAutoscrolls(true);
		//Let the user select multiple contacts at a time
		m_ListOfContacts.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		m_Model2 = new DefaultListModel();
		m_ListOfSelectedContacts = new JList(m_Model2);
		m_ListOfSelectedContacts.setName("List2");
		m_ListOfSelectedContacts.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		m_ScrollPane2 = new JScrollPane(m_ListOfSelectedContacts);
		m_ScrollPane2.setBounds(SCROLLPANE_BOUNDS_X, SCROLLPANE2_BOUNDS_Y, 
								SCROLLPANE_WIDTH, SCROLLPANE_HEIGHT);
	}
	
	
	/**
	 * Initialize the text field for entering the file name
	 */
	private void initialiseTextField()
	{
		//Initialize the text field with default text, position and make it visible
		m_FileNameInput = new JTextField(DEFAULT_FILENAME);
		m_FileNameInput.setBounds(TEXTFIELD_BOUNDS_X, TEXTFIELD_BOUNDS_Y, 
								TEXTFIELD_WIDTH, TEXTFIELD_HEIGHT);
		m_FileNameInput.setVisible(true);
	}
	
	/**
	 * Method for testing NewDrawingScreen class
	 * For full testing see DrawingEnvironment.java
	 * @param args Command line arguments
	 */
	public static void main(String[] args)
	{
		System.out.println("NewDrawingScreen::main() BEGIN");
		System.out.println("Setting and retrieving a list of all file names");
		NewDrawingScreen.setFileNames();
		ArrayList<String> fileNames = getFileNames();
		System.out.println("Displaying file names, if any are present");
		if(!fileNames.isEmpty())
		{
			for(int i = 0; i < fileNames.size(); i++)
			{
				System.out.println(fileNames.get(i));
			}
		}
		System.out.println("NewDrawingScreen::main() END");
	}
	
	/** Both scrollpanes X bounds */
	private final int SCROLLPANE_BOUNDS_X = 15;
	/** Both scrollpanes widths */
	private final int SCROLLPANE_WIDTH = 450;
	/** Both scrollpanes height */
	private final int SCROLLPANE_HEIGHT = 200;
	/** The second scrollpane's Y bounds */
	private final int SCROLLPANE2_BOUNDS_Y = 230;
	/** Remove and add button width */
	private final int ADD_REMOVE_BUTTON_WIDTH = 150;
	/** All buttons height */
	private final int BUTTON_HEIGHT = 25;
	/** Create button width */
	private final int CREATE_BUTTON_WIDTH = 100;
	
	/** Add button name and action command */
	private final String ADD_BUTTON_NAME = "Add Contact";
	/** Remove button name and action command */
	private final String REMOVE_BUTTON_NAME = "Remove Contact";
	/** Create button name and action command */
	private final String CREATE_BUTTON_NAME = "Create";
	
	/** Invalid file name error message */
	private final String FILE_NAME_INVALID = "File name is taken or invalid. "
											+ "Please try a different name.";
	/** File error popup title */
	private final String FILE_ERROR_POPUP_TITLE = "File Name Error"; 
	/** The default text field text */
	private final String DEFAULT_FILENAME = "Enter File Name";
	
	/** Add button X bounds */
	private final int ADD_BOUNDS_X = 75;
	/** Remove button X bounds */
	private final int REMOVE_BOUNDS_X = 275;
	/** Add and remove button Y bounds */
	private final int ADD_REMOVE_BOUNDS_Y = 205;
	/** Create button X bounds */
	private final int CREATE_BOUNDS_X = 380;
	/** Create button y bounds */
	private final int CREATE_BOUNDS_Y =	450;
	
	/** Textfield X bounds */
	private final int TEXTFIELD_BOUNDS_X = 10;
	/** Textfield Y bounds */
	private final int TEXTFIELD_BOUNDS_Y = 450;
	/** Textfield width */
	private final int TEXTFIELD_WIDTH = 360;
	/** Textfield height */
	private final int TEXTFIELD_HEIGHT = 25;
	
	/** Reference to remove contact button */
	private JButton m_RemoveContact;
	/** Reference to add contact button */
	private JButton m_AddContact;
	/** Reference to create button  */
	private JButton m_NewDrawing;
	/** Reference to current user */
	private String m_User;
	/** Current user's contact list */
	private ArrayList<String> m_ContactList;
	/** Reference to file name text field */
	private JTextField m_FileNameInput;
	/** ArrayList which holds all file names */
	private static ArrayList<String> m_FileNames;
	/** Reference to list2 model */
	private DefaultListModel m_Model2;
	/** List that holds all the user's contacts */
	private JList m_ListOfContacts;
	/** List that holds the selected users */
	private JList m_ListOfSelectedContacts;
	/** Scrollpane that displays all the user's contacts */
	private JScrollPane m_ScrollPane1;
	/** Scrollpane that displays the list of selected users */
	private JScrollPane m_ScrollPane2;
	/** Reference to the DrawingEnvironment */	
	private DrawingEnvironment m_DrawingEnvironment;
}
