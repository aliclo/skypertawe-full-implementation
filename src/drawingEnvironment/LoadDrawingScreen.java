package drawingEnvironment;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 * @File LoadDrawingScreen.java
 * @author Jonathan Hepplewhite
 * @date 18 March 2017
 * @Brief Panel class that holds the GUI for loading a saved drawing
 * \n \n
 * LoadDrawingScreen is a panel class that holds the GUI that allows 
 * the user to load a saved image file.
 */
public class LoadDrawingScreen extends JPanel implements ActionListener
{
	/**
	 * @return The ArrayList of file names
	 */
	public static ArrayList<String> getFileNames()
	{
		return m_FileNames;
	}
	
	/**
	 * Set the file names
	 */
	public static void setFileNames()
	{
		m_FileNames = ReferenceFileReader.getFileNames();
	}
	
	/**
	 * Method that handles the mouse clicks
	 * @param The event that was triggered
	 */
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		switch(e.getActionCommand())
		{
		
			case LOAD_NAME:	//If a file in the list is selected
							if(m_FileList.getSelectedValue() != null)
							{
								//Create a File reference to the image
								File file = new File(FILE_FOLDER + 
										m_FileList.getSelectedValue().toString() + FILE_EXTENSION);
								//If the file does not exist
								if(!file.exists())
								{
									//Display a pane with a message displaying the issue
									JOptionPane.showMessageDialog(this,
												IMAGE_NOT_FOUND, NOT_FOUND_POPUP_TITLE, 
												JOptionPane.ERROR_MESSAGE);
								}
								else
								{
									//Pass the file name to the file reader for use when saving
									ReferenceFileReader.setFileName(m_FileList.getSelectedValue().toString());
									//Switch the message to be displayed when clicking the frame close button
									DrawingEnvironment.setMessage(true);
									m_DrawingEnvironment.setupDrawingArea(m_FileList.getSelectedValue().toString(), false);
								}
							}
							break;
		case DELETE_NAME:	//If an item in the list is selected
							if(!m_FileList.isSelectionEmpty())
							{
								//Read the choice the user makes from the popup
								int choice = JOptionPane.showConfirmDialog(this, 
													CONFIRM_DELETION_MESSAGE, 
													DELETION_POPUP_TITLE, JOptionPane.YES_NO_OPTION);
								File file = new File(FILE_FOLDER + 
													m_FileList.getSelectedValue().toString() 
													+ FILE_EXTENSION);
								//If they choose yes
								if(choice == 0)
								{
									//Delete the file if it exists
									if(file.exists())
									{
										file.delete();
									}
									//Call FileDeleter to delete the file
									FileDeleter.deleteFile(m_FileList.getSelectedValue().toString());
									//Update the names of file on the system
									setFileNames();
									//Remove the item deleted from the list
									m_Model.removeElement(m_FileList.getSelectedValue());
								}
							}
							break;	
		default: 			break;
		}
	}
	
	/**
	 * Constructor that creates a new LoadDrawingScreen object
	 * @param user The current user
	 * @param contacts The current user's contacts
	 * @param de The a reference to the DrawingEnvironment
	 */
	public LoadDrawingScreen(String user, ArrayList<String> contacts, DrawingEnvironment de) 
	{
		this.m_DrawingEnvironment = de;
		this.m_User = user;
		this.setLayout(null);
		this.setName("LDS");
		setFileNames();
		//Initialize list and buttons
		initialiseList();
		initialiseButtons();
		//Add all scrollpane and buttons
		this.add(m_ScrollPane);
		this.add(m_LoadDrawing);
		this.add(m_DeleteDrawing);
	}
	
	/**
	 * Initialize the list that contains the image files
	 */
	private void initialiseList()
	{
		m_Model = new DefaultListModel();
		//Populate the list with all the names of the image files
		for(int i = 0; i< getFileNames().size(); i++)
		{
			if(ReferenceFileReader.checkUserValid(m_User, getFileNames().get(i)))
			{
				m_Model.addElement(getFileNames().get(i));
			}
		}
		
		m_FileList = new JList(m_Model);
		m_FileList.setName("List");
		//Create a scroll pane with the list and set its size
		m_ScrollPane = new JScrollPane(m_FileList);
		m_ScrollPane.setBounds(SCROLLPANE_BOUNDS_X, SCROLLPANE_BOUNDS_Y, 
						SCROLLPANE_WIDTH, SCROLLPANE_HEIGHT);
	}
	
	/**
	 * Intialize the buttons on the GUI
	 */
	private void initialiseButtons()
	{
		//Initialize and setup the load button
		m_LoadDrawing = new JButton(LOAD_NAME);
		m_LoadDrawing.setActionCommand(LOAD_NAME);
		m_LoadDrawing.addActionListener(this);
		m_LoadDrawing.setBounds(LOAD_BUTTON_BOUNDS_X, BUTTON_BOUNDS_Y, 
								BUTTON_WIDTH, BUTTON_HEIGHT);
		
		//Initialize and setup the delete button
		m_DeleteDrawing = new JButton(DELETE_NAME);
		m_DeleteDrawing.setActionCommand(DELETE_NAME);
		m_DeleteDrawing.addActionListener(this);
		m_DeleteDrawing.setBounds(DELETE_BUTTON_BOUNDS_X, BUTTON_BOUNDS_Y, 
								BUTTON_WIDTH, BUTTON_HEIGHT);
	}
	
	/**
	 * Method for testing LoadDrawingScreen class
	 * For full testing see DrawingEnvironment.java
	 * @param args Command line arguments
	 */
	public static void main(String[] args)
	{
		System.out.println("LoadDrawingScreen::main() BEGIN");
		System.out.println("Setting and retrieving a list of all file names");
		LoadDrawingScreen.setFileNames();
		ArrayList<String> fileNames = getFileNames();
		System.out.println("Displaying file names, if any are present");
		if(!fileNames.isEmpty())
		{
			for(int i = 0; i < fileNames.size(); i++)
			{
				System.out.println(fileNames.get(i));
			}
		}
		System.out.println("LoadDrawingScreen::main() END");
	}
	
	/** The name and action command for load button */
	private final String LOAD_NAME = "Load";
	/** The name and action command for delete button */
	private final String DELETE_NAME = "Delete";
	/** The location of the image and reference file */
	private final String FILE_FOLDER = "drawings/";
	/** Popup title */
	private final String DELETION_POPUP_TITLE = "Confirm file deletion";
	/** Deletion popup message */
	private final String CONFIRM_DELETION_MESSAGE = 
				"Are you sure you would like to delete this file?";
	/** Image file not found error message */
	private final String IMAGE_NOT_FOUND = "Image File Not Found";
	/** Image file not found popup title */
	private final String NOT_FOUND_POPUP_TITLE = "Image File Error";
	/** File extension name */
	private final String FILE_EXTENSION = ".png";
	
	/** Width of each button */
	private final int BUTTON_WIDTH = 100;
	/** Height of each button */
	private final int BUTTON_HEIGHT = 25;
	/** X bounds of the load button */
	private final int LOAD_BUTTON_BOUNDS_X = 100;
	/** X bounds of the delete button */
	private final int DELETE_BUTTON_BOUNDS_X = 300;
	/** Y bounds of both button */
	private final int BUTTON_BOUNDS_Y = 250;
	
	/** ScrollPane X bounds */
	private final int SCROLLPANE_BOUNDS_X = 15;
	/** ScrollPane Y bounds */
	private final int SCROLLPANE_BOUNDS_Y = 25;
	/** ScrollPane width */
	private final int SCROLLPANE_WIDTH = 450;
	/** ScrollPane height */
	private final int SCROLLPANE_HEIGHT = 200;
	
	/** A reference to the list model that holds the items */
	private DefaultListModel m_Model;
	/** The current user's name */
	private String m_User;
	/** The scrollpane that holds the list of image files */
	private JScrollPane m_ScrollPane;
	/** Create load button */
	private JButton m_LoadDrawing;
	/** Create delete button */
	private JButton m_DeleteDrawing;
	/** Create ArrayList which will store the image file names */
	private static ArrayList<String> m_FileNames;
	/** Reference to the DrawingEnvironment `*/
	private DrawingEnvironment m_DrawingEnvironment;
	/** List that holds the displays the image file names */
	private JList m_FileList;
}