package drawingEnvironment;

import java.awt.Color;
import java.awt.Graphics;

/**
 * @File Particletrace.java
 * @author Jonathan Hepplewhite
 * @date 5 Dec 2016
 * @see DrawingElement.java	
 * @brief ParticleTrace is an object that can be drawn to the screen. 
 * \n \n
 * ParticleTrace is an object that can be drawn to the screen. 
 * It inherits from DrawingElement
 */
public class Particle extends DrawingElement
{	
	/**
	 * Draw the ParticleTrace to the screen
	 */
	public void draw(Graphics g)
	{
		g.setColor(m_Colour);
		g.fillOval(getStartX(), getStartY(), 
					getDrawSize(), getDrawSize());
	}
	
	/**
	 * Constructor that creates a new ParticleTrace
	 * @param x1 The start X position
	 * @param y1 The start Y position
	 */
	public Particle(int x1, int y1)
	{
		super(x1, y1);
		m_Colour = super.getColour();
	}
	
	/**
	 * Method for testing Particle class
	 * @param args Command line arguments
	 */
	public static void main(String[] args)
	{
		System.out.println("Particle::main() BEGIN");
		Particle testParticle = new Particle(10,100);
		System.out.println("Particle x position: " + testParticle.getStartX());
		System.out.println("Particle y position: " + testParticle.getStartY());
		System.out.println("Set particle colour to blue");
		testParticle.setColour(Color.BLUE);
		System.out.println("The colour of the particle is: " + testParticle.getColour());
		System.out.println("The particle draw size is: " + testParticle.getDrawSize());
		System.out.println("Particle::main() BEGIN");
	}
	
	/** The colour of the ParticleTrace */ 
	private Color m_Colour;
}
