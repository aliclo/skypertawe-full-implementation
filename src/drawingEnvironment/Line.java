package drawingEnvironment;

import java.awt.Color;
import java.awt.Graphics;

/**
 * @File Line.java
 * @author Jonathan Hepplewhite
 * @date 5 Dec 2016
 * @see DrawingElement.java
 * @brief Line is an object that can be drawn to the screen.
 * \n \n
 * Line is an object that can be drawn to the screen. 
 * It inherits from DrawingElement
 */
public class Line extends DrawingElement
{
	/**
	 * @return The lines end X position
	 */
	public int getEndX()
	{
		return m_EndX;
	}
	
	/**
	 * @return The lines end Y position
	 */
	public int getEndY()
	{
		return m_EndY;
	}
	
	/**
	 * Set the lines end X position
	 * @param x The X position
	 */
	public void setEndX(int x)
	{
		m_EndX = x;
	}
	
	/**
	 * Set the lines end Y position
	 * @param x The Y position
	 */
	public void setEndY(int y)
	{
		m_EndY = y;
	}
	
	/**
	 * Draw the line to screen
	 */
	public void draw(Graphics g)
	{
		g.setColor(m_Colour);
		g.drawLine(getStartX(), getStartY(), getEndX(), getEndY());	
	}
	
	/**
	 * Constructor that creates a Line object
	 * @param x1 The start X position of the Line
	 * @param y1 The start Y position of the Line
	 */
	public Line(int x1, int y1)
	{
		super(x1,y1);
		m_Colour = super.getColour(); 
	}
	
	/**
	 * Method to test Line class
	 * @param args Command line arguments
	 */
	public static void main(String[] args)
	{
		System.out.println("Line::main() BEGIN");
		Line testLine = new Line(10,100);
		testLine.setEndX(50);
		testLine.setEndY(250);
		System.out.println("Line start x: " + testLine.getStartX());
		System.out.println("Line end x: " + testLine.getEndX());
		System.out.println("Line start y: " + testLine.getStartY());
		System.out.println("Line end y: " + testLine.getEndY());
		testLine.setStartX(24);
		testLine.setStartY(255);
		System.out.println("Line start x: " + testLine.getStartX());
		System.out.println("Line start y: " + testLine.getStartY());
		System.out.println("Set particle colour to blue");
		testLine.setColour(Color.RED);
		System.out.println("The colour of the line is: " + testLine.getColour());
		System.out.println("The line draw size is: " + testLine.getDrawSize());
		System.out.println("Line::main() END");
	}
		
	/** The colour of the line */
	private Color m_Colour;
	/** The end X coordinate of the line */
	private int m_EndX;
	/** The end Y coordinate of the line */
	private int m_EndY;
}
