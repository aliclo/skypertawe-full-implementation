package drawingEnvironment;

import java.awt.Component;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.*;

import junit.extensions.abbot.ComponentTestFixture;
import abbot.finder.matchers.ClassMatcher;
import abbot.finder.matchers.JMenuItemMatcher;
import abbot.finder.matchers.JMenuMatcher;
import abbot.tester.*;
import org.junit.Test;

import javax.swing.JList;

/**
 * @File DrawingEnvironmentTest.java
 * @author Jonathan Hepplewhite
 * @date 26 March 2017
 * @Brief Class used for testing the DrawingEnvironment
 * \n \n
 * This class tests the GUI of the DrawingEnvironment, NewDrawingScreen, 
 * LoadDrawingScreen.
 */
public class DrawingEnvironmentTest extends ComponentTestFixture
{	
    /**
     * Clicks on the load button
     */
    public void clickLoad()
    {
        try {
        	button = (JButton)getFinder().find(new ClassMatcher(JButton.class) {
                    public boolean matches(Component c) {
                        return c instanceof JButton && ((JButton)c).
                        					getActionCommand().equals("Load");
                    }
                });
        } catch (Exception e) {
            e.printStackTrace();
        }
        bt = new JButtonTester();	 
        bt.actionClick(button);
    }
    
    /**
     * Clicks on a contact that has been added
     */
    public void clickOnAddedContact()
    {
        try {
        	list = (JList)getFinder().find(new ClassMatcher(JList.class) {
                    public boolean matches(Component c) {
                        return c instanceof JList && ((JList)c).getName().equals("List2");
                    }
                });
        } catch (Exception e) {
            e.printStackTrace();
        }
        bt = new JButtonTester();	 
        bt.actionClick(list);
    }
    
    /**
     * Clicks on a contact to add from the list of contacts
     */
    public void clickOnNewContact()
    {
        try {
        	list = (JList)getFinder().find(new ClassMatcher(JList.class) {
                    public boolean matches(Component c) {
                        return c instanceof JList && ((JList)c).getName().equals("List1");
                    }
                });
        } catch (Exception e) {
            e.printStackTrace();
        }
        int[] ints = {0,1};
        list.setSelectedIndices(ints);
    }
    
    /**
     * Provides a filename and then clicks on the create new drawing button
     * @param filename The filename to use for the file
     */
    public void createNewDrawing(String filename)
    {
        try {
        	textField = (JTextField)getFinder().find(new ClassMatcher(JTextField.class) {
                    public boolean matches(Component c) {
                        return c instanceof JTextField;
                    }
                });
        } catch (Exception e) {
            e.printStackTrace();
        }
    	
        textField.setText(filename);
        
        try {
        	button = (JButton)getFinder().find(new ClassMatcher(JButton.class) {
                    public boolean matches(Component c) {
                        return c instanceof JButton && ((JButton)c).
                        					getActionCommand().equals("Create");
                    }
                });
        } catch (Exception e) {
            e.printStackTrace();
        }
        bt = new JButtonTester();	 
        bt.actionClick(button);
    }
    
    /**
     * Clicks on add contact button
     */
    public void pressAddContact()
    {
        try {
        	button = (JButton)getFinder().find(new ClassMatcher(JButton.class) {
                    public boolean matches(Component c) {
                        return c instanceof JButton && ((JButton)c).
                        					getActionCommand().equals("Add Contact");
                    }
                });
        } catch (Exception e) {
            e.printStackTrace();
        }
        bt = new JButtonTester();	 
        bt.actionClick(button);
    }
    
    /**
     * Clicks on the JMenuBar "File" button
     */
    public void pressFile()
	{
        try {
        	menu = (JMenu)getFinder().find(new JMenuMatcher("File") {
                    public boolean matches(Component c) {
                        return c instanceof JMenu && ((JMenu)c).
                        					getActionCommand().equals("File");
                    }
                });
        } catch (Exception e) {
            e.printStackTrace();
        }
        mt = new JMenuItemTester();	 
        mt.actionClick(menu);
  
	}
    
    /**
     * Clicks on the JMenuItem "Load"
     */
    public void pressLoad()
    {
        try {
        	menuItem = (JMenuItem)getFinder().find(new JMenuItemMatcher("Load") {
                    public boolean matches(Component c) {
                        return c instanceof JMenuItem && ((JMenuItem)c).
                        					getActionCommand().equals("Load");
                    }
                });
        } catch (Exception e) {
            e.printStackTrace();
        }
        bt = new JButtonTester();	 
        bt.actionClick(menuItem);
    }
    
    /**
     * Clicks on the JMenuBarItem "New"
     */
    public void pressNew()
	{
        try {
        	menuItem = (JMenuItem)getFinder().find(new JMenuItemMatcher("New") {
                    public boolean matches(Component c) {
                        return c instanceof JMenuItem && ((JMenuItem)c).
                        						getActionCommand().equals("New");
                    }
                });
        } catch (Exception e) {
            e.printStackTrace();
        }
        bt = new JButtonTester();	 
        bt.actionClick(menuItem);
	}
    
    /**
     * Clicks on the remove contact button
     */
    public void pressRemoveContact()
    {
        try {
        	button = (JButton)getFinder().find(new ClassMatcher(JButton.class) {
                    public boolean matches(Component c) {
                        return c instanceof JButton && ((JButton)c).
                        					getActionCommand().equals("Remove Contact");
                    }
                });
        } catch (Exception e) {
            e.printStackTrace();
        }
        bt = new JButtonTester();	 
        bt.actionClick(button);
    }
   
    /**
     * Clicks on the JMenuBarItem "Save" 
     */
    public void pressSave()
    {
        try {
        	menuItem = (JMenuItem)getFinder().find(new JMenuItemMatcher("Save") {
                    public boolean matches(Component c) {
                        return c instanceof JMenuItem && ((JMenuItem)c).
                        							getActionCommand().equals("Save");
                    }
                });
        } catch (Exception e) {
            e.printStackTrace();
        }
        bt = new JButtonTester();	 
        bt.actionClick(menuItem);
    }
    
    /**
     * Select the first file from the list of saved files
     */
    public void selectFile()
    {
        try {
        	list = (JList)getFinder().find(new ClassMatcher(JList.class) {
                    public boolean matches(Component c) {
                        return c instanceof JList && ((JList)c).
                        						getName().equals("List");
                    }
                });
        } catch (Exception e) {
            e.printStackTrace();
        }
        list.setSelectedIndex(0);
    }
    
    /**
     * Method that tests the drawing environment
     * WARNING: deletes all save files on completion
     */
    @Test
    public void test()
    {
    	File f = new File("drawings");
    	abbot.tester.Robot.setAutoDelay(200);
    	System.out.println("Open a new DrawingEnvironment");
		String user = "Bob";
		ArrayList<String> contactList = new ArrayList<String>(Arrays.asList(
								"Bob", "Bill", "Frank", "Fred", "Jimmy"));
    	new DrawingEnvironment(user, contactList);
    	System.out.println("Click on file");
    	pressFile();
    	System.out.println("Click on new");
    	pressNew();
    	System.out.println("Click on a contact");
    	clickOnNewContact();
    	System.out.println("Click on \"Add Contact\"");
    	pressAddContact();
    	System.out.println("Click on added contact");
    	clickOnAddedContact();
    	System.out.println("Click on \"Remove Contact\"");
    	pressRemoveContact();
    	System.out.println("Create the new drawing");
    	createNewDrawing("TestFile");
    	System.out.println("Click on file");
    	pressFile();
    	System.out.println("Save the image");
    	pressSave();
    	System.out.println("Click on file");
    	pressFile();
    	System.out.println("Click on load");
    	pressLoad();
    	System.out.println("Select the file to load");
    	selectFile();
    	System.out.println("Open the file");
    	clickLoad();
    	System.out.println("Deleting image save files");
    	filemanager.FileManager.deleteDirectory(f);
    }
    
    
	private JButtonTester bt;
	private JMenuItemTester mt;
	private JButton button;
	private JMenu menu;
	private JMenuItem menuItem;
	private JList list;
	private JTextField textField;
}
