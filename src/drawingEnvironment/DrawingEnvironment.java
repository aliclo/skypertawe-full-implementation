package drawingEnvironment;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

/**
 * @File DrawingEnvironment.java
 * @author Jonathan Hepplewhite
 * @date 19 March 2017
 * @see DrawingToolBar.java, DrawingArea.java, DrawingEnvironmentTest.java
 * @see LoadDrawingScreen.java, NewDrawingScreen.java
 * @brief Class that allows users to create collaborative drawings
 * \n \n
 * DrawingEnvironment is a allows users to create collaborative drawings.
 * It holds the DrawingArea and DrawingToolBar, and uses a JMenu to allow 
 * users to create a new drawing, save the current drawing, and load drawings 
 * they have access to.
 */
public class DrawingEnvironment extends JFrame implements ActionListener
{	
	/**
	 * @return The output message for the popup
	 */
	public static String getMessage()
	{
		return m_OutputMessage;
	}
	
	/**
	 * Set the message depending on whether the user has opened a drawing
	 * @param isDrawing True if the user has opened a new drawing, false otherwise
	 */
	public static void setMessage(boolean isDrawing)
	{
		if(isDrawing)
		{
			m_OutputMessage = MESSAGE1;
		}
		else
		{
			m_OutputMessage = MESSAGE2;
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e)
	{
		switch(e.getActionCommand())
		{
			case NEW_FILE : System.out.println("New file");
							NewDrawingScreen newScreen = new NewDrawingScreen(m_User, m_ContactList, this);
							m_NewScreenFrame = new JFrame("New Drawing");
							m_NewScreenFrame.add(newScreen);
							m_NewScreenFrame.setSize(FRAME_WIDTH,DRAWING_HEIGHT);
							m_NewScreenFrame.setLocationRelativeTo(null);
							
							m_NewScreenFrame.setVisible(true);
							break;
			case SAVE_FILE: System.out.println("Save file");
							m_ContentPane = this.getContentPane();
							boolean noImageToSave = true;
							for(int i = 0; i<m_ContentPane.getComponentCount(); i++)
							{
								if(m_ContentPane.getComponent(i).equals(m_Drawing))
								{
									m_Drawing.saveImage();
									noImageToSave = false;
								}
							}
							if(noImageToSave == true)
							{	
								System.out.println("No image to save");
							}
							break;
			case LOAD_FILE: System.out.println("Load file");
							LoadDrawingScreen loadScreen = new LoadDrawingScreen(m_User, m_ContactList, this);
							m_LoadScreenFrame = new JFrame("New Drawing");
							m_LoadScreenFrame.add(loadScreen);
							m_LoadScreenFrame.setSize(FRAME_WIDTH,DRAWING_HEIGHT);
							m_LoadScreenFrame.setLocationRelativeTo(null);
							m_LoadScreenFrame.setVisible(true);
							break;
			case CLOSE	  : //If the user clicks yes on the optionpane, exit the program				
					        if (JOptionPane.showConfirmDialog(m_ThisFrame, 
					        		getMessage(), DIALOG_SCREEN_TITLE, 
					            JOptionPane.YES_NO_OPTION,
					            JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION)
					        {
					        	m_ThisFrame.dispose();
					        }
							break;
		}
		
	}
	
	/**
	 * Constructor that creates a new DrawingEnvironment
	 * @param user The current user
	 * @param contacts The current users contacts
	 */
	public DrawingEnvironment(String user, ArrayList<String> contacts)
	{
		this.m_User = user;
		m_ContactList = contacts;
		setupGUI();
	}
	
	/**
	 * Method that clears and sets up the drawing area
	 * @param fileName The name of the current file
	 * @param isNew If the drawing is new or not
	 */
	public void setupDrawingArea(String fileName, boolean isNew)
	{
		if(isNew)
		{
			System.out.println("New Drawing created");
			m_NewScreenFrame.dispose();
		}
		else
		{
			System.out.println("Loaded drawing");
			m_LoadScreenFrame.dispose();
		}

		m_ContentPane = this.getContentPane();
		for(int i = 0; i<m_ContentPane.getComponentCount(); i++)
		{
			if(m_ContentPane.getComponent(i).equals(m_Drawing))
			{
				m_ContentPane.remove(m_Drawing);
			}
		}
		m_Drawing = new DrawingArea(fileName, isNew);
		this.add(m_Drawing,BorderLayout.CENTER);

		this.validate();
		this.repaint();
	}
	
	/**
	 * Method that sets up the DrawingEnvironment GUI
	 */
	private void setupGUI()
	{
		this.setLayout(new BorderLayout());
		/**
		 * Create and setup a JMenuBar that allows the user to create a
		 * new drawing, save the current drawing, load a drawing or
		 * close the DrawingEnvironment
		 */
		m_MenuBar = new JMenuBar();
		m_Menu = new JMenu("File");
		m_MenuBar.add(m_Menu);
		m_NewMenuItem = new JMenuItem("New");
		m_NewMenuItem.addActionListener(this);
		m_Menu.add(m_NewMenuItem);
		
		m_SaveMenuItem = new JMenuItem("Save");
		m_SaveMenuItem.addActionListener(this);
		m_Menu.add(m_SaveMenuItem);
		
		m_LoadMenuItem = new JMenuItem("Load");
		m_LoadMenuItem.addActionListener(this);
		m_Menu.add(m_LoadMenuItem);
		
		m_CloseMenuItem = new JMenuItem("Close");
		m_CloseMenuItem.addActionListener(this);
		m_Menu.add(m_CloseMenuItem);
		
		this.setJMenuBar(m_MenuBar);
		
		m_DrawingToolbar = new DrawingToolBar();
		this.add(m_DrawingToolbar,BorderLayout.NORTH);
		
		m_ContentPane = m_ThisFrame.getContentPane();
		this.setSize(new Dimension(FRAME_WIDTH,FRAME_HEIGHT));
		this.setResizable(false);
		m_ContentPane.setBackground(new Color(m_RGB_BACKGROUND_VALUE,
				m_RGB_BACKGROUND_VALUE,m_RGB_BACKGROUND_VALUE));
		
		this.addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) 
		    {
		    	//If the user clicks yes on the optionpane, exit the program
		        if (JOptionPane.showConfirmDialog(m_ThisFrame, 
		        		getMessage(), DIALOG_SCREEN_TITLE, 
		            JOptionPane.YES_NO_OPTION,
		            JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION){
		        	m_ThisFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		        }
		    }
		});
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.setVisible(true);
	}
	
	/**
	 * Method for whole system integration testing
	 * @see DrawingEnvironmentTest.java for comprehensive testing
	 * @param args Command line arguments
	 */
	public static void main(String[] args)
	{
		System.out.println("DrawingEnvironment::main() BEGIN");
		System.out.println("Output message: " + DrawingEnvironment.getMessage());
		DrawingEnvironment.setMessage(true);
		System.out.println("Output message: " + DrawingEnvironment.getMessage());
		String user = "Bob";
		ArrayList<String> contactList = new ArrayList<String>(Arrays.asList("Bob", "Bill", "Frank", "Fred", "Jimmy"));
		new DrawingEnvironment(user, contactList);
		
		System.out.println("DrawingEnvironment::main() END");
	}

	/** The list of all the current users contacts */
	private ArrayList<String> m_ContactList;
	/** The name of current user */
	private String m_User;
	/** The frame width */
	private final int FRAME_WIDTH = 500;
	/** The frame height - 500 (height of drawing) + 50 
	 * (height of toolbar) + 44 (height of rest of frame) */
	private final int FRAME_HEIGHT = 594;
	/** Height of a drawing */
	private final int DRAWING_HEIGHT = 520;
	
	/** The string to be shown the JOptionPane if the user is currently drawing */
	private static final String MESSAGE1 = "Are you sure you want to close the window?" 
											+ "\n" + "All unsaved progress will be lost.";
	/** The string to be shown the JOptionPane if the user is NOT currently drawing */
	private static final String MESSAGE2 = "Are you sure you want to close the window?";
	/** The output message displayed */
	private static String m_OutputMessage = MESSAGE2;
	/** The title of the popup window */
	private final String DIALOG_SCREEN_TITLE = "Confirm Close Window?";
	
	/** JMenuBar that holds the menu */
	private JMenuBar m_MenuBar;
	/** Menu that holds each of the user options for handing images */
	private JMenu m_Menu;
	/** The JMenuItem for creating a new image */
	private JMenuItem m_NewMenuItem;
	/** The JMenuItem for saving an image */
	private JMenuItem m_SaveMenuItem;
	/** The JMenuItem for loading an image */
	private JMenuItem m_LoadMenuItem;
	/** The JMenuItem for closing the DrawingEnvironment */
	private JMenuItem m_CloseMenuItem;
	
	/** New drawing JMenuItem action command string */
	private final String NEW_FILE = "New";
	/** Save drawing JMenuItem action command string */
	private final String SAVE_FILE = "Save";
	/** Load drawing JMenuItem action command string */
	private final String LOAD_FILE = "Load";
	/** Close drawing JMenuItem action command string */
	private final String CLOSE = "Close";
	
	/** Create a reference to this frame for WindowListener/JOptionPanes */
	private JFrame m_ThisFrame = this;
	/** Reference to this frame's container to access contained objects */
	private Container m_ContentPane;
	
	/** Reference to created NewDrawingScreen */
	private JFrame m_NewScreenFrame;
	/** Reference to created LoadDrawingScreen */
	private JFrame m_LoadScreenFrame;
	/** Reference to the DrawingToolBar */
	private DrawingToolBar m_DrawingToolbar;
	/** Reference to the DrawingArea */
	private DrawingArea m_Drawing;
	
	private final int m_RGB_BACKGROUND_VALUE = 130;
}
