package drawingEnvironment;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * @File DrawingArea.java
 * @author Jonathan Hepplewhite
 * @date 9 Dec 2016
 * @see DrawingListener.java
 * @brief DrawingArea is the panel which the user draws onto 
 * \n \n
 * DrawingArea is a class that that displays the area which the user
 * can draw to. The DrawingArea uses a nested DrawingListener class to 
 * take the user mouse input and draw DrawingElements to the DrawingArea.
 */
public class DrawingArea extends JPanel
{
	/**
	 * Constructor that creates a new DrawingArea
	 * @param fileName The name the user specifies for the file
	 * @param isNew Specifies if this is a new or loaded drawing
	 */
	public DrawingArea(String fileName, boolean isNew)
	{
		this.m_FileName = fileName;
		m_IsFirstSave = isNew;
		//Set the layout to null to allow custom placement of components
		this.setLayout(null);
		//Make the DrawingArea visible
		this.setVisible(true);
		this.setBackground(Color.WHITE);
		/**
		 * Create the listener which will catch all drawing actions and add it 
		 * to the this DrawingArea 
		 */
		DrawingListener drawingListener = new DrawingListener();
		this.addMouseListener(drawingListener);
		this.addMouseMotionListener(drawingListener);
		//Initialize the BufferedImage which will hold the drawn elements
		m_Drawing = new BufferedImage(FILE_WIDTH,FILE_HEIGHT,
									BufferedImage.TYPE_INT_ARGB);
		//If the drawing is new, run new setup, else load setup
		if(isNew)
		{
			initialiseNewDrawing();
		}
		else
		{
			initialiseLoadDrawing();
		}
		//Ensure all components are displayed on screen
		repaint();
	}
	
	/**
	 * Draw the DrawingArea panel and any drawn elements
	 * @param The graphics component related to this panel
	 */
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		//Draw all drawn DrawingElements to screen
		for(DrawingElement e: m_MyDrawingElements)
		{
			e.draw(g);
		}
	}
	
	/**
	 * Save the current state of the drawing to the file system
	 */
	public void saveImage()
	{
		if(m_IsFirstSave)
		{
			//Create/write to the file that stores the filename and users
			ReferenceFileReader.createFile();
		}
		//Try to write the drawing to file
		try 
		{
			//Make sure all drawn elements are contained by BufferedImage
			this.paintAll(m_Drawing.getGraphics());
			/*Write drawing to the file system in the designated file location 
			 * and file extension */ 
			ImageIO.write(m_Drawing, IMAGE_FILE_TYPE, 
								new File(FILE_FOLDER_LOCATION + m_FileName + "." 
								+ IMAGE_FILE_TYPE));
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * If a drawing was loaded, load the image file and apply it 
	 * to the DrawingArea
	 */
	private void initialiseLoadDrawing()
	{
		//Try to read the image file and add it to the BufferedImage
		try 
		{
			File image = new File(FILE_FOLDER_LOCATION + 
								m_FileName + "." + IMAGE_FILE_TYPE);
			m_Drawing = ImageIO.read(image);
		} 
		catch (IOException e1)
		{
			e1.printStackTrace();
		}

		ImageIcon icon = new ImageIcon(FILE_FOLDER_LOCATION +
				m_FileName + "." + IMAGE_FILE_TYPE);
		//Remove old reference to the image from memory
		icon.getImage().flush();
		JLabel picture = new JLabel(icon)
		{
		//Paint drawing elements to the JLabel
		public void paintComponent(Graphics g)
		{
			super.paintComponent(g);
			//Draw all drawn DrawingElements to JLabel
			for(DrawingElement e: m_MyDrawingElements)
			{
				e.draw(g);
			}
		}};
		//Add the loaded drawing image and ensure it displays properly
		this.add(picture);
		picture.setBounds(0, 0, FILE_WIDTH, FILE_HEIGHT);
		picture.setVisible(true);
	}
	
	/**
	 * If this is a new drawing, setup a new JPanel
	 */
	private void initialiseNewDrawing()
	{
		JPanel p = new JPanel()		
		{
			public void paintComponent(Graphics g)
			{
				super.paintComponent(g);
				//Draw all drawn DrawingElements to screen
				for(DrawingElement e: m_MyDrawingElements)
				{
					e.draw(g);
				}
			}};
		//Make background white and make visible
		p.setBackground(Color.WHITE);
		this.setVisible(true);
		repaint();
	}
	
	/**
	 * @author Jonathan Hepplewhite
	 * @Date 9 Dec 2016
	 * 
	 * Class that deals with all drawing area mouse inputs
	 */
	private class DrawingListener implements MouseMotionListener, MouseListener
	{
		/**
		 * Methods required by MouseListener and MouseMotionListener 
		 * that are not used 
		 */
		public void mouseClicked(MouseEvent e) {}
		public void mouseEntered(MouseEvent e) {}
		public void mouseExited(MouseEvent e) {}
		public void mouseMoved(MouseEvent e) {}	

		/**
		 * Method that is called if the mouse is moved across the screen whilst pressed
		 * @param The mouse event that triggered on the DrawingArea
		 */
		@Override
		public void mouseDragged(MouseEvent e) 
		{
			//If the current selected element is ParticleTrace
			if(DrawingToolBar.getCurrentElement() == 0)
			{
				/**Create a new ParticleTrace object with the current mouse coordinates */
				DrawingElement newElement = new Particle(e.getX(), e.getY());
				/** Add this new ParticleTrace to the Arraylist of all DrawingElements */
				m_MyDrawingElements.add(newElement);
				//Draw the elements to screen
				repaint();
			}
			//If the current selected element is Line
			else if (DrawingToolBar.getCurrentElement() == 1)
			{
				//Set the X and Y coordinates of the Line 
				((Line) m_MyDrawingElements.get(m_MyDrawingElements.size()- 1)).setEndX(e.getX());
				((Line) m_MyDrawingElements.get(m_MyDrawingElements.size()- 1)).setEndY(e.getY());
				//Paint the line to screen at its current position
				repaint();
			}
		}
		
		/**
		 * Method that is called is the mouse is pressed
		 * @param The mouse event that triggered on the DrawingArea
		 */
		@Override
		public void mousePressed(MouseEvent e) 
		{
			//If the current element is Line
			if(DrawingToolBar.getCurrentElement() == 1)
			{
				// Create a new Line element and add it the ArrayList of all elements
				DrawingElement newElement = new Line(e.getX(), e.getY());
				m_MyDrawingElements.add(newElement);
			}
		}

		/**
		 * Method that is called of the mouse is released
		 * @param The mouse event that triggered on the DrawingArea
		 */
		@Override
		public void mouseReleased(MouseEvent e) 
		{
			//If the current element is Line
			if(DrawingToolBar.getCurrentElement() == 1)
			{
				//Set the X and Y coordinates of the Line 
				((Line) m_MyDrawingElements.get(m_MyDrawingElements.size()- 1)).setEndX(e.getX());
				((Line) m_MyDrawingElements.get(m_MyDrawingElements.size()- 1)).setEndY(e.getY());
				//Paint the line to screen at its final position
				repaint();
			}
		}
	}
	
	/**
	 * Method to test DrawingArea class
	 * @param args Command line arguments
	 */
	public static void main(String[] args)
	{
		System.out.println("DrawingArea::main() BEGIN"); 
		JFrame frame = new JFrame();
		System.out.println("Creating new DrawingArea");
		DrawingArea testArea = new DrawingArea("testfile", true);
		frame.setSize(500, 500);
		frame.add(testArea);
		frame.setVisible(true);
		System.out.println("Inititalising new drawing");
		testArea.initialiseNewDrawing();
		System.out.println("Creating test users for ReferenceFileReader");
		String[] testUsers = {"Jon", "Bob"};
		ReferenceFileReader.setUsers(testUsers);
		System.out.println("Saving the image");
		testArea.saveImage();
		System.out.println("Loading a drawing");
		testArea.initialiseLoadDrawing();
		System.out.println("Testing complete");
		System.out.println("DrawingArea::main() END"); 
		frame.dispose();
	}
	
	/** The width of the drawing */
	private final int FILE_WIDTH = 500;
	/** The height of the drawing */
	private final int FILE_HEIGHT = 500;
	/** The location of the drawings and save file */
	private final String FILE_FOLDER_LOCATION = "drawings/";
	/** The image file type */
	private final String IMAGE_FILE_TYPE = "png";
	/** The BufferedImage involved with saving the drawing to file */
	private BufferedImage m_Drawing;
	/** The ArrayList that stores all the DrawingElement objects */
	private ArrayList<DrawingElement> m_MyDrawingElements = new ArrayList<>();
	/** The name of the file */
	private String m_FileName;
	/** 
	 * Used to express whether or not the file is new for saving to 
	 * the reference file if necessary 
	 * */
	private boolean m_IsFirstSave;

}