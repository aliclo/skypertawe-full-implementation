package resources;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

import components.ImageComponent;

/**
 * @File	-Resources.java
 * @author	-Alistair Cloney
 * @date	-15/3/2017
 * @see		-https://docs.oracle.com/javase/7/docs/api/javax/imageio/
 * 			ImageIO.html
 * 
 * @brief	Loads all the resources (such as Images) for classes of
 * 			Skypertawe to use.
 */
public class Resources {
	
	/**
	 * @return The bin image icon.
	 */
	public static ImageIcon getBinIcon() {
		return m_binIcon;
	}
	
	/**
	 * @return The edit image icon.
	 */
	public static ImageIcon getEditIcon() {
		return m_editIcon;
	}
	
	/**
	 * @return The Skypertawe logo image.
	 */
	public static BufferedImage getLogoImage() {
		return m_logoImage;
	}
	
	/**
	 * Loads all resources ready for other classes to use.
	 */
	public static void loadAllResources() {
		m_binIcon = new ImageIcon(loadImage(BIN_ICON_FILE));
		m_editIcon = new ImageIcon(loadImage(EDIT_ICON_FILE));
		m_logoImage = loadImage(LOGO_FILE);
	}
	
	/**
	 * Used to load an image and handle problems if the image is unavailable.
	 * @param resource The file to the image resource to load.
	 * @return The image from the given file.
	 */
	public static BufferedImage loadImage(File resource) {
		try {
			return ImageIO.read(resource);
		} catch (IOException e) {
			System.out.println("Couldn't find resource "
					+ resource.getAbsolutePath());
		}
		
		return null;
	}
	
	/**
	 * Tests methods of the class Resources.
	 * 
	 * Integration testing with classes: ImageComponent
	 * 
	 * @param args Not used.
	 */
	public static void main(String[] args) {
		JFrame testFrame = new JFrame("Test");
		testFrame.setSize(TEST_WINDOW_SIZE);
		JPanel testPanel = (JPanel) testFrame.getContentPane();
		testPanel.setLayout(new BoxLayout(testPanel, BoxLayout.PAGE_AXIS));
		
		loadAllResources();
		
		//Test if all resources were loaded correctly by displaying them
		ImageComponent binImageComp = new ImageComponent(
				(BufferedImage) getBinIcon().getImage());
		
		testPanel.add(binImageComp);
		
		ImageComponent editImageComp = new ImageComponent(
				(BufferedImage) getEditIcon().getImage());
		
		testPanel.add(editImageComp);
		
		ImageComponent logoImageComp = new ImageComponent(getLogoImage());
		
		testPanel.add(logoImageComp);
		
		File backgroundFile = new File("background.png");
		BufferedImage backgroundImage = loadImage(backgroundFile);
		
		ImageComponent backgroundImageComp = new ImageComponent(
				backgroundImage);
		
		testPanel.add(backgroundImageComp);
		
		testFrame.setVisible(true);
	}
	
	//Resources files
	private static final File BIN_ICON_FILE = new File("bin.png");
	private static final File EDIT_ICON_FILE = new File("edit.png");
	private static final File LOGO_FILE = new File("Logo.png");
	
	private static final Dimension TEST_WINDOW_SIZE = new Dimension(300,500);
	
	//Resources
	private static ImageIcon m_binIcon;
	private static ImageIcon m_editIcon;
	private static BufferedImage m_logoImage;
	
}
