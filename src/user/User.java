package user;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Scanner;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import components.ImageComponent;
import contactList.Contact;
import conversation.Chat;
import conversation.ChatIO;
import dataStructure.BST;
import filemanager.FileManager;
import login.Encryption;
import message.MessageBuilder;
import message.TextMessage;

/**
 * @File	-User.java
 * @author	-Ianis Tutunaru 874225, edited by Alistair Cloney
 * @date	-11 Dec 16, edited on 10/3/2017
 * @see		-BST.java, Contact.java, Chat.java, ChatIO.java
 * @brief	User class that stores the user account data both in memory and
 * 			in storage, also handles password validation and deals with storing
 * 			contact requests both in memory and storage and holds its list
 * 			of contacts.
 * 
 * Stores and saves account data including: username, name, telephone number,
 * birth date, city of residence, and profile picture. This data is stored in
 * one line in a file named after the account name.
 * 
 * The last time the user logged in is also noted.
 * 
 * The user's list of contacts and contact requests are also stored and saved,
 * as well as group chats the user is a member of.
 * 
 * The list of contact requests are stored in a file named after the user and
 * contains lines of users who have sent a request to this user.
 * 
 * See the class Contact for a description of how the list of contacts are
 * stored.
 * 
 * See the class ChatIO for a description of how the list of group chats are
 * stored.
 * 
 * Also provides a rewrite method that allows the user to change details.
 * 
 * Password validation is also provided for the login process.
 */
public class User {
	
	/**
	 * @return The full birth date of the user.
	 */
	public String getBirthDate(){
		return m_birthDate;
	}
	
	/**
	 * Set the birth date with the given birth date.
	 * 
	 * @param birthDate The new birth date.
	 */
	public void setBirthDate(String birthDate){
		m_birthDate = birthDate;
		
		rewriteUser(this);
	}
	
	/**
	 * @return The current city of residence of the user.
	 */
	public String getCity(){
		return m_currCity;
	}
	
	/**
	 * Set the current city of residence to the given city.
	 * 
	 * @param currCity The new city of residence of the user.
	 */
	public void setCity(String currCity){
		m_currCity = currCity;
		
		rewriteUser(this);
	}
	
	/**
	 * @return The array list of user accounts that request to be
	 * contacts to this user.
	 */
	public ArrayList<User> getContactRequests() {
		return m_contactRequests;
	}
	
	/**
	 * @return The array list of contacts to this user.
	 */
	public ArrayList<Contact> getContacts() {
		return m_contacts;
	}
	
	/**
	 * Set the list of contacts to the given list of contacts.
	 * 
	 * @param contacts The new list of contacts.
	 */
	public void setContacts(ArrayList<Contact> contacts) {
		this.m_contacts = contacts;
	}
	
    /**
	 * @return The description of the User detailing the values of
	 * each of its attributes.
	 */
	public String getDescription() {
		String description = "";
		
		description += "Username: " + m_username + '\n';
		description += "Name: " + m_name + '\n';
		description += "Telephone Number: " + m_telNumber + '\n';
		description += "Birth Date: " + m_birthDate + '\n';
		description += "City: " + m_currCity + '\n';
		description += "Last Login: " + m_lastLogTime + '\n';
		description += "Profile Picture Path: " + m_profilePicture + '\n';
		
		description += "Contacts:\n";
		
		for(Contact contact : m_contacts) {
			description += contact.getUser().getUsername() + '\n';
		}
		
		description += "Contact Requests:\n";
		
		for(User request : m_contactRequests) {
			description += request.getUsername() + '\n';
		}
		
		description += "Group Chats:\n";
		
		for(Chat chat : m_groupChats) {
			description += chat.getName() + '\n';
		}
		
		return description;
	}
	
	/**
	 * @return The array list of group chats the user is a member of.
	 */
	public ArrayList<Chat> getGroupChats() {
		return m_groupChats;
	}
	
	/**
	 * Set the list of group chats to the given list of group chats.
	 * 
	 * @param groupChats The new list of group chats.
	 */
	public void setGroupChats(ArrayList<Chat> groupChats) {
		m_groupChats = groupChats;
	}
	
	/**
	 * @return A string containing the date and time of the last logout.
	 */
	public String getLastLogTime(){
		return m_lastLogTime;
	}
	
	/**
	 * @return The name of the user.
	 */
	public String getName(){
		return m_name;
	}
	
	/**
	 * Sets the name of the user to the given name.
	 * 
	 * @param name The new name.
	 */
	public void setName(String name){
		m_name = name;
		
		rewriteUser(this);
	}
	
	/**
	 * Method that returns a profile picture image using the path of the
	 * profile picture.
	 * 
	 * @return The profile picture image.
	 */
	public BufferedImage getProfilePicture() {
		//If there is a path, otherwise the image is null
		if(getProfilePicturePath() != null) {
			File file = new File(getProfilePicturePath());
			
			if(file.exists()) {
				try {
					return ImageIO.read(file);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return null;
	}
	
	/**
	 * @return The profile picture path.
	 */
	public String getProfilePicturePath() {
		return m_profilePicture;
	}
	
	/**
	 * Sets the profile picture path to the given path.
	 * 
	 * @param profilePicturePath The new path the profile picture should use.
	 */
	public void setProfilePicturePath(String profilePicturePath) {
		m_profilePicture = profilePicturePath;
		
		rewriteUser(this);
	}
	
	/**
	 * Get the plain text data that is used to store the user.
	 * 
	 * @param password The user's password.
	 * @return The plain text data storing details of the user.
	 */
    public String getSaveData(String password) {
		return password + SEPARATOR + m_name + SEPARATOR + m_telNumber
				+ SEPARATOR + m_birthDate + SEPARATOR + m_currCity + SEPARATOR
				+ m_lastLogTime + SEPARATOR + m_profilePicture;
	}
    
    /**
	 * @return The telephone number of the user.
	 */
	public String getTNum(){
		return m_telNumber;
	}
	
	/**
	 * Set the telephone number of the user to the given telephone number.
	 * 
	 * @param telNumber The new telephone number.
	 */
	public void setTNum(String telNumber){
		m_telNumber = telNumber;
		
		rewriteUser(this);
	}
	
	/**
	 * @return The total number of messages the user hasn't seen.
	 */
	public int getTotalNumNewMessages() {
		int count = 0;
		
		//Accumulate the number of new messages from each of the group chats
		for(Chat chat : m_groupChats) {
			count += chat.getNumNewMessages(this);
		}
		
		//Accumulate the number of new messages from each of the contact chats
		for(Contact contact : m_contacts) {
			count += contact.getChat().getNumNewMessages(this);
		}
		
		return count;
	}
    
	/**
	 * Generate a user object from reading the given data.
	 * 
	 * @param data The data to read.
	 * @param accountName The user's account name.
	 * @return The user object.
	 */
	public static User getUser(String data, String accountName) {
		String[] tokens =  data.split(SEPARATOR);
		
		String name = tokens[NAME_POS];
		String teleNum = tokens[TELE_NUM_POS];
		String date = tokens[DATE_POS];
		String city = tokens[CITY_POS];
		String lastLogin = tokens[LAST_LOGIN_POS];
		String imagePath = tokens[IMAGE_PATH_POS];
		
		User user = new User()
				.withUsername(accountName)
				.withName(name)
				.withTelNumber(teleNum)
				.withBirthDate(date)
				.withCurrentCity(city)
				.withLastLogTime(lastLogin)
				.withProfilePicturePath(imagePath);
		
		return user;
	}
	
	/**
	 * @return The username of the user.
	 */
	public String getUsername(){
		return m_username;
	}
	
	/**
	 * Accept the contact request from the given user who is requesting it.
	 * 
	 * @param contactRequest The user whose requesting to be a contact.
	 */
	public void acceptContactRequest(User contactRequest) {
		removeContactRequest(contactRequest);
		
		ArrayList<User> members = new ArrayList<User>();
		
		members.add(this);
		members.add(contactRequest);
		
		Chat chat = new Chat(members);
		
		chat.saveData();
		
		addContact(contactRequest, chat);
		contactRequest.addContact(this, chat);
	}
	
	/**
	 * Add the given contact object to this user's contact list.
	 * 
	 * @param contact The contact to add.
	 */
	public void addContact(Contact contact) {
		m_contacts.add(contact);
		
		Contact.saveNewContact(this, contact);
	}
	
	/**
	 * Add the given user to this user's contact list where both users can
	 * chat using the given chat, a contact object is created first.
	 * 
	 * @param user The user to add to the contact list.
	 * @param chat The chat the contact conversation will use.
	 */
	public void addContact(User user, Chat chat) {
		Contact contact = new Contact(user, chat);
		
		addContact(contact);
	}
	
	/**
	 * Add the given user to the user's contact request list.
	 * 
	 * @param contactRequest The user to add.
	 */
	public void addContactRequest(User contactRequest) {
		m_contactRequests.add(contactRequest);
		
		saveContactRequest(contactRequest);
	}
	
	/**
	 * Add the given chat to the user's list of group chats.
	 * 
	 * @param groupChat The group chat to add.
	 */
	public void addGroupChat(Chat groupChat) {
		m_groupChats.add(groupChat);
		
		ChatIO.saveGroupChat(this, groupChat);
	}
	
	/**
	 * Changes the user's password.
     * 
     * @param username The username of the User.
     * @param newPassword The new password to change to.
     * @return True if successful.
     */
    public static boolean changePassword(String username, String newPassword) {
    	File userFile = new File(USERS_PATH + username + EXTENSION);
    	
    	/**
    	 * If the file exists, the user exists, otherwise the user
    	 * doesn't exist.
    	 */
    	if(userFile.exists()) {
			try {
				Scanner in = new Scanner(userFile);
				
				String data = in.nextLine();
				
				in.close();
				
				//Get the old password
				String[] tokens = data.split(SEPARATOR);
	    		
				String oldPassword = tokens[PASSWORD_POS];
				
				//Encrypt the new password
				String encPassword = Encryption.md5(newPassword);
				
				/**
				 * Replace the old password with the new password,
				 * this is easy as the password is stored in the
				 * first token of the file and so the first token
				 * is replaced.
				 */
				data = encPassword + data.substring(oldPassword.length());
				
				//Rewrite the data into the file
				PrintWriter out = new PrintWriter(userFile);
				
				out.println(data);
				
				out.close();
				
	    		return true;
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	
    	return false;
    }
    
    /**
     * Check if the given object is equal to this user.
     * 
     * @param o The object to check.
     * @return True if the object is equal to this user, otherwise false.
     */
    @Override
	public boolean equals(Object o) {
    	//Not equal if the object is null
		if(o == null) {
			return false;
		}
		
		//Not equal if the object is of a different class
		if(o.getClass() != this.getClass()) {
			return false;
		}
		
		//From the above, this can assume the given object is of type User
		User u = (User) o;
		
		/**
		 * If both the usernames are equal, the user is equal as the
		 * usernames must be unique.
		 */
		if(u.getUsername().equals(this.getUsername())) {
			return true;
		}
		
		return false;
	}
    
    /**
	 * @return Does the user have any notifications.
	 */
	public boolean hasNotification() {
		/**
		 * If the user has any contact requests,
		 * there is a notification to display.
		 */
		return (m_contactRequests.size() > 0);
	}
	
	/**
	 * Test if the given user is already a contact to this user or not.
	 * 
	 * @param user The user to check.
	 * @return True if already contacts.
	 */
	public boolean isAlreadyContact(User user) {
		for(Contact contact : m_contacts) {
			if(contact.getUser().equals(user)) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Load the contact request as a user from the given data.
	 * 
	 * @param data The data to read.
	 * @param users The existing users of Skypertawe.
	 * @return The user who is the contact request.
	 */
	public static User loadContactRequestData(String data, BST users) {
		String contactRequestName = data;
		User contactRequestAccount = users.getProfileRoot(contactRequestName);
		
		return contactRequestAccount;
	}
	
	/**
	 * Load all the contact requests of the given user.
	 * 
	 * @param user The user whose contact requests are loaded.
	 * @param users The existing users of Skypertawe.
	 */
	public static void loadContactRequests(User user, BST users) {
		ArrayList<User> contactRequests = new ArrayList<User>();
		
		/**
		 * Load the file containing the list of contact requests
		 * of the given user.
		 */
		File file = new File(CONTACT_REQUESTS_PATH + user.getUsername() +
				".txt");
		
		FileManager.createFileWhenNonExistent(file);
		
		try {
			Scanner in = new Scanner(file);
			
			while(in.hasNextLine()) {
				String username = in.nextLine();
				
				User request = User.loadContactRequestData(username, users);
				
				contactRequests.add(request);
			}
			
			in.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		user.setContactRequests(contactRequests);
	}
	
	/**
	 * Load the user with the given username.
	 * 
	 * @param username The username of the user to load.
	 * @return The loaded user.
	 */
	public static User loadUser(String username) {
		String filePath = USERS_PATH + username + EXTENSION;
		File userFile = new File(filePath);
		
		try {
			Scanner in = new Scanner(userFile);
			
			String line = in.nextLine();
			
			in.close();
			
			return getUser(line, username);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	/**
	 * Test the methods of the class User.
	 * 
	 * Integration testing with classes: BST, Chat
	 * 
	 * @param args Not used.
	 */
	public static void main(String[] args) {
		User loggedUser = new User()
				.withUsername("test")
				.withName("test")
				.withBirthDate("05/05/2005")
				.withProfilePicturePath("Test")
				.withTelNumber("07123456789")
				.withCurrentCity("Cardiff");
		
		settersTest(loggedUser);
		
		User userAlice = new User().withUsername("Alice");
		User userBob = new User().withUsername("Bob");
		User userJake = new User().withUsername("Jake");
		User userJess = new User().withUsername("Jess");
		User userLarry = new User().withUsername("Larry");
		User userDan = new User().withUsername("Dan");
		User userEmily = new User().withUsername("Emily");
		
		BST users = new BST();
		users.insertUser(loggedUser);
		users.insertUser(userAlice);
		users.insertUser(userBob);
		users.insertUser(userJake);
		users.insertUser(userJess);
		users.insertUser(userLarry);
		users.insertUser(userDan);
		users.insertUser(userEmily);
		
		ArrayList<User> members = new ArrayList<User>();
		members.add(loggedUser);
		members.add(userAlice);
		members.add(userBob);
		
		Chat groupChatA = new Chat("Group Chat A", members);
		
		ArrayList<Chat> groupChats = new ArrayList<Chat>();
		groupChats.add(groupChatA);
		loggedUser.setGroupChats(groupChats);
		
		members = new ArrayList<User>();
		members.add(loggedUser);
		members.add(userLarry);
		members.add(userBob);
		members.add(userJess);
		
		Chat groupChatB = new Chat("Group Chat B", members);
		loggedUser.addGroupChat(groupChatB);
		
		members = new ArrayList<User>();
		members.add(loggedUser);
		members.add(userAlice);
		Chat aliceContactChat = new Chat(members);
		Contact contactAlice = new Contact(userAlice, aliceContactChat);
		
		addingContactTest(loggedUser, contactAlice);
		addingContactRequestsTest(loggedUser, userBob, userJake);
		oneSideContactRemovalTest(loggedUser, userDan);
		
		loggedUser.acceptContactRequest(userJake);
		loggedUser.addContactRequest(userJess);
		
		members = new ArrayList<User>();
		members.add(loggedUser);
		members.add(userLarry);
		Chat larryContactChat = new Chat("Larry contact", members);
		loggedUser.addContact(new Contact(userLarry, larryContactChat));
		
		messageCountTest(loggedUser, userLarry, larryContactChat, userDan,
				groupChatB);
		
		User loadUser = saveLoadTest(loggedUser, userEmily, users);
		groupAndContactTest(loadUser,userBob,contactAlice,groupChatA,users);
		readWriteUserTest(loggedUser, userAlice);
	}
	
	/**
	 * Reads every user file to generate a user object and adds this user
	 * to a binary search tree.
	 * 
	 * @return The binary search tree that has accumulated all the stored
	 * users.
	 */
	public static BST readAllUsers(){
		BST users = new BST();
		
		//The folder that contains all the user data files
		File usersFolder = new File(USERS_PATH);
		
		if(usersFolder.exists()) {
			/**
			 * For each file within the folder, read its data to
			 * generate a user.
			 */
			for(File userFile : usersFolder.listFiles()) {
				String fileName = userFile.getName();
				int nameLength = fileName.length()-EXTENSION.length();
				
				String accountName = fileName.substring(0, nameLength);
				
				try {
					Scanner scanner = new Scanner(userFile);
					
				    String line = scanner.nextLine();
				   	
				    users.insertUser(User.getUser(line, accountName));
				    
					scanner.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}
		} else {
			usersFolder.mkdir();
		}
		
		return users;
	}
	
	/**
	 * Remove the given contact from the user's contact list, and
	 * vice versa.
	 * 
	 * @param contact The contact to remove.
	 */
	public void removeContact(Contact contact) {
		removeOneSideContact(contact);
		
		/**
		 * Creates a contact of this user that would match one of the other
		 * user's contacts (which should be this user that should be removed).
		 */
		Contact thisContact = new Contact(this);
		
		contact.getUser().removeOneSideContact(thisContact);
		
		contact.getChat().deleteChat();
	}
	
	/**
	 * Remove the given contact request from this user's list
	 * of contact requests.
	 * 
	 * @param contactRequest The contact request to remove.
	 */
	public void removeContactRequest(User contactRequest) {
		m_contactRequests.remove(contactRequest);
		
		saveContactRequests();
	}
	
	/**
	 * Remove the given group chat from this user's list of group chats.
	 * 
	 * @param groupChat The group chat to remove.
	 */
	public void removeGroupChat(Chat groupChat) {
		m_groupChats.remove(groupChat);
		
		ChatIO.saveGroupChats(this);
	}
	
	/**
	 * Rewrite the data file with the new data from the given user.
	 * 
	 * The file must be written to first before this method can be called
	 * (e.g. use writeUser).
	 * 
	 * @param user The user whose save data will be rewritten.
	 */
	public static void rewriteUser(User user) {
		try {
			String userFileName = USERS_PATH + user.getUsername() + EXTENSION;
			File userFile = new File(userFileName);
			
			if(userFile.exists()) {
				/**
				 * Make sure the stored password is preserved by first
				 * taking it out of the old data file and putting it
				 * into the new rewritten data file.
				 */
				Scanner in = new Scanner(userFile);
				
				String data = in.nextLine();
				
				in.close();
				
				//Find the password from the tokens
				String encryptedPassword = data.split(SEPARATOR)[PASSWORD_POS];
				
				//Now write to the file the new data
				PrintWriter writer = new PrintWriter(userFile); 
				
			    writer.println(user.getSaveData(encryptedPassword));
			    writer.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Save the new contact request into the data file representing this
	 * user's contact requests, this is done by appending the username of
	 * the contact request to the data file.
	 * 
	 * @param contactRequest The new contact request to save.
	 */
	public void saveContactRequest(User contactRequest) {
		String fileName = CONTACT_REQUESTS_PATH + getUsername() + ".txt";
		File file = new File(fileName);
		
		FileManager.createFileWhenNonExistent(file);
		
		try {
			//Set to append to the data file
			FileWriter fileWriter = new FileWriter(file, true);
			PrintWriter out = new PrintWriter(fileWriter);
			
			out.println(contactRequest.saveContactRequestData());
			
			out.close();
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @return The plain text data required to save a contact request.
	 */
	public String saveContactRequestData() {
		return getUsername();
	}
	
	/**
	 * Save all references of contact requests of this user.
	 */
	public void saveContactRequests() {
		String fileName = CONTACT_REQUESTS_PATH + getUsername() + ".txt";
		File file = new File(fileName);
		
		FileManager.createFileWhenNonExistent(file);
		
		try {
			PrintWriter out = new PrintWriter(file);
			
			for(User contactRequest : m_contactRequests) {
				out.println(contactRequest.saveContactRequestData());
			}
			
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Send a contact request to the given user or otherwise output a
	 * message explaining why this is not possible.
	 * 
	 * @param user The user to send the request to.
	 * @param users The existing users of Skypertawe.
	 */
	public void sendContactRequest(User user, BST users) {
		//User can't send request to self
		if(user.equals(this)) {
			JOptionPane.showMessageDialog(null, "Can't add self as friend");
			
		//User can't request a user who is already a contact
		} else if(isAlreadyContact(user)) {
			JOptionPane.showMessageDialog(null, "Already a contact");
		} else {
			User.loadContactRequests(user, users);
			
			/**
			 * User can't request if they have already sent a request to
			 * the other user.
			 */
			if(user.getContactRequests().contains(this)) {
				String message = "Contact request already sent";
				
				JOptionPane.showMessageDialog(null, message);
				
			//User can't request if the other user has already requested
			} else if (getContactRequests().contains(user)) {
				String message = "Contact request already recieved";
				
				JOptionPane.showMessageDialog(null, message);
			} else {
				user.addContactRequest(this);
				
				String message = "Request sent";
				
				JOptionPane.showMessageDialog(null, message);
			}
		}
	}
	
	/**
	 * Update the time the user last logged in to be the current time.
	 */
	public void updateLastLogTime() {
		//Create Calendar instance that holds the current time
    	Calendar cal = Calendar.getInstance();
    	
    	/**
    	 * Set format of time to be hours followed by minutes and then
    	 * seconds, each separated by a colon.
    	 */
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        
        /**
         * Set format of date to be years followed by months and then
         * days, each separated by a slash.
         */
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        
        //Create an instance of LocalDate with the current date
        LocalDate localDate = LocalDate.now();
        
        //Create the last login time using these two formats
        m_lastLogTime = dtf.format(localDate) + " "
        		+ sdf.format(cal.getTime());
        
		rewriteUser(this);
	}
	
	/**
	 * Validates the given username and password to check if a user
	 * exists with the given username and then check if the password
	 * when encrypted matches with the stored password thats also
	 * encrypted.
	 * 
	 * @param username The username to validate.
	 * @param password The corresponding password to validate.
	 * @return If the validation was successful.
	 */
	public static boolean validateUserDetails(String username,
			String password) {
		
    	File file = new File(USERS_PATH + username + EXTENSION);
    	
    	//If the file exists, the user exists
    	if(file.exists()) {
			try {
	    		//Find the stored password
				Scanner in = new Scanner(file);
				
				String line = in.nextLine();
	    		
	    		in.close();
	    		
	    		String[] tokens = line.split(SEPARATOR);
	    		
	    		String actualPassword = tokens[PASSWORD_POS];
	    		
	    		/**
	    		 * Check if the given password matches with the stored
	    		 * password when encrypted.
	    		 */
	    		if(actualPassword.equals(Encryption.md5(password))) {
	    			return true;
	    		}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	
    	return false;
    }
	
	/**
	 * @param date The birth date of the user.
	 * @return This object (allows for multiple "with" calls all at once).
	 */
	public User withBirthDate(String date) {
		m_birthDate = date;
		return this;
	}
	
	/**
	 * @param requests The list of contact requests the user has.
	 * @return This object (allows for multiple "with" calls all at once).
	 */
	public User withContactRequests(ArrayList<User> requests) {
		m_contactRequests = requests;
		return this;
	}
	
	/**
	 * @param contacts The list of contacts the user has.
	 * @return This object (allows for multiple "with" calls all at once).
	 */
	public User withContacts(ArrayList<Contact> contacts) {
		m_contacts = contacts;
		return this;
	}
	
	/**
	 * @param city The city of residence of the user.
	 * @return This object (allows for multiple "with" calls all at once).
	 */
	public User withCurrentCity(String city) {
		m_currCity = city;
		return this;
	}
	
	/**
	 * @param groupChats The list of group chats the user has.
	 * @return This object (allows for multiple "with" calls all at once).
	 */
	public User withGroupChats(ArrayList<Chat> groupChats) {
		m_groupChats = groupChats;
		return this;
	}
	
	/**
	 * @param logTime The last time the user logged on.
	 * @return This object (allows for multiple "with" calls all at once).
	 */
	public User withLastLogTime(String logTime) {
		m_lastLogTime = logTime;
		return this;
	}
	
	/**
	 * @param name The name of the user.
	 * @return This object (allows for multiple "with" calls all at once).
	 */
	public User withName(String name) {
		m_name = name;
		return this;
	}
	
	/**
	 * @param path The path to the profile picture of the user account.
	 * @return This object (allows for multiple "with" calls all at once).
	 */
	public User withProfilePicturePath(String path) {
		m_profilePicture = path;
		return this;
	}
	
	/**
	 * @param telNumber The telephone number of the user.
	 * @return This object (allows for multiple "with" calls all at once).
	 */
	public User withTelNumber(String telNumber) {
		m_telNumber = telNumber;
		return this;
	}
	
	/**
	 * @param username The username of the user account.
	 * @return This object (allows for multiple "with" calls all at once).
	 */
	public User withUsername(String username) {
		m_username = username;
		return this;
	}
	
	/**
	 * Writes the given user to a file.
	 * 
	 * @param user The user to write.
	 * @param password The user's password.
	 */
	public static void writeUser(User user, String password){		
		try{
			String userFileName = USERS_PATH + user.getUsername() + EXTENSION;
			File userFile = new File(userFileName);
			
			FileManager.createFileWhenNonExistent(userFile);
			
			PrintWriter writer = new PrintWriter(userFile); 
			
		    writer.println(user.getSaveData(Encryption.md5(password)));
		    writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Test of adding contact requests to a test user and testing if a
	 * notification should be displayed as a result.
	 * 
	 * @param loggedUser The user to test on.
	 * @param firstRequest The first request that will be added.
	 * @param secondRequest The second request that will be added.
	 */
	private static void addingContactRequestsTest(User loggedUser,
			User firstRequest, User secondRequest) {
		
		ArrayList<User> requests = new ArrayList<User>();
		requests.add(firstRequest);
		requests.add(secondRequest);
		
		System.out.println("Has notification: "
				+ loggedUser.hasNotification());
		
		System.out.println("Adding contact requests");
		
		loggedUser.setContactRequests(requests);
		
		System.out.println("Has notification: "
				+ loggedUser.hasNotification());
	}
	
	/**
	 * Test of adding a contact to a test user.
	 * 
	 * Integration testing with class: Contact
	 * 
	 * @param loggedUser The user to test on.
	 * @param contact The contact to add.
	 */
	private static void addingContactTest(User loggedUser, Contact contact) {
		User contactUser = contact.getUser();
		
		ArrayList<Contact> contacts = new ArrayList<Contact>();
		
		System.out.println("Is " + contact.getUser().getUsername()
			+ " a contact: " + loggedUser.isAlreadyContact(contactUser));
		
		System.out.println("Add " + contact.getUser().getUsername());
		
		contacts.add(contact);
		
		loggedUser.setContacts(contacts);
		
		System.out.println("Is " + contactUser.getUsername() + " a contact: "
			+ loggedUser.isAlreadyContact(contactUser));
	}
	
	/**
	 * Test method to reset a forgotten password.
	 * 
	 * @param user User to test on.
	 */
	private static void forgotPasswordTest(User user) {
		System.out.println("Forgot password with existing account");
		
		String passwordChange = "Hello 123";
		
		boolean passwordMatch = validateUserDetails(user.getUsername(),
				passwordChange);
		
		System.out.println("Before change, password is new password: "
				+ passwordMatch);
		
		changePassword(user.getUsername(), passwordChange);
		
		passwordMatch = validateUserDetails(user.getUsername(),
				passwordChange);
		
		System.out.println("After change, password is new password: "
				+ passwordMatch);
		
		System.out.println("Forgot password with non-existing account");
		
		String incorrectUsername = "incorrect";
		
		passwordMatch = validateUserDetails(incorrectUsername, passwordChange);
		
		System.out.println("Before change, password is new password: "
				+ passwordMatch);
		
		changePassword(incorrectUsername, passwordChange);
		
		passwordMatch = validateUserDetails(incorrectUsername, passwordChange);
		
		System.out.println("After change, password is new password: "
				+ passwordMatch);
	}
	
	/**
	 * Tests related to groups and contacts.
	 * 
	 * @param loggedUser The user to test on.
	 * @param user Another user to test on.
	 * @param contact The contact used for the tests.
	 * @param groupChat The group chat to test on.
	 * @param users The test users.
	 */
	private static void groupAndContactTest(User loggedUser, User user,
			Contact contact, Chat groupChat, BST users) {
		
		removeGroupChatTest(loggedUser, groupChat);
		removeContactTest(loggedUser, contact);
		removeContactRequestTest(loggedUser, user);
		sendContactRequestTest(loggedUser, user, users);
	}
	
	/**
	 * Test the method that updates the last time a user logged in.
	 * 
	 * @param loggedUser The user to test on.
	 */
	private static void lastLoginTimeTest(User loggedUser) {
		System.out.println(loggedUser.getUsername() + " last logged time: "
				+ loggedUser.getLastLogTime());
		
		System.out.println("Updated last logged time");
		loggedUser.updateLastLogTime();
		
		System.out.println(loggedUser.getUsername() + " last logged time: "
				+ loggedUser.getLastLogTime());
	}
	
	/**
	 * Test the message counter method.
	 * 
	 * Integration testing with classes: MessageBuilder, TextMessage, Chat
	 * 
	 * @param loggedUser The user to test on.
	 * @param contactUser The contact user to test on.
	 * @param contactChat The contact chat to test on.
	 * @param groupUser The user to test on that is a
	 * member of the given group.
	 * 
	 * @param groupChat The group chat to test on.
	 */
	private static void messageCountTest(User loggedUser, User contactUser,
			Chat contactChat, User groupUser, Chat groupChat) {
		
		System.out.println("Total num of messages: "
				+ loggedUser.getTotalNumNewMessages());
		
		MessageBuilder message = new MessageBuilder(groupUser)
				.withText("Test");
		
		TextMessage textMessage = new TextMessage(message);
		
		System.out.println(groupUser.getUsername()
				+ " sent a message to group chat: " + groupChat.getName());
		groupChat.sendMessage(textMessage);
		
		message = new MessageBuilder(contactUser)
				.withText("Hello");
		textMessage = new TextMessage(message);
		
		System.out.println(contactUser.getUsername()
				+ " sent a message to the logged user");
		contactChat.sendMessage(textMessage);
		
		System.out.println("Total num of messages: "
			+ loggedUser.getTotalNumNewMessages());
	}
	
	/**
	 * Test the method that removes the contact from the given user
	 * and not vice versa.
	 * 
	 * Integration testing with classes: Chat, Contact
	 * 
	 * @param loggedUser The user to test on.
	 * @param user The other user used for this test.
	 */
	private static void oneSideContactRemovalTest(User loggedUser,
			User user) {
		
		ArrayList<User> members = new ArrayList<User>();
		members.add(loggedUser);
		members.add(user);
		
		Chat contactChat = new Chat(members);
		
		loggedUser.addContact(user, contactChat);
		user.addContact(loggedUser, contactChat);
		
		System.out.println(loggedUser.getUsername() + "'s contacts: ");
		for(Contact contact : loggedUser.getContacts()) {
			System.out.println(contact.getUser().getUsername());
		}
		
		System.out.println(user.getUsername() + "'s contacts: ");
		for(Contact contact : user.getContacts()) {
			System.out.println(contact.getUser().getUsername());
		}
		
		System.out.println("Remove logged user contact from only "
			+ user.getUsername() + "'s side");
		
		Contact loggedUserContact = new Contact(loggedUser);
		user.removeOneSideContact(loggedUserContact);
		
		System.out.println(loggedUser.getUsername() + "'s contacts: ");
		for(Contact contact : loggedUser.getContacts()) {
			System.out.println(contact.getUser().getUsername());
		}
		
		System.out.println(user.getUsername() + "'s contacts: ");
		for(Contact contact : user.getContacts()) {
			System.out.println(contact.getUser().getUsername());
		}
	}
	
	/**
	 * Test the method that reads all the users currently stored in data files.
	 * 
	 * Also tests the loading of contact request data.
	 * 
	 * Integration testing with class: BST
	 * 
	 * @param user The user to test on.
	 */
	private static void readAllUsersTest(User user) {
		writeUser(user,"Some Password");
		
		BST readUsers = readAllUsers();
		
		System.out.println("All stored users:");
		readUsers.inOrder();
		
		User readContact = User.loadContactRequestData(
				user.getUsername(), readUsers);
		
		boolean readContactCorrect =  user.equals(readContact);
		
		System.out.println("Read contact correctly: " + readContactCorrect);
	}
	
	/**
	 * Tests the reading of user data.
	 */
	private static void readUserDataTest() {
		String usernameData = "test";
		String passwordData = "some password";
		String nameData = "a name";
		String teleNumData = "07567846479";
		String dateData = "07/12/2002";
		String cityData = "Cardiff";
		String lastLoginData = "12/5/2016";
		String picturePathData = "A Path";
		
		String testUserData = passwordData + SEPARATOR
				+ nameData + SEPARATOR
				+ teleNumData + SEPARATOR
				+ dateData + SEPARATOR
				+ cityData + SEPARATOR
				+ lastLoginData + SEPARATOR
				+ picturePathData;
		
		User readUser = getUser(testUserData, usernameData);
		
		boolean usernameMatch = readUser.getUsername().equals(usernameData);
		boolean nameMatch = readUser.getName().equals(nameData);
		boolean teleNumMatch = readUser.getTNum().equals(teleNumData);
		boolean dateMatch = readUser.getBirthDate().equals(dateData);
		boolean cityMatch = readUser.getCity().equals(cityData);
		boolean lastLoginMatch = readUser.getLastLogTime().equals(
				lastLoginData);
		
		boolean picturePathMatch = readUser.getProfilePicturePath().equals(
				picturePathData);
		
		boolean allMatch = (usernameMatch && nameMatch && teleNumMatch
				&& dateMatch && cityMatch && lastLoginMatch
				&& picturePathMatch);
		
		System.out.println("Successfully reads correct given string of data: "
				+ allMatch);
	}
	
	/**
	 * Test the methods involved with the reading and writing of user data.
	 * 
	 * @param loggedUser The user to test on.
	 * @param user A test user.
	 */
	private static void readWriteUserTest(User loggedUser, User user) {
		rewriteUserTest(loggedUser);
		readAllUsersTest(user);
		readUserDataTest();
		forgotPasswordTest(loggedUser);
		lastLoginTimeTest(loggedUser);
	}
	
	/**
	 * Test the removal of contact requests.
	 * 
	 * @param loggedUser The user to test on.
	 * @param removeContactRequest The user who will be removed from
	 * the contact request list.
	 */
	private static void removeContactRequestTest(User loggedUser,
			User removeContactRequest) {
		
		System.out.println("Contact Requests: ");
		for(User request : loggedUser.getContactRequests()) {
			System.out.println(request.getUsername());
		}
		
		System.out.println("Remove contact request: "
				+ removeContactRequest.getUsername());
		
		loggedUser.removeContactRequest(removeContactRequest);
		
		System.out.println("Contact Requests: ");
		for(User request : loggedUser.getContactRequests()) {
			System.out.println(request.getUsername());
		}
	}
	
	/**
	 * Test the removal of contacts.
	 * 
	 * Integration testing with class: Contact
	 * 
	 * @param loggedUser The user to test on.
	 * @param removeContact The user who will be removed from the contact list.
	 */
	private static void removeContactTest(User loggedUser,
			Contact removeContact) {
		
		User contactUser = removeContact.getUser();
		
		System.out.println("Contacts: ");
		for(Contact contact : loggedUser.getContacts()) {
			System.out.println(contact.getUser().getUsername());
		}
		
		System.out.println("Removed contact: " + contactUser.getUsername());
		loggedUser.removeContact(removeContact);
		
		System.out.println("Contacts: ");
		for(Contact contact : loggedUser.getContacts()) {
			System.out.println(contact.getUser().getUsername());
		}
	}
	
	/**
	 * Test the removal of group chats.
	 * 
	 * Integration testing with class: Chat
	 * 
	 * @param loggedUser The user to test on.
	 * @param groupChat The group chat to remove.
	 */
	private static void removeGroupChatTest(User loggedUser, Chat groupChat) {
		System.out.println("Group chats: ");
		for(Chat chat : loggedUser.getGroupChats()) {
			System.out.println(chat.getName());
		}
		
		System.out.println("Removed chat: " + groupChat.getName());
		loggedUser.removeGroupChat(groupChat);
		
		System.out.println("Group chats: ");
		for(Chat chat : loggedUser.getGroupChats()) {
			System.out.println(chat.getName());
		}
	}
	
	/**
	 * Removes the contact from only this user and not vice versa.
	 * 
	 * @param contact The contact to remove.
	 */
	private void removeOneSideContact(Contact contact) {
		m_contacts.remove(contact);
		
		Contact.saveContacts(this);
	}
	
	/**
	 * Test the rewriting of a user.
	 * 
	 * @param loggedUser The user to test on.
	 */
	private static void rewriteUserTest(User loggedUser) {
		String newBirthDate = "02/02/2002";
		
		System.out.println("Changing user's birth date: " + newBirthDate);
		
		loggedUser.setBirthDate(newBirthDate);
		
		System.out.println("Rewriting user");
		
		rewriteUser(loggedUser);
		
		System.out.println("Loading user: " + loggedUser.getUsername());
		User loadedUser = loadUser(loggedUser.getUsername());
		
		System.out.println("Loaded user's birth date: "
			+ loadedUser.getBirthDate());
	}
	
	/**
	 * Test the saving and loading of users data.
	 * 
	 * Tests the writing and loading of a user, its contact requests,
	 * contacts and group chats, and with the loaded user, it displays
	 * the description along with the display of its profile picture.
	 * 
	 * Also tests user validation.
	 * 
	 * Integration testing with classes: ChatIO, Contact, ImageComponent
	 * 
	 * @param loggedUser The user to test on.
	 * @param anotherRequest A contact request to send to the user.
	 * @param users The test users.
	 * @return The loaded user from the test.
	 */
	private static User saveLoadTest(User loggedUser, User anotherRequest,
			BST users) {
		
		String password = "secret";
		
		System.out.println("Saving user");
		System.out.println("Saving in format: "
				+ loggedUser.getSaveData(password));
		
		writeUser(loggedUser, password);
		loggedUser.saveContactRequests();		
		ChatIO.saveGroupChats(loggedUser);
		loggedUser.saveContactRequest(anotherRequest);
		
		System.out.println("Should save contact request data as: "
				+ loggedUser.saveContactRequestData());
		
		Contact.saveContacts(loggedUser);
		
		String incorrectPassword = "incorrect";
		
		System.out.println("Validating user passwords");
		boolean result = validateUserDetails(loggedUser.getUsername(),
				incorrectPassword);
		
		System.out.println("Password match with " + incorrectPassword
			+  ": "+ result);
		
		result = validateUserDetails(loggedUser.getUsername(),
				password);
		
		System.out.println("Password match with " + password +  ": "+ result);
		
		System.out.println("Loading user: " + loggedUser.getUsername());
		User loadUser = loadUser(loggedUser.getUsername());
		User.loadContactRequests(loadUser, users);
		Contact.loadContacts(loadUser, users);
		ChatIO.loadGroupChat(loadUser, users);
		
		System.out.println(loadUser.getDescription());
		
		//Display profile picture
		JFrame frame = new JFrame("Profile Picture Image Test");
		frame.setSize(TEST_PICTURE_FRAME_SIZE);
		ImageComponent profileImageComp = new ImageComponent(
				loadUser.getProfilePicture());
		
		frame.add(profileImageComp);
		
		frame.setVisible(true);
		
		return loadUser;
	}
	
	/**
	 * Tests the sending of contact requests.
	 * 
	 * @param loggedUser The user to test on.
	 * @param user The user who will receive the contact request.
	 * @param users The test users.
	 */
	private static void sendContactRequestTest(User loggedUser, User user,
			BST users) {
		
		System.out.println(user.getUsername() + "'s Contact Requests: ");
		for(User contactRequest : user.getContactRequests()) {
			System.out.println(contactRequest.getUsername());
		}
		
		System.out.println("Sent contact request to: "
				+ user.getUsername());
		
		loggedUser.sendContactRequest(user, users);
		
		System.out.println(user.getUsername() + "'s Contact Requests: ");
		for(User contactRequest : user.getContactRequests()) {
			System.out.println(contactRequest.getUsername());
		}
	}
	
	/**
	 * Set the list of contact requests to the given list of contact requests.
	 * 
	 * @param contactRequests The list of contact requests.
	 */
	private void setContactRequests(ArrayList<User> contactRequests) {
		m_contactRequests = contactRequests;
	}
	
	/**
	 * Tests the setters of User.
	 * 
	 * @param user The user to test on.
	 */
	private static void settersTest(User user) {
		System.out.println(user.getDescription());
		
		user.setCity("Swansea");
		user.setName("Another name");
		user.setProfilePicturePath("Logo.png");
		user.setTNum("07987654321");
		
		System.out.println(user.getDescription());
	}
	
	private static final int UNIT_SEPARATOR_NUM = 31;
	private static final String SEPARATOR = Character.toString(
			((char) UNIT_SEPARATOR_NUM));
	
	private static final String USERS_PATH = "Users/";
	private static final String EXTENSION = ".txt";
	private static final String CONTACT_REQUESTS_PATH = "Contacts Requests/";
	private static final int PASSWORD_POS = 0;
	private static final int NAME_POS = 1;
	private static final int TELE_NUM_POS = 2;
	private static final int DATE_POS = 3;
	private static final int CITY_POS = 4;
	private static final int LAST_LOGIN_POS = 5;
	private static final int IMAGE_PATH_POS = 6;
	
	private static final Dimension TEST_PICTURE_FRAME_SIZE =
			new Dimension(100,100);
	
	//username of the User.
	private String m_username;
	
	//name of the User.
	private String m_name;
	
	//telephone number of the User.
	private String m_telNumber;
	
	//date that User was born on (DD/MM/YYYY format).
	private String m_birthDate;
	
	//current city that User resides in.
	private String m_currCity;
	
	//last time User logged off the Skypertawe.
	private String m_lastLogTime;
	
	//path to the profile picture of the User.
	private String m_profilePicture;
	
	private ArrayList<Contact> m_contacts = new ArrayList<Contact>();
	private ArrayList<User> m_contactRequests = new ArrayList<User>();
	private ArrayList<Chat> m_groupChats = new ArrayList<Chat>();
}
