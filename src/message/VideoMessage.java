package message;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JPanel;

import gui.VideoMsgGUI;
import user.User;

/**
 * @File	-VideoMessage.java
 * @author	-Alikhan Tagybergen
 * @date	-29/03/2017
 * @brief	-This class handles all the information that will need to be
 * stored by a Video message.
 *
 * Stores the Video file location both in memory and in storage, also has a
 * method that will handle how the specific type of message should be displayed
 * and any user interaction that needs to be handled.
 */
public class VideoMessage extends DescriptionMessage {

    /**
     * @return The message description, this includes the description from the
     * superclass and the path of the video file being used.
     */
    @Override
    public String getDescription() {
        return "Video Message: " + super.getDescription() + '\n'
                + "Directs to: " + m_filePath;
    }
	
    /**
     * @return The path of the video file being used.
     */
    public String getFilePath() {
        return m_filePath;
    }

    /**
     * Adds the components needed to display the message's contents related to
     * this specific type.
     *
     * Adds the button to the video file so that the user can click to open the
     * video in a separate GUI.
     *
     * @param panel The panel to add the contents to.
     */
    @Override
    public void getTypeContents(JPanel panel) {
        JButton videoButton = new JButton("See video");
        VideoMsgGUI vidMsg = new VideoMsgGUI();
        vidMsg.importFile(m_filePath);
        panel.add(videoButton);
        videoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                vidMsg.runVideoGui();
            }
        });
    }
    
    /**
     * @return The data specifically about this type of message that needs to
     * be stored.
     *
     * Stores its type and the file path.
     */
    @Override
    protected String getTypeSaveData() {
        return TYPE + MESSAGE_TOKEN_SEP + m_filePath;
    }

    /**
     * @param filePath The new video file path the message should use.
     */
    public void setFilePath(String filePath) {
        this.m_filePath = filePath;
    }

    /**
     * Loads the data specifically related to this type of message.
     *
     * Reads the data that describes the file path.
     *
     * @param tokens The tokens that were encountered when reading.
     * @param message The message currently being built.
     * @return The loaded file message.
     */
    public static Message loadMessageTypeData(String[] tokens,
            MessageBuilder message) {

        String filePath = tokens[FILE_PATH_POS];

        return new VideoMessage(message).withFilePath(filePath);
    }
    
    /**
     * Constructor just uses constructor of DescriptionMessage class.
     *
     * @param message The message builder that will initialise the message
     * object.
     */
    public VideoMessage(MessageBuilder message) {
        super(message);
    }

    /**
     * @param filePath The file path the message should have.
     * @return This object (allows for multiple "with" calls all at once).
     */
    public VideoMessage withFilePath(String filePath) {
        m_filePath = filePath;
        return this;
    }
    
    /**
     * Tests the methods of the class VideoMessage.
     *
     * Integration testing with classes: User, FileManager, MessageBuilder,
     * Message
     *
     * @param args Not used.
     */
    public static void main(String[] args) {
        User sender = new User().withUsername("Sender");

        Scanner in = new Scanner(System.in);
        System.out.println("Please enter an absolute path to the video file "
        										+ "(.mp4 or .flv format)");
        testFile = in.nextLine();
        MessageBuilder testMessage = new MessageBuilder(sender)
                .withText("Testing")
                .withDate("17/03/2017")
                .withTime("21:07");

        String[] givenTokens = new String[]{"Sender", "hello", "10/10/2000",
            "10:00", "", "", Integer.toString(TYPE), "test directory"};

        Message loadedMessage = loadMessageTypeData(givenTokens, testMessage);
        VideoMessage videoMessage = (VideoMessage) loadedMessage;

        System.out.println(videoMessage.getFilePath());
        videoMessage.withFilePath("another directory");
        System.out.println(videoMessage.getFilePath());
        videoMessage.setFilePath(testFile);
        System.out.println(videoMessage.getFilePath());

        System.out.println(loadedMessage.getTypeSaveData());
        System.out.println(loadedMessage.getDescription());

        testContentDisplay(loadedMessage);
    }
    
    
    public static final int TYPE = 4;

    private static final int FILE_PATH_REL_POS = 0;
    private static final int FILE_PATH_POS = Message.MESSAGE_TYPE_START_POS
            + FILE_PATH_REL_POS;
    //Set testfile manually or use scanner input if main method. 
    private static String testFile = "";
    private String m_filePath;
}
