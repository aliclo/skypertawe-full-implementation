package message;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;

import components.ImageComponent;
import filemanager.FileManager;
import user.User;

/**
 * @File ImageMessage.java
 * @author Annabel Kimber
 * @date 24 March 2017
 * @class ImageMessage
 * @brief This class handles all the information that will need to be stored by
 *        an Image Message.
 * 
 *        Stores the image file in both memory and storage. It allows the user
 *        to search through their files in order to choose a picture to send.
 */

public class ImageMessage extends DescriptionMessage {

	/**
	 * Constructor just uses constructor of DescriptionMessage class.
	 * 
	 * @param message The message builder that will initialise the message
	 * object.
	 */
	public ImageMessage(MessageBuilder message) {
		super(message);
	}

	/**
	 * @return The message description, this includes the description from the
	 *         superclass and the path of the file being used.
	 */
	@Override
	public String getDescription() {
		return "Image Message: " + super.getDescription() + '\n'
				+ "Directs to: " + m_filePath;
	}

	/**
	 * @return the filePath
	 */
	public String getFilePath() {
		return m_filePath;
	}

	/**
	 * @param filePath the file path to set
	 */
	public void setFilePath(String filePath) {
		this.m_filePath = filePath;
	}
	
	/**
	 * Adds the components needed to display the message's contents related to
	 * this specific type.
	 * 
	 * Adds the link to the file that the user can click to open the file.
	 * 
	 * @param panel The panel to add the contents to.
	 */
	@Override
	public void getTypeContents(JPanel panel) {
		try {
			File imageFile = new File(m_filePath);
			BufferedImage loadImage = ImageIO.read(imageFile);

			ImageComponent component = new ImageComponent(loadImage);

			panel.add(component);
		} catch (IOException e) {
			JLabel error = new JLabel("Unable to load image");
			panel.add(error);
		}
	}

	/**
	 * Loads the data specifically related to this type of message.
	 * 
	 * Reads the data that describes the file path.
	 * 
	 * @param tokens The tokens that were encountered when reading.
	 * @param message The message currently being built.
	 * @return The loaded file message.
	 */
	public static Message loadMessageTypeData(String[] tokens,
			MessageBuilder message) {

		String filePath = tokens[FILE_PATH_POS];

		return new ImageMessage(message).withFilePath(filePath);
	}
	
	/**
	 * Tests the methods of the class ImageMessage.
	 * 
	 * Integration testing with classes: User, FileManager, MessageBuilder,
	 * Message
	 * 
	 * @param args Not used.
	 */
	public static void main(String[] args) {
		User sender = new User().withUsername("Sender");

		FileManager.createFileWhenNonExistent(TEST_FILE);

		MessageBuilder testMessage = new MessageBuilder(sender)
				.withText("Testing")
				.withDate("17/03/2017")
				.withTime("21:07");

		String[] givenTokens = new String[] {
				"Sender", "hello", "10/10/2000", "10:00",
				"", "", Integer.toString(TYPE), "test directory" };

		Message loadedMessage = loadMessageTypeData(givenTokens, testMessage);
		ImageMessage fileMessage = (ImageMessage) loadedMessage;

		System.out.println(fileMessage.getFilePath());
		fileMessage.withFilePath("another directory");
		System.out.println(fileMessage.getFilePath());
		fileMessage.setFilePath(TEST_FILE_PATH);
		System.out.println(fileMessage.getFilePath());

		System.out.println(loadedMessage.getTypeSaveData());
		System.out.println(loadedMessage.getDescription());
		System.out.println(loadedMessage);
		
		JFileChooser imageChooser = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter(
				"Image Files", "jpg", "png", "gif", "jpeg");
		
		imageChooser.setFileFilter(filter);
		
		imageChooser.showOpenDialog(null);
		
		String selectedPath = imageChooser.getSelectedFile().getAbsolutePath();
		
		fileMessage.setFilePath(selectedPath);
		
		testContentDisplay(loadedMessage);
	}

	/**
	 * @param filePath The file path the message should have.
	 * @return This object (allows for multiple "with" calls all at once).
	 */

	public ImageMessage withFilePath(String filePath) {
		m_filePath = filePath;

		return this;

	}

	/**
	 * @return The data specifically about this type of message that needs
	 * to be stored.
	 * 
	 * Stores its type and the file path.
	 */

	@Override
	protected String getTypeSaveData() {
		return TYPE + MESSAGE_TOKEN_SEP + m_filePath;
	}
	
	private String m_filePath;

	public static final int TYPE = 3;
	private static final int FILE_PATH_REL_POS = 0;
	private static final int FILE_PATH_POS = Message.MESSAGE_TYPE_START_POS
			+ FILE_PATH_REL_POS;

	// Locate to the home directory
	private static final String TEST_FILE_PATH = System.getProperty(
			"user.home") + "/testing/test.txt";

	private static final File TEST_FILE = new File(TEST_FILE_PATH);

}
