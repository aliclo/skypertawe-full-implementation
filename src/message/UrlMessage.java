package message;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import user.User;

/**
 * @File	-UrlMessage.java
 * @author	-Rhys John Miller, edited by Annabel Kimber
 * @date	-Edited on 9/3/2017
 * @brief	-This class handles all the information that will need to be
 * 			stored by a URL message.
 * 
 * Stores the URL link both in memory and in storage, also has a method
 * that will handle how the specific type of message should be displayed
 * and any user interaction that needs to be handled.
 */
public class UrlMessage extends DescriptionMessage {
	
	/**
	 * Constructor just uses constructor of DescriptionMessage class.
	 * 
	 * @param message The message builder that will initialise the
	 * message object.
	 */
	public UrlMessage(MessageBuilder messsage) {
		super(messsage);
	}
	
	/**
	 * @return The message description, this includes the description
	 * from the superclass and the URL link.
	 */
	@Override
	public String getDescription() {
		return "URL Message: " + super.getDescription() + '\n'
				+ "Directs to: " + m_link;
	}
	
	/**
	 * @return The URL link.
	 */
	public String getLink() {
    	return m_link;
    }
    
	/**
	 * @param link The new URL link the message should use.
	 */
    public void setLink(String link) {
    	m_link = link;
    }

    
    /**
	 * Adds the components needed to display the message's contents related to
	 * this specific type.
	 * 
	 * Adds the URL link that the user can click to open a webpage to the link.
	 * 
	 * @param panel The panel to add the contents to.
	 */
	@Override
	public void getTypeContents(JPanel panel) {
		JLabel linkLabel = new JLabel(getLink());
		linkLabel.setForeground(Color.BLUE);
		
		try {
			URI site = new URI(getLink());
			
			linkLabel.addMouseListener(new MouseListener() {

				@Override
				public void mouseClicked(MouseEvent e) {
					openWebpage(site);
				}

				@Override
				public void mouseEntered(MouseEvent e) {
					
				}

				@Override
				public void mouseExited(MouseEvent e) {
					
				}

				@Override
				public void mousePressed(MouseEvent e) {
					
				}

				@Override
				public void mouseReleased(MouseEvent e) {
					
				}
	    	});
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		
		panel.add(linkLabel);
	}
	
	/**
	 * Loads the data specifically related to this type of message.
	 * 
	 * Reads the data that describes the URL link.
	 * 
	 * @param tokens The tokens that were encountered when reading.
	 * @param message The message currently being built.
	 * @return The loaded URL message.
	 */
	public static Message loadMessageTypeData(String[] tokens,
			MessageBuilder message) {
		
		String link = tokens[URL_POS];
		
		return new UrlMessage(message).withLink(link);
	}
	
	/**
	 * Tests the methods of the class UrlMessage.
	 * 
	 * Integration testing with classes: User, MessageBuilder, Message
	 * 
	 * @param args Not used.
	 */
	public static void main(String[] args) {
		User sender = new User().withUsername("Sender");
		
		MessageBuilder testMessage = new MessageBuilder(sender)
			.withText("Testing")
			.withDate("17/03/2017")
			.withTime("21:07");
		
		String[] givenTokens = new String[] {"Sender", "hello", "10/10/2000",
				"10:00", "", "", Integer.toString(TYPE), "www.test.co.uk"};
		
		Message loadedMessage = loadMessageTypeData(givenTokens, testMessage);
		UrlMessage urlMessage = (UrlMessage) loadedMessage;
		
		System.out.println(urlMessage.getLink());
		urlMessage.withLink("www.hello.co.uk");
		System.out.println(urlMessage.getLink());
		urlMessage.setLink("www.world.co.uk");
		System.out.println(urlMessage.getLink());
		
		System.out.println(loadedMessage.getTypeSaveData());
		System.out.println(loadedMessage.getDescription());
		
		try {
			URI webDestination = new URI("www.test.co.uk");
			openWebpage(webDestination);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		
		testContentDisplay(loadedMessage);
	}
	
	/**
	 * Opens the web page from the given link using a browser if the
	 * desktop supports it.
	 * 
	 * @param uri The link to a web page.
	 */
	public static void openWebpage(URI uri) {
	    Desktop desktop =  Desktop.getDesktop();
	    if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
	        try {
	            desktop.browse(uri);
	        } catch (Exception e) {
	            JOptionPane.showMessageDialog(null, "Invalid URL");
	        }
	    }
	}
	
	/**
	 * @param link The link the message should have.
	 * @return This object (allows for multiple "with" calls all at once).
	 */
	public UrlMessage withLink(String link) {
		m_link = link;
		
		return this;
	}
    
	/**
	 * @return The data specifically about this type of message that
	 * needs to be stored.
	 * 
	 * Stores its type and the URL link.
	 */
	@Override
	protected String getTypeSaveData() {
		return TYPE + MESSAGE_TOKEN_SEP + m_link;
	}
	
	public static final int TYPE = 1;
	
	private static final int URL_REL_POS = 0;
	private static final int URL_POS = Message.MESSAGE_TYPE_START_POS
			+ URL_REL_POS;
	
	private String m_link;
	
}
