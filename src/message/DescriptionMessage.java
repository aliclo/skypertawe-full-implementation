package message;

import user.User;

/**
 * @File	-DescriptionMessage.java
 * @author	-Alistair Cloney
 * @date	-9\3\2017
 * @see		-Message.java
 * @brief	Enforces the restriction for URL, File, Image and Video messages
 * 			to have descriptions that don't exceed the length of 50 characters.
 */
public abstract class DescriptionMessage extends Message {
	
	/**
	 * Constructor just uses constructor of Message class.
	 * 
	 * @param message The message builder that will initialise the
	 * message object.
	 */
	public DescriptionMessage(MessageBuilder message) {
		super(message);
	}
	
	/**
	 * @return The message description, this includes the max amount of
	 * characters allowed along with the description from the superclass.
	 */
	@Override
	public String getDescription() {
		return "Max Character Limit: " + MAX_CHAR + '\n'
				+ super.getDescription();
	}
	
	/**
	 * Tests the methods of the class DescriptionMessage.
	 * 
	 * Integration testing with classes: User, MessageBuilder, UrlMessage
	 * 
	 * @param args Not used.
	 */
	public static void main(String[] args) {
    	User sender = new User().withUsername("Sender");
    	
    	MessageBuilder builder = new MessageBuilder(sender).withText("Test");
    	
    	//Test from a sub class
    	UrlMessage urlMessage = new UrlMessage(builder);
    	
    	String textOverLimit = "";
    	
    	int totalAmount = MAX_CHAR+TEST_AMOUNT_OVER_LIMIT;
    	for(int i = 0; i < totalAmount; i++) {
    		textOverLimit += "a";
    	}
    	
    	urlMessage.setText(textOverLimit);
    	
    	System.out.println("Text length: " + urlMessage.getText().length());
    	
    	System.out.println(urlMessage.getDescription());
    }
	
	/**
     * @param text The text/description of the message, if above the character
     * limit, it cuts off any characters at the end that are above this limit.
     */
	@Override
    protected void setText(String text) {
		if(text.length() <= MAX_CHAR) {
			super.setText(text);
		} else {
			super.setText(text.substring(0, MAX_CHAR));
		}
    }
	
	private static final int MAX_CHAR = 50;
    private static final int TEST_AMOUNT_OVER_LIMIT = 10;
	
}
