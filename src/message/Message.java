package message;
import java.awt.Dimension;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import dataStructure.BST;
import user.User;
/**
 * @File	-Message.java
 * @author	-rhysj, edited by Alistair Cloney
 * @date	-9/3/2017
 * @see		-DescriptionMessage.java, TextMessage.java, UrlMessage.java,
 * FileTransferMessage.java, MessageBuilder.java
 * 
 * @brief	Handles the storing of the message both in memory and in storage,
 * 			also handles how the message should be displayed (the components
 * 			that are needed).
 * 
 * All control of how a message is both loaded and saved is given to this class
 * and its subclasses.
 * 
 * All control of how the message contents should be displayed is given to this
 * class and its subclasses.
 * 
 * Message data is stored in a plain text format using separators to separate
 * units of data, this Message class provides methods to load and save this
 * data, the subclasses of Message will provide methods that complete the
 * loading and saving of a specific type of message.
 */
public abstract class Message {
    
	/**
	 * Create the message from the message builder.
	 * 
	 * @param message The message builder that has the data required
	 * to create the message.
	 */
	public Message(MessageBuilder message) {
    	m_author = message.getAuthor();
    	m_received = message.getReceived();
    	m_time = message.getTime();
    	m_date = message.getDate();
    	m_text = message.getText();
    	m_edit = message.getEdit();
    }
	
	/**
     * @return The author of the message.
     */
    public User getAuthor() {
        return m_author;
    }
    
    /**
     * @return The contents of the message in the form of components.
     */
    public JPanel getContents() {
    	JPanel panel = new JPanel();
    	panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
    	
    	String authorsString = "Sent by " + getAuthor().getUsername();
    	
    	if(getEdit() != null) {
    		authorsString += ", edited by " + getEdit().getUsername();
    	}
    	
    	JLabel author = new JLabel(authorsString);
    	
    	panel.add(author);
    	
    	//Add the specific contents of the type of message
    	getTypeContents(panel);
    	
    	JLabel text = new JLabel(getText());
    	
    	panel.add(text);
    	
    	JLabel dateTime = new JLabel(getDate() + " " + getTime());
    	
    	panel.add(dateTime);
    	
    	JLabel received = new JLabel(getReceived());
    	
    	panel.add(received);
    	
    	return panel;
    }
    
    /**
     * @return The date of when the message was sent.
     */
    public String getDate() {
        return m_date;
    }
    
    /**
     * @return The description about the message which includes the values of
     * its members.
     */
    public String getDescription() {
    	String authorDesc = "Author: " + m_author.getUsername();
    	String receivedDesc = getReceived();
    	String timeDesc = "Time Sent: " + m_time;
    	String dateDesc = "Date Sent: " + m_date;
    	String text = "Text: " + m_text;
    	
    	String desc = authorDesc
    			+ '\n' + receivedDesc
    			+ '\n' + timeDesc
    			+ '\n' + dateDesc
    			+ '\n' + text;
    	
    	if(m_edit != null) {
    		desc += '\n' + "Edited: " + m_edit.getUsername();
    	}
    	
    	return desc;
    }
	
    /**
     * @return Get the user who edited the message last (null if no one
     * has edited the message).
     */
    public User getEdit() {
	    return m_edit;
	}
    
    /**
     * Set who edited the message last.
     * 
     * @param edit The user who has edited the message.
     */
    public void setEdit(User edit) {
    	m_edit = edit;
    }
    
    /**
     * @return The set of (user) recipients that will (or has) received
     * the message.
     */
    public Set<User> getRecipients() {
        return m_received.keySet();
    }
    
    /**
     * @return The plain text data that will store the message along with its
     * specific type.
     */
    public String getSaveData() {
    	String edit = "";
    	
    	if(getEdit() != null) {
    		edit = getEdit().getUsername();
    	}
    	
    	return getAuthor().getUsername() + MESSAGE_TOKEN_SEP + getText()
    	+ MESSAGE_TOKEN_SEP + getDate() + MESSAGE_TOKEN_SEP + getTime()
    	+ MESSAGE_TOKEN_SEP + getReceivedSaveData() + MESSAGE_TOKEN_SEP
    	+ edit + MESSAGE_TOKEN_SEP + getTypeSaveData();
    }
    
	/**
     * @return The text of the message.
     */
    public String getText() {
        return m_text;
    }
    
    /**
     * @return The time the message was sent.
     */
    public String getTime() {
        return m_time;
    }
    
    /**
     * Add a new (user) recipient to the message, this is so that the given
     * user can be accounted to have received the message or not.
     * 
     * @param user The user recipient to add.
     */
    public void addRecipient(User user) {
    	m_received.put(user, false);
    }
	
    /**
     * Checks whether the given user has seen this message or not.
     * 
     * @param user The user to check.
     * @return Whether the user has seen this message or not.
     */
    public boolean hasSeenMessage(User user) {
    	return m_received.get(user);
    }
    
    /**
     * Loads the message from the given data by splitting it into tokens
     * and storing the individual pieces of data, and then letting its
     * sub classes deal with loading the rest depending on which message
     * is being loaded.
     * 
     * @param data The data to read.
     * @param users The existing users of Skypertawe.
     * @return The loaded message.
     */
	public static Message loadMessage(String data, BST users) {
    	String[] tokens = data.split(MESSAGE_TOKEN_SEP);
    	
    	User author = users.getProfileRoot(tokens[AUTHOR_POS]);
    	String text = tokens[TEXT_POS];
    	String date = tokens[DATE_POS];
    	String time = tokens[TIME_POS];
    	String receivedString = tokens[RECEIVED_POS];
    	
    	HashMap<User, Boolean> received = getReceived(receivedString, users);
    	
    	String editUser = tokens[EDIT_POS];
    	User edit = users.getProfileRoot(editUser);
    	
    	MessageBuilder message = new MessageBuilder()
    			.withAuthor(author)
		    	.withText(text)
		    	.withTime(time)
		    	.withDate(date)
		    	.withReceived(received)
		    	.withEdit(edit);
    	
    	//The method to load the rest of the message depends on its type
    	int type = Integer.parseInt(tokens[TYPE_POS]);
    	
    	switch(type) {
	    	case TextMessage.TYPE:
	    		return TextMessage.loadMessageTypeData(tokens, message);
	    		
			case UrlMessage.TYPE:
	    		return UrlMessage.loadMessageTypeData(tokens, message);
	    		
			case FileTransferMessage.TYPE:
	    		return FileTransferMessage.loadMessageTypeData(tokens,
	    			message);
	    	
			case ImageMessage.TYPE:
				return ImageMessage.loadMessageTypeData(tokens, message);
	    		
			case VideoMessage.TYPE:
				return VideoMessage.loadMessageTypeData(tokens, message);
    	}
    	
    	return null;
    }
	
	/**
	 * Test the methods of the class Message.
	 * 
	 * Integration testing with classes: User, BST, MessageBuilder, TextMessage
	 * 
	 * @param args Not used.
	 */
    public static void main(String[] args) {
    	User userAlice = new User().withUsername("Alice");
		User userBob = new User().withUsername("Bob");
		User userJake = new User().withUsername("Jake");
		User userJess = new User().withUsername("Jess");
    	
		BST users = new BST();
		users.insertUser(userAlice);
		users.insertUser(userBob);
		users.insertUser(userJake);
		users.insertUser(userJess);
		
		loadMessageTest(userAlice, userBob, userJake, userJess, users);
		
		loadReceivedTest(userAlice, userBob, userJake, userJess, users);
		
		MessageBuilder messageBuild = new MessageBuilder(userAlice)
				.withText("Hello");
		
		TextMessage textMessage = new TextMessage(messageBuild);
		
		testRecipients(textMessage, userBob);
		
		testSetters(textMessage, userBob);
		testGetters(textMessage);
		
		testMessageSaveFormat(textMessage);
		
		testContentDisplay(textMessage);
    }
    
    /**
     * Set the given user to have seen this message.
     * 
     * @param user The user whose seen this message.
     */
    public void seenMessage(User user) {
    	m_received.put(user, true);
    }
    
	/**
	 * @param author The author of this message.
	 * @return This object (allows for multiple "with" calls all at once).
	 */
    public Message withAuthor(User author) {
    	m_author = author;
    	
    	return this;
    }
    
	/**
	 * @param date The date of when this message was sent.
	 * @return This object (allows for multiple "with" calls all at once).
	 */
    public Message withDate(String date) {
    	m_date = date;
    	
    	return this;
    }
    
	/**
	 * @param edit The editor of this message.
	 * @return This object (allows for multiple "with" calls all at once).
	 */
    public Message withEdit(User edit) {
    	m_edit = edit;
    	
    	return this;
    }
    
	/**
	 * @param received The HashMap of receivers of this message.
	 * @return This object (allows for multiple "with" calls all at once).
	 */
    public Message withReceived(HashMap<User, Boolean> received) {
    	m_received = received;
    	
    	return this;
    }
    
	/**
	 * @param text The text of this message.
	 * @return This object (allows for multiple "with" calls all at once).
	 */
    public Message withText(String text) {
    	m_text = text;
    	
    	return this;
    }
    
	/**
	 * @param time The time of when this message was sent.
	 * @return This object (allows for multiple "with" calls all at once).
	 */
    public Message withTime(String time) {
    	m_time = time;
    	
    	return this;
    }
    
    /**
     * Adds the contents of the specific type of message to the given panel.
     * 
     * @param panel The panel the contents are added to.
     */
    protected abstract void getTypeContents(JPanel panel);
    
    /**
     * @return The plain text data from the specific type of message.
     */
    protected abstract String getTypeSaveData();
    
    /**
     * Sets the date of when the message was sent.
     * 
     * @param date The date of when the message was sent.
     */
    protected void setDate(String date) {
        this.m_date = date;
    }
    
    /**
     * Sets the text of the message.
     * 
     * @param text The text of the message.
     */
    protected void setText(String text) {
    	m_text = text;
    }

    /**
     * Sets the time of when the message was sent.
     * 
     * @param time The time of when the message was sent.
     */
    protected void setTime(String time) {
        this.m_time = time;
    }
    
    /**
     * Tests the content display and specific content display of
     * a given message.
     * 
     * @param message The message to test.
     */
	protected static void testContentDisplay(Message message) {
		JFrame testTypeContentsFrame = new JFrame("Test Type Contents");
		testTypeContentsFrame.setSize(TEST_FRAME_SIZE);
		JPanel contentsPanel = (JPanel) testTypeContentsFrame.getContentPane();
		
		message.getTypeContents(contentsPanel);
		
		testTypeContentsFrame.setVisible(true);
		
		JFrame testContentsFrame = new JFrame("Test Contents");
		testContentsFrame.setSize(TEST_FRAME_SIZE);
		testContentsFrame.add(message.getContents());
		
		testContentsFrame.setVisible(true);
	}
	
	/**
	 * @return A string that states who (if anyone) has received this message.
	 */
	private String getReceived() {
    	String received = "";
    	boolean anyReceived = false;
    	
    	for(User user : getRecipients()) {
    		//Doesn't note the author to have received the message
    		if(user != m_author && hasSeenMessage(user)) {
    			if(anyReceived) {
    				received += ", ";
    			} else {
    				received = "Seen by ";
    				
    				anyReceived = true;
    			}
    			
    			received += user.getUsername();
    		}
    	}
    	
    	if(!anyReceived) {
    		received = "Seen by no one";
    	}
    	
    	return received;
    }
	
	/**
	 * Reads the given data to generate a HashMap of users indicating who's
	 * seen the message.
	 * 
	 * @param data The data to read.
	 * @param users The existing users of Skypertawe.
	 * @return The generated HashMap indicating which users have seen
	 * the message.
	 */
	private static HashMap<User, Boolean> getReceived(String data, BST users) {
    	HashMap<User, Boolean> received = new HashMap<User, Boolean>();
    	
    	Scanner in = new Scanner(data);
    	in.useDelimiter(RECEIVED_TOKEN_SEP);
    	
    	while(in.hasNext()) {
    		String username = in.next();
    		User user = users.getProfileRoot(username);
    		boolean seenMessage = in.nextBoolean();
    		
    		received.put(user, seenMessage);
    	}
    	
    	in.close();
    	
    	return received;
    }
    
	/**
	 * @return Plain text data to store who received this message.
	 */
    private String getReceivedSaveData() {
    	String data = "";
    	
    	for(User user : getRecipients()) {
    		data += user.getUsername() + RECEIVED_TOKEN_SEP
    				+ hasSeenMessage(user) + RECEIVED_TOKEN_SEP;
    	}
    	
    	return data;
    }
    
    /**
     * Tests the loading of the individual message types.
     * 
     * Integration testing with classes: User, TextMessage, UrlMessage,
     * FileTransferMessage
     * 
     * @param userA A test user.
     * @param userB A test user.
     * @param userC A test user.
     * @param userD A test user.
     * @param users Test users.
     */
    private static void loadMessageTest(User userA, User userB, User userC,
    		User userD, BST users) {
    	
    	String received = userB.getUsername() + RECEIVED_TOKEN_SEP + "true"
    			+ RECEIVED_TOKEN_SEP + userC.getUsername() + RECEIVED_TOKEN_SEP
    			+ "false" + RECEIVED_TOKEN_SEP + userD.getUsername()
    			+ RECEIVED_TOKEN_SEP + "false";
		
    	String textMessageData = userA.getUsername() + MESSAGE_TOKEN_SEP
    			+ "Some text" + MESSAGE_TOKEN_SEP
    			+ "17/03/2017" + MESSAGE_TOKEN_SEP
    			+ "18:48" + MESSAGE_TOKEN_SEP
    			+ received + MESSAGE_TOKEN_SEP
    			+ userB.getUsername() + MESSAGE_TOKEN_SEP
    			+ TextMessage.TYPE;
    	
    	TextMessage textMessage = (TextMessage) loadMessage(textMessageData,
    			users);
    	
    	System.out.println(textMessage.getDescription());
    	
    	received = userB.getUsername() + RECEIVED_TOKEN_SEP + "false"
    			+ RECEIVED_TOKEN_SEP + userA.getUsername()
    			+ RECEIVED_TOKEN_SEP + "true" + RECEIVED_TOKEN_SEP
    			+ userD.getUsername() + RECEIVED_TOKEN_SEP + "true";
    	
    	String urlMessageData = userC.getUsername() + MESSAGE_TOKEN_SEP
    			+ "A URL message" + MESSAGE_TOKEN_SEP
    			+ "17/03/2017" + MESSAGE_TOKEN_SEP
    			+ "19:08" + MESSAGE_TOKEN_SEP
    			+ received + MESSAGE_TOKEN_SEP
    			+ "" + MESSAGE_TOKEN_SEP
    			+ UrlMessage.TYPE + MESSAGE_TOKEN_SEP + "www.test.co.uk";
    	
    	UrlMessage urlMessage = (UrlMessage) loadMessage(urlMessageData,
    			users);
    	
    	System.out.println(urlMessage.getDescription());
    	
    	received = userB.getUsername() + RECEIVED_TOKEN_SEP + "false"
    			+ RECEIVED_TOKEN_SEP + userA.getUsername()
    			+ RECEIVED_TOKEN_SEP + "false" + RECEIVED_TOKEN_SEP
    			+ userC.getUsername() + RECEIVED_TOKEN_SEP + "false";
    	
    	String fileMessageData = userD.getUsername() + MESSAGE_TOKEN_SEP
    			+ "A File message" + MESSAGE_TOKEN_SEP
    			+ "17/03/2017" + MESSAGE_TOKEN_SEP
    			+ "19:08" + MESSAGE_TOKEN_SEP
    			+ received + MESSAGE_TOKEN_SEP
    			+ "" + MESSAGE_TOKEN_SEP
    			+ FileTransferMessage.TYPE + MESSAGE_TOKEN_SEP + "C:/Test";
    	
    	FileTransferMessage fileMessage = (FileTransferMessage) loadMessage(
    			fileMessageData, users);
    	
    	System.out.println(fileMessage.getDescription());
    }
    
    /**
     * Tests the loading of recipients.
     * 
     * Integration testing with class: User
     * 
     * @param userA A test user.
     * @param userB A test user.
     * @param userC A test user.
     * @param userD A test user.
     * @param users Test users.
     */
    private static void loadReceivedTest(User userA, User userB, User userC,
    		User userD, BST users) {
    	
    	String receivedData = userA.getUsername() + RECEIVED_TOKEN_SEP + "true"
    			+ RECEIVED_TOKEN_SEP + userB.getUsername() + RECEIVED_TOKEN_SEP
    			+ "true" + RECEIVED_TOKEN_SEP + userC.getUsername()
    			+ RECEIVED_TOKEN_SEP + "false" + RECEIVED_TOKEN_SEP
    			+ userD.getUsername() + RECEIVED_TOKEN_SEP + "false";
    	
    	HashMap<User, Boolean> received = getReceived(receivedData, users);
    	System.out.println(userA.getUsername() + " has seen: "
    			+ received.get(userA));
    	
    	System.out.println(userB.getUsername() + " has seen: "
    			+ received.get(userB));
    	
    	System.out.println(userC.getUsername() + " has seen: "
    			+ received.get(userC));
    	
    	System.out.println(userD.getUsername() + " has seen: "
    			+ received.get(userD));
    }
    
    /**
     * Test the getters of Message.
     * 
     * @param message The message to test on.
     */
    private static void testGetters(Message message) {
    	System.out.println(message.getDescription());
    	System.out.println("Author: " + message.getAuthor().getUsername());
    	System.out.println("Date: " + message.getDate());
    	System.out.println("Time: " + message.getTime());
    	
    	System.out.println("Recipients");
    	for(User recipient : message.getRecipients()) {
    		System.out.println(recipient.getUsername());
    	}
    	
    	System.out.println("Edited by: " + message.getEdit().getUsername());
    	System.out.println("Text: " + message.getText());
    }
    
    /**
     * Test the plain text storage outputs of the message.
     * 
     * @param message The message to test on.
     */
    private static void testMessageSaveFormat(Message message) {
    	System.out.println("Message receiving format: ");
		System.out.println(message.getReceivedSaveData());
		
    	System.out.println("Message type specific format: ");
		System.out.println(message.getTypeSaveData());
		
		System.out.println("Message save format: ");
		System.out.println(message.getSaveData());
    }
    
    /**
     * Test the recipient methods, including checking if a user has seen a
     * particular message, outputting who has seen the message, adding
     * recipients to the message, setting users to have seen the message.
     * 
     * Integration testing with class: User
     * 
     * @param message The message to test on.
     * @param user The user to test on.
     */
    private static void testRecipients(Message message, User user) {
    	//Test to see if user is supposed to be able to receive the message
    	try {
    		System.out.println("Seen message: "
    			+ message.hasSeenMessage(user));
    	} catch (NullPointerException e) {
    		System.out.println("Recipient can't receive message");
    	}
    	
    	System.out.println(message.getReceived());
    	
    	System.out.println("Add " + user.getUsername() + " as a recipient");
    	message.addRecipient(user);
    	
    	System.out.println("Seen message: " + message.hasSeenMessage(user));
    	System.out.println(message.getReceived());
    	
    	System.out.println("Set " + user.getUsername()
    		+ " to of seen the message");
    	
    	message.seenMessage(user);
    	
    	System.out.println("Seen message: " + message.hasSeenMessage(user));
    	System.out.println(message.getReceived());
    }
    
    /**
     * Test the setters of Message.
     * 
     * @param message The message to test on.
     * @param differentUser The user to test on.
     */
    private static void testSetters(Message message, User differentUser) {
    	System.out.println(message.getDescription());
		
		message.setDate("18/03/2017");
		message.setTime("19:57");
		message.setText("Changed");
		message.setEdit(differentUser);
		
		System.out.println(message.getDescription());
    }
    
    protected static final int MESSAGE_TYPE_START_POS = 7;
	protected static final int GROUP_SEPARATOR_NUM = 29;
	
    protected static final String MESSAGE_TOKEN_SEP =
			Character.toString(((char) GROUP_SEPARATOR_NUM));
	
	private static final int UNIT_SEPARATOR_NUM = 31;
	
	private static final String RECEIVED_TOKEN_SEP =
			Character.toString(((char) UNIT_SEPARATOR_NUM));
	
	private static final int AUTHOR_POS = 0;
	private static final int TEXT_POS = 1;
	private static final int DATE_POS = 2;
	private static final int TIME_POS = 3;
	private static final int RECEIVED_POS = 4;
	private static final int EDIT_POS = 5;
	private static final int TYPE_POS = 6;
	
	private static final Dimension TEST_FRAME_SIZE =
			new Dimension(200,200);
	
	//Variables to store the information for a message.
    private User m_author;
    private HashMap<User,Boolean> m_received;
    private String m_time;
    private String m_date;
    private String m_text;
    private User m_edit;
}
