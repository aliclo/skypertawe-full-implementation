package message;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import filemanager.FileManager;
import user.User;

/**
 * @File	-FileTransferMessage.java
 * @author	-rhysj, edited by Alistair Cloney
 * @date	-edited on 9/3/2017
 * @see		-Message.java, DescriptionMessage.java
 * @brief	This class handles all the information that will need
 * 			to be stored by a File Transfer message.
 * 
 * Stores the file path both in memory and in storage, also has a method
 * that will handle how the specific type of message should be displayed
 * and any user interaction that needs to be handled.
 */

public class FileTransferMessage extends DescriptionMessage {
	
	/**
	 * Constructor just uses constructor of DescriptionMessage class.
	 * 
	 * @param message The message builder that will initialise the
	 * message object.
	 */
	public FileTransferMessage(MessageBuilder message) {
		super(message);
	}
	
	/**
	 * @return The message description, this includes the description
	 * from the superclass and the path of the file being used.
	 */
	@Override
	public String getDescription() {
		return "File Message: " + super.getDescription() + '\n'
				+ "Directs to: " + m_filePath;
	}
	
	/**
	 * @return The path of the file being used.
	 */
	public String getFilePath() {
		return m_filePath;
	}
	
	/**
	 * @param filePath The new file path the message should use.
	 */
	public void setFilePath(String filePath) {
		this.m_filePath = filePath;
	}
	
	/**
	 * Adds the components needed to display the message's contents related to
	 * this specific type.
	 * 
	 * Adds the link to the file that the user can click to open the file.
	 * 
	 * @param panel The panel to add the contents to.
	 */
	@Override
	public void getTypeContents(JPanel panel) {
		JLabel fileLabel = new JLabel(getFilePath());
		fileLabel.setForeground(Color.BLUE);
		
		fileLabel.addMouseListener(new LinkListener(fileLabel.getText()));
		
		panel.add(fileLabel);
	}
	
	/**
	 * Loads the data specifically related to this type of message.
	 * 
	 * Reads the data that describes the file path.
	 * 
	 * @param tokens The tokens that were encountered when reading.
	 * @param message The message currently being built.
	 * @return The loaded file message.
	 */
	public static Message loadMessageTypeData(String[] tokens,
			MessageBuilder message) {
		
		String filePath = tokens[FILE_PATH_POS];
		
		return new FileTransferMessage(message).withFilePath(filePath);
	}
	
	/**
	 * Tests the methods of the class FileTransferMessage.
	 * 
	 * Integration testing with classes: User, FileManager, MessageBuilder,
	 * Message
	 * 
	 * @param args Not used.
	 */
	public static void main(String[] args) {
		User sender = new User().withUsername("Sender");
		
		FileManager.createFileWhenNonExistent(TEST_FILE);
		
		MessageBuilder testMessage = new MessageBuilder(sender)
			.withText("Testing")
			.withDate("17/03/2017")
			.withTime("21:07");
		
		String[] givenTokens = new String[] {"Sender", "hello", "10/10/2000",
				"10:00", "", "", Integer.toString(TYPE), "test directory"};
		
		Message loadedMessage = loadMessageTypeData(givenTokens, testMessage);
		FileTransferMessage fileMessage = (FileTransferMessage) loadedMessage;
		
		System.out.println(fileMessage.getFilePath());
		fileMessage.withFilePath("another directory");
		System.out.println(fileMessage.getFilePath());
		fileMessage.setFilePath(TEST_FILE_PATH);
		System.out.println(fileMessage.getFilePath());
		
		System.out.println(loadedMessage.getTypeSaveData());
		System.out.println(loadedMessage.getDescription());
		
		testContentDisplay(loadedMessage);
	}
	
	/**
	 * @param filePath The file path the message should have.
	 * @return This object (allows for multiple "with" calls all at once).
	 */
	public FileTransferMessage withFilePath(String filePath) {
		m_filePath = filePath;
		
		return this;
	}
	
	/**
	 * @return The data specifically about this type of message that
	 * needs to be stored.
	 * 
	 * Stores its type and the file path.
	 */
	@Override
	protected String getTypeSaveData() {
		return TYPE + MESSAGE_TOKEN_SEP + m_filePath;
	}
	
	/**
	 * @author	-Alistair Cloney
	 * @date	-9/3/2017
	 * @see		-https://docs.oracle.com/javase/7/docs/api/java/awt/event/
	 * 			MouseListener.html
	 * 
	 * @brief	Listens to when the link is pressed and opens the file.
	 */
	private class LinkListener implements MouseListener {
		
		/**
		 * Constructor that takes the link that will be used to open the file
		 * when clicked.
		 * 
		 * @param link The link to the file to open.
		 */
		public LinkListener(String link) {
			m_link = link;
		}
		
		/**
		 * Respond to mouse click by opening the file from the link
		 * if the desktop supports this.
		 * 
		 * @param e not used.
		 */
		@Override
		public void mouseClicked(MouseEvent e) {
			if(Desktop.isDesktopSupported()) {
				File file = new File(m_link);
				
				openFile(file);
			}
		}
		
		/**
		 * Not of interest.
		 * 
		 * @param e not used.
		 */
		@Override
		public void mouseEntered(MouseEvent e) {
			
		}
		
		/**
		 * Not of interest.
		 * 
		 * @param e not used.
		 */
		@Override
		public void mouseExited(MouseEvent e) {
			
		}
		
		/**
		 * Not of interest.
		 * 
		 * @param e not used.
		 */
		@Override
		public void mousePressed(MouseEvent e) {
			
		}
		
		/**
		 * Not of interest.
		 * 
		 * @param e not used.
		 */
		@Override
		public void mouseReleased(MouseEvent e) {
			
		}
		
		/**
		 * Opens the given file if the file exists.
		 * 
		 * @param file The file to open.
		 */
		private void openFile(File file) {
			if(file.exists()) {
				try {
					Desktop.getDesktop().open(file);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				JOptionPane.showMessageDialog(null, "File not found");
			}
		}
		
		//Given link to file that will open when clicked
		private String m_link;
	}
	
	public static final int TYPE = 2;
	
	private static final int FILE_PATH_REL_POS = 0;
	private static final int FILE_PATH_POS = Message.MESSAGE_TYPE_START_POS
			+ FILE_PATH_REL_POS;
	
	//Locate to the home directory
	private static final String TEST_FILE_PATH =
			System.getProperty("user.home") + "/testing/test.txt";
	
	private static final File TEST_FILE = new File(TEST_FILE_PATH);
	
	private String m_filePath;
}
