package message;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.HashMap;

import user.User;

/**
 * @File	-MessageBuilder.java
 * @author	-Alistair Cloney
 * @date	-16/3/2017
 * @see		-Message.java
 * @brief	Used to develop a new message for initialisation of the
 * 			Message object by passing this object into its constructor.
 * 
 * Includes 'with' methods that allow for multiple calls of these methods that
 * together allow the creation of the Message and lowers the amount of method
 * parameters.
 */
public class MessageBuilder {
    
	/**
	 * Used for initialising a message for sending.
	 * 
	 * The current date and time of when this constructor was called
	 * would be stored (to indicate when this message is being sent)
	 * and a HashMap of who will receive the message is generated
	 * (initially the author has already seen the message).
	 * 
	 * @param author The user who is the author of the message being built.
	 */
    public MessageBuilder(User author) {
    	//Get an instance of the Calendar object with the current time
    	Calendar cal = Calendar.getInstance();
    	
    	/**
    	 * Set the format of the time so that hours followed by minutes
    	 * and then seconds are displayed with colons to separate them.
    	 */
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
    
        /**
         * Set the format of the date so that the year followed by the
         * month and then days are displayed with slashes to separate them.
         */
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        
        //Get an instance of the LocalDate object with the current date
        LocalDate localDate = LocalDate.now();
        
        //Set the time and date using the formats
        m_time = sdf.format(cal.getTime());
        m_date = dtf.format(localDate);
        
        //Set the author to have seen the message
        m_received = new HashMap<User, Boolean>();
        m_received.put(author, true);
                
        m_author = author;
    }
    
    /**
     * Empty constructor where the builder starts off with empty data.
     */
    public MessageBuilder() {
    	
    }
    
    /**
     * @return The author of the message being built.
     */
    public User getAuthor() {
        return m_author;
    }
    
    /**
     * @return The date of when the message is being sent.
     */
    public String getDate() {
        return m_date;
    }
    
    /**
     * @return The user who edited the message last (null if not edited).
     */
    public User getEdit() {
    	return m_edit;
    }
    
    /**
     * @return The HashMap of users who may or may not of seen the message
     * being developed.
     */
    public HashMap<User,Boolean> getReceived() {
    	return m_received;
    }
    
    /**
     * @return The text of the message.
     */
    public String getText() {
        return m_text;
    }
    
    /**
     * @return The time the message was sent.
     */
    public String getTime() {
        return m_time;
    }
    
    /**
     * Tests the methods of the class MessageBuilder.
     * 
     * @param args Not used.
     */
    public static void main(String[] args) {
    	User userTest = new User().withUsername("Test");
    	User userHello = new User().withUsername("Hello");
    	
    	HashMap<User,Boolean> received = new HashMap<User,Boolean>();
    	received.put(userHello, true);
    	
    	MessageBuilder builder = new MessageBuilder(userTest)
    		.withDate("05/02/2017")
    		.withTime("15:00")
    		.withEdit(userHello)
    		.withReceived(received)
    		.withText("Hello World");
    	
    	testGetters(builder, userHello);
    }
    
    /**
	 * @param author The author of the message being developed.
	 * @return This object (allows for multiple "with" calls all at once).
	 */
    public MessageBuilder withAuthor(User author) {
    	m_author = author;
    	
    	return this;
    }
    
    /**
	 * @param date The date of when this message is being sent.
	 * @return This object (allows for multiple "with" calls all at once).
	 */
    public MessageBuilder withDate(String date) {
    	m_date = date;
    	
    	return this;
    }
    
    /**
	 * @param edit The editor of the message being sent.
	 * @return This object (allows for multiple "with" calls all at once).
	 */
    public MessageBuilder withEdit(User edit) {
    	m_edit = edit;
    	
    	return this;
    }
    
    /**
	 * @param received The HashMap of receivers of the message being sent.
	 * @return This object (allows for multiple "with" calls all at once).
	 */
    public MessageBuilder withReceived(HashMap<User, Boolean> received) {
    	m_received = received;
    	
    	return this;
    }
    
	/**
	 * @param text The text of the message being sent.
	 * @return This object (allows for multiple "with" calls all at once).
	 */
    public MessageBuilder withText(String contents) {
    	m_text = contents;
    	
    	return this;
    }
    
	/**
	 * @param time The time of when this message is being sent.
	 * @return This object (allows for multiple "with" calls all at once).
	 */
    public MessageBuilder withTime(String time) {
    	m_time = time;
    	
    	return this;
    }
    
    /**
     * Tests the getters of MessageBuilder.
     * 
     * @param builder The builder to test on.
     * @param receiver The user who will/has received the message.
     */
    private static void testGetters(MessageBuilder builder, User receiver) {
    	System.out.println("Username: " + builder.getAuthor().getUsername());
    	System.out.println("Date: " + builder.getDate());
    	System.out.println("Time: " + builder.getTime());
    	System.out.println("Edit: " + builder.getEdit().getUsername());
    	System.out.println("Received: " + builder.getReceived().get(receiver));
    	System.out.println("Text: " + builder.getText());
    }
    
    private User m_author;
    private HashMap<User,Boolean> m_received;
    private String m_time;
    private String m_date;
    private String m_text;
    private User m_edit;
	
}
