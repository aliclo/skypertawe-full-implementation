package message;

import javax.swing.JPanel;

import user.User;

/**
 * @File	-TextMessage.java
 * @author	-Rhys John Miller, edited by Annabel Kimber
 * @date	-Edited on 9/3/2017
 * @see		-Message.java
 * @brief	This class handles all the information that will need to be
 * 			stored by a Text message.
 */
public class TextMessage extends Message {
	
	/**
	 * Constructor just uses constructor of Message class.
	 * 
	 * @param message The message builder that will initialise the
	 * message object.
	 */
	public TextMessage(MessageBuilder message) {
		super(message);
	}
	
	/**
	 * @return The message description, this includes the type of message
	 * along with the description from the superclass.
	 */
	@Override
	public String getDescription() {
		return "Text message: " + '\n' + super.getDescription();
	}
	
	/**
	 * Adds the components needed to display the message's contents related to
	 * this specific type.
	 * 
	 * Currently nothing is added because the text is handled in the
	 * super class.
	 * 
	 * @param panel The panel to add the contents to.
	 */
	@Override
	public void getTypeContents(JPanel panel) {
		
	}
	
	/**
	 * Loads the data specifically related to this type of message.
	 * 
	 * Currently reads nothing more as this is handled in the super class.
	 * 
	 * @param tokens The tokens that were encountered when reading.
	 * @param message The message currently being built.
	 * @return The loaded text message.
	 */
	public static Message loadMessageTypeData(String[] tokens,
			MessageBuilder message) {
		
		return new TextMessage(message);
	}
	
	/**
	 * Testing the methods of the class TextMessage.
	 * 
	 * Integration testing with classes: User, MessageBuilder, Message
	 * 
	 * @param args Not used.
	 */
	public static void main(String[] args) {
		User sender = new User().withUsername("Sender");
		
		MessageBuilder testMessage = new MessageBuilder(sender)
			.withText("Testing")
			.withDate("17/03/2017")
			.withTime("21:07");
		
		Message loadedMessage = loadMessageTypeData(null, testMessage);
		System.out.println(loadedMessage.getTypeSaveData());
		System.out.println(loadedMessage.getDescription());
		
		testContentDisplay(loadedMessage);
	}
	
	/**
	 * @return The data specifically about this type of message that
	 * needs to be stored.
	 * 
	 * Stores its type.
	 */
	@Override
	protected String getTypeSaveData() {
		return Integer.toString(TYPE);
	}
	
	public static final int TYPE = 0;
}
