package contactList;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

import conversation.Chat;
import dataStructure.BST;
import filemanager.FileManager;
import message.Message;
import message.MessageBuilder;
import message.TextMessage;
import user.User;

/**
 * @File	-Contact.java
 * @author	-Alistair Cloney
 * @date	-9/3/2017
 * @see		-User.java, Chat.java
 * @brief	Class that stores the contact user and the chat the contact
 * 			can use to chat to the other user, also handles the saving
 * 			and loading of contacts.
 * 
 * Handles the storing of the contact with its corresponding chat both
 * in memory and in storage.
 * 
 * The contacts of a user are stored in a text file named after the username
 * and contains lines of of users who are contacts followed by the chat the
 * contact can communicate through.
 */
public class Contact {
	
	/**
	 * @param user Initial user to store.
	 * @param chat Initial chat to store.
	 */
	public Contact(User user, Chat chat) {
		this.m_user = user;
		this.m_chat = chat;
	}
	
	/**
	 * @param user Initial user to store.
	 */
	public Contact(User user) {
		this.m_user = user;
	}
	
	/**
	 * @return The stored chat.
	 */
	public Chat getChat() {
		return m_chat;
	}
	
	/**
	 * @return The stored user.
	 */
	public User getUser() {
		return m_user;
	}
	
	/**
	 * @param o The object to find if there is a match.
	 * @return If this contact object and another given object are equal.
	 */
	@Override
	public boolean equals(Object o) {
		if(o == null) {
			return false;
		}
		
		if(o.getClass() != this.getClass()) {
			return false;
		}
		
		Contact c = (Contact) o;
		
		//Equality based on the users
		if(c.getUser().equals(this.getUser())) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Load the contacts of the given user using the
	 * BST of all existing users.
	 * 
	 * @param user The user to load contacts to.
	 * @param users The existing users of Skypertawe.
	 */
	public static void loadContacts(User user, BST users) {
		ArrayList<Contact> contacts = new ArrayList<Contact>();
		
		//Index the file with the username.
		File file = new File(CONTACTS_PATH + user.getUsername() + ".txt");
		
		FileManager.createFileWhenNonExistent(file);
		
		try {
			Scanner in = new Scanner(file);
			
			//Load all contacts into a list
			while(in.hasNextLine()) {
				contacts.add(Contact.loadData(in.nextLine(), users));
			}
			
			in.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		user.setContacts(contacts);
	}
	
	/**
	 * Get the user and chat to create a contact from the given data.
	 * 
	 * @param data The data to read.
	 * @param users The existing users of Skypertawe.
	 * @return The contact created from the data.
	 */
	public static Contact loadData(String data, BST users) {
		String[] tokens = data.split(SEPARATOR);
		
		String contactName = tokens[CONTACT_POS];
		User contactAccount = users.getProfileRoot(contactName);
		
		int chatID = Integer.parseInt(tokens[CHAT_ID_POS]);
		Chat chat = Chat.loadChat(chatID, users);
		
		Contact contact =  new Contact(contactAccount, chat);
		
		return contact;
	}
	
	/**
	 * Test the methods of Contact.
	 * 
	 * Classes used for integration testing: BST, User, Contact,
	 * Chat, MessageBuilder, TextMessage
	 * 
	 * @param args Not used.
	 */
	public static void main(String[] args) {
		//Create test users
		BST users = new BST();
		
		User userA = new User().withUsername("User A")
				.withBirthDate("25/02/2000");
		
		User userB = new User().withUsername("User B")
				.withBirthDate("13/05/1995");
		
		User userC = new User().withUsername("User C")
				.withBirthDate("23/12/2009");
		
		users.insertUser(userA);
		users.insertUser(userB);
		users.insertUser(userC);
		
		ArrayList<User> members = new ArrayList<User>();
		
		members.add(userA);
		members.add(userB);
		
		Chat chat = new Chat(members);
		
		Contact contactA = new Contact(userA, chat);
		
		System.out.println("To string test: " + contactA.toString());
		
		userB.addContact(contactA);
		
		MessageBuilder messageBuilder = new MessageBuilder(userA)
				.withText("Hello Word");
		
		TextMessage textMessage = new TextMessage(messageBuilder);
		
		chat.sendMessage(textMessage);
		
		System.out.println("Save data format would be: "
			+ contactA.saveData());
		
		//Save all contacts of user B (which is just user A being a contact)
		Contact.saveContacts(userB);
		
		Contact contactC = new Contact(userC, chat);
		
		//userB.addContact(contactC);
		//This would be called twice if addContact was called
		Contact.saveNewContact(userB, contactC);
		
		loadUserContactsTest(userB, users);
		
		loadContactTest(userA, chat, users);
	}
	
	/**
	 * Save all contacts of the given user.
	 * 
	 * @param user The user whose contacts are going to be saved.
	 */
	public static void saveContacts(User user) {
		
		ArrayList<Contact> contacts = user.getContacts();
		
		//Index the contact file of the given user
		File file = new File(CONTACTS_PATH + user.getUsername() + ".txt");
		
		try {
			PrintWriter out = new PrintWriter(file);
			
			for(Contact contact : contacts) {
				out.println(contact.saveData());
			}
			
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @return The save data of the contact.
	 */
	public String saveData() {
		return m_user.getUsername() + SEPARATOR + m_chat.getID();
	}
	
	/**
	 * Append the new contact to the save file.
	 * 
	 * @param user The user that will have the new contact stored.
	 * @param contact The contact to store.
	 */
	public static void saveNewContact(User user, Contact contact) {
		File file = new File(CONTACTS_PATH + user.getUsername() + ".txt");
		
		FileManager.createFileWhenNonExistent(file);
		
		try {
			//Set to append
			FileWriter fileWriter = new FileWriter(file, true);
			PrintWriter out = new PrintWriter(fileWriter);
			
			out.println(contact.saveData());
			
			out.close();
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @return The contact when converting to a string, this is used
	 * so that JLists can display the contact as just the username.
	 */
	@Override
	public String toString() {
		return getUser().getUsername();
	}
	
	/**
	 * Test if contacts are being loaded properly.
	 * 
	 * Classes used for integration testing: User, Contact, Message
	 * 
	 * @param user The user which will be a contact that is loaded.
	 * @param chat The chat that the contact will communicate on.
	 * @param users The existing users of Skypertawe.
	 */
	private static void loadContactTest(User user, Chat chat, BST users) {
		//Test data
		String data = user.getUsername() + SEPARATOR + chat.getID();
		Contact contact = loadData(data, users);
		System.out.println(contact.getUser().getBirthDate());
		
		ArrayList<Message> messages = contact.getChat().getMessages();
		
		for(Message message : messages) {
			System.out.println(message.getText());
		}
		
		Contact sameContact = loadData(data, users);
		
		System.out.println("Both contacts the same: "
			+ contact.equals(sameContact));
	}
	
	/**
	 * Test if all the contacts of the given user are being loaded
	 * properly.
	 * 
	 * Classes used for integration testing: User, Contact
	 * 
	 * @param user The user whose contacts are loaded.
	 * @param users The existing users of Skypertawe.
	 */
	private static void loadUserContactsTest(User user, BST users) {
		//Reload user
		System.out.println("Reloading contacts of " + user.getUsername());
		
		User loadUser = new User().withUsername(user.getUsername());
		
		System.out.println("Current contacts of " + loadUser.getUsername());
		for(Contact contact : loadUser.getContacts()) {
			System.out.println(contact.getUser().getUsername());
		}
		
		System.out.println("Loading contacts back into "
			+ loadUser.getUsername());
		
		loadContacts(loadUser, users);
		
		System.out.println("Current contacts of " + loadUser.getUsername());
		for(Contact contact : loadUser.getContacts()) {
			System.out.println(contact.getUser().getUsername());
		}
	}
	
	/**
	 * The ASCII character is used for separation in the data file and
	 * helps avoid delimiter collision.
	 */
	private static final int UNIT_SEPARATOR_ASCII_NUM = 31;
	private static final String SEPARATOR = Character.toString(
			((char) UNIT_SEPARATOR_ASCII_NUM));
	
	private static final String CONTACTS_PATH = "Contacts/";
	
	//Positions in each data entry
	private static final int CONTACT_POS = 0;
	private static final int CHAT_ID_POS = 1;
	
	private User m_user;
	private Chat m_chat;

}