package dataStructure;

import java.util.ArrayList;

import user.User;

/**
 * @File	-BST.java
 * @author	-Jeyhan Hizli, edited by Alistair Cloney
 * @date	-5/12/2016, edited on 10/3/2017
 * @see		-BSTNode.java, User.java
 * @brief	Stores data into a binary search tree.
 * 
 * This class will create a binary search trees populated with nodes
 * where each node contains a user. Sorted by a recursive method into
 * the tree.
 */
public class BST {
	
	/**
	 * Empty constructor for creating an empty tree.
	 */
	public BST() {
		
	}
	
	/**
	 * This method allows the program to search for the given user
	 * starting at the root.
	 * 
	 * @param username The username of the user to find.
	 * @return The found user (if exists, null otherwise).
	 */
	public User getProfileRoot(String username){
		return getProfile(username, m_root);
	}
	
	/**
     * @return The root of the tree.
     */
	public BSTNode getRoot() {
		return m_root;
	}
	
	/**
	 * @param root Sets the root of the tree.
	 */
	public void setRoot(BSTNode root) {
		this.m_root = root;
	}
	
	/**
	 * @return An array list of users starting from the root.
	 */
	public ArrayList<User> getUserListRoot() {
		return getUserListRoot(new ArrayList<User>());
	}
	
	/**
	 * @param userList All users of the binary search tree will
	 * be added to this list.
	 * 
	 * @return The array list added with the users from the tree.
	 */
	public ArrayList<User> getUserListRoot(ArrayList<User> userList) {
		userList = getUserList(userList, m_root);
		return userList;
	}
	
	/**
	 * This method will make sure the traversal will run
	 * from the root node every time it is called, printing
	 * all the users' usernames as it visits them.
	 */
	public void inOrder() {
		printAlphabetical(m_root);
	}
	
	/**
	 * Inserts the given user (starting from the root) into the tree while
	 * preserving its search order properties.
	 * 
	 * @param u The user to insert into the tree.
	 */
	public void insertUser(User u) {
		BSTNode node = new BSTNode(u);
		
		//If there is no root, the new node will become the root node.
		if (m_root == null) {
			this.m_root = node;
		} else {
			insertUser(m_root, node);
		}
	}
	
	/**
	 * @return If the tree is empty or not (if any users are stored).
	 */
	public boolean isEmpty(){
		return this.m_root == null;
	}
	
	/**
	 * Tests the methods of the class BST.
	 * 
	 * @param args Not used.
	 */
	public static void main(String[] args) {
		BST bst = new BST();
		
		System.out.println("Is empty: " + bst.isEmpty());
		
		testAddUsers(bst);
		
		System.out.println("Is empty: " + bst.isEmpty());
		
		testGetDetails(bst);
		
		testReplaceRoot(bst);
	}
	
	/**
	 * The in-order traversal which will recursively call the left child,
	 * then the parent and then the right child.
	 * 
	 * @param n The current node thats being traversed.
	 */
	public void printAlphabetical(BSTNode n) {
		if (n.getRight() != null) {
			printAlphabetical(n.getRight());
		}
		
		System.out.println(n.getUser().getUsername());
		
		if (n.getLeft() != null) {
			printAlphabetical(n.getLeft());
		}
	}
	
	/**
	 * Finds which node to go to next from the current node when searching
	 * for the user with the given username.
	 * 
	 * This method assumes that the current node's username isn't equal to
	 * the given username to find.
	 * 
	 * @param username The username to find.
	 * @param n The current node being looked at.
	 * @return The new node to look at.
	 */
	private BSTNode findDirection(String username, BSTNode n) {
		boolean test = true;
		
		int compareResult = n.getUser().getUsername().compareTo(username);
		
		/**
		 * compareResult cannot be 0 because its been established that
		 * the usernames don't match.
		 */
		if(compareResult < 0) {
			if(test) {
				System.out.println("BST::findProfile() search left");
			}
			
			return n.getLeft();
		} else  {
			if(test) {
				System.out.println("BST::findProfile() search right");
			}
			
			return n.getRight();
		}
	}
	
	/**
	 * Find the user with the given username starting from the given node.
	 * 
	 * @param username The username to search for.
	 * @param n The node to start from.
	 * @return The found user (if it exists from the given node or its
	 * descendants, otherwise it returns null).
	 */
	private User getProfile(String username, BSTNode n) {
		boolean test = true;
		
		if(n != null) {
			//Because the usernames are not case sensitive
			if(n.getUser().getUsername().equalsIgnoreCase(username)) {
				if(test) {
					System.out.println("BST::getProfile() found");
				}
				
				return n.getUser();
			} else {
				BSTNode nextNode = findDirection(username, n);
				
				return getProfile(username, nextNode);
			}
		}
		
		return null;
	}
	
	/**
	 * Add to the given list all the users from the given node and
	 * its descendants.
	 * 
	 * @param userList The list to add the users to.
	 * @param n The node to start from.
	 * @return The array list with the added users.
	 */
	private ArrayList<User> getUserList(ArrayList<User> userList, BSTNode n) {
		boolean test = true;
		
		if (n.getRight() != null) {
			if(test) {
				System.out.println("BST::getUserList() go right");
			}
			
			getUserList(userList, n.getRight());
		}
		
		if(test) {
			System.out.println("BST::getUserList() add "
				+ n.getUser().getUsername());
		}
		userList.add(n.getUser());
		
		if (n.getLeft() != null) {
			if(test) {
				System.out.println("BST::getUserList() go left");
			}
			
			getUserList(userList,n.getLeft());
		}
		
		return userList;
	}
	
	/**
	 * Recursive part of the insertion, comparing with children.
	 * 
	 * @param curr the current node that the traversal is on.
	 * @param newNode the new node being placed into the tree.
	 */
	private void insertUser(BSTNode curr, BSTNode newNode) {
		boolean test = true;
		
		if (curr.getUser().getUsername().compareTo(
				newNode.getUser().getUsername()) <= 0) {
			
			if(test) {
				System.out.println("BST::insertUser() go left");
			}
			
			if (curr.getLeft() == null) {
				curr.setLeft(newNode);
			} else {
				insertUser(curr.getLeft(), newNode);
			}
		}

		if (curr.getUser().getUsername().compareTo(
				newNode.getUser().getUsername()) > 0) {
			
			if(test) {
				System.out.println("BST::insertUser() go right");
			}
			
			if (curr.getRight() == null) {
				curr.setRight(newNode);
			} else {
				insertUser(curr.getRight(), newNode);
			}
		}
	}
	
	/**
	 * Tests the insertion of users in the tree.
	 * 
	 * Integration testing with class: User
	 * 
	 * @param bst The binary search tree to test on.
	 */
	private static void testAddUsers(BST bst) {
		System.out.println("Adding users");
		User userTest = new User().withUsername("Test")
				.withBirthDate("10/05/1994");
		
		User userHello = new User().withUsername("Hello")
				.withBirthDate("05/07/2000");
		
		User userWorld = new User().withUsername("World")
				.withBirthDate("09/09/1999");
		
		User userAnn = new User().withUsername("Ann")
				.withBirthDate("26/03/1996");
		
		User userJames = new User().withUsername("James")
				.withBirthDate("30/06/1994");
		
		User userAlice = new User().withUsername("Alice")
				.withBirthDate("22/01/2004");
		
		User userBob = new User().withUsername("Bob")
				.withBirthDate("11/03/2001");
		
		bst.insertUser(userTest);
		bst.insertUser(userHello);
		bst.insertUser(userWorld);
		bst.insertUser(userAnn);
		bst.insertUser(userJames);
		bst.insertUser(userAlice);
		bst.insertUser(userBob);
	}
	
	/**
	 * Tests getting the details of the binary search tree, including
	 * getting the root, left and right child of a node, retrieving a
	 * list or print of the users, and finding a user from username.
	 * 
	 * Integration testing with classes: User, BSTNode
	 * 
	 * @param bst The binary search tree to test on.
	 */
	private static void testGetDetails(BST bst) {
		System.out.println("Root: " + bst.getRoot().getUser().getUsername());
		
		BSTNode left = bst.getRoot().getLeft();
		System.out.println("Left: " + left.getUser().getUsername());
		
		BSTNode right = bst.getRoot().getRight();
		System.out.println("Right: " + right.getUser().getUsername());
		
		System.out.println("Overall alphabetical prints");
		bst.inOrder();
		
		System.out.println("Alphabetical prints left");
		bst.printAlphabetical(left);
		
		System.out.println("Alphabetical prints right");
		bst.printAlphabetical(right);
		
		System.out.println("Overall list (given list)");
		ArrayList<User> users = new ArrayList<User>();
		bst.getUserListRoot(users);
		
		for(User user : users) {
			System.out.println(user.getUsername());
		}
		
		System.out.println("Overall list (no given list)");
		users = bst.getUserListRoot();
		
		for(User user : users) {
			System.out.println(user.getUsername());
		}
		
		System.out.println("List left");
		users = new ArrayList<User>();
		users = bst.getUserList(users, left);
		
		for(User user : users) {
			System.out.println(user.getUsername());
		}
		
		System.out.println("List right");
		users = new ArrayList<User>();
		users = bst.getUserList(users, right);
		
		for(User user : users) {
			System.out.println(user.getUsername());
		}
		
		System.out.println("Search for Bob");
		System.out.println(bst.getProfileRoot("Bob").getBirthDate());
		
		System.out.println("Search for Bob from left");
		User result = bst.getProfile("Bob", left);
		
		System.out.println("Found: " + (result != null));
		
		System.out.println("Search for Bob from right");
		result = bst.getProfile("Bob", right);
		
		System.out.println("Found: " + (result != null));
		
		BSTNode nodeToDest = bst.findDirection("Bob", bst.getRoot());
		
		System.out.println("Towards left: " + (nodeToDest == left));
		System.out.println("Towards right: " + (nodeToDest == right));
	}
	
	/**
	 * Test the replacement of the root.
	 * 
	 * Integration testing with classes: User, BSTNode
	 * 
	 * @param bst The binary search tree to test on.
	 */
	private static void testReplaceRoot(BST bst) {
		System.out.println("Adding users");
		
		User userJames = new User().withUsername("James")
				.withBirthDate("30/06/1994");
		
		User userHello = new User().withUsername("Hello")
				.withBirthDate("05/07/2000");
		
		User userWorld = new User().withUsername("World")
				.withBirthDate("09/09/1999");
		
		BSTNode jamesNode = new BSTNode(userJames);
		BSTNode helloNode = new BSTNode(userHello);
		BSTNode worldNode = new BSTNode(userWorld);
		bst.insertUser(jamesNode, helloNode);
		bst.insertUser(jamesNode, worldNode);
		
		System.out.println("Set root to James node");
		bst.setRoot(jamesNode);
		
		System.out.println("Current root: "
			+ bst.getRoot().getUser().getUsername());
		
		System.out.println("Overall alphabetical prints");
		bst.inOrder();
	}
	
	private BSTNode m_root;
}
