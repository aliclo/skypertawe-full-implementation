package dataStructure;

import user.User;

/**
 * @File	-BSTNode.java
 * @author	-Jeyhan Hizli, edited by Alistair Cloney
 * @date	-5/12/2016, edited on 9/3/2017
 * @see		-User.java
 * @brief	Stores the user, a left child node and right child node
 * 			(a child that is a null reference means that there is no child).
 */
public class BSTNode {
	
	/**
	 * Creates a binary search tree node.
	 * 
	 * @param data The user stored in the node.
	 */
	public BSTNode(User data) {
		this.m_data = data;
	}
	
	/**
     * @return The reference to the left child.
     */
	public BSTNode getLeft() {
		return m_left;
	}
	
	/**
	 * @param l The new left child node.
	 */
	public void setLeft(BSTNode l) {
		this.m_left = l;
	}
	
	/**
     * @return The reference to the right child.
     */
	public BSTNode getRight() {
		return m_right;
	}
	
	/**
	 * @param The new right child node.
	 */
	public void setRight(BSTNode r) {
		this.m_right = r;
	}
	
	/**
     * @return The user stored in the node.
     */
	public User getUser() {
		return m_data;
	}
	
	/**
	 * Tests the methods of the class BSTNode.
	 * 
	 * Integration testing with class: User
	 * 
	 * @param args Not used.
	 */
	public static void main(String[] args) {
		User userTest = new User().withUsername("Test")
				.withBirthDate("10/05/1994");
		
		User userHello = new User().withUsername("Hello")
				.withBirthDate("05/07/2000");
		
		User userWorld = new User().withUsername("World")
				.withBirthDate("09/09/1999");
		
		User userJames = new User().withUsername("James")
				.withBirthDate("30/06/1994");
		
		BSTNode node = new BSTNode(userTest);
		
		System.out.println("Is the stored data the same as the given data: "
			+ (node.getUser() == userTest));
		
		BSTNode leftNode = new BSTNode(userWorld);
		
		node.setLeft(leftNode);
		
		BSTNode rightNode = new BSTNode(userHello);
		
		node.setRight(rightNode);
		
		BSTNode rightLeftNode = new BSTNode(userJames);
		
		rightNode.setLeft(rightLeftNode);
		
		System.out.println("At root: " + node.getUser().getUsername());
		
		System.out.println("At left from root: "
			+ node.getLeft().getUser().getUsername());
		
		System.out.println("At right from root: "
			+ node.getRight().getUser().getUsername());
		
		System.out.println("At right left from root: "
				+ node.getRight().getLeft().getUser().getUsername());
	}
	
	private User m_data;
	private BSTNode m_left;
	private BSTNode m_right;
}
