package login;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
/**
 * @File	-Encryption.java
 * @author	-Antonis Kritikos, edited by Alistair Cloney
 * @date	-11 Dec 16, edited on 19/3/2017
 * @see		-https://docs.oracle.com/javase/7/docs/api/java/lang/Integer.html,
 * 			https://docs.oracle.com/javase/7/docs/api/java/security/
 * 			MessageDigest.html
 * 
 * @brief	Encrypts given Strings using MD5.
 */
public class Encryption {
	
	/**
	 * Tests the methods of class Encryption.
	 * 
	 * @param args Not used.
	 */
	public static void main(String[] args) {
		String password = "Test";
		
		System.out.println(password + " encrypted to " + md5(password));
	}
	
	/**Hashes parsed in String and gives it back.
	 * 
	 * @param original original String.
	 * @return string hashed in MD5.
	 */
	public static String md5(String original){
		try {
			String algorithm = "MD5";
			MessageDigest md = MessageDigest.getInstance(algorithm);
			
			//Gives the original data to the message digest
			md.update(original.getBytes());
			
			//Completes the hash function's computation
			byte[] digest = md.digest();
			
			StringBuffer sb = new StringBuffer();
			for (byte b : digest) {
				//Because bytes are not unsigned in java
				int v = b & LARGEST_ASCII_VALUE;
				
				/**
				 * Append the resulting byte as a hex string
				 * (which would be two hex values for each byte).
				 */
				sb.append(Integer.toHexString(v));
			}		
			return sb.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	private static final int ASCII_BITS = 8;
	private static final int NUM_OF_ASCII_VALUES =
			(int) Math.pow(2, ASCII_BITS);
	
	private static final int LARGEST_ASCII_VALUE = NUM_OF_ASCII_VALUES-1;
	
}
