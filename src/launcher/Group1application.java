package launcher;

import gui.GUILogin;
import resources.Resources;

/**
 * @File	-Group1application.java
 * @author	-Alistair Cloney
 * @date	-13\3\2017
 * @see		-GUILogin.java
 * @brief	Launches Skypertawe and initially takes the user to the login GUI.
 */
public class Group1application {
	
	/**
	 * The method where Skypertawe starts off, this is the method to run when
	 * users want to use Skypertawe rather then test.
	 * 
	 * @param args Not used.
	 */
	public static void main(String[] args){
		Resources.loadAllResources();
		GUILogin loginScreen = new GUILogin();
		loginScreen.setVisible(true);
	}
	
}
